package com.resocoder.databinding.model.subscriberDid

import android.os.Parcel
import android.os.Parcelable
import com.example.one_suite.model.subscriberDid.SubscriberDIDResponseDataModel


data class SubscriberDIDResponseModel(
    var Response: String?,
    var SubscriberDIDs: ArrayList<SubscriberDIDResponseDataModel>?
) :
    Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.createTypedArrayList(SubscriberDIDResponseDataModel)
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(Response)
        parcel.writeTypedList(SubscriberDIDs)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<SubscriberDIDResponseModel> {
        override fun createFromParcel(parcel: Parcel): SubscriberDIDResponseModel {
            return SubscriberDIDResponseModel(parcel)
        }

        override fun newArray(size: Int): Array<SubscriberDIDResponseModel?> {
            return arrayOfNulls(size)
        }
    }
}
//class SubscriberDIDResponseModel {
//    constructor(Response: String, SubscriberDIDs: ArrayList<SubscriberDIDResponseDataModel>) {
//        this.Response = Response
//        this.SubscriberDIDs = SubscriberDIDs
//    }
//
//     var Response: String
//     var SubscriberDIDs:ArrayList<SubscriberDIDResponseDataModel>
//
//}