package com.example.one_suite.model.callForwarding.getForwardingCallRate

class GetCallForwardingRateResponseModel {
    var RateList:ArrayList<GetCallForwardingRateListResponseModel>

    constructor(RateList: ArrayList<GetCallForwardingRateListResponseModel>) {
        this.RateList = RateList
    }
}