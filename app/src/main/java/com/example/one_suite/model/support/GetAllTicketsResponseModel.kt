package com.example.one_suite.model.support

class GetAllTicketsResponseModel {
    var Tickets:ArrayList<GetAllTicketsResponseDataModel>
    var RESPONSE:String

    constructor(Tickets: ArrayList<GetAllTicketsResponseDataModel>, RESPONSE: String) {
        this.Tickets = Tickets
        this.RESPONSE = RESPONSE
    }
}