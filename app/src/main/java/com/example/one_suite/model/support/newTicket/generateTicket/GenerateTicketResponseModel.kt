package com.example.one_suite.model.support.newTicket.generateTicket

class GenerateTicketResponseModel {
    var TicketId:Int
    var RESPONSECode:String

    constructor(TicketId: Int, RESPONSECode: String) {
        this.TicketId = TicketId
        this.RESPONSECode = RESPONSECode
    }
}