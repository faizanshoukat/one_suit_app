package com.example.one_suite.model.subscriberCustomNumbers

class SubscriberCustomNumbersResponseModel {
    constructor(Response: String, subscriberDIDDIDS: ArrayList<SubscriberCustomNumbersResponseDataModel>) {
        this.Response = Response
        this.Numbers = subscriberDIDDIDS
    }

     var Response: String
     var Numbers:ArrayList<SubscriberCustomNumbersResponseDataModel>

}