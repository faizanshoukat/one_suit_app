package com.example.one_suite.model.callForwarding.getForwardingCallRate

class GetCallForwardingRateRequestModel {
    var PREFIX:String
    var CURRENCY:String
    var CALLING_METHOD:String
    var COUNTRY:String

    constructor(PREFIX: String, CURRENCY: String, CALLING_METHOD: String, COUNTRY: String) {
        this.PREFIX = PREFIX
        this.CURRENCY = CURRENCY
        this.CALLING_METHOD = CALLING_METHOD
        this.COUNTRY = COUNTRY
    }
}