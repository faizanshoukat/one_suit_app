package com.example.one_suite.model.country


class GetCountryModel {
    var ResponseCode:String
    var ListOfCountries:ArrayList<GetCountryDataModel>

    constructor(ResponseCode: String, ListOfCountries: ArrayList<GetCountryDataModel>) {
        this.ResponseCode = ResponseCode
        this.ListOfCountries = ListOfCountries
    }


    //    "ResponseCode": "OK",
//    "ListOfCountries"
}