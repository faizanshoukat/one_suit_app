package com.example.one_suite.model.transactionHistory

class GetTransactionHistoryRequestModel {
    var ACCOUNT:String
    var FromDate:String
    var ToDate:String

    constructor(ACCOUNT: String, FromDate: String, ToDate: String) {
        this.ACCOUNT = ACCOUNT
        this.FromDate = FromDate
        this.ToDate = ToDate
    }
}