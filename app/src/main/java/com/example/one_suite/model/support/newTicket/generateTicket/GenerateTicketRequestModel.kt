package com.example.one_suite.model.support.newTicket.generateTicket

class GenerateTicketRequestModel {
    var CreatedByAccount:String
    var CreatedBy:Int
    var TicketSubTypeId:Int
    var PriorityId:Int
    var Title:String
    var Description:String

    constructor(CreatedByAccount: String, CreatedBy: Int, TicketSubTypeId: Int, PriorityId: Int, Title: String, Description: String) {
        this.CreatedByAccount = CreatedByAccount
        this.CreatedBy = CreatedBy
        this.TicketSubTypeId = TicketSubTypeId
        this.PriorityId = PriorityId
        this.Title = Title
        this.Description = Description
    }
}