package com.example.one_suite.model.setting.setProfile

class SetProfileRequestModel {

    lateinit var ACCOUNT:String
    lateinit var FIRST_NAME:String
    lateinit var LAST_NAME:String
    lateinit var ADDRESS1:String

    lateinit var ADDRESS2:String
    lateinit var ADDRESS3:String
    lateinit var COMPANY:String
    lateinit var STATE_REGION:String

    lateinit var CITY:String
    lateinit var COUNTRY:String
    lateinit var POSTAL_CODE:String
    lateinit var LOCAL_PHONE:String

    lateinit var FAX:String
    lateinit var E_MAIL:String
    lateinit var DATE_OF_BIRTH:String
    lateinit var PROFILE_PICTURE:String



    constructor(ACCOUNT: String, FIRST_NAME: String, LAST_NAME: String, ADDRESS1: String, ADDRESS2: String, ADDRESS3: String, COMPANY: String, STATE_REGION: String, CITY: String, COUNTRY: String, POSTAL_CODE: String, LOCAL_PHONE: String, FAX: String, E_MAIL: String, DATE_OF_BIRTH: String, PROFILE_PICTURE: String) {
        this.ACCOUNT = ACCOUNT
        this.FIRST_NAME = FIRST_NAME
        this.LAST_NAME = LAST_NAME
        this.ADDRESS1 = ADDRESS1
        this.ADDRESS2 = ADDRESS2
        this.ADDRESS3 = ADDRESS3
        this.COMPANY = COMPANY
        this.STATE_REGION = STATE_REGION
        this.CITY = CITY
        this.COUNTRY = COUNTRY
        this.POSTAL_CODE = POSTAL_CODE
        this.LOCAL_PHONE = LOCAL_PHONE
        this.FAX = FAX
        this.E_MAIL = E_MAIL
        this.DATE_OF_BIRTH = DATE_OF_BIRTH
        this.PROFILE_PICTURE = PROFILE_PICTURE
    }

    constructor()
}