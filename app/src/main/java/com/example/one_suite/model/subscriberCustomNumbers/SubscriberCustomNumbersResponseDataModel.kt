package com.example.one_suite.model.subscriberCustomNumbers


class SubscriberCustomNumbersResponseDataModel {

    constructor(Number: String, CliEnabled: Boolean) {
        this.Number = Number
        this.CliEnabled = CliEnabled
    }

    var Number: String
    var CliEnabled: Boolean

}