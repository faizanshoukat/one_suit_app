package com.example.one_suite.model.support.newTicket.ticketPriorities

class GetTicketPrioritiesRequestModel {
    var TicketTypeId:String

    constructor(TicketTypeId: String) {
        this.TicketTypeId = TicketTypeId
    }
}