package com.example.one_suite.model.callBack

class SetCallBackResponseModel {

    var RESPONSE:String
    var ResponseCode:String

    constructor(RESPONSE: String, ResponseCode: String) {
        this.RESPONSE = RESPONSE
        this.ResponseCode = ResponseCode
    }
}