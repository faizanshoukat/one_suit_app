package com.example.one_suite.model.support.newTicket.ticketPriorities

class GetTicketPrioritiesResponseDataModel {
    var PriorityId:Int
    var PriorityTitle:String

    constructor(PriorityId: Int, PriorityTitle: String) {
        this.PriorityId = PriorityId
        this.PriorityTitle = PriorityTitle
    }
}