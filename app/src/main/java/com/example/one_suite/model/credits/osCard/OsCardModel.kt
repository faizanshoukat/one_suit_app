package com.example.one_suite.model.credits.osCard

data class isValidVoucherRequestModel(var VoucherNumber: String)

data class isValidVoucherResponseModel(
    var VoucherAmount: Double,
    var Status: Int,
    var ResponseCode: String
)


/*.........................Redeem Voucher API................*/
data class RedeemVoucherRequestModel(
    val ACCOUNT: String,
    val VOUCHER_NUMBER: String,
)


data class RedeemVoucherResponseModel(
    val PREVIOUS_BALANCE: String,
    val VOUCHER_AMOUNT: String,
    val NEW_BALANCE: String, )

/*.........................Redeem Voucher API................*/