package com.example.one_suite.model.subscriberDid

import android.os.Parcel
import android.os.Parcelable



data class SubscriberDIDResponseDataModel (var Number: String?,
                                           var Status: Boolean?,
                                           var Cancelled: Boolean?,
                                           var Type: Int?,
                                           var IsForwardingEnable: Boolean?,
                                           var ForwardingNumber: String?,
                                           var CallerId: Boolean?,
                                           var ExpiryDate: String?,
                                           var CreatedDate: String?,
                                           var Amount: Double?,
                                           var AutoRenewNumber: Boolean?,
                                           var Name: String?,
                                           var State: String?
                                   ) :Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readValue(Boolean::class.java.classLoader) as? Boolean,
        parcel.readValue(Boolean::class.java.classLoader) as? Boolean,
        parcel.readValue(Int::class.java.classLoader) as? Int,
        parcel.readValue(Boolean::class.java.classLoader) as? Boolean,
        parcel.readString(),
        parcel.readValue(Boolean::class.java.classLoader) as? Boolean,
        parcel.readString(),
        parcel.readString(),
        parcel.readValue(Double::class.java.classLoader) as? Double,
        parcel.readValue(Boolean::class.java.classLoader) as? Boolean,
        parcel.readString(),
        parcel.readString()
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(Number)
        parcel.writeValue(Status)
        parcel.writeValue(Cancelled)
        parcel.writeValue(Type)
        parcel.writeValue(IsForwardingEnable)
        parcel.writeString(ForwardingNumber)
        parcel.writeValue(CallerId)
        parcel.writeString(ExpiryDate)
        parcel.writeString(CreatedDate)
        parcel.writeValue(Amount)
        parcel.writeValue(AutoRenewNumber)
        parcel.writeString(Name)
        parcel.writeString(State)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<SubscriberDIDResponseDataModel> {
        override fun createFromParcel(parcel: Parcel): SubscriberDIDResponseDataModel {
            return SubscriberDIDResponseDataModel(parcel)
        }

        override fun newArray(size: Int): Array<SubscriberDIDResponseDataModel?> {
            return arrayOfNulls(size)
        }
    }
}


//class SubscriberDIDResponseDataModel {
//    constructor(
//        Number: String,
//        Status: Boolean,
//        Cancelled: Boolean,
//        Type: Int,
//        IsForwardingEnable: Boolean,
//        ForwardingNumber: String,
//        CallerId: Boolean,
//        ExpiryDate: String,
//        CreatedDate: String,
//        Amount: Double,
//        AutoRenewNumber: Boolean,
//        Name: String,
//        State: String
//    ) {
//        this.Number = Number
//        this.Status = Status
//        this.Cancelled = Cancelled
//        this.Type = Type
//        this.IsForwardingEnable = IsForwardingEnable
//        this.ForwardingNumber = ForwardingNumber
//        this.CallerId = CallerId
//        this.ExpiryDate = ExpiryDate
//        this.CreatedDate = CreatedDate
//        this.Amount = Amount
//        this.AutoRenewNumber = AutoRenewNumber
//        this.Name = Name
//        this.State = State
//    }
//
//    var Number: String
//    var Status: Boolean
//    var Cancelled: Boolean
//    var Type: Int
//    var IsForwardingEnable: Boolean
//    var ForwardingNumber: String
//    var CallerId: Boolean
//    var ExpiryDate: String
//    var CreatedDate: String
//    var Amount: Double
//    var AutoRenewNumber: Boolean
//    var Name: String
//    var State: String
//
//
//}

