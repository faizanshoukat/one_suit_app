package com.example.one_suite.model.credits.payInvoice

class CreditPayInvoiceRequestModel {
    var PaymentHistoryId:Int
    var ACCOUNT:String
    var CREDIT_AMOUNT:Double

    constructor(PaymentHistoryId: Int, ACCOUNT: String, CREDIT_AMOUNT: Double) {
        this.PaymentHistoryId = PaymentHistoryId
        this.ACCOUNT = ACCOUNT
        this.CREDIT_AMOUNT = CREDIT_AMOUNT
    }
}