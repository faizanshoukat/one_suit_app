package com.example.one_suite.model.callForwarding.setSubscriberDID

class SetSubscriberDIDRequestModel {

    var ACCOUNT:String
    var DID_NUMBER:String
    var IS_AUTO_RENEW:Boolean
    var IS_FORWARDING_ENABLE:Boolean
    var FORWARD_NUMBER:String
    var NumberType:String
    var Location:String

    constructor(ACCOUNT: String, DID_NUMBER: String, IS_AUTO_RENEW: Boolean, IS_FORWARDING_ENABLE: Boolean, FORWARD_NUMBER: String, NumberType: String, Location: String) {
        this.ACCOUNT = ACCOUNT
        this.DID_NUMBER = DID_NUMBER
        this.IS_AUTO_RENEW = IS_AUTO_RENEW
        this.IS_FORWARDING_ENABLE = IS_FORWARDING_ENABLE
        this.FORWARD_NUMBER = FORWARD_NUMBER
        this.NumberType = NumberType
        this.Location = Location
    }
}