package com.example.one_suite.model.support.newTicket.ticketTypes

class GetTicketTypesResponseDataModel {
    var TypeId:Int
    var SubTypeId:Int
    var Title:String
    var CreaetdDate:String

    constructor(TypeId: Int, SubTypeId: Int, Title: String, CreaetdDate: String) {
        this.TypeId = TypeId
        this.SubTypeId = SubTypeId
        this.Title = Title
        this.CreaetdDate = CreaetdDate
    }
}