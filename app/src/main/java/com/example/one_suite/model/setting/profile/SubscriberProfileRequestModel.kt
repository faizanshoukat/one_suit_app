package com.example.one_suite.model.setting.profile

class SubscriberProfileRequestModel {
    var ACCOUNT:String

    constructor(ACCOUNT: String) {
        this.ACCOUNT = ACCOUNT
    }
}