package com.example.one_suite.model.transactionHistory

class GetTransactionHistoryResponseModel {
    var PaymentHistories:ArrayList<GetTransactionHistoryResponseDataModel>

    constructor(PaymentHistories: ArrayList<GetTransactionHistoryResponseDataModel>) {
        this.PaymentHistories = PaymentHistories
    }
}