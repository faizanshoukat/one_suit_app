package com.example.one_suite.model.transactionHistory

class GetTransactionHistoryResponseDataModel {
    var PaymentHistoryId:Int
    var OrderId:String
    var OrderDate:String
    var Amount:Double
    var Description:String
    var TranscationType:String

    constructor(PaymentHistoryId: Int, OrderId: String, OrderDate: String, Amount: Double, Description: String, TranscationType: String) {
        this.PaymentHistoryId = PaymentHistoryId
        this.OrderId = OrderId
        this.OrderDate = OrderDate
        this.Amount = Amount
        this.Description = Description
        this.TranscationType = TranscationType
    }
}