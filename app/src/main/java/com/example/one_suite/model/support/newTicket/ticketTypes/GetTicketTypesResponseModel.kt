package com.example.one_suite.model.support.newTicket.ticketTypes

class GetTicketTypesResponseModel {
    var TicketTypes:ArrayList<GetTicketTypesResponseDataModel>
    var RESPONSECode:String

    constructor(TicketTypes: ArrayList<GetTicketTypesResponseDataModel>, RESPONSECode: String) {
        this.TicketTypes = TicketTypes
        this.RESPONSECode = RESPONSECode
    }
}