package com.example.one_suite.model.credits.autoRecharge

class AutoRechargeResponseModel {

    var AMOUNT:Double
    var IS_AUTO_RECHARGE:Boolean

    constructor(AMOUNT: Double, IS_AUTO_RECHARGE: Boolean) {
        this.AMOUNT = AMOUNT
        this.IS_AUTO_RECHARGE = IS_AUTO_RECHARGE
    }
}