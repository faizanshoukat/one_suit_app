package com.example.one_suite.model.credits

class GetSystemSettingsResponseModel {
    var DIDMaxQuantity: Int
    var StaticDIDMarkup: Boolean
    var DIDMarkup: Double
    var TelephoneExpiryDays: Int
    var TelephoneExpiryNotifyDays: Int
    var RecyclePoolExpiryDays: Int
    var VATPercentage: Double
    var FundFirst: Int
    var FundSecond: Int
    var FundThird: Int
    var FundFourth: Int
    var FundThreshold: Double
    var MinimumTopup: Double
    var MaximumTopup: Double

    constructor(DIDMaxQuantity: Int, StaticDIDMarkup: Boolean, DIDMarkup: Double, TelephoneExpiryDays: Int, TelephoneExpiryNotifyDays: Int, RecyclePoolExpiryDays: Int, VATPercentage: Double, FundFirst: Int, FundSecond: Int, FundThird: Int, FundFourth: Int, FundThreshold: Double, MinimumTopup: Double, MaximumTopup: Double) {
        this.DIDMaxQuantity = DIDMaxQuantity
        this.StaticDIDMarkup = StaticDIDMarkup
        this.DIDMarkup = DIDMarkup
        this.TelephoneExpiryDays = TelephoneExpiryDays
        this.TelephoneExpiryNotifyDays = TelephoneExpiryNotifyDays
        this.RecyclePoolExpiryDays = RecyclePoolExpiryDays
        this.VATPercentage = VATPercentage
        this.FundFirst = FundFirst
        this.FundSecond = FundSecond
        this.FundThird = FundThird
        this.FundFourth = FundFourth
        this.FundThreshold = FundThreshold
        this.MinimumTopup = MinimumTopup
        this.MaximumTopup = MaximumTopup
    }
}