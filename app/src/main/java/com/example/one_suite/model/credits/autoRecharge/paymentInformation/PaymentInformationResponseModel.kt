package com.example.one_suite.model.credits.autoRecharge.paymentInformation

class PaymentInformationResponseModel {
    var FIRST_NAME:String
    var LAST_NAME:String
    var OffersUpdateSubscription:Boolean
    var CARD_NUMBER:String
    var EXPIRY_DATE:String
    var SAVED_FOR_FUTURE:Boolean
    var ADDRESS1:String
    var ADDRESS2:String
    var ADDRESS3:String
    var POSTAL_CODE:String
    var STATE:String
    var COUNTRY_CODE:String
    var BILLING_CITY:String
    var LOCAL_PHONE:String
    var E_MAIL:String
    var DATE_OF_BIRTH:String
    var IS_AUTO_RENEW:Boolean
    var AUTO_AMOUNT:Double
    var CardNumberHash:String

//    "CVV2": null,

    constructor(FIRST_NAME: String, LAST_NAME: String, OffersUpdateSubscription: Boolean, CARD_NUMBER: String, EXPIRY_DATE: String, SAVED_FOR_FUTURE: Boolean, ADDRESS1: String, ADDRESS2: String, ADDRESS3: String, POSTAL_CODE: String, STATE: String, COUNTRY_CODE: String, BILLING_CITY: String, LOCAL_PHONE: String, E_MAIL: String, DATE_OF_BIRTH: String, IS_AUTO_RENEW: Boolean, AUTO_AMOUNT: Double, CardNumberHash: String) {
        this.FIRST_NAME = FIRST_NAME
        this.LAST_NAME = LAST_NAME
        this.OffersUpdateSubscription = OffersUpdateSubscription
        this.CARD_NUMBER = CARD_NUMBER
        this.EXPIRY_DATE = EXPIRY_DATE
        this.SAVED_FOR_FUTURE = SAVED_FOR_FUTURE
        this.ADDRESS1 = ADDRESS1
        this.ADDRESS2 = ADDRESS2
        this.ADDRESS3 = ADDRESS3
        this.POSTAL_CODE = POSTAL_CODE
        this.STATE = STATE
        this.COUNTRY_CODE = COUNTRY_CODE
        this.BILLING_CITY = BILLING_CITY
        this.LOCAL_PHONE = LOCAL_PHONE
        this.E_MAIL = E_MAIL
        this.DATE_OF_BIRTH = DATE_OF_BIRTH
        this.IS_AUTO_RENEW = IS_AUTO_RENEW
        this.AUTO_AMOUNT = AUTO_AMOUNT
        this.CardNumberHash = CardNumberHash
    }
}