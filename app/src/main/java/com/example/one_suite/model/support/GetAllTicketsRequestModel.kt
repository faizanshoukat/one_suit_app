package com.example.one_suite.model.support

class GetAllTicketsRequestModel {
    var UserAccount:String
    var StatusId:Int
    var IsAdmin:Boolean

    constructor(UserAccount: String, StatusId: Int, IsAdmin: Boolean) {
        this.UserAccount = UserAccount
        this.StatusId = StatusId
        this.IsAdmin = IsAdmin
    }
}