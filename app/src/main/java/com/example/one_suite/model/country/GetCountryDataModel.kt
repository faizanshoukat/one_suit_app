package com.example.one_suite.model.country

class GetCountryDataModel {
    var Name:String
    var Code:String
    var Region:String
    var ISO:String

    constructor(Name: String, Code: String, Region: String, ISO: String) {
        this.Name = Name
        this.Code = Code
        this.Region = Region
        this.ISO = ISO
    }
}