package com.example.one_suite.model.credits.payInvoice

class CreditPayInvoiceResponseModel {
    var OLD_BALANCE:String
    var NEW_BALANCE:String

    constructor(OLD_BALANCE: String, NEW_BALANCE: String) {
        this.OLD_BALANCE = OLD_BALANCE
        this.NEW_BALANCE = NEW_BALANCE
    }
}