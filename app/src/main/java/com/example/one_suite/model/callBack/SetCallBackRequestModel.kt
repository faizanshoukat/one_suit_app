package com.example.one_suite.model.callBack

class SetCallBackRequestModel {

    var Account:String
    var CallBackAtNumber:String
    var NumberToCall:String
    var CallCLI:String

    constructor(Account: String, CallBackAtNumber: String, NumberToCall: String, CallCLI: String) {
        this.Account = Account
        this.CallBackAtNumber = CallBackAtNumber
        this.NumberToCall = NumberToCall
        this.CallCLI = CallCLI
    }
}