package com.example.one_suite.model.support.newTicket.ticketPriorities

class GetTicketPrioritiesResponseModel {
    var TicketPriorities:ArrayList<GetTicketPrioritiesResponseDataModel>

    constructor(TicketPriorities: ArrayList<GetTicketPrioritiesResponseDataModel>) {
        this.TicketPriorities = TicketPriorities
    }
}