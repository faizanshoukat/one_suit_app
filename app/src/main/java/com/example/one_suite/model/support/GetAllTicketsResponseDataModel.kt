package com.example.one_suite.model.support

class GetAllTicketsResponseDataModel {
    var TicketId:Int
    var CreatedBy:Int
    var CreatedByName:String
    var TicketTypeId:Int
    var TicketTypeTitle:String
    var TicketPriorityId:Int
    var TicketPriorityTitle:String
    var TicketTitle:String
    var Description:String
    var Status:Int
    var StatusTitle:String
    var CreatedDate:String

    constructor(TicketId: Int, CreatedBy: Int, CreatedByName: String, TicketTypeId: Int, TicketTypeTitle: String, TicketPriorityId: Int, TicketPriorityTitle: String, TicketTitle: String, Description: String, Status: Int, StatusTitle: String, CreatedDate: String) {
        this.TicketId = TicketId
        this.CreatedBy = CreatedBy
        this.CreatedByName = CreatedByName
        this.TicketTypeId = TicketTypeId
        this.TicketTypeTitle = TicketTypeTitle
        this.TicketPriorityId = TicketPriorityId
        this.TicketPriorityTitle = TicketPriorityTitle
        this.TicketTitle = TicketTitle
        this.Description = Description
        this.Status = Status
        this.StatusTitle = StatusTitle
        this.CreatedDate = CreatedDate
    }
}