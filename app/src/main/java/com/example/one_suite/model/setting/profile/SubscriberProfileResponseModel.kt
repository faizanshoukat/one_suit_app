package com.example.one_suite.model.setting.profile

class SubscriberProfileResponseModel {

    var FIRST_NAME:String
    var LAST_NAME:String
    var ADDRESS1:String
    var ADDRESS2:String

    var COMPANY:String
    var STATE_REGION:String
    var CITY:String


    var COUNTRY:String
    var POSTAL_CODE:String
    var LOCAL_PHONE:String

    var FAX:String
    var E_MAIL:String
    var DATE_OF_BIRTH:String
    var SIGNUP_DATE:String
    var PROFILE_PICTURE:String

    constructor(FIRST_NAME: String, LAST_NAME: String, ADDRESS1: String, ADDRESS2: String, COMPANY: String, STATE_REGION: String, CITY: String, COUNTRY: String, POSTAL_CODE: String, LOCAL_PHONE: String, FAX: String, E_MAIL: String, DATE_OF_BIRTH: String, SIGNUP_DATE: String, PROFILE_PICTURE: String) {
        this.FIRST_NAME = FIRST_NAME
        this.LAST_NAME = LAST_NAME
        this.ADDRESS1 = ADDRESS1
        this.ADDRESS2 = ADDRESS2
        this.COMPANY = COMPANY
        this.STATE_REGION = STATE_REGION
        this.CITY = CITY
        this.COUNTRY = COUNTRY
        this.POSTAL_CODE = POSTAL_CODE
        this.LOCAL_PHONE = LOCAL_PHONE
        this.FAX = FAX
        this.E_MAIL = E_MAIL
        this.DATE_OF_BIRTH = DATE_OF_BIRTH
        this.SIGNUP_DATE = SIGNUP_DATE
        this.PROFILE_PICTURE = PROFILE_PICTURE
    }
}