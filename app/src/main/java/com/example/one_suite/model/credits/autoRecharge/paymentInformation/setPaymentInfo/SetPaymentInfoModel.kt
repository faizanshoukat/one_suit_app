package com.example.one_suite.model.credits.autoRecharge.paymentInformation.setPaymentInfo

data class SetPaymentInfoRequestModel (var Username:String,var ACCOUNT:String,var FIRST_NAME:String,var LAST_NAME:String,var ADDRESS1:String,
var ADDRESS2:String,var POSTAL_CODE:String,var STATE:String,var COUNTRY_CODE:String,var BILLING_CITY:String,var LOCAL_PHONE:String,
                                       var E_MAIL:String,var OffersUpdateSubscription:Boolean)

data class SetPaymentInfoResponseModel(var RESPONSE:String)