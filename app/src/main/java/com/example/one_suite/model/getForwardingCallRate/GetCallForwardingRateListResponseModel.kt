package com.example.one_suite.model.callForwarding

class GetCallForwardingRateListResponseModel {
    var RATE:String
    var CURRENCY:String
    var DESCRIPTION:String
    var CALLING_METHOD:String

    constructor(RATE: String, CURRENCY: String, DESCRIPTION: String, CALLING_METHOD: String) {
        this.RATE = RATE
        this.CURRENCY = CURRENCY
        this.DESCRIPTION = DESCRIPTION
        this.CALLING_METHOD = CALLING_METHOD
    }
}