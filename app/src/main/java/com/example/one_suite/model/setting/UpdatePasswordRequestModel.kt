package com.example.one_suite.model.setting

class UpdatePasswordRequestModel {
    var ACCOUNT:String
    var OLD_PASSWORD:String
    var NEW_PASSWORD:String

    constructor(ACCOUNT: String, OLD_PASSWORD: String, NEW_PASSWORD: String) {
        this.ACCOUNT = ACCOUNT
        this.OLD_PASSWORD = OLD_PASSWORD
        this.NEW_PASSWORD = NEW_PASSWORD
    }
}