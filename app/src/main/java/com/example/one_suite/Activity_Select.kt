package com.example.one_suite

import android.app.ProgressDialog
import android.content.Intent
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.WindowManager
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.example.one_suite.databinding.ActivitySelectBinding
import com.example.one_suite.ui.login.Login
import com.example.one_suite.ui.login.LoginRequestModel
import com.example.one_suite.ui.login.LoginViewModel
import com.example.one_suite.utils.Constant
import com.example.one_suite.utils.Constant.Companion.password
import com.example.one_suite.utils.PreferenceUtils

class Activity_Select : AppCompatActivity() {

    private lateinit var binding: ActivitySelectBinding

    var userText=""
    var passwordText=""


    override fun onStart() {
        super.onStart()


        var name  = PreferenceUtils.getEmail(applicationContext)
        var password  = PreferenceUtils.getPassword(applicationContext)


        if (name != null && password != null) {

            Log.d("CredentialsSave", "User :$name")
            Log.d("CredentialsSave", "Password :$password")

            userText=name
            passwordText=password

        }


    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_select)


        val viewModel = ViewModelProviders.of(this)
            .get(LoginViewModel::class.java)




        val progressDialog = ProgressDialog(this@Activity_Select)
//        progressDialog.setTitle("Progress Bar")
        progressDialog.setMessage("Please wait...")
        //progressDialog.show()



        changeStatusBarColor()
        binding.txtSignIn.setOnClickListener {

            progressDialog.show()

            if ( userText.isNullOrBlank()||  passwordText.isNullOrBlank()){
                val signInActivity = Intent(this@Activity_Select, Login::class.java )
                startActivity(signInActivity)
                Log.d("TAG", "if: "+userText+"  "+passwordText)


            }
            else {



                progressDialog.dismiss()

                var requestModel = LoginRequestModel(userText!!, passwordText!!)
                viewModel.makeLoginApiCall(requestModel)
                Log.d("TAG", "else: "+userText+"  "+passwordText)

            }


        }




        viewModel.getLoginDataResult().observe(this, androidx.lifecycle.Observer {
            progressDialog.dismiss()

            if (it!=null) {

                if (it.ACCOUNT != null) {
                    Constant.name = it.Name!! + "!"
                    Constant.account = it.ACCOUNT!!

                    if (it.UrlImage==null || it.UrlImage.isNullOrEmpty()){
                        Log.d("ImageIssue", "img null ${it.UrlImage}")

                        // add the default string
                        Constant.imageUrl="http://osportal2.inovedia.com/Content/assests/profile.png";

                    }else{
                        Log.d("ImageIssue", "img not null ${it.UrlImage}")

                        Constant.imageUrl =
                            "http://osportal2.inovedia.com/UploadedStuff/ProfilePictures/" + it.UrlImage

                    }

//                    if (it.UrlImage != null || it.UrlImage != "" || it.UrlImage!="null") {
//                        Log.d("ImageIssue", "img not null ${it.UrlImage}")
//
//                        Constant.imageUrl =
//                            "http://osportal2.inovedia.com/UploadedStuff/ProfilePictures/" + it.UrlImage
//                    }else{
//                        Constant.imageUrl = "http://osportal2.inovedia.com/Content/assests/profile.png"
//                    }

                    Log.d("ImageIssue", "Img :${it.UrlImage}")


                    if (it.UrlImage != null) {
                        PreferenceUtils.setIs_Profile_Link(true, applicationContext)
                        PreferenceUtils.setProfile_Link(
                            "http://osportal2.inovedia.com/UploadedStuff/ProfilePictures/" + it.UrlImage,
                            applicationContext
                        )

                    } else {
                        PreferenceUtils.setIs_Profile_Link(false, applicationContext)
                        PreferenceUtils.setProfile_Link(
                            "",
                            applicationContext
                        )

                    }


//                PreferenceUtils.setFull_Name(it.Name, applicationContext)
//                PreferenceUtils.setPassword(password,applicationContext)

//                PreferenceUtils.setDesignation(it., applicationContext)

//                    if (activityBinding.rememberMe.isChecked) {

                    if (userText.isNullOrBlank()||passwordText.isNullOrBlank()){


                        RememberFunction("", "")
                        Toast.makeText(this@Activity_Select, "Data not save", Toast.LENGTH_SHORT).show();


                    } else {

                        Log.d("CredentialsSave", "Saving User :${userText}")
                        Log.d("CredentialsSave", "Saving Password :$passwordText")

                        RememberFunction(userText, passwordText)

                        Toast.makeText(this@Activity_Select, "Data save", Toast.LENGTH_SHORT).show();

                    }

                    val intent = Intent(this, MainActivity::class.java)
//                    val bundle = Bundle()
//                    intent.putExtra("Bundle", bundle)
                    intent.putExtra("Fragment","Dashboard")
                    startActivity(intent)

                } else {
                 }

            }else{
             }
        })



    }


    //remember me functoin

    private fun RememberFunction(s_mail:String,  s_password:String ) {
//        save email and password

        PreferenceUtils.setEmail(s_mail,getApplicationContext());
        PreferenceUtils.setPassword(s_password,getApplicationContext());

    }




    //status bar color
    fun changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= 21) {
            val window = this.window
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
            window.statusBarColor = this.resources.getColor(R.color.colorPrimaryDark)
        }
    }

    // Remember me function

}