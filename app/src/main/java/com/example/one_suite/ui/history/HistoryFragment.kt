package com.example.one_suite.ui.history

import android.graphics.Color
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.example.one_suite.R
import com.example.one_suite.ui.credit.addBalance.AddBalanceFragment
import com.example.one_suite.ui.credit.autoRechargeFragment.AutoRechargeFragment
import com.example.one_suite.ui.credit.osCard.OsCardFragment
import com.example.one_suite.ui.history.callHistory.CallHistoryFragment
import com.example.one_suite.ui.history.creditNotes.CreditNotesFragment
import com.example.one_suite.ui.history.transactionHistory.TransactionHistoryFragment
import com.skyhope.showmoretextview.ShowMoreTextView

class HistoryFragment : Fragment() {

    companion object {
        fun newInstance() = HistoryFragment()
    }

    //private lateinit var viewModel: MyNumbersViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {


        val view = inflater.inflate(R.layout.history_fragment, container, false)


        /*..............For Show More And Less Text..........*/
        val textView = view.findViewById<ShowMoreTextView>(R.id.text_view_show_more)
        textView.setShowingChar(30)
        textView.setShowingLine(2)
        textView.addShowMoreText("see more");
        textView.addShowLessText("Less");

        textView.setShowMoreColor(Color.RED); // or other color
        textView.setShowLessTextColor(Color.RED); // or other color

        /*..............For Show More And Less Text..........*/




        /* added fragment My Numbers by default  */
        attachCallHistoryFragment()

        val callHistory = view.findViewById<TextView>(R.id.callHistory)
        val transactionHistory = view.findViewById<TextView>(R.id.transactionHistory)
        val creditNotes = view.findViewById<TextView>(R.id.creditNotes)


        callHistory.setOnClickListener(View.OnClickListener {
            callHistory.setTextColor(Color.parseColor("#7ec243"))
//            addBalance.textSize = 16F

            transactionHistory.setTextColor(Color.parseColor("#959898"))
            creditNotes.setTextColor(Color.parseColor("#959898"))
            attachCallHistoryFragment()
        })


        transactionHistory.setOnClickListener(View.OnClickListener {
            transactionHistory.setTextColor(Color.parseColor("#7ec243"))
//            autoRechargeButton.textSize = 16F


            callHistory.setTextColor(Color.parseColor("#959898"))
            creditNotes.setTextColor(Color.parseColor("#959898"))
            attachTransactionHistoryFragment()
        })

        creditNotes.setOnClickListener(View.OnClickListener {
            creditNotes.setTextColor(Color.parseColor("#7ec243"))
//            osCardButton.textSize = 16F


            callHistory.setTextColor(Color.parseColor("#959898"))
            transactionHistory.setTextColor(Color.parseColor("#959898"))
            attachCreditNotesFragment()
        })

        return view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        //viewModel = ViewModelProvider(this).get(MyNumbersViewModel::class.java)
        // TODO: Use the ViewModel
    }

    private fun attachCallHistoryFragment() {
        //creating fragment object
        var fragment: Fragment? = null


        fragment = CallHistoryFragment()

        //replacing the fragment
        if (fragment != null) {
            val ft = activity?.supportFragmentManager?.beginTransaction()
            ft?.replace(R.id.historyContainer, fragment)
            ft?.commit()
        }


    }


    private fun attachTransactionHistoryFragment() {
        //creating fragment object
        var fragment: Fragment? = null


        fragment = TransactionHistoryFragment()

        //replacing the fragment
        if (fragment != null) {
            val ft = activity?.supportFragmentManager?.beginTransaction()
            ft?.replace(R.id.historyContainer, fragment)
            ft?.commit()
        }


    }


    private fun attachCreditNotesFragment() {
        //creating fragment object
        var fragment: Fragment? = null


        fragment = CreditNotesFragment()

        //replacing the fragment
        if (fragment != null) {
            val ft = activity?.supportFragmentManager?.beginTransaction()
            ft?.replace(R.id.historyContainer, fragment)
            ft?.commit()
        }


    }

}