package com.example.one_suite.ui.history.creditNotes

data class CreditNotesRequestModel( var account:String)

data class CreditNotesResponseModel(
 var ResponseCode:String,
 var List:ArrayList<CreditNotesResponseDataModel>
)


data class CreditNotesResponseDataModel( var CNID:Int,
var TicketRefID:Int,var Amount:Int,var Description:String, var TranscationID:Int)
