package com.example.one_suite.ui.login

class LoginRequestModel {
    var USER_NAME:String
    var PASSWORD:String

    constructor(USER_NAME: String, PASSWORD: String) {
        this.USER_NAME = USER_NAME
        this.PASSWORD = PASSWORD
    }
}

class LoginResponseModel {
    var ACCOUNT:String
    var APP_USER_ID:Int
    var Name:String
    var Balance:Double
    var UrlImage:String
    var PersonalFundThreshold:Double

    constructor(
        ACCOUNT: String,
        APP_USER_ID: Int,
        Name: String,
        Balance: Double,
        UrlImage: String,
        PersonalFundThreshold: Double
    ) {
        this.ACCOUNT = ACCOUNT
        this.APP_USER_ID = APP_USER_ID
        this.Name = Name
        this.Balance = Balance
        this.UrlImage = UrlImage
        this.PersonalFundThreshold = PersonalFundThreshold
    }

    //
//    "ACCOUNT": "52426418377",
//    "APP_USER_ID": 51,
//    "Name": "faizan",
//    "Balance": 40.9550,
//    "UrlImage": null,
//    "PersonalFundThreshold": 0.00


}