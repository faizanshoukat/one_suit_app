package com.example.one_suite.ui.home

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.app.AlertDialog.Builder
import android.app.Dialog
import android.app.ProgressDialog
import android.content.Intent
import android.graphics.Color
import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.example.one_suite.MainActivity
import com.example.one_suite.R
import com.example.one_suite.databinding.BuyNumberListFragmentBinding
import com.example.one_suite.databinding.CallForwardingFragmentBinding
import com.example.one_suite.databinding.FragmentHomeBinding
import com.example.one_suite.databinding.MyNumbersListFragmentBinding
import com.example.one_suite.model.callForwarding.setSubscriberDID.SetSubscriberDIDRequestModel
import com.example.one_suite.ui.buyNumber.checkOut.AutoRenewNumberRequestModel
import com.example.one_suite.ui.callForwarding.setForwarding.SetForwardingDialog1
import com.example.one_suite.utils.Constant
import com.example.one_suite.utils.DateConversion
import com.example.one_suite.utils.DateConversion.Companion.getAbbreviatedFromDateTime
import com.resocoder.databinding.utils.DialogUtils
import javax.security.auth.DestroyFailedException

class HomeFragment : Fragment() {

    companion object {
        fun newInstance() = HomeFragment()
    }

    private lateinit var homeViewModel: HomeViewModel
    private var _binding: FragmentHomeBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!


    //    var dialog = DialogUtils
    //var dialog = AlertDialog.Builder(activity?.applicationContext)
//    @SuppressLint("FragmentLiveDataObserve")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        homeViewModel = ViewModelProvider(this).get(HomeViewModel::class.java)



        _binding = FragmentHomeBinding.inflate(inflater, container, false).apply {
            this.lifecycleOwner = this@HomeFragment
            this.viewModel = homeViewModel
        }

        val root: View = binding.root

//        val dialog = ProgressDialog(getContext())
//        dialog.setMessage("Please wait...")
//
//        dialog.show()
//        dialog.dismiss()


//                +1
//                set caller Id
//                redirect to specific page
//                My history show detail page


        //progress dialog
        var dialog = DialogUtils.setProgressDialog(container!!.context, "Please Wait.....")


        _binding!!.executePendingBindings()
        _binding!!.recyclerView.apply {
            Log.d("SearchTelephoneApiCall", "List size in fragment")
            layoutManager = LinearLayoutManager(activity?.applicationContext)
        }


        Log.d("FaizanTestingDialog", "Call History API Call")
        //calling API
        dialog!!.show()
        homeViewModel.makeSubscriberCallHistoryApiCall()


        //Call history Data Observer
        homeViewModel.getRecyclerListDataObserver().observe(viewLifecycleOwner, Observer {
            Log.d("FaizanTestingDialog", "Recycler list data")



            //set row names
            _binding!!.field1.text = "Date"
            _binding!!.field2.text = "Duration"
            _binding!!.field3.text = "Amount"

            if (it != null) {

                homeViewModel.noDataFound.postValue(false)


                //update the adapter
                var recyclerListData = ArrayList<CustomMyHistoryHomeModel>()

                for (item in it) {

                    var number: String = "N/A"
                    var destination: String = "N/A"
                    var lastValue: String = "N/A"
                    var status: Boolean = false
                    var switchVisibility = false


                    if (item.DATE != null) {

                        var date = item.DATE!!

                        val str = date
                        val delim = "T"

                        val list = str.split(delim)

                        date = list[0]

//                        val year=
//                            getAbbreviatedFromDateTime("2021-07-14", "yyyy-MM-dd", "YYYY")
//                        println("Year--$year")


                        val year =
                            getAbbreviatedFromDateTime(date, "yyyy-MM-dd", "YYYY")
                        println("Year--$year")

                        val monthName =
                            getAbbreviatedFromDateTime(date, "yyyy-MM-dd", "MM")
                        println("monthName--$monthName")

                        val dayOfWeek =
                            getAbbreviatedFromDateTime(date, "yyyy-MM-dd", "dd")
                        println("dayOfWeek--$dayOfWeek")


                        number = "$dayOfWeek-$monthName-$year"

                    }

                    if (item.EndTime != null) {
                        destination = item.DURATION!!
                    }

                    if (item.CHARGE != null) {
                        lastValue = "\$" + item.CHARGE.toString()!!
                    }

                    var dataObject = CustomMyHistoryHomeModel(
                        number,
                        destination,
                        lastValue,
                        status,
                        switchVisibility
                    )
                    recyclerListData.add(dataObject)
                }

                homeViewModel.setAdapterData(recyclerListData)

            } else {
                homeViewModel.noDataFound.postValue(true)
            }


            Log.d("FaizanTestingDialog","Balance API Call")
            homeViewModel.makeBalanceApiCall()

        })

        //Transaction Observer
        homeViewModel.getTransectionHistoryListDataObserver().observe(viewLifecycleOwner, Observer {
            Log.d("FaizanTestingDialog", "Transaction history data")

            //set row names
            _binding!!.field1.text = "Date"
            _binding!!.field2.text = "Type"
            _binding!!.field3.text = "Amount"


            if (it != null) {

                homeViewModel.noDataFound.postValue(false)


                //update the adapter
                var recyclerListData = ArrayList<CustomMyHistoryHomeModel>()

                for (item in it) {

                    var number: String = "N/A"
                    var destination: String = "N/A"
                    var lastValue: String = "N/A"
                    var status: Boolean = false
                    var switchVisibility = false





                    if (item.OrderDate != null) {

                        var date = item.OrderDate!!

                        val str = date
                        val delim = "T"

                        val list = str.split(delim)

                        date = list[0]

//                        val year=
//                            getAbbreviatedFromDateTime("2021-07-14", "yyyy-MM-dd", "YYYY")
//                        println("Year--$year")


                        val year =
                            getAbbreviatedFromDateTime(date, "yyyy-MM-dd", "YYYY")
                        println("Year--$year")

                        val monthName =
                            getAbbreviatedFromDateTime(date, "yyyy-MM-dd", "MM")
                        println("monthName--$monthName")

                        val dayOfWeek =
                            getAbbreviatedFromDateTime(date, "yyyy-MM-dd", "dd")
                        println("dayOfWeek--$dayOfWeek")


                        number = "$dayOfWeek-$monthName-$year"

                    }





                    if (item.TranscationType != null) {
                        if (item.TranscationType.isNotEmpty()) {
                            destination = item.TranscationType!!
                        } else {
                            destination = "N/A"
                        }

                    }


                    if (item.Amount != null) {
                        lastValue = "\$" + item.Amount.toString()!!
                    }


                    var dataObject = CustomMyHistoryHomeModel(
                        number,
                        destination,
                        lastValue,
                        status,
                        switchVisibility
                    )
                    recyclerListData.add(dataObject)
                }

                homeViewModel.setAdapterData(recyclerListData)

            } else {
                homeViewModel.noDataFound.postValue(true)
            }


            homeViewModel.makeBalanceApiCall()

        })


        //Balance Data Observer
        homeViewModel.getBalanceObserver().observe(viewLifecycleOwner, Observer {
            Log.d("FaizanTestingDialog", "Balance Data")

            dialog!!.dismiss()
        })

        //Call Forwarding Data Observer
        homeViewModel.getCallForwardingDataObserver().observe(viewLifecycleOwner, Observer {
            Log.d("SubscriberApiCall", "get recycler data")

            //set row names
            _binding!!.field1.text = "Number"
            _binding!!.field2.text = "Destination"
            _binding!!.field3.text = "Status"


            dialog!!.dismiss()

            if (it != null) {
                homeViewModel.noDataFound.postValue(false)

                var recyclerListData = ArrayList<CustomMyHistoryHomeModel>()

                for (item in it) {

                    var number: String = "N/A"
                    var destination: String = "N/A"
                    var lastValue: String = "N/A"
                    var status: Boolean = false
                    var switchVisibility = true


                    //set values
                    if (item.Number != null) {
                        number = "+" + item.Number!!
                    }

                    if (item.ForwardingNumber != null) {
                        if (item.IsForwardingEnable == false) {
                            destination = "N/A"
                        } else {
                            destination = item.ForwardingNumber!!
                        //                            destination = "+" + item.ForwardingNumber!!
                        }

                    }

                    if (item.IsForwardingEnable != null) {
                        status = item.IsForwardingEnable!!
                    }


                    var dataObject = CustomMyHistoryHomeModel(
                        number,
                        destination,
                        lastValue,
                        status,
                        switchVisibility
                    )
                    recyclerListData.add(dataObject)
                }

                //update the adapter
                homeViewModel.setAdapterData(recyclerListData)

            } else {
                homeViewModel.noDataFound.postValue(true)
            }


        })


        //Call Forwarding Data Observer
        homeViewModel.getCallingPlanHistoryListDataObserver().observe(viewLifecycleOwner, Observer {
            Log.d("SubscriberApiCall", "get recycler data")

            //set row names
            _binding!!.field1.text = "Name"
            _binding!!.field2.text = "Total Mins"
            _binding!!.field3.text = "Price"



            dialog!!.dismiss()

            if (it != null) {
                homeViewModel.noDataFound.postValue(false)

                var recyclerListData = ArrayList<CustomMyHistoryHomeModel>()

                for (item in it) {

                    var number: String = "N/A"
                    var destination: String = "N/A"
                    var lastValue: String = "N/A"
                    var status: Boolean = false
                    var switchVisibility = false


                    //set values
                    if (item.BundleName != null) {
                        number = item.BundleName!!
                    }

                    if (item.TotalMinutes != null) {
                        destination = item.TotalMinutes.toString()!!
                    }

                    if (item.Price != null) {
                        lastValue = "\$" + item.Price.toString()!!
                    }


                    var dataObject = CustomMyHistoryHomeModel(
                        number,
                        destination,
                        lastValue,
                        status,
                        switchVisibility
                    )
                    recyclerListData.add(dataObject)
                }

                //update the adapter
                homeViewModel.setAdapterData(recyclerListData)

            } else {
                homeViewModel.noDataFound.postValue(true)
            }


        })


        //Call Forwarding Switch Data Observer
        homeViewModel.getCallForwardingSwitchValueDataObserver()
            .observe(viewLifecycleOwner, Observer {
                Log.d("SwitchObserver", "Value :${it.IsForwardingEnable}")




                var number = ""
                var callForwarding = false

                if (it != null) {
                    number = it.Number.toString()
                    callForwarding = it.IsForwardingEnable!!

                    if (callForwarding){

                        var accountNumber= Constant.account
                        var didNumber=it.Number
                        var IS_AUTO_RENEW=false
                        var IS_FORWARDING_ENABLE=false
                        var forwardPhoneNumber:String=""
                        var NumberType=""
                        var Location=""

                        val builder = AlertDialog.Builder(activity)
                        //set title for alert dialog
                        builder.setTitle("Confirmation dialog")
                        //set message for alert dialog
                        builder.setMessage("Are you ")
                        builder.setIcon(android.R.drawable.ic_dialog_alert)

                        //performing positive action
                        builder.setPositiveButton("Yes"){dialogInterface, which ->
                            Log.d("dialog", "onCreateView:  yess")
                            dialog!!.show()
                            var requestModel= SetSubscriberDIDRequestModel(accountNumber,didNumber!!,IS_AUTO_RENEW,IS_FORWARDING_ENABLE,forwardPhoneNumber,NumberType,Location)
                            homeViewModel.makeSetDIDSettingApiCall(requestModel)


                        }
                        //performing cancel action
                        builder.setNeutralButton("Cancel"){dialogInterface , which ->
                            Log.d("dialog", "onCreateView:  Cancel")
                        }
                        //performing negative action
                        builder.setNegativeButton("No"){dialogInterface, which ->
                            Log.d("dialog", "onCreateView:  NO")
                        }
                        // Create the AlertDialog
                        val alertDialog: AlertDialog = builder.create()
                        // Set other dialog properties
                        alertDialog.setCancelable(false)
                        alertDialog.show()




                    }else{
                        if (activity != null) {
                            SetForwardingDialog1.newInstance(
                                number,
                                "Dashboard"
                            ).show(
                                requireActivity().supportFragmentManager,
                                SetForwardingDialog1.TAG
                            )
                        }
                    }

                }





            })

        //Set Call Forwarding Data Observer
        homeViewModel.getCallForwardingResultDataObserver().observe(viewLifecycleOwner, Observer {
            dialog.dismiss()
        })




//        on click listeners
        _binding!!.callHistory.setOnClickListener(View.OnClickListener {
            dialog!!.show()
            homeViewModel.makeSubscriberCallHistoryApiCall()

            //make it visible
            homeViewModel.isCallHistory.postValue(true)

            _binding!!.callHistoryText.setTextColor(Color.parseColor("#1754A4"))
            _binding!!.transactionHistoryText.setTextColor(Color.parseColor("#959898"))
            _binding!!.callForwardingText.setTextColor(Color.parseColor("#959898"))
            _binding!!.callPlansText.setTextColor(Color.parseColor("#959898"))
        })

        _binding!!.transactionHistory.setOnClickListener(View.OnClickListener {
            dialog!!.show()
            homeViewModel.makeTransactionHistoryApiCall()


            _binding!!.callHistoryText.setTextColor(Color.parseColor("#959898"))
            _binding!!.transactionHistoryText.setTextColor(Color.parseColor("#1754A4"))
            _binding!!.callForwardingText.setTextColor(Color.parseColor("#959898"))
            _binding!!.callPlansText.setTextColor(Color.parseColor("#959898"))
        })

        _binding!!.callForwarding.setOnClickListener(View.OnClickListener {
            dialog!!.show()
            homeViewModel.makeSubscriberDIDApiCall()

            _binding!!.callHistoryText.setTextColor(Color.parseColor("#959898"))
            _binding!!.transactionHistoryText.setTextColor(Color.parseColor("#959898"))
            _binding!!.callForwardingText.setTextColor(Color.parseColor("#1754A4"))
            _binding!!.callPlansText.setTextColor(Color.parseColor("#959898"))
        })


        _binding!!.callPlansText.setOnClickListener(View.OnClickListener {
            dialog!!.show()
            homeViewModel.makeCallingPlanHistoryApiCall()


            _binding!!.callHistoryText.setTextColor(Color.parseColor("#959898"))
            _binding!!.transactionHistoryText.setTextColor(Color.parseColor("#959898"))
            _binding!!.callForwardingText.setTextColor(Color.parseColor("#959898"))
            _binding!!.callPlansText.setTextColor(Color.parseColor("#1754A4"))


        })


        //Button Click listner
        _binding!!.oneSuiteNumber.setOnClickListener(View.OnClickListener {
            (activity as MainActivity?)?.displaySelectedScreen("My Numbers")
        })

//        homeViewModel.makeGetBalanceApi(requireActivity().getApplicationContext())
//        Log.d("TAG", "makeGetBalanceApi: "+homeViewModel.makeGetBalanceApi(requireActivity().getApplicationContext()))
//
//
//        homeViewModel.getBalanceObserveraz().observe(this, androidx.lifecycle.Observer {
//            dialog.dismiss()
//        })


        return root;
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        //homeViewModel = ViewModelProvider(this).get(HomeViewModel::class.java)
        // TODO: Use the ViewModel
    }

}