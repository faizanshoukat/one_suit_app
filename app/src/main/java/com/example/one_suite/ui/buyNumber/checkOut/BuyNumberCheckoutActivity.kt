package com.example.one_suite.ui.buyNumber.checkOut

import android.app.Dialog
import android.app.ProgressDialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.example.one_suite.MainActivity
import com.example.one_suite.R
import com.example.one_suite.databinding.ActivityCheckoutBinding
import com.example.one_suite.utils.ClickListener
import com.example.one_suite.utils.Constant
import com.resocoder.databinding.utils.DialogUtils

class BuyNumberCheckoutActivity : AppCompatActivity(), ClickListener {
    lateinit var progressDialog: ProgressDialog;
    lateinit var dialog: Dialog;
    var balance: Double = 0.0;

    var viewModel1 = BuyNumberCheckoutViewModel();

    var isPayWithOS = false;
    var isPayWithReward = false;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        setContentView(R.layout.activity_checkout)


        //getting data using Parcelable
        val bundle = intent.getBundleExtra("Bundle");
        var checkOutList = ArrayList<BuyNumbersCheckoutModel>();
        if (bundle != null) {
            checkOutList = bundle.getParcelableArrayList<BuyNumbersCheckoutModel>("CheckOutList")!!
            Log.d("LocalRateApiCall", "check out list size :${checkOutList.size}")
        }


        //init progress dialog
        progressDialog = ProgressDialog(this@BuyNumberCheckoutActivity)
        progressDialog.setTitle("Progress Bar")
        progressDialog.setMessage("Application is loading, please wait")


        var viewModel = ViewModelProviders.of(this)
            .get(BuyNumberCheckoutViewModel::class.java)

        viewModel1 = viewModel;

        var activityBinding = DataBindingUtil.setContentView<ActivityCheckoutBinding>(
            this, R.layout.activity_checkout
        ).apply {
            this.setLifecycleOwner(this@BuyNumberCheckoutActivity)
            this.viewModel = viewModel
        }

        activityBinding.executePendingBindings()
        activityBinding.recyclerView.apply {
            layoutManager = LinearLayoutManager(context)
//            val decoration = DividerItemDecoration(context, StaggeredGridLayoutManager.VERTICAL)
//            addItemDecoration(decoration)
        }

        //setting Data
        Log.d("ViewModelIssue", "Check out List :${checkOutList.size}")
        viewModel.setAdapterData(checkOutList)


        viewModel.getDeleteItemPositionObserver().observe(this, Observer {
            if (it != null) {
                Toast.makeText(this, "Delete at Position :${it}", Toast.LENGTH_SHORT).show()
                checkOutList.removeAt(it)

                Constant.checkoutListStatic = checkOutList

                viewModel.setAdapterData(checkOutList)
            }
        })


        viewModel.getSwitchItemPositionObserver().observe(this, Observer {
            if (it != null) {

                var autoRenewNumber = false
                if (it != null) {
                    autoRenewNumber =!checkOutList[it].isAutoRenew
                }


                /*......now replace the old object with new one so that we can use the auto renew flag in API........*/
                var oldObject:BuyNumbersCheckoutModel=checkOutList[it]

                var country: String?=oldObject.country
                var state: String?=oldObject.state
                var number: String?=oldObject.number
                var price: String?=oldObject.price
                var type: String?=oldObject.type
                var isAutoRenew: Boolean=autoRenewNumber

                var newObject:BuyNumbersCheckoutModel=BuyNumbersCheckoutModel(country,state,number,price,type,isAutoRenew)


                checkOutList[it] = newObject
                /*......now replace the old object with new one so that we can use the auto renew flag in API........*/

                Constant.checkoutListStatic = checkOutList

                viewModel.setAdapterData(checkOutList)
            }
        })

//        viewModel.getSwitchItemDataObserver().observe(this, Observer {
//            if (it != null) {
//                Toast.makeText(this, "Switch at Position :${it.isAutoRenew}", Toast.LENGTH_SHORT).show()
//
//
//                progressDialog.show()
//
//                var number = ""
//                var autoRenewNumber = false
//                if (it != null) {
//                    number = it.number.toString()
//                    autoRenewNumber = !it.isAutoRenew!!
//                }
//
//                var requestModel =
//                    AutoRenewNumberRequestModel(Constant.account, autoRenewNumber, number)
//
//                viewModel.makeAutoRenewNumberApiCall(requestModel)
//            }
//        })


        //to delete the cart
        activityBinding.deleteAllButton.setOnClickListener(View.OnClickListener {

            Constant.checkoutListStatic.clear()

            Toast.makeText(this, "All items are deleted", Toast.LENGTH_SHORT).show()
            checkOutList.clear()
            viewModel.setAdapterData(checkOutList)
        })


        activityBinding.continueShoppingButton.setOnClickListener(
            View.OnClickListener {
            val intent = Intent(this, MainActivity::class.java)
            val bundle = Bundle()
            intent.putExtra("Bundle", bundle)
            intent.putExtra("Fragment", "Buy Numbers")
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            startActivity(intent)
        })

        //pay with card
//        activityBinding.payWithCardButton.setOnClickListener(View.OnClickListener {
//            //var amount=activityBinding.osCost.text.toString()
//            print("Amount :$balance")
//
////            var intent= Intent(this,PayWithCard::class.java)
////            intent.putExtra("Amount",amount)
////            startActivity(intent)
//        })

        //Pay with OS Balance
        activityBinding.payWithOSButton.setOnClickListener(View.OnClickListener {

            isPayWithOS = true;
            isPayWithReward = false;
            dialog = DialogUtils.getCustomConfirmationDialog(this, this)
            dialog.show()
        })


        //Pay with OS Balance
        activityBinding.payWithRewardsButton.setOnClickListener(View.OnClickListener {

            isPayWithOS = false;
            isPayWithReward = true;
            dialog = DialogUtils.getCustomConfirmationDialog(this, this)
            dialog.show()
        })


        //total cost observer
        viewModel.getCostDataObserver().observe(this, Observer {
            balance = it
        })


        //get data returning from pay with OS balance API
        viewModel.getPayWithOSBalanceDataObserver().observe(this, Observer {

            Constant.checkoutListStatic.clear()

            //progressDialog.dismiss()
            if (it == null) {
                Log.d("CustomDialog", "Data Null");
            } else {
                Log.d("CustomDialog", "Data :${it.PaymentId}");


                var thnMessageModel = OrderTelephoneNumberTNItemMessagingModel("A2PLC", "SMS");
                var tnFeatureModel = OrderTelephoneNumberTNFeatureModel("", thnMessageModel)

                var numberTnList = ArrayList<OrderTelephoneNumberTnModel>()
                //tn list
                for (item in checkOutList) {
                    if (item != null) {
                        if (item.number != null) {
                            var tnModel = OrderTelephoneNumberTnModel(
                                item.number!!,
                                "",
                                "",
                                tnFeatureModel,
                                item.isAutoRenew
                            );
                            numberTnList.add(tnModel)
                        }
                    }
                }

                var tnListModel = OrderTelephoneNumberTNListModel(numberTnList)
                var tnOrder = OrderTelephoneNumberTNOrderModel(tnListModel)


                var requestModel =
                    OrderTelephoneNumberRequestModel(it.PaymentId, Constant.account, tnOrder)

                Log.d("OrderNumberIssue", "Request :${requestModel}");

                viewModel.makeOrderTelephoneNumberApiCall(requestModel)


            }
        })


        //get data returning from pay with OS balance API
        viewModel.getPayWithRewardPointsDataObserver().observe(this, Observer {
            Constant.checkoutListStatic.clear()
            //progressDialog.dismiss()
            if (it == null) {
                Log.d("CustomDialog", "Data Null");
            } else {
                Log.d("CustomDialog", "Data :${it.PayId}");


                var thnMessageModel = OrderTelephoneNumberTNItemMessagingModel("A2PLC", "SMS");
                var tnFeatureModel = OrderTelephoneNumberTNFeatureModel("", thnMessageModel)

                var numberTnList = ArrayList<OrderTelephoneNumberTnModel>()
                //tn list
                for (item in checkOutList) {
                    if (item != null) {
                        if (item.number != null) {
                            var tnModel = OrderTelephoneNumberTnModel(
                                item.number!!,
                                "",
                                "",
                                tnFeatureModel,
                                false
                            );
                            numberTnList.add(tnModel)
                        }
                    }
                }

                var tnListModel = OrderTelephoneNumberTNListModel(numberTnList)
                var tnOrder = OrderTelephoneNumberTNOrderModel(tnListModel)


                var requestModel =
                    OrderTelephoneNumberRequestModel(it.PayId, Constant.account, tnOrder)

                Log.d("OrderNumberIssue", "Request :${requestModel}");

                viewModel.makeOrderTelephoneNumberApiCall(requestModel)


            }
        })

        //get final order number api response
        viewModel.getOrderTelephoneNumberResponseDataObserver().observe(this, Observer {
            progressDialog.dismiss()

            val intent = Intent(this, MainActivity::class.java)
            val bundle = Bundle()
            intent.putExtra("Bundle", bundle)
            intent.putExtra("Fragment", "My Numbers")
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            startActivity(intent)

            Log.d("OrderNumberIssue", "Order Response status :${it!!.status}");
            Log.d("OrderNumberIssue", "Order Response order id :${it!!.orderId}");
            Log.d("OrderNumberIssue", "Order Response Status code :${it!!.statusCode}");
        })

        viewModel.getAutoRenewNumberDataObserver().observe(this, Observer {
            progressDialog.dismiss()
        })
    }

    override fun onViewClick(view: View, position: Int) {
//        TODO("Not yet implemented")
        Log.d("CustomDialog", "on View Click");
        if (view.id == R.id.okBtn) {
            print("ok button click")
            Log.d("CustomDialog", "ok button click");

            dialog.dismiss()
            progressDialog.show()


            if (isPayWithOS && !isPayWithReward) {
                Log.d("CustomDialog", "Balance :$balance");
                var requestModel = PayWithOSBalanceRequestModel(Constant.account, balance)
                viewModel1.makePayWithOSBalanceApiCall(requestModel)
            } else if (!isPayWithOS && isPayWithReward) {
                Log.d("CustomDialog", "Balance :$balance");
                var requestModel = PayWithRewardPointsRequestModel(Constant.account, balance, 3)
                viewModel1.makePayWithRewardPointsApiCall(requestModel)
            }


        } else if (view.id == R.id.cancelBtn) {
            print("cancel button click")
            Log.d("CustomDialog", "cancel button click");
            dialog.dismiss()
        }
    }

    override fun onRowClick(position: Int) {
//        TODO("Not yet implemented")
        Log.d("CustomDialog", "on Row click");
    }


    override fun onResume() {
        super.onResume()
        App.activityResumed()
    }

    override fun onPause() {
        super.onPause()
        App.activityPaused()
    }
}