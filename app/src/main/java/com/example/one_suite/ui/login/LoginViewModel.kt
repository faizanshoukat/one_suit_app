package com.example.one_suite.ui.login

import android.content.Context
import android.util.Log
import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.one_suite.network.APIService
import com.example.one_suite.network.RetroInstance
import com.example.one_suite.utils.Constant

import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class LoginViewModel : ViewModel() {

//    var user= ObservableField<String> (Constant.account)
//    var password= ObservableField<String> (Constant.password)

    var user= ObservableField<String> ("")
    var password= ObservableField<String> ("")
    var loginResult:MutableLiveData<LoginResponseModel> = MutableLiveData()


    fun getLoginDataResult():MutableLiveData<LoginResponseModel>{
        return loginResult
    }





    fun makeLoginApiCall(loginModel: LoginRequestModel) {

        val apiService = RetroInstance.getRetrofitInstance().create(APIService::class.java)
        val call = apiService.getLogin(Constant.credentials, loginModel)

        call.enqueue(object : Callback<LoginResponseModel> {
            override fun onResponse(call: Call<LoginResponseModel>, response: Response<LoginResponseModel>) {

                if (response.isSuccessful) {

//                    result.postValue(true)

                    if (response.body()!=null) {

                        var account: String = ""
                        account = response.body()?.ACCOUNT.toString()
                        Log.d("LoginApiCall", "Number : $account")

                        loginResult.postValue(response.body())



                    }else{
                        Log.d("LoginApiCall", "body null")
                        loginResult.postValue(null)
                    }


                } else {
                    loginResult.postValue(null)
                    Log.d("LoginApiCall", "response not successful")
                }
            }

            override fun onFailure(call: Call<LoginResponseModel>, t: Throwable) {
                Log.d("LoginApiCall", "Failure : ${t.message}")
//                result.postValue(false)
                loginResult.postValue(null)

            }

        })


    }

}