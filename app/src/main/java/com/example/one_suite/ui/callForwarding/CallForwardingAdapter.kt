package com.example.one_suite.ui.callForwarding

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.one_suite.databinding.CallForwardingRecyclerItemBinding
import com.example.one_suite.model.subscriberDid.SubscriberDIDResponseDataModel
import com.example.one_suite.utils.SwitchClickListener

class CallForwardingAdapter(private val listener: SwitchClickListener) :
    RecyclerView.Adapter<CallForwardingAdapter.MyViewHolder>() {
    var items = ArrayList<SubscriberDIDResponseDataModel>()

    fun setDataList(data: ArrayList<SubscriberDIDResponseDataModel>) {
        this.items = data
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): CallForwardingAdapter.MyViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = CallForwardingRecyclerItemBinding.inflate(layoutInflater)
        return MyViewHolder(binding)
    }

    override fun getItemCount() = items.size

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.bind(items[position])

        holder.binding.switchBtn.setOnClickListener(View.OnClickListener {
            var isChecked:Boolean

            isChecked=holder.binding.switchBtn.isChecked
            listener.onSwitchClick(holder.binding.switchBtn, position,isChecked)
        })

    }

    class MyViewHolder(val binding: CallForwardingRecyclerItemBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(data: SubscriberDIDResponseDataModel) {
            binding.viewModel = data
            binding.executePendingBindings()
        }
    }


}