package com.example.one_suite.ui.reWards

import com.example.one_suite.ui.reWards.SubCatagory

class Category(val catId:Int, val catName:String, val childPoint:String, val suCatagories:ArrayList<SubCatagory>? = null,
               var isExpanded:Boolean = false) {
}