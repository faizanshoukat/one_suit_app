package com.example.one_suite.ui.myNumbers

import android.graphics.Color
import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import com.android.navigationtesting.Category1Fragment
import com.example.one_suite.R
import com.example.one_suite.ui.buyNumber.buyNumberList.BuyNumberListFragment
import com.example.one_suite.ui.myNumbers.addName.AddNameFragment
import com.example.one_suite.ui.myNumbers.myNumbersList.MyNumbersListFragment

class MyNumbersFragment : Fragment() {

    companion object {
        fun newInstance() = MyNumbersFragment()
    }

    //private lateinit var viewModel: MyNumbersViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {


        val view =inflater.inflate(R.layout.my_numbers_fragment, container, false)

        /* added fragment My Numbers by default  */
        attachMyNumberFragment()
        val button1=view.findViewById<TextView>(R.id.myNumbers)
        val button2=view.findViewById<TextView>(R.id.addName)


        button1.setOnClickListener(View.OnClickListener {
            button1.setTextColor(Color.parseColor("#7ec243"))
            button2.setTextColor(Color.parseColor("#959898"))
            attachMyNumberFragment()
        })


        button2.setOnClickListener(View.OnClickListener {
            button2.setTextColor(Color.parseColor("#7ec243"))
            button1.setTextColor(Color.parseColor("#959898"))
            attachAddNameFragment()
        })

        return view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        //viewModel = ViewModelProvider(this).get(MyNumbersViewModel::class.java)
        // TODO: Use the ViewModel
    }

   private fun attachMyNumberFragment(){
       //creating fragment object
       var fragment: Fragment? = null


       fragment = MyNumbersListFragment()

       //replacing the fragment
       if (fragment != null) {
           val ft = activity?.supportFragmentManager?.beginTransaction()
           ft?.replace(R.id.myNumbersContainer, fragment)
           ft?.commit()
       }


   }


    fun attachAddNameFragment(){
        //creating fragment object
        var fragment: Fragment? = null


        fragment = AddNameFragment()

        //replacing the fragment
        if (fragment != null) {
            val ft = activity?.supportFragmentManager?.beginTransaction()
            ft?.replace(R.id.myNumbersContainer, fragment)
            ft?.commit()
        }


    }

}