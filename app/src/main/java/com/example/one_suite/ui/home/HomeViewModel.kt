package com.example.one_suite.ui.home

import android.content.Context
import android.util.Log
import android.view.View
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.one_suite.R
import com.example.one_suite.model.callForwarding.setSubscriberDID.SetSubscriberDIDRequestModel
import com.example.one_suite.model.callForwarding.setSubscriberDID.SetSubscriberDIDResponseModel
import com.example.one_suite.model.subscriberDid.SubscriberDIDResponseDataModel
import com.example.one_suite.model.transactionHistory.GetTransactionHistoryRequestModel
import com.example.one_suite.model.transactionHistory.GetTransactionHistoryResponseDataModel
import com.example.one_suite.model.transactionHistory.GetTransactionHistoryResponseModel
import com.example.one_suite.network.APIService
import com.example.one_suite.network.RetroInstance
import com.example.one_suite.ui.callForwarding.setForwarding.SetForwardingDialog1
import com.example.one_suite.utils.Constant
import com.example.one_suite.utils.SwitchClickListener
import com.resocoder.databinding.model.subscriberDid.SubscriberDIDRequestModel

import com.resocoder.databinding.model.subscriberDid.SubscriberDIDResponseModel
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.*
import kotlin.collections.ArrayList

class HomeViewModel() : ViewModel(), SwitchClickListener {
    //    private val switchIsCheckValue = MutableLiveData<String>()
//    private val switchNotCheckValue = MutableLiveData<String>()

    lateinit var recyclerViewAdapter: CallHistoryAdapter

    lateinit var recyclerListData: MutableLiveData<ArrayList<GetSubscriberCallHistoryResponseDataModel>>
    lateinit var callForwardingListdata: MutableLiveData<ArrayList<SubscriberDIDResponseDataModel>>
    lateinit var transectionHistoryListdata: MutableLiveData<ArrayList<GetTransactionHistoryResponseDataModel>>
    lateinit var callingPlanHistoryListdata: MutableLiveData<ArrayList<GetSubscriberPromotionResponseDataModel>>

    lateinit var callForwardingSwitchDataObserver: MutableLiveData<SubscriberDIDResponseDataModel>
    var callForwardingResult = MutableLiveData<Boolean>()

    var noDataFound = MutableLiveData<Boolean>(true)
    var isCallHistory = MutableLiveData<Boolean>(true)

    var account = MutableLiveData<String>(Constant.account)
    var balance = MutableLiveData<String>()
    var name = MutableLiveData<String>(Constant.name)

//    lateinit var balanceData: MutableLiveData<GetBalanceResponseModel>
//    var callForwardingResult = MutableLiveData<Boolean>()

    init {
//        switchIsCheckValue.value = ""
//        switchNotCheckValue.value = ""
        recyclerListData = MutableLiveData()
        transectionHistoryListdata = MutableLiveData()
        callForwardingListdata = MutableLiveData()
        callingPlanHistoryListdata = MutableLiveData()
        callForwardingSwitchDataObserver = MutableLiveData()
//        balanceData=MutableLiveData()
        recyclerViewAdapter = CallHistoryAdapter(this)
    }

    fun getAdapter(): CallHistoryAdapter {
        return recyclerViewAdapter
    }

    fun setAdapterData(data: ArrayList<CustomMyHistoryHomeModel>) {
        recyclerViewAdapter.setDataList(data)
        recyclerViewAdapter.notifyDataSetChanged()
    }


    fun getRecyclerListDataObserver(): MutableLiveData<ArrayList<GetSubscriberCallHistoryResponseDataModel>> {
        return recyclerListData
    }

    fun getCallForwardingDataObserver(): MutableLiveData<ArrayList<SubscriberDIDResponseDataModel>> {
        return callForwardingListdata
    }

    fun getTransectionHistoryListDataObserver(): MutableLiveData<ArrayList<GetTransactionHistoryResponseDataModel>> {
        return transectionHistoryListdata
    }

    fun getCallingPlanHistoryListDataObserver(): MutableLiveData<ArrayList<GetSubscriberPromotionResponseDataModel>> {
        return callingPlanHistoryListdata
    }

    fun getCallForwardingSwitchValueDataObserver(): MutableLiveData<SubscriberDIDResponseDataModel> {
        return callForwardingSwitchDataObserver
    }

    fun getCallForwardingResultDataObserver(): MutableLiveData<Boolean> {
        return callForwardingResult;
    }

    fun getBalanceObserver(): MutableLiveData<String> {
//        Log.d("FaizanTestingDialog","balance :$balance")
        return balance
    }
//    fun getBalanceDataObserver(): MutableLiveData<GetBalanceResponseModel> {
//        return balanceData
//    }

//    fun getSwitchIsCheckObserver(): MutableLiveData<String> {
//        return switchIsCheckValue
//    }
//
//    fun getSwitchNotCheckObserver(): MutableLiveData<String> {
//        return switchNotCheckValue
//    }

//    fun getCallForwardingResultObserver(): MutableLiveData<Boolean> {
//        return callForwardingResult
//    }


    fun makeSubscriberCallHistoryApiCall() {


        val sdf = SimpleDateFormat("MM-dd-yyyy")
        val currentDate = sdf.format(Date())


        var FROM_DATE: String = "04-19-2018"
//        var TO_DATE: String = "09-08-2021"
        var TO_DATE: String = currentDate
        var PAGE_SIZE: Int = 100
        var PAGE_NUMBER: Int = 1


        val requestModel: GetSubscriberCallHistoryRequestModel =
            GetSubscriberCallHistoryRequestModel(
                Constant.account,
                FROM_DATE,
                TO_DATE,
                PAGE_SIZE,
                PAGE_NUMBER
            )

        Log.d("CallHistoryIssue", "Current Date and Time is: $currentDate")
        Log.d("CallHistoryIssue", "Account : ${Constant.account}")


        val apiService = RetroInstance.getRetrofitInstance().create(APIService::class.java)
        val call = apiService.getCallHistory(Constant.credentials, requestModel)

        call.enqueue(object : Callback<GetSubscriberCallHistoryResponseModel> {
            override fun onResponse(
                call: Call<GetSubscriberCallHistoryResponseModel>,
                response: Response<GetSubscriberCallHistoryResponseModel>
            ) {

                Log.d("CallHistoryIssue", "response")
                if (response.isSuccessful) {


                    if (response.body() != null) {

                        recyclerListData.postValue(response.body()!!.HISTORY)
//                        Log.d("SubscriberApiCall", "Size :" + response.body()!!.SubscriberDIDs.size)
//                        Log.d("SubscriberApiCall", "Number :" + response.body()!!.SubscriberDIDs.get(0).Number)
                    } else {
                        recyclerListData.postValue(null)
                    }


                } else {
                    recyclerListData.postValue(null)
                }
            }

            override fun onFailure(
                call: Call<GetSubscriberCallHistoryResponseModel>,
                t: Throwable
            ) {
                Log.d("CallHistoryIssue", "Failure : ${t.message}")
                recyclerListData.postValue(null)

            }

        })

    }

    fun makeSubscriberDIDApiCall() {

        val subscriberDIDRequest: SubscriberDIDRequestModel =
            SubscriberDIDRequestModel(Constant.account)

        Log.d("SubscriberApiCall", "country list api call")
        val apiService = RetroInstance.getRetrofitInstance().create(APIService::class.java)
        val call = apiService.getSubscriberDID(Constant.credentials, subscriberDIDRequest)

        call.enqueue(object : Callback<SubscriberDIDResponseModel> {
            override fun onResponse(
                call: Call<SubscriberDIDResponseModel>,
                response: Response<SubscriberDIDResponseModel>
            ) {

                Log.d("SubscriberApiCall", "response")
                if (response.isSuccessful) {

                    //result.postValue(true)

                    if (response.body() != null) {
                        callForwardingListdata.postValue(response.body()!!.SubscriberDIDs)
                    } else {
                        callForwardingListdata.postValue(null)
                    }


                } else {
                    callForwardingListdata.postValue(null)
                }
            }

            override fun onFailure(call: Call<SubscriberDIDResponseModel>, t: Throwable) {
                Log.d("SubscriberApiCall", "Failure : ${t.message}")
                callForwardingListdata.postValue(null)
            }

        })

    }

    fun makeTransactionHistoryApiCall() {

        val sdf = SimpleDateFormat("yyyy-MM-dd")
//        val sdf = SimpleDateFormat("yyyy-dd-MM")
        val currentDate = sdf.format(Date())


        var FROM_DATE: String = "2015-10-31"
        var TO_DATE: String = currentDate


        val requestModel: GetTransactionHistoryRequestModel =
            GetTransactionHistoryRequestModel(Constant.account, FROM_DATE, TO_DATE)

        Log.d("SubscriberApiCall", "country list api call")
        val apiService = RetroInstance.getRetrofitInstance().create(APIService::class.java)
        val call = apiService.getTransactionHistory(Constant.credentials, requestModel)

        call.enqueue(object : Callback<GetTransactionHistoryResponseModel> {
            override fun onResponse(
                call: Call<GetTransactionHistoryResponseModel>,
                response: Response<GetTransactionHistoryResponseModel>
            ) {

                Log.d("SubscriberApiCall", "response")
                if (response.isSuccessful) {


                    if (response.body() != null) {
                        transectionHistoryListdata.postValue(response.body()!!.PaymentHistories)
                    } else {
                        transectionHistoryListdata.postValue(null)
                    }


                } else {
                    transectionHistoryListdata.postValue(null)
                }
            }

            override fun onFailure(call: Call<GetTransactionHistoryResponseModel>, t: Throwable) {
                Log.d("SubscriberApiCall", "Failure : ${t.message}")
                transectionHistoryListdata.postValue(null)

            }

        })

    }

    fun makeCallingPlanHistoryApiCall() {


        val requestModel: GetSubscriberPromotionRequestModel =
            GetSubscriberPromotionRequestModel(Constant.account)

        Log.d("SubscriberApiCall", "country list api call")
        val apiService = RetroInstance.getRetrofitInstance().create(APIService::class.java)
        val call = apiService.getCallingPlanHistory(Constant.credentials, requestModel)

        call.enqueue(object : Callback<GetSubscriberPromotionResponseModel> {
            override fun onResponse(
                call: Call<GetSubscriberPromotionResponseModel>,
                response: Response<GetSubscriberPromotionResponseModel>
            ) {

                Log.d("SubscriberApiCall", "response")
                if (response.isSuccessful) {


                    if (response.body() != null) {
                        callingPlanHistoryListdata.postValue(response.body()!!.Promotions)
                    } else {
                        callingPlanHistoryListdata.postValue(null)
                    }


                } else {
                    callingPlanHistoryListdata.postValue(null)
                }
            }

            override fun onFailure(call: Call<GetSubscriberPromotionResponseModel>, t: Throwable) {
                Log.d("SubscriberApiCall", "Failure : ${t.message}")
                callingPlanHistoryListdata.postValue(null)

            }

        })

    }

    fun makeSetDIDSettingApiCall(didModel: SetSubscriberDIDRequestModel) {

        val apiService = RetroInstance.getRetrofitInstance().create(APIService::class.java)
        val call = apiService.setSubscriberDID(Constant.credentials, didModel)

        call.enqueue(object : Callback<SetSubscriberDIDResponseModel> {
            override fun onResponse(
                call: Call<SetSubscriberDIDResponseModel>,
                response: Response<SetSubscriberDIDResponseModel>
            ) {

                if (response.isSuccessful) {

                    callForwardingResult.postValue(true)

                    if (response.body() != null) {

                        var account: String = ""
                        account = response.body()?.RESPONSE.toString()
                        Log.d("DIDApiCall", "Number : $account")


                    }


                } else {
                    callForwardingResult.postValue(false)
                }
            }

            override fun onFailure(call: Call<SetSubscriberDIDResponseModel>, t: Throwable) {
                Log.d("DIDApiCall", "Failure : ${t.message}")
                callForwardingResult.postValue(false)

            }

        })


    }


    fun makeBalanceApiCall() {


        val requestModel: GetBalanceRequestModel = GetBalanceRequestModel(Constant.account)

        Log.d("FaizanTestingDialog", "balance Api")
        val apiService = RetroInstance.getRetrofitInstance().create(APIService::class.java)
        val call = apiService.getBalance(Constant.credentials, requestModel)

        call.enqueue(object : Callback<GetBalanceResponseModel> {
            override fun onResponse(
                call: Call<GetBalanceResponseModel>,
                response: Response<GetBalanceResponseModel>
            ) {

                Log.d("SubscriberApiCall", "response")
                if (response.isSuccessful) {


                    if (response.body() != null) {
                        //balanceData.postValue(response.body())

                        balance.postValue(response.body()!!.BALANCE)
                    } else {
                        balance.postValue("")
                        //balanceData.postValue(null)
                    }


                } else {
                    balance.postValue("0.0")
//                    balanceData.postValue(null)
                }
            }

            override fun onFailure(
                call: Call<GetBalanceResponseModel>,
                t: Throwable
            ) {
                Log.d("SubscriberApiCall", "Failure : ${t.message}")
//                balanceData.postValue(null)
                balance.postValue("")
            }

        })

    }


    override fun onViewClick(view: View, position: Int) {
    }

    override fun onSwitchClick(view: View, position: Int, isChecked: Boolean) {

        Log.d(
            "SubscriberApiCall",
            "On View Click switch " + (callForwardingListdata.value?.get(position)?.Number
                ?: "Null exception")
        )

        if (view.id == R.id.switchBtn) {

            callForwardingSwitchDataObserver.postValue((callForwardingListdata.value?.get(position)))
        }


    }


    //SubscriberBalance Api call

//    //step1
//    val txt_BALANCE: LiveData<String>
//        get() = _getBALANCE
//
//    var getBalanceResponseMutable : MutableLiveData<Responce_GetBalance>
//
//
//    //step 2
//    fun getBalanceObserver(): MutableLiveData<Responce_GetBalance>{
//        return getBalanceResponseMutable
//    }
//
//    //Step 3
//    val _getBALANCE =MutableLiveData<String>()
//
//
//    // step 4
//    init {
//        _getBALANCE.value = ""
//        getBalanceResponseMutable = MutableLiveData()
//    }
//
//    // step 5
//
//    fun makeGetBalanceApi(context: Context){
//
//        val requestGetbalance: Request_GetBalance =
//            Request_GetBalance(Constant.account)
//
//
//        val apiServiesL=  RetroInstance.getRetrofitInstance().create(APIService::class.java)
//
//        val callApi =apiServiesL.showBalance(Constant.credentials,
//            requestGetbalance)
//
//        callApi.enqueue(object : Callback<Responce_GetBalance>{
//
//            override fun onResponse(call: Call<Responce_GetBalance>, response: Response<Responce_GetBalance>) {
//                Log.d("TAG", "onResponse: ")
//                Log.d("TAG", "onResponse: "+response.code())
//                if (response.isSuccessful){
//                    Log.d("TAG", "onResponse: "+response.isSuccessful)
//                    getBalanceResponseMutable.postValue(response.body())
//
//                    var balance: String = ""
//                    balance =response.body()?.BALANCE.toString()
//                    Log.d("TAG", "BALANCE: $balance ")
//                    _getBALANCE.value="$ "+balance
//                }
//                getBalanceResponseMutable.postValue(null)
//            }
//
//            override fun onFailure(call: Call<Responce_GetBalance>, t: Throwable) {
//                getBalanceResponseMutable.postValue(null)
//                Log.d("TAG", "onFailure: ")
//            }
//        })
//    }


}
