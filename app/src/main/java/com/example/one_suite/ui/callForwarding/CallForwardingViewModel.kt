package com.example.one_suite.ui.callForwarding

import android.content.Context
import android.util.Log
import android.view.View
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.one_suite.R
import com.example.one_suite.model.callForwarding.setSubscriberDID.SetSubscriberDIDRequestModel
import com.example.one_suite.model.callForwarding.setSubscriberDID.SetSubscriberDIDResponseModel
import com.example.one_suite.model.subscriberDid.SubscriberDIDResponseDataModel
import com.example.one_suite.network.APIService
import com.example.one_suite.network.RetroInstance
import com.example.one_suite.utils.Constant
import com.example.one_suite.utils.SwitchClickListener
import com.resocoder.databinding.model.subscriberDid.SubscriberDIDRequestModel

import com.resocoder.databinding.model.subscriberDid.SubscriberDIDResponseModel
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class CallForwardingViewModel() : ViewModel(), SwitchClickListener {
    private val switchIsCheckValue = MutableLiveData<String>()
    private val switchNotCheckValue = MutableLiveData<String>()
    lateinit var recyclerListData: MutableLiveData<ArrayList<SubscriberDIDResponseDataModel>>
    lateinit var recyclerViewAdapter: CallForwardingAdapter


    var callForwardingResult = MutableLiveData<Boolean>()

    init {
        switchIsCheckValue.value = ""
        switchNotCheckValue.value = ""
        recyclerListData = MutableLiveData()
        recyclerViewAdapter = CallForwardingAdapter(this)
    }

    fun getAdapter(): CallForwardingAdapter {
        return recyclerViewAdapter
    }

    fun setAdapterData(data: ArrayList<SubscriberDIDResponseDataModel>) {
        recyclerViewAdapter.setDataList(data)
        recyclerViewAdapter.notifyDataSetChanged()
    }


    fun getRecyclerListDataObserver(): MutableLiveData<ArrayList<SubscriberDIDResponseDataModel>> {
        return recyclerListData
    }

    fun getSwitchIsCheckObserver(): MutableLiveData<String> {
        return switchIsCheckValue
    }

    fun getSwitchNotCheckObserver(): MutableLiveData<String> {
        return switchNotCheckValue
    }

    fun getCallForwardingResultObserver(): MutableLiveData<Boolean> {
        return callForwardingResult
    }

    fun makeSubscriberDIDApiCall() {

        val subscriberDIDRequest: SubscriberDIDRequestModel =
            SubscriberDIDRequestModel(Constant.account)

        Log.d("SubscriberApiCall", "country list api call")
        val apiService = RetroInstance.getRetrofitInstance().create(APIService::class.java)
        val call = apiService.getSubscriberDID(Constant.credentials, subscriberDIDRequest)

        call.enqueue(object : Callback<SubscriberDIDResponseModel> {
            override fun onResponse(call: Call<SubscriberDIDResponseModel>, response: Response<SubscriberDIDResponseModel>) {

                Log.d("SubscriberApiCall", "response")
                if (response.isSuccessful) {

                    //result.postValue(true)

                    if (response.body() != null) {
                        recyclerListData.postValue(response.body()!!.SubscriberDIDs)
                    }else{
                        recyclerListData.postValue(null)
                    }


                } else {
                    recyclerListData.postValue(null)
                }
            }

            override fun onFailure(call: Call<SubscriberDIDResponseModel>, t: Throwable) {
                Log.d("SubscriberApiCall", "Failure : ${t.message}")
                recyclerListData.postValue(null)
            }

        })

    }


    override fun onViewClick(view: View, position: Int) {
    }

    override fun onSwitchClick(view: View, position: Int, isChecked: Boolean) {

        Log.d(
                "SubscriberApiCall",
                "On View Click switch " + (recyclerListData.value?.get(position)?.Number
                        ?: "Null exception"))

        if (view.id == R.id.switchBtn) {

            if (isChecked) {
                switchIsCheckValue.postValue(recyclerListData.value?.get(position)?.Number ?: "Null")
            }else{
                switchNotCheckValue.postValue(recyclerListData.value?.get(position)?.Number ?: "Null")
            }

        }



    }




    fun makeSetDIDSettingApiCall(didModel: SetSubscriberDIDRequestModel) {

        val apiService = RetroInstance.getRetrofitInstance().create(APIService::class.java)
        val call = apiService.setSubscriberDID(Constant.credentials, didModel)

        call.enqueue(object : Callback<SetSubscriberDIDResponseModel> {
            override fun onResponse(call: Call<SetSubscriberDIDResponseModel>, response: Response<SetSubscriberDIDResponseModel>) {

                if (response.isSuccessful) {

                    callForwardingResult.postValue(true)

                    if (response.body() != null) {

                        var account: String = ""
                        account = response.body()?.RESPONSE.toString()
                        Log.d("DIDApiCall", "Number : $account")


                    }


                } else {
                    callForwardingResult.postValue(false)
                }
            }

            override fun onFailure(call: Call<SetSubscriberDIDResponseModel>, t: Throwable) {
                Log.d("DIDApiCall", "Failure : ${t.message}")
                callForwardingResult.postValue(false)

            }

        })


    }

}
