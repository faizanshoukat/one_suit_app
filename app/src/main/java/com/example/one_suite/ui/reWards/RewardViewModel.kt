package com.example.one_suite.ui.reWards

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.one_suite.model.subscriberDid.SubscriberDIDResponseDataModel
import com.example.one_suite.network.APIService
import com.example.one_suite.network.RetroInstance
import com.example.one_suite.ui.callForwarding.CallForwardingAdapter
import com.example.one_suite.ui.reWards.models.*
import com.example.one_suite.utils.Constant
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class RewardViewModel : ViewModel() {
    var rewardHistoryList: MutableLiveData<ArrayList<GetRewardHistoryResponseDataModel>> =
        MutableLiveData()
    var referralHistoryList: MutableLiveData<ArrayList<GetReferralHistoryResponseDataModel>> =
        MutableLiveData()
    var redeemHistoryList: MutableLiveData<ArrayList<GetRedeemRewardResponseDataModel>> =
        MutableLiveData()


    var rewardPointsData: MutableLiveData<GetRewardPointResponseModel> =
        MutableLiveData()

    var rewardAdapter: ExpandableCatAdapter = ExpandableCatAdapter()



    fun getAdapter(): ExpandableCatAdapter {
        return rewardAdapter
    }

    fun setAdapterData(data: ArrayList<Category>) {
        rewardAdapter.setDataList(data)
        rewardAdapter.notifyDataSetChanged()
    }


    fun getRewardPointsObserver(): MutableLiveData<GetRewardPointResponseModel> {
        return rewardPointsData
    }

    fun getRewardHistoryListObserver(): MutableLiveData<ArrayList<GetRewardHistoryResponseDataModel>> {
        return rewardHistoryList
    }

    fun getReferralHistoryListObserver(): MutableLiveData<ArrayList<GetReferralHistoryResponseDataModel>> {
        return referralHistoryList
    }

    fun getRedeemHistoryListObserver(): MutableLiveData<ArrayList<GetRedeemRewardResponseDataModel>> {
        return redeemHistoryList
    }


    fun makeRewardHistoryApiCall(userId: String) {
        val requestModel: GetRewardHistoryRequestModel = GetRewardHistoryRequestModel(userId)

        //creating retro instance
        val apiServices = RetroInstance.getRetrofitInstance().create(APIService::class.java)
        val call = apiServices.getRewardHistoryApiCall(Constant.credentials, requestModel)
        call.enqueue(object : Callback<GetRewardHistoryResponseModel> {
            override fun onResponse(
                call: Call<GetRewardHistoryResponseModel>,
                response: Response<GetRewardHistoryResponseModel>
            ) {
                Log.d("TAG", "onResponse: main is calling ")
                if (response.isSuccessful) {
                    Log.d("TAG", "onResponse: " + response.code())

                    if (response.body() != null) {
                        rewardHistoryList.postValue(response.body()!!.RewardList)
                    } else {
                        rewardHistoryList.postValue(null)
                    }
                } else {
                    rewardHistoryList.postValue(null)
                }
            }


            override fun onFailure(call: Call<GetRewardHistoryResponseModel>, t: Throwable) {
                Log.d("TAG", "onFailure: " + t.message)
                rewardHistoryList.postValue(null)
            }

        })
    }


    fun makeReferralHistoryApiCall(userId: String) {
        val requestValues: GetReferralHistoryRequestModel =
            GetReferralHistoryRequestModel(userId)

        val retroServes = RetroInstance.getRetrofitInstance().create(APIService::class.java)
        val callingApi =
            retroServes.getReferralHistoryApiInstance(Constant.credentials, requestValues)
        callingApi.enqueue(object : Callback<GetReferralHistoryResponseModel> {
            override fun onResponse(
                call: Call<GetReferralHistoryResponseModel>,
                response: Response<GetReferralHistoryResponseModel>
            ) {
                Log.d("TAG", "onResponse calling: ")
                if (response.isSuccessful) {
                    if (response.body() != null) {
                        referralHistoryList.postValue(response.body()?.ReferralList)
                    } else {
                        referralHistoryList.postValue(null)
                    }
                } else {
                    referralHistoryList.postValue(null)
                }
            }

            override fun onFailure(call: Call<GetReferralHistoryResponseModel>, t: Throwable) {
                Log.d("TAG", "onFailure: ." + t.message)
                referralHistoryList.postValue(null)
            }
        })
    }

    fun makeRedeemApiCall() {

        val requestBody: GetRedeemRewardRequestModel = GetRedeemRewardRequestModel(Constant.account)
        //create the retrofit Instance
        val apiServices = RetroInstance.getRetrofitInstance().create(APIService::class.java)
        val call = apiServices.getRedeemHistoryApiCall(Constant.credentials, requestBody)
        call.enqueue(object : Callback<GetRedeemRewardResponseModel> {
            override fun onResponse(
                call: Call<GetRedeemRewardResponseModel>,
                response: Response<GetRedeemRewardResponseModel>
            ) {
                Log.d("TAG", "onResponse: ")
                if (response.isSuccessful) {
                    if (response.body() != null) {
                        redeemHistoryList.postValue(response.body()!!.List)
                    } else {
                        redeemHistoryList.postValue(null)
                    }
                } else {
                    redeemHistoryList.postValue(null)
                }
            }

            override fun onFailure(call: Call<GetRedeemRewardResponseModel>, t: Throwable) {
                Log.d("TAG", "onFailure: " + t.message)
                redeemHistoryList.postValue(null)
            }

        })


    }

    fun makeRewardPointsApiCall(userId: String) {
        val requestValues: GetRewardPointRequestModel =
            GetRewardPointRequestModel(userId)

        val retroServes = RetroInstance.getRetrofitInstance().create(APIService::class.java)
        val callingApi =
            retroServes.getRewardPointsApiCall(Constant.credentials, requestValues)
        callingApi.enqueue(object : Callback<GetRewardPointResponseModel> {
            override fun onResponse(
                call: Call<GetRewardPointResponseModel>,
                response: Response<GetRewardPointResponseModel>
            ) {
                Log.d("TAG", "onResponse calling: ")
                if (response.isSuccessful) {
                    if (response.body() != null) {
                        rewardPointsData.postValue(response.body())
                    } else {
                        rewardPointsData.postValue(null)
                    }
                } else {
                    rewardPointsData.postValue(null)
                }
            }

            override fun onFailure(call: Call<GetRewardPointResponseModel>, t: Throwable) {
                Log.d("TAG", "onFailure: ." + t.message)
                rewardPointsData.postValue(null)
            }
        })
    }

}