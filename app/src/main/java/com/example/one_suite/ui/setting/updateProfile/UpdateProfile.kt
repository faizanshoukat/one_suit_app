package com.example.one_suite.ui.setting.updateProfile

import android.app.ProgressDialog
import android.graphics.Color
import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import androidx.lifecycle.Observer
import com.example.one_suite.databinding.UpdateProfileFragmentBinding
import com.example.one_suite.model.country.GetCountryDataModel
import com.example.one_suite.model.credits.autoRecharge.paymentInformation.PaymentInformationResponseModel
import com.example.one_suite.model.credits.autoRecharge.paymentInformation.setPaymentInfo.SetPaymentInfoRequestModel
import com.example.one_suite.utils.Constant
import com.example.one_suite.utils.customSpiner.CustomSpinerAdapter
import com.example.one_suite.utils.customSpiner.Data
import com.resocoder.databinding.utils.DialogUtils

class UpdateProfile : Fragment() {

    companion object {
        fun newInstance() = UpdateProfile()
    }


    private lateinit var updateProfileViewModel: UpdateProfileViewModel
    private var _binding: UpdateProfileFragmentBinding? = null
    private val binding get() = _binding!!


    var previousCountryCode = ""
    var finalCountry=""
    var finalCountryCode=""
    var countydataModel: MutableList<Data> = mutableListOf<Data>()
//    private lateinit var countryList:ArrayList<GetCountryDataModel>=

    var countryList= arrayListOf<GetCountryDataModel>()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        updateProfileViewModel = ViewModelProvider(this).get(UpdateProfileViewModel::class.java)

        _binding = UpdateProfileFragmentBinding.inflate(inflater, container, false).apply {
            this.lifecycleOwner = this@UpdateProfile
            this.viewModel = updateProfileViewModel
        }

        val root: View = binding.root

        /*..............For Show More And Less Text..........*/
        _binding!!.textViewShowMore.setShowingChar(30)
        _binding!!.textViewShowMore.setShowingLine(2)
        _binding!!.textViewShowMore.addShowMoreText("see more");
        _binding!!.textViewShowMore.addShowLessText("Less");

        _binding!!.textViewShowMore.setShowMoreColor(Color.RED); // or other color
        _binding!!.textViewShowMore.setShowLessTextColor(Color.RED); // or other color
        /*..............For Show More And Less Text..........*/


        //progress dialog
        var progressDialog = DialogUtils.setProgressDialog(container!!.context, "Please Wait....")


        //Country custom spinner
        countydataModel.add(Data(1, ""))


        val countrySpinner = binding.countrySpinner
        val countryAdapter = CustomSpinerAdapter()
        countryAdapter.customeSpinnerAdapter(requireContext(), countydataModel)
        countrySpinner.adapter = countryAdapter


        countrySpinner!!.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                adapter: AdapterView<*>?,
                view: View?,
                position: Int,
                p3: Long
            ) {
                if (countydataModel.size > 0) {
                    finalCountry = countydataModel[position].country.toString()
                    Log.d("SpinnerText", "Country :$finalCountry")
                }

            }

            override fun onNothingSelected(p0: AdapterView<*>?) {
            }

        }


        progressDialog!!.show()
        updateProfileViewModel.makePaymentInfoApiCall()
//        var requestModel = SubscriberProfileRequestModel(Constant.account)
//        updateProfileViewModel.makeProfileApiCall(requestModel)



        updateProfileViewModel.getPaymentInfoDataObserver().observe(viewLifecycleOwner, Observer {
            if (it != null) {

                var responseModel: PaymentInformationResponseModel = it!!
                Log.d("ProfileApiCall", "name : ${responseModel.FIRST_NAME}")

                updateProfileViewModel.firstName.set(responseModel.FIRST_NAME)
                updateProfileViewModel.lastName.set(responseModel.LAST_NAME)
                updateProfileViewModel.email.set(responseModel.E_MAIL)
                updateProfileViewModel.contactNumber.set(responseModel.LOCAL_PHONE)

                updateProfileViewModel.address1.set(responseModel.ADDRESS1)
                updateProfileViewModel.address2.set(responseModel.ADDRESS2)
                updateProfileViewModel.city.set(responseModel.BILLING_CITY)
                updateProfileViewModel.state.set(responseModel.STATE)

                previousCountryCode = responseModel.COUNTRY_CODE

                updateProfileViewModel.country.set(responseModel.COUNTRY_CODE)
                updateProfileViewModel.zipCode.set(responseModel.POSTAL_CODE)
            }

            updateProfileViewModel.makeCountryListApiCall()
        })

//        updateProfileViewModel.getProfileDataObserver().observe(viewLifecycleOwner, Observer {
//
////            progressDialog.dismiss()
//            if (it != null) {
//
//                var responseModel: SubscriberProfileResponseModel = it!!
//                Log.d("ProfileApiCall", "name : ${responseModel.FIRST_NAME}")
//
//                updateProfileViewModel.firstName.set(responseModel.FIRST_NAME)
//                updateProfileViewModel.lastName.set(responseModel.LAST_NAME)
//                updateProfileViewModel.email.set(responseModel.E_MAIL)
//                updateProfileViewModel.contactNumber.set(responseModel.LOCAL_PHONE)
//
//                updateProfileViewModel.address1.set(responseModel.ADDRESS1)
//                updateProfileViewModel.address2.set(responseModel.ADDRESS2)
//                updateProfileViewModel.city.set(responseModel.CITY)
//                updateProfileViewModel.state.set(responseModel.STATE_REGION)
//
//                previousCountry = responseModel.COUNTRY
//
//                updateProfileViewModel.country.set(responseModel.COUNTRY)
//                updateProfileViewModel.zipCode.set(responseModel.POSTAL_CODE)
//            }
//
//            updateProfileViewModel.makeCountryListApiCall()
//        })

        updateProfileViewModel.getCountryListDataObserver().observe(viewLifecycleOwner, Observer {
            progressDialog.dismiss()

            var previousCountry=""

            if (it == null) {
                Log.d("StatesIssue", "There is an issue in fetching data")
            } else {

                /*........to use it for country code......*/
                countryList=it


                Log.d("StatesIssue", "Sates Size: ${it.size}")
                countydataModel.clear()
                countydataModel.add(Data(1, "--ALL--"))
                for (item in it) {

                    /*......as we have country code thats why we are getting the country name for further use.......*/
                    if (item.ISO==previousCountryCode){
                        previousCountry=item.Name
                        finalCountry=item.Name
                    }

                    countydataModel.add(Data(1, item.Name))

                }

//                statedataModel.sortBy { data: Data -> item.State }
                countydataModel.sortBy { Data -> Data.country }

                countryAdapter.customeSpinnerAdapter(requireContext(), countydataModel)
                countrySpinner.adapter = countryAdapter

                var index = 0
                for ((i, item) in countydataModel.withIndex()) {
                    if (previousCountry == item.country) {
                        index = i
                    }
                }

                countrySpinner.setSelection(index)

            }
        })


        updateProfileViewModel.getProfileResult().observe(viewLifecycleOwner, androidx.lifecycle.Observer {
            progressDialog.dismiss()
        })

        /*....................Update button click........................*/
        _binding!!.submitButton.setOnClickListener(View.OnClickListener {


            /*..........get the country code...........*/
            for (item in countryList){
                if (finalCountry==item.Name){
                    finalCountryCode=item.ISO
                }
            }


            progressDialog.show()

            var firstName: String = _binding!!.firstName.text.toString()
            var lastName: String = _binding!!.lastName.text.toString()
            var email: String =_binding!!.email.text.toString()
            var contactNumber: String = _binding!!.contactNumber.text.toString()

            var address1: String = _binding!!.address1.text.toString()
            var address2: String = _binding!!.address2.text.toString()
            var city: String = _binding!!.city.text.toString()
            var state: String = _binding!!.state.text.toString()
            var country: String = finalCountryCode
            var postalCode: String = _binding!!.zipCode.text.toString()
            var allowUpdates:Boolean=_binding!!.allowUpdates.isChecked



            Log.d("ProfileApiCall", "first name :$firstName")

            Log.d("ProfileApiCall", "country :$finalCountry")

            if (firstName.trim().isEmpty()) {
                _binding!!.firstName.error = "Please Enter First Name"
            } else if (lastName.trim().isEmpty()) {
                _binding!!.lastName.error = "Please Enter Last Name"
            } else {
                progressDialog.show()

//                var requestModel = SetProfileRequestModel()
//                requestModel.ACCOUNT = Constant.account
//                requestModel.FIRST_NAME = firstName
//                requestModel.LAST_NAME = lastName
//                requestModel.E_MAIL = email
//                requestModel.LOCAL_PHONE = contactNumber
//                requestModel.ADDRESS1 = address1
//                requestModel.ADDRESS2 = address2
//                requestModel.CITY = city
//                requestModel.STATE_REGION = state
//                requestModel.COUNTRY = country
//                requestModel.POSTAL_CODE = postalCode
//



                var Username:String=email
                var ACCOUNT:String=Constant.account
                var FIRST_NAME:String=firstName
                var LAST_NAME:String=lastName
                var ADDRESS1:String=address1
                var ADDRESS2:String=address2
                var POSTAL_CODE:String=postalCode
                var STATE:String=state
                var COUNTRY_CODE:String=country
                var BILLING_CITY:String=city
                var LOCAL_PHONE:String=contactNumber
                var E_MAIL:String=email
                var OffersUpdateSubscription:Boolean=allowUpdates



                var requestModel=SetPaymentInfoRequestModel(Username, ACCOUNT, FIRST_NAME, LAST_NAME, ADDRESS1, ADDRESS2, POSTAL_CODE, STATE, COUNTRY_CODE, BILLING_CITY, LOCAL_PHONE, E_MAIL, OffersUpdateSubscription)

                updateProfileViewModel.makeSetPaymentInfoApiCall(requestModel)
            }
        })

        return root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        updateProfileViewModel = ViewModelProvider(this).get(UpdateProfileViewModel::class.java)
        // TODO: Use the ViewModel
    }

}