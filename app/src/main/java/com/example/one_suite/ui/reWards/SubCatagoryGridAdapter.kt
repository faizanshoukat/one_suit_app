package com.example.one_suite.ui.reWards

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.one_suite.databinding.SubCatagoryGridItemnBinding


class SubCatagoryGridAdapter ( private var categories: ArrayList<SubCatagory> ):
    RecyclerView.Adapter<SubCatagoryGridAdapter.ViewHolder>() {

    lateinit var setonClickListeners: SetOnClickListner
    fun setClickListener(setonClickListener: SetOnClickListner) {
        setonClickListeners = setonClickListener
    }

    inner class ViewHolder( val binding: SubCatagoryGridItemnBinding) : RecyclerView.ViewHolder(binding.root){
        fun bind(item: SubCatagory){
            binding.category = item
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = SubCatagoryGridItemnBinding.inflate(LayoutInflater.from(parent.context),parent,false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(categories[position])
        holder.itemView.setOnClickListener {
            setonClickListeners.onClick(categories[position],position)
        }
    }

    override fun getItemCount() = categories.size
    interface SetOnClickListner {
        fun onClick(testType: SubCatagory, pos:Int)
    }
}