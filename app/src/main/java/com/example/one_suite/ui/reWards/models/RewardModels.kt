package com.example.one_suite.ui.reWards.models


/*..............................Reward history models.......................*/
data class GetRewardPointRequestModel (val userId : String)


data class GetRewardPointResponseModel(
    val SumOfRewards : Int,
    val AddThirthyDays : Int,
    val RedeemedThirthyDays : Int,
    val ExpiredThirthyDays : Int,
    val ExpiringThirthyDays : Int,
    val ResponseCode : String,
    val ResponseMessage : String,
)
/*..............................Reward history models.......................*/
data class GetRewardHistoryRequestModel (val userId : String)

data class GetRewardHistoryResponseModel (val RewardList : ArrayList<GetRewardHistoryResponseDataModel> )

data class GetRewardHistoryResponseDataModel(
    val ReferredUsername : String,
    val ReferredRewardData : String,
    val RewardAmount : String,
    val TotalRewardAmount : String,
    val Status : String,
    val Description : String,
    val Type : String,
)




/*............................Referral history models.........................*/
data class GetReferralHistoryRequestModel(val userId: String)


data class GetReferralHistoryResponseModel( val  ReferralList: ArrayList<GetReferralHistoryResponseDataModel>)

data class GetReferralHistoryResponseDataModel(
    val ReferredEmail : String,
    val ReferredCreatedDate : String,
    val Status : String,
    val PhoneNumber : String,
    val SignUpDate : String
)


/*............................Redeem History Models..........................*/
data class GetRedeemRewardRequestModel (val Account: String)


data class GetRedeemRewardResponseModel (val List : ArrayList<GetRedeemRewardResponseDataModel>)

data class GetRedeemRewardResponseDataModel(
    val Points: String,
    val Description: String,
    val Date: String,
)