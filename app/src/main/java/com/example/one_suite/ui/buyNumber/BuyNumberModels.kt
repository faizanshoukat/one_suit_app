package com.example.one_suite.ui.buyNumber

import android.os.Parcel
import android.os.Parcelable

//States Model
data class GetStatesModel(val States: ArrayList<GetStatesDataModel>)

data class GetStatesDataModel(
    val State: String,
    val StateName: String
)
//Cities Model
data class GetCitiesRequestModel(var State:String)
data class GetCitiesResponseModel(var Cities:ArrayList<String>)


//full parameter constructor
data class SearchTelephoneNumberRequestModel(
    var Account: String,
    var tnMask: String,
    var province: String,
    var city: String,
    var quantity: String,
    var pageSort: String,
)

//No Parameter pass
data class SearchTelephoneNumberEmptyRequestModel(
    var Account: String,
    var tnMask: String,
    var city: String,
    var quantity: String,
    var pageSort: String,
)


data class SearchTelephoneNumberResponseModel(
    var statusCode: String?,
    var status: String?,
    var tnResult: ArrayList<SearchTelephoneNumberDataModel>?,
    var page: String?,
    var totalPages: String?,
    var totalItems: String?,
    var searchId: String?,
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readString(),
        TODO("tnResult"),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString()
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(statusCode)
        parcel.writeString(status)
        parcel.writeString(page)
        parcel.writeString(totalPages)
        parcel.writeString(totalItems)
        parcel.writeString(searchId)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<SearchTelephoneNumberResponseModel> {
        override fun createFromParcel(parcel: Parcel): SearchTelephoneNumberResponseModel {
            return SearchTelephoneNumberResponseModel(parcel)
        }

        override fun newArray(size: Int): Array<SearchTelephoneNumberResponseModel?> {
            return arrayOfNulls(size)
        }
    }
}


data class SearchTelephoneNumberDataModel(
    var telephoneNumber: String?,
    var rateCenter: String?,
    var rateCenterName: String?,
    var lata: String?,
    var province: String?,
    var city: String?,
    var rateCenterTier: String?,
    var cnamAllowed: String?,
    var dlAllowed: String?,
    var e911Allowed: String?,
    var msgAllowed: String?,
    var countryCode: String?,
    var portOutPinSupported: String?,
    var portOutNotificationSupported: String?,
    var smsOverrideAllowed: String?,
    var mmsAllowed: String?,
    var mmsOverrideAllowed: String?,
    var Vitelity: Boolean?,
    var buttonText:String?="Add To Cart"
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readValue(Boolean::class.java.classLoader) as? Boolean
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(telephoneNumber)
        parcel.writeString(rateCenter)
        parcel.writeString(rateCenterName)
        parcel.writeString(lata)
        parcel.writeString(province)
        parcel.writeString(city)
        parcel.writeString(rateCenterTier)
        parcel.writeString(cnamAllowed)
        parcel.writeString(dlAllowed)
        parcel.writeString(e911Allowed)
        parcel.writeString(msgAllowed)
        parcel.writeString(countryCode)
        parcel.writeString(portOutPinSupported)
        parcel.writeString(portOutNotificationSupported)
        parcel.writeString(smsOverrideAllowed)
        parcel.writeString(mmsAllowed)
        parcel.writeString(mmsOverrideAllowed)
        parcel.writeValue(Vitelity)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<SearchTelephoneNumberDataModel> {
        override fun createFromParcel(parcel: Parcel): SearchTelephoneNumberDataModel {
            return SearchTelephoneNumberDataModel(parcel)
        }

        override fun newArray(size: Int): Array<SearchTelephoneNumberDataModel?> {
            return arrayOfNulls(size)
        }
    }
}


//for get local rates API
data class GetLocalRatesRequestModel(
    var Provider: String,
    var Type: String
)

data class GetLocalRatesResponseModel(
    var Rates: ArrayList<GetLocalRatesDataModel>,
)

data class GetLocalRatesDataModel(
    var Rate: String? = "",
    var Type: String? = "",
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readString()
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(Rate)
        parcel.writeString(Type)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<GetLocalRatesDataModel> {
        override fun createFromParcel(parcel: Parcel): GetLocalRatesDataModel {
            return GetLocalRatesDataModel(parcel)
        }

        override fun newArray(size: Int): Array<GetLocalRatesDataModel?> {
            return arrayOfNulls(size)
        }
    }
}