package com.example.one_suite.ui.buyNumber.buyNumberList

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewTreeLifecycleOwner
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.example.one_suite.R
import com.example.one_suite.databinding.BuyNumberFragmentBinding
import com.example.one_suite.databinding.BuyNumberListFragmentBinding
import com.example.one_suite.ui.buyNumber.BuyNumberViewModel
import com.example.one_suite.ui.buyNumber.GetLocalRatesDataModel
import com.example.one_suite.ui.buyNumber.SearchTelephoneNumberDataModel

class BuyNumberListFragment : Fragment() {

    companion object {
        fun newInstance() = BuyNumberListFragment()
    }


    private lateinit var buyNumberListViewModel: BuyNumberListViewModel
    private var _binding: BuyNumberListFragmentBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    private var searchList = ArrayList<SearchTelephoneNumberDataModel>();
    private var priceList = ArrayList<GetLocalRatesDataModel>();

    var addToCartList = ArrayList<SearchTelephoneNumberDataModel>();
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        buyNumberListViewModel = ViewModelProvider(this).get(BuyNumberListViewModel::class.java)

        _binding = BuyNumberListFragmentBinding.inflate(inflater, container, false).apply {
            this.lifecycleOwner=this@BuyNumberListFragment
            this.viewmodel=buyNumberListViewModel
        }

        val root: View = binding.root



        _binding!!.executePendingBindings()
        _binding!!.recyclerView.apply {
            Log.d("SearchTelephoneApiCall", "List size in fragment")
            layoutManager = LinearLayoutManager(activity?.applicationContext)
            val decoration = DividerItemDecoration(activity?.applicationContext, StaggeredGridLayoutManager.VERTICAL)
            addItemDecoration(decoration)
        }



//        searchList = arguments?.getString("inputText")
        searchList = arguments?.getParcelableArrayList<SearchTelephoneNumberDataModel>("DataList")!!
        priceList = arguments?.getParcelableArrayList<GetLocalRatesDataModel>("RateList")!!

        Log.d("SearchTelephoneApiCall", "List size in fragment :${searchList.size}")
        Log.d("SearchTelephoneApiCall", "Rate List size in fragment :${priceList.size}")

        buyNumberListViewModel.recyclerListData.value=searchList
        buyNumberListViewModel.setAdapterData(searchList)
        buyNumberListViewModel.setLocalRates(priceList)


        var qty="1"
        //when user click on add to cart button
//        buyNumberListViewModel.getNumberClickDataObserver().observe(viewLifecycleOwner, Observer {
//
//            Log.d("AddToCartIssue","Observer")
//            if (it!=null){
//                Log.d("AddToCartIssue","Data not null")
//
//                if(qty==(addToCartList.size).toString()){
//                    Log.d("AddToCartIssue","Change Quantity")
//                    Toast.makeText(context, "Opps Please Change Your Quantity", Toast.LENGTH_SHORT).show();
//                }else{
//                    var isAlreadyInList: Boolean = false
//                    for (item in addToCartList){
//                        if (it.telephoneNumber==item.telephoneNumber){
//                            isAlreadyInList=true
//                        }
//                    }
//
//                    if (!isAlreadyInList){
//                        Log.d("AddToCartIssue","Added to Cart")
//                        addToCartList.add(it)
//                        Toast.makeText(context, "Item added to the cart", Toast.LENGTH_SHORT).show();
//                    }
//
//                }
//            }
//        })


        return root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        buyNumberListViewModel = ViewModelProvider(this).get(BuyNumberListViewModel::class.java)

    }

}