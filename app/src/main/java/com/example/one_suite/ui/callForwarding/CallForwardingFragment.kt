package com.example.one_suite.ui.callForwarding

import android.app.AlertDialog
import android.app.Dialog
import android.app.ProgressDialog
import android.content.Intent
import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.example.one_suite.MainActivity
import com.example.one_suite.R
import com.example.one_suite.databinding.BuyNumberListFragmentBinding
import com.example.one_suite.databinding.CallForwardingFragmentBinding
import com.example.one_suite.model.callForwarding.setSubscriberDID.SetSubscriberDIDRequestModel
import com.example.one_suite.ui.buyNumber.checkOut.AutoRenewNumberRequestModel
import com.example.one_suite.ui.callForwarding.setForwarding.SetForwardingDialog
import com.example.one_suite.ui.callForwarding.setForwarding.SetForwardingDialog1
import com.example.one_suite.utils.Constant
import com.resocoder.databinding.utils.DialogUtils

class CallForwardingFragment : Fragment() {


    companion object {
        fun newInstance() = CallForwardingFragment()
    }

    private lateinit var callForwardingViewModel: CallForwardingViewModel
    private var _binding: CallForwardingFragmentBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!


    //    var dialog = DialogUtils
    //var dialog = AlertDialog.Builder(activity?.applicationContext)
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        callForwardingViewModel = ViewModelProvider(this).get(CallForwardingViewModel::class.java)

        _binding = CallForwardingFragmentBinding.inflate(inflater, container, false).apply {
            this.lifecycleOwner = this@CallForwardingFragment
            this.viewModel = callForwardingViewModel
        }

        val root: View = binding.root


        //progress dialog
        //var dialog = activity?.let { DialogUtils.setProgressDialog(it.applicationContext, "Please wait...") }
        var dialog=DialogUtils.setProgressDialog(container!!.context,"Please Wait....")


//        val dialog = ProgressDialog(getContext())
//        dialog.setMessage("Please wait...")
//
//        dialog.show()
//        dialog.dismiss()



        _binding!!.executePendingBindings()
        _binding!!.recyclerView.apply {
            Log.d("SearchTelephoneApiCall", "List size in fragment")
            layoutManager = LinearLayoutManager(activity?.applicationContext)
        }


        //calling API
        dialog!!.show()
        callForwardingViewModel.makeSubscriberDIDApiCall()


        //Data Observer
        callForwardingViewModel.getRecyclerListDataObserver().observe(viewLifecycleOwner, Observer {
            Log.d("SubscriberApiCall", "get recycler data")

            dialog!!.dismiss()
            if (it != null) {
                //update the adapter
                callForwardingViewModel.setAdapterData(it)

            } else {
                //Toast.makeText(this, "Error in fetching data", Toast.LENGTH_LONG).show()
            }
        })


        callForwardingViewModel.getSwitchIsCheckObserver().observe(viewLifecycleOwner, Observer {
            Log.d("SubscriberApiCall", "country data :$it")
            if (it.trim().isNotEmpty() ){

                if (activity!=null) {
                    SetForwardingDialog1.newInstance(
                        it,
                        "Call Forwarding"
                    ).show(requireActivity().supportFragmentManager, SetForwardingDialog1.TAG)
                }

            }


        })


        callForwardingViewModel.getSwitchNotCheckObserver().observe(viewLifecycleOwner, Observer {
            Log.d("SubscriberApiCall", "country data :$it")

            if (it.trim().isNotEmpty()) {

                var accountNumber= Constant.account
                var didNumber=it
                var IS_AUTO_RENEW=false
                var IS_FORWARDING_ENABLE=false
                var forwardPhoneNumber:String=""
                var NumberType=""
                var Location=""


                dialog!!.show()
                var requestModel= SetSubscriberDIDRequestModel(accountNumber,didNumber,IS_AUTO_RENEW,IS_FORWARDING_ENABLE,forwardPhoneNumber,NumberType,Location)
                callForwardingViewModel.makeSetDIDSettingApiCall(requestModel)
            }


        })

        callForwardingViewModel.getCallForwardingResultObserver().observe(viewLifecycleOwner, Observer {
            dialog!!.dismiss()

//            var intent = Intent(this, Dashboard::class.java)
//            startActivity(intent)

//            val intent = Intent (getActivity(), MainActivity::class.java)
//            getActivity()?.startActivity(intent)

            val intent = Intent(getActivity(), MainActivity::class.java)
            intent.putExtra("Fragment","Call Forwarding")
            getActivity()?.startActivity(intent)

        })



        return root;
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        callForwardingViewModel = ViewModelProvider(this).get(CallForwardingViewModel::class.java)
        // TODO: Use the ViewModel
    }

}