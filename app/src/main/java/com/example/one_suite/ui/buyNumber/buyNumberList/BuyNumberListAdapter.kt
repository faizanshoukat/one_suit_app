package com.example.one_suite.ui.buyNumber.buyNumberList

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.one_suite.databinding.BuyNumbersRecyclerItemsBinding
import com.example.one_suite.ui.buyNumber.GetLocalRatesDataModel
import com.example.one_suite.ui.buyNumber.SearchTelephoneNumberDataModel
import com.example.one_suite.utils.ClickListener

class BuyNumberListAdapter(private val listener: ClickListener) :
    RecyclerView.Adapter<BuyNumberListAdapter.MyViewHolder>() {
    var items = ArrayList<SearchTelephoneNumberDataModel>()
    var localRatesList = ArrayList<GetLocalRatesDataModel>()
    //var localRate=GetLocalRatesDataModel()

    fun setDataList(data: ArrayList<SearchTelephoneNumberDataModel>) {
        this.items = data
    }

    fun setLocalRateList(data: ArrayList<GetLocalRatesDataModel>) {
        this.localRatesList = data
    }


    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): BuyNumberListAdapter.MyViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = BuyNumbersRecyclerItemsBinding.inflate(layoutInflater)

        Log.d("SearchTelephoneApiCall", "onCreateViewHolder Size  :${items.size}")

        return MyViewHolder(binding)
    }

    override fun getItemCount() = items.size

//    override fun getItemCount(): Int {
//        Log.d("SearchTelephoneApiCall", "getItemCOunt Size  :${items.size}")
//        return items.size
//    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.bind(items[position],localRatesList)

        var number=items[position].telephoneNumber
        var city=items[position].city
        var state=items[position].province
        var rate=localRatesList[0].Rate
        var buttonText=items[position].buttonText

        holder.binding.number.text="+1$number"
        holder.binding.city.text="City : $city"
        holder.binding.province.text="State : $state"
        holder.binding.price.text="$ $rate"
        holder.binding.addToCartButton.text=buttonText




        holder.binding.addToCartButton.setOnClickListener(View.OnClickListener {
            Log.d("AddToCartIssue", "Telephone  :${items[position].telephoneNumber}")

            if (buttonText=="Added to Cart"){
                Log.d("AddToCartIssue", "Number is already added")
                holder.binding.addToCartButton.isClickable=false
            }else{
                listener.onViewClick(holder.binding.addToCartButton, position)
            }




            //added to cart
//            holder.binding.addToCartButton.text = "Added to cart"
//            holder.binding.addToCartButton.isClickable=false

        })
    }

    class MyViewHolder(val binding: BuyNumbersRecyclerItemsBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(data: SearchTelephoneNumberDataModel,localRateData :ArrayList<GetLocalRatesDataModel>) {
            binding.viewModel = data
            if (localRateData!=null ){
                binding.viewModelLocalRates=localRateData[0]
            }
            binding.executePendingBindings()
        }
    }

}