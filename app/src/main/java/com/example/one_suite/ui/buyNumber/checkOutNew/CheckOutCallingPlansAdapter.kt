package com.example.one_suite.ui.buyNumber.checkOutNew

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.one_suite.databinding.CheckoutCartRecyclerviewItemsBinding

import com.example.one_suite.model.subscriberDid.SubscriberDIDResponseDataModel
import com.example.one_suite.ui.callingPlan.callingPlans.GetCallingPlansDataModel
import com.example.one_suite.utils.ClickListener
import com.example.one_suite.utils.SwitchClickListener

class CheckOutCallingPlansAdapter(private val listener: ClickListener) :
    RecyclerView.Adapter<CheckOutCallingPlansAdapter.MyViewHolder>() {
    var items = ArrayList<GetCallingPlansDataModel>()

    fun setDataList(data: ArrayList<GetCallingPlansDataModel>) {
        this.items = data
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): CheckOutCallingPlansAdapter.MyViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = CheckoutCartRecyclerviewItemsBinding.inflate(layoutInflater)
        return MyViewHolder(binding)
    }

    override fun getItemCount() = items.size

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.bind(items[position])


        holder.binding.deleteBundleBtn.setOnClickListener(View.OnClickListener {
            listener.onViewClick(holder.binding.deleteBundleBtn, position)
        })


//        if (position % 2 == 0) {
//            println("$position is even")
//            holder.binding.llCpBg.setBackgroundResource(R.drawable.ic_blue_bg)
//        } else {
//            holder.binding.llCpBg.setBackgroundResource(R.drawable.ic_greenbg)
//            println("$position is odd")
//        }

    }

    class MyViewHolder(val binding: CheckoutCartRecyclerviewItemsBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(data: GetCallingPlansDataModel) {
            binding.viewModel = data
            binding.executePendingBindings()
        }
    }


}