package com.example.one_suite.ui.callingPlan.azamCode

import android.os.Parcel
import android.os.Parcelable

class model_callingPlan_rv(
    val mints: String?,
    val country: String?,
    val des: String?,
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readString(),
        parcel.readString()
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(mints)
        parcel.writeString(country)
        parcel.writeString(des)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<model_callingPlan_rv> {
        override fun createFromParcel(parcel: Parcel): model_callingPlan_rv {
            return model_callingPlan_rv(parcel)
        }

        override fun newArray(size: Int): Array<model_callingPlan_rv?> {
            return arrayOfNulls(size)
        }
    }
}