package com.example.one_suite.ui.buyNumber.buyNumberList

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.example.one_suite.R
import com.example.one_suite.databinding.ActivityBuyNumberListBinding
import com.example.one_suite.ui.buyNumber.GetLocalRatesDataModel
import com.example.one_suite.ui.buyNumber.SearchTelephoneNumberDataModel
import com.example.one_suite.ui.buyNumber.checkOut.BuyNumbersCheckoutModel
import android.content.Intent
import android.view.View
import android.widget.Toast
import androidx.lifecycle.Observer
import com.example.one_suite.ui.buyNumber.checkOut.BuyNumberCheckoutActivity
import com.example.one_suite.utils.Constant


class BuyNumberListActivity : AppCompatActivity() {

    private lateinit var buyNumberListViewModel: BuyNumberListViewModel

    private var searchList = ArrayList<SearchTelephoneNumberDataModel>();
    private var priceList = ArrayList<GetLocalRatesDataModel>();

    var addToCartList = ArrayList<SearchTelephoneNumberDataModel>();

    var qty = ""
    var state = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        setContentView(R.layout.activity_buy_number_list)

        buyNumberListViewModel = ViewModelProvider(this).get(BuyNumberListViewModel::class.java)


        var activityBinding = DataBindingUtil.setContentView<ActivityBuyNumberListBinding>(
            this, R.layout.activity_buy_number_list
        ).apply {
            this.setLifecycleOwner(this@BuyNumberListActivity)
            this.viewmodel = buyNumberListViewModel
        }



        activityBinding.executePendingBindings()
        activityBinding.recyclerView.apply {

            layoutManager = LinearLayoutManager(context)
            val decoration = DividerItemDecoration(context, StaggeredGridLayoutManager.VERTICAL)
            addItemDecoration(decoration)
        }


        val i = intent
//        val name = i.getStringExtra("NAME_KEY")
//        val year = i.getIntExtra("YEAR_KEY", 0)

        searchList = i.getParcelableArrayListExtra<SearchTelephoneNumberDataModel>("DataList")!!
        priceList = i.getParcelableArrayListExtra<GetLocalRatesDataModel>("RateList")!!
        qty = i.getStringExtra("Qty").toString()
        state = i.getStringExtra("State").toString()
        Log.d("DataIssue", "list :${searchList.size}")
//        searchList = i.getParcelableArrayList<SearchTelephoneNumberDataModel>("DataList")!!
//        priceList = i.getParcelableArrayList<GetLocalRatesDataModel>("RateList")!!

//        val bundle = intent.getBundleExtra("Bundle");
//        if (bundle != null) {
//            Log.d("DataIssue", "Bundle not null")
//            searchList = bundle.getParcelableArrayList<SearchTelephoneNumberDataModel>("DataList")!!
//            priceList = bundle.getParcelableArrayList<GetLocalRatesDataModel>("RateList")!!
//            Log.d("DataIssue", "list :${searchList.size}")
//        }


        if (Constant.checkoutListStatic.size == 0) {
            buyNumberListViewModel.recyclerListData.value = searchList
            buyNumberListViewModel.setAdapterData(searchList)
            buyNumberListViewModel.setLocalRates(priceList)
        } else {

            for (item in Constant.checkoutListStatic) {
                var number = item.number

                for ((index, searchItem) in searchList.withIndex()) {
                    if (searchItem.telephoneNumber == number) {
                        searchList[index].buttonText = "Added To Cart"

                    }
                }

            }



            buyNumberListViewModel.recyclerListData.value = searchList
            buyNumberListViewModel.setAdapterData(searchList)
            buyNumberListViewModel.setLocalRates(priceList)


        }


////        var qty="1"
//        //when user click on add to cart button
//        buyNumberListViewModel.getNumberClickDataObserver().observe(this, Observer {
//
//            Log.d("AddToCartIssue", "Observer")
//            if (it != null) {
//                Log.d("AddToCartIssue", "Data not null")
//
//                if (qty == "") {
//                    qty = "1"
//                }
//
//
//                /*................in case of quantity match but state change.........*/
//                if (qty == (Constant.checkoutListStatic.size).toString()) {
//                    Log.d("AddToCartIssue", "Change Quantity")
//
//
//                    var stateAlready = false
//                    for (item in addToCartList) {
//                        if (state == item.province) {
//                            stateAlready = true
//                        }
//                    }
//
//                    if (!stateAlready) {
//
//
//                        var isAlreadyInList: Boolean = false
//                        for (item in addToCartList) {
//                            if (it.telephoneNumber == item.telephoneNumber) {
//                                isAlreadyInList = true
//                            }
//                        }
//
//                        if (!isAlreadyInList) {
//                            Log.d("AddToCartIssue", "Added to Cart")
//
//                            addToCartList.add(it)
//                            Toast.makeText(this, "Item added to the cart", Toast.LENGTH_SHORT)
//                                .show();
//                        }
//
//
//                    } else {
//                        Toast.makeText(this, "Opps Please Change Your Quantity", Toast.LENGTH_SHORT)
//                            .show();
//                    }
//
//
//                } else {
//
//                    /*................in case of quantity .........*/
//
//                    var isAlreadyInList: Boolean = false
//                    for (item in addToCartList) {
//                        if (it.telephoneNumber == item.telephoneNumber) {
//                            isAlreadyInList = true
//                        }
//                    }
//
//                    if (!isAlreadyInList) {
//                        Log.d("AddToCartIssue", "Added to Cart")
//
//                        addToCartList.add(it)
//                        Toast.makeText(this, "Item added to the cart", Toast.LENGTH_SHORT).show();
//                    }
//
//                }
//            }
//        })


        buyNumberListViewModel.getNumberPositionObserver().observe(this, Observer {


            var numberData = searchList[it]


            if (numberData != null) {


                if (qty == "") {
                    qty = "1"
                }


                Log.d("AddToCartIssue", "qty :$qty")
                Log.d("AddToCartIssue", "static list :${Constant.checkoutListStatic.size}")



                /*................in case of quantity match but state change.........*/
                if (qty == (Constant.checkoutListStatic.size).toString()) {
                    Log.d("AddToCartIssue", "qty = list")




                    var stateAlready = false

                    /*...........check in both arrays static and local........*/
                    for (item in Constant.checkoutListStatic) {
                        Log.d("AddToCartIssue", "state = $state")
                        Log.d("AddToCartIssue", "Province = ${item.state}")
                        if (state == item.state) {
                            stateAlready = true
                        }
                    }


                    for (item in addToCartList) {
                        Log.d("AddToCartIssue", "state = $state")
                        Log.d("AddToCartIssue", "Province = ${item.province}")
                        if (state == item.province) {
                            stateAlready = true
                        }
                    }


                    if (!stateAlready) {

                        Log.d("AddToCartIssue", "state = false")

                        var isAlreadyInList: Boolean = false
                        for (item in addToCartList) {
                            if (numberData.telephoneNumber == item.telephoneNumber) {
                                isAlreadyInList = true
                            }
                        }

                        /*.........to check number already in static list..............*/
                        for (item in Constant.checkoutListStatic) {
                            if (numberData.telephoneNumber == item.number) {
                                isAlreadyInList = true
                            }
                        }



                        if (!isAlreadyInList) {
                            Log.d("AddToCartIssue", "Added to Cart")


                            searchList[it].buttonText = "Added To Cart"

                            buyNumberListViewModel.setAdapterData(searchList)
                            buyNumberListViewModel.setLocalRates(priceList)

                            addToCartList.add(numberData)
                            Toast.makeText(this, "Item added to the cart", Toast.LENGTH_SHORT)
                                .show();
                        }


                    } else {
                        Toast.makeText(this, "Opps Please Change Your Quantity", Toast.LENGTH_SHORT)
                            .show();
                    }


                } else {
                    Log.d("AddToCartIssue", "qty !=list")

                    /*................in case of quantity .........*/

                    var isAlreadyInList: Boolean = false
                    for (item in addToCartList) {
                        if (numberData.telephoneNumber == item.telephoneNumber) {
                            isAlreadyInList = true
                        }
                    }

                    /*.........to check number already in static list..............*/
                    for (item in Constant.checkoutListStatic) {
                        if (numberData.telephoneNumber == item.number) {
                            isAlreadyInList = true
                        }
                    }



                    if (!isAlreadyInList) {
                        Log.d("AddToCartIssue", "Added to Cart")

                        searchList[it].buttonText = "Added To Cart"

                        buyNumberListViewModel.setAdapterData(searchList)
                        buyNumberListViewModel.setLocalRates(priceList)


                        addToCartList.add(numberData)
                        Toast.makeText(this, "Item added to the cart", Toast.LENGTH_SHORT).show();
                    }

                }
            }


        })


        //check out
        activityBinding.checkOutButton.setOnClickListener(View.OnClickListener {



            var price: String? = ""
            var type: String? = ""

            if (priceList.size > 0) {
                price = priceList[0].Rate
                type = priceList[0].Type
            }


            var checkoutList = ArrayList<BuyNumbersCheckoutModel>()

            for (item in addToCartList) {
                if (item != null) {


                    checkoutList.add(
                        BuyNumbersCheckoutModel(
                            "USA",
                            item.province!!,
                            item.telephoneNumber!!,
                            price!!,
                            type!!,
                            false
                        )
                    )
                }

            }

            Log.d("LocalRateApiCall", "Add to cart list size :${addToCartList.size}")


            if (Constant.checkoutListStatic.size == 0) {
                Constant.checkoutListStatic = checkoutList
            } else {
                Constant.checkoutListStatic.addAll(checkoutList)
            }



            var intent = Intent(this, BuyNumberCheckoutActivity::class.java)

            val bundle = Bundle()

            intent.putExtra("Bundle", bundle)
//            bundle.putParcelableArrayList("CartList", addToCartList)
//            bundle.putParcelableArrayList("PriceList", addToCartPriceList)
            bundle.putParcelableArrayList("CheckOutList", Constant.checkoutListStatic)
//            bundle.putParcelable("CartItem",addToCartList[0])
            startActivity(intent)
        })

    }


    override fun onResume() {
        super.onResume()
        App.activityResumed()

        addToCartList.clear()

        Log.d("OnResumeIssue", "List Activity Resumed")
    }

    override fun onPause() {
        super.onPause()
        App.activityPaused()
    }
}