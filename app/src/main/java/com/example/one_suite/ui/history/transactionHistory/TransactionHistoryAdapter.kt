package com.example.one_suite.ui.history.transactionHistory

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.one_suite.databinding.*
import com.example.one_suite.model.subscriberDid.SubscriberDIDResponseDataModel
import com.example.one_suite.model.transactionHistory.GetTransactionHistoryResponseDataModel
import com.example.one_suite.ui.home.GetSubscriberPromotionResponseDataModel
import com.example.one_suite.ui.home.GetSubscriberPromotionResponseModel
import com.example.one_suite.utils.SwitchClickListener

class TransactionHistoryAdapter() :
    RecyclerView.Adapter<TransactionHistoryAdapter.MyViewHolder>() {
    var items = ArrayList<GetTransactionHistoryResponseDataModel>()

    fun setDataList(data: ArrayList<GetTransactionHistoryResponseDataModel>) {
        this.items = data
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): TransactionHistoryAdapter.MyViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = TransactionHistoryRecyclerviewItemsBinding.inflate(layoutInflater)
        return MyViewHolder(binding)
    }

    override fun getItemCount() = items.size

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.bind(items[position])

    }

    class MyViewHolder(val binding: TransactionHistoryRecyclerviewItemsBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(data: GetTransactionHistoryResponseDataModel) {
            binding.viewModel = data
            binding.executePendingBindings()
        }
    }


}