package com.example.one_suite.ui.credit.addBalance

import android.util.Log
import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.one_suite.model.credits.GetSystemSettingsResponseModel
import com.example.one_suite.network.APIService
import com.example.one_suite.network.RetroInstance
import com.example.one_suite.utils.Constant
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class AddBalanceViewModel : ViewModel() {
    // TODO: Implement the ViewModel

    var enterAmount = ObservableField<String>("")
    var enterAmountVisibility = MutableLiveData<Boolean>(false)
    var enterAmountMandatoryVisibility = MutableLiveData<Boolean>(false)






    var systemSettingResult = MutableLiveData<Boolean>()
    var systemSettingData: MutableLiveData<GetSystemSettingsResponseModel> = MutableLiveData()

    fun setEnterAmountVisibility(value:Boolean) {
        Log.d("ViewVisibility","Enter Amount visibility :$value")
        enterAmountVisibility.postValue(value)
    }


    fun setEnterAmountMandatoryVisibility(value:Boolean) {
        Log.d("ViewVisibility","Enter Amount mandatory visibility :$value")
        enterAmountMandatoryVisibility.postValue(value)
    }


    fun getSystemSettingsResult(): MutableLiveData<Boolean> {
        return systemSettingResult
    }

    fun getSystemSettingsData(): MutableLiveData<GetSystemSettingsResponseModel> {
        return systemSettingData
    }


    fun makeSystemSettingApiCall() {

        val apiService = RetroInstance.getRetrofitInstance().create(APIService::class.java)
        val call = apiService.getSystemSettings(Constant.credentials)

        call.enqueue(object : Callback<GetSystemSettingsResponseModel> {
            override fun onResponse(call: Call<GetSystemSettingsResponseModel>, response: Response<GetSystemSettingsResponseModel>) {

                Log.d("SubscriberApiCall", "response")
                if (response.isSuccessful) {

                    systemSettingResult.postValue(true)

                    if (response.body() != null) {

                        systemSettingData.postValue(response.body())
                    }


                } else {
                    systemSettingResult.postValue(false)
                }
            }

            override fun onFailure(call: Call<GetSystemSettingsResponseModel>, t: Throwable) {
                Log.d("SubscriberApiCall", "Failure : ${t.message}")
                systemSettingResult.postValue(false)

            }

        })

    }
//
//    fun makeSetCallBackApiCall(requestModel: SetCallBackRequestModel) {
//        Log.d("SubscriberApiCall", "country list api call")
//        val apiService = RetroInstance.getRetrofitInstance().create(APIService::class.java)
//        val call = apiService.setCallBack(Constant.credentials, requestModel)
//
//        call.enqueue(object : Callback<SetCallBackResponseModel> {
//            override fun onResponse(call: Call<SetCallBackResponseModel>, response: Response<SetCallBackResponseModel>) {
//
//                Log.d("SubscriberApiCall", "response")
//                if (response.isSuccessful) {
//
//                    setCallBackresult.postValue(true)
//
//                    if (response.body() != null) {
//
////                        recyclerListData.postValue(response.body()!!.SubscriberDIDs)
////                        Log.d("SubscriberApiCall", "Size :" + response.body()!!.SubscriberDIDs.size)
////                        Log.d("SubscriberApiCall", "Number :" + response.body()!!.SubscriberDIDs.get(0).Number)
//////                        var responseModel:SubscriberProfileResponseModel= response.body()!!
//                    }
//
//
//                } else {
//                    setCallBackresult.postValue(false)
//                }
//            }
//
//            override fun onFailure(call: Call<SetCallBackResponseModel>, t: Throwable) {
//                Log.d("SubscriberApiCall", "Failure : ${t.message}")
//                setCallBackresult.postValue(false)
//
//            }
//
//        })
//
//    }


}