package com.example.one_suite.ui

import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.example.one_suite.Activity_Select
import com.example.one_suite.MainActivity
import com.example.one_suite.R
import com.example.one_suite.databinding.ActivityLoginBinding
import com.example.one_suite.ui.login.LoginRequestModel
import com.example.one_suite.ui.login.LoginViewModel
import com.example.one_suite.utils.Constant

class OfflineActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_offline)

        Constant.isOnline = false

    }


    override fun onResume() {
        super.onResume()
        App.activityResumed()
    }

    override fun onPause() {
        super.onPause()
        App.activityPaused()
    }
}