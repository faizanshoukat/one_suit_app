package com.example.one_suite.ui.credit.autoRechargeFragment

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import com.example.one_suite.MainActivity
import com.example.one_suite.R
import com.example.one_suite.databinding.AddBalanceFragmentBinding
import com.example.one_suite.databinding.AutoRechargeFragmentBinding
import com.example.one_suite.model.credits.GetSystemSettingsResponseModel
import com.example.one_suite.ui.credit.addBalance.AddBalanceViewModel
import com.example.one_suite.utils.Constant
import com.resocoder.databinding.utils.DialogUtils

class AutoRechargeFragment : Fragment() {

    companion object {
        fun newInstance() = AutoRechargeFragment()
    }

    private lateinit var autoRechargeViewModel: AutoRechargeViewModel
    private var _binding: AutoRechargeFragmentBinding? = null
    private val binding get() = _binding!!

    var amountDouble: Double? = 0.0
    var isAmountLabelVisible: Boolean = false

    lateinit var systemSettingsData: GetSystemSettingsResponseModel
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        autoRechargeViewModel = ViewModelProvider(this).get(AutoRechargeViewModel::class.java)

        _binding = AutoRechargeFragmentBinding.inflate(inflater, container, false).apply {
            this.lifecycleOwner = this@AutoRechargeFragment
            this.viewModel = autoRechargeViewModel
        }
        val root: View = binding.root


        //progress dialog
        var dialog = DialogUtils.setProgressDialog(container!!.context, "Please Wait....")
        dialog!!.show()
        autoRechargeViewModel.makeAutoRechargeApiCall()




        _binding!!.autoRechargeSwitch.setOnClickListener(View.OnClickListener {
            var isChecked: Boolean = false

            isChecked = _binding!!.autoRechargeSwitch.isChecked
            autoRechargeViewModel.setIsAutoRecharge(isChecked)
        })






        _binding!!.editThreshold.setOnClickListener(View.OnClickListener {
            autoRechargeViewModel.setThresholdVisibility(true)
        })

        _binding!!.cancelButton.setOnClickListener(View.OnClickListener {
            autoRechargeViewModel.setThresholdVisibility(false)
        })

        //to call threshold API call
        _binding!!.saveButton.setOnClickListener(View.OnClickListener {
            var thresholdAmount=_binding!!.threshold.text.toString()

            dialog.show()
            var requestModel=UpdateThresholdRequestModel(Constant.account,thresholdAmount.toDouble())
            autoRechargeViewModel.makeUpdateThresholdApiCall(requestModel)
        })



        //to show and hide the enter amount edit text
        _binding!!.fiveDollar.setOnClickListener(View.OnClickListener {

            autoRechargeViewModel.setIsAmountFive(true)
            autoRechargeViewModel.setIsAmountTen(false)
            autoRechargeViewModel.setIsAmountTwenty(false)
            autoRechargeViewModel.setIsAmountThirty(false)
            autoRechargeViewModel.setIsAmountOther(false)


            autoRechargeViewModel.setEnterAmountVisibility(false)
            autoRechargeViewModel.setEnterAmountMandatoryVisibility(false)
            isAmountLabelVisible = false
            amountDouble = 5.0
        })

        _binding!!.tenDollar.setOnClickListener(View.OnClickListener {


            autoRechargeViewModel.setIsAmountFive(false)
            autoRechargeViewModel.setIsAmountTen(true)
            autoRechargeViewModel.setIsAmountTwenty(false)
            autoRechargeViewModel.setIsAmountThirty(false)
            autoRechargeViewModel.setIsAmountOther(false)


            autoRechargeViewModel.setEnterAmountVisibility(false)
            autoRechargeViewModel.setEnterAmountMandatoryVisibility(false)
            isAmountLabelVisible = false
            amountDouble = 10.0
        })
        _binding!!.twentyDollar.setOnClickListener(View.OnClickListener {
            autoRechargeViewModel.setIsAmountFive(false)
            autoRechargeViewModel.setIsAmountTen(false)
            autoRechargeViewModel.setIsAmountTwenty(true)
            autoRechargeViewModel.setIsAmountThirty(false)
            autoRechargeViewModel.setIsAmountOther(false)


            autoRechargeViewModel.setEnterAmountVisibility(false)
            autoRechargeViewModel.setEnterAmountMandatoryVisibility(false)
            isAmountLabelVisible = false
            amountDouble = 20.0
        })
        _binding!!.thirtyDollar.setOnClickListener(View.OnClickListener {

            autoRechargeViewModel.setIsAmountFive(false)
            autoRechargeViewModel.setIsAmountTen(false)
            autoRechargeViewModel.setIsAmountTwenty(false)
            autoRechargeViewModel.setIsAmountThirty(true)
            autoRechargeViewModel.setIsAmountOther(false)


            autoRechargeViewModel.setEnterAmountVisibility(false)
            autoRechargeViewModel.setEnterAmountMandatoryVisibility(false)
            isAmountLabelVisible = false
            amountDouble = 30.0
        })


        _binding!!.other.setOnClickListener(View.OnClickListener {

            autoRechargeViewModel.setIsAmountFive(false)
            autoRechargeViewModel.setIsAmountTen(false)
            autoRechargeViewModel.setIsAmountTwenty(false)
            autoRechargeViewModel.setIsAmountThirty(false)
            autoRechargeViewModel.setIsAmountOther(true)


            autoRechargeViewModel.setEnterAmountVisibility(true)
            autoRechargeViewModel.setEnterAmountMandatoryVisibility(false)
            isAmountLabelVisible = true
        })


        //to call the API
        _binding!!.updateButton.setOnClickListener(View.OnClickListener {


            if (isAmountLabelVisible) {
                Log.d("AutoRechargeIssue", "amount label visible")

                if (systemSettingsData != null) {
                    val amount = _binding!!.enterAmount.text.toString()
                    //val amount = ""

                    if (amount.isEmpty()) {
                        autoRechargeViewModel.setEnterAmountMandatoryVisibility(true)
                    } else {
                        amountDouble = amount.toDouble()
                        if (amountDouble != null) {
                            if (amountDouble!! >= systemSettingsData.MinimumTopup && amountDouble!! <= systemSettingsData.MaximumTopup) {
                                autoRechargeViewModel.setEnterAmountMandatoryVisibility(false)

//                                var intent = Intent(this, CreditPayInvoice::class.java)
//                                intent.putExtra("Amount", amountDouble!!)
//                                startActivity(intent)


                                dialog.show()
                                var requestModel =
                                    AddAutoRechargeRequestModel(Constant.account, amount.toDouble(), true)

                                autoRechargeViewModel.makeAddAutoRechargeApiCall(requestModel)



                            } else {
                                Log.d("AutoRechargeIssue", "Amount empty")
                                autoRechargeViewModel.setEnterAmountMandatoryVisibility(true)
                            }
                        }else{
                            dialog.show()
                            var requestModel =
                                AddAutoRechargeRequestModel(Constant.account, amountDouble!!, true)

                            autoRechargeViewModel.makeAddAutoRechargeApiCall(requestModel)
                        }

                    }

                }


            }else{
                Log.d("AutoRechargeIssue", "Amount Label invisible")

                dialog.show()
                var requestModel =
                    AddAutoRechargeRequestModel(Constant.account, amountDouble!!, true)

                autoRechargeViewModel.makeAddAutoRechargeApiCall(requestModel)
            }


        })


        _binding!!.deleteCardButton.setOnClickListener(View.OnClickListener {
            dialog.show()
            autoRechargeViewModel.makeDeleteCardApiCall()
        })

        /*.................Data Observer.................*/
        autoRechargeViewModel.getAutoRechargeDataObserver().observe(viewLifecycleOwner, Observer {
            //progressDialog.dismiss()

            autoRechargeViewModel.setIsAutoRecharge(it.IS_AUTO_RECHARGE)

            if (it.AMOUNT == 5.0) {
                autoRechargeViewModel.setIsAmountFive(true)
            } else if (it.AMOUNT == 10.0) {
                autoRechargeViewModel.setIsAmountTen(true)
            } else if (it.AMOUNT == 20.0) {
                autoRechargeViewModel.setIsAmountTwenty(true)
            } else if (it.AMOUNT == 30.0) {
                autoRechargeViewModel.setIsAmountThirty(true)
            } else if (it.AMOUNT == 0.0) {
                //do nothing
            } else {
                autoRechargeViewModel.setIsAmountOther(true)
                autoRechargeViewModel.setAmount(it.AMOUNT.toString())


                autoRechargeViewModel.setEnterAmountVisibility(true)
                autoRechargeViewModel.setEnterAmountMandatoryVisibility(false)
                isAmountLabelVisible = true
            }


            autoRechargeViewModel.makePaymentInfoApiCall()


        })


        autoRechargeViewModel.getPaymentInfoDataObserver().observe(viewLifecycleOwner, Observer {
            //progressDialog.dismiss()



            if (it!=null){
                var thresholdAmount=it.AUTO_AMOUNT
                if (thresholdAmount==0.0){
                    thresholdAmount=5.0
                }
                autoRechargeViewModel.setThreshold(thresholdAmount.toString())

                autoRechargeViewModel.name.set(it.FIRST_NAME + " " + it.LAST_NAME)
                autoRechargeViewModel.cardNumber.set(it.CARD_NUMBER)
                autoRechargeViewModel.expiryDate.set(it.EXPIRY_DATE)

                if (it.CARD_NUMBER!=null) {
                    autoRechargeViewModel.securityCode.set("xxx")
                }
            }

            autoRechargeViewModel.makeSystemSettingApiCall()
        })

        autoRechargeViewModel.getSystemSettingDataObserver().observe(viewLifecycleOwner, Observer {
            dialog.dismiss()

            systemSettingsData = it
        })



        autoRechargeViewModel.getAddAutoRechargeDataObserver().observe(viewLifecycleOwner, Observer {
            dialog.dismiss()

            (activity as MainActivity?)?.displaySelectedScreen("Credit")
        })

        autoRechargeViewModel.getThresholdDataObserver().observe(viewLifecycleOwner, Observer {
            dialog.dismiss()

            (activity as MainActivity?)?.displaySelectedScreen("Credit")

        })



        autoRechargeViewModel.getDeleteCardDataObserver().observe(viewLifecycleOwner, Observer {
            dialog.dismiss()

            (activity as MainActivity?)?.displaySelectedScreen("Credit")

        })

        /*.................Data Observer.................*/

        return root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        autoRechargeViewModel = ViewModelProvider(this).get(AutoRechargeViewModel::class.java)
        // TODO: Use the ViewModel
    }

}