package com.example.one_suite.ui.callingPlan.callingPlans

import android.app.ProgressDialog
import android.content.Intent
import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.one_suite.MainActivity
import com.example.one_suite.databinding.CallingPlansFragmentBinding
import com.example.one_suite.databinding.MyNumbersListFragmentBinding
import com.example.one_suite.ui.buyNumber.checkOut.BuyNumberCheckoutActivity
import com.example.one_suite.ui.buyNumber.checkOutNew.CheckOutCartActivity
import com.example.one_suite.utils.Constant
import com.resocoder.databinding.utils.DialogUtils

class CallingPlansFragment : Fragment() {

    companion object {
        fun newInstance() = CallingPlansFragment()
    }

    private lateinit var callingPlansViewModel:CallingPlansViewModel
    private var _binding: CallingPlansFragmentBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!


    //    var dialog = DialogUtils
    //var dialog = AlertDialog.Builder(activity?.applicationContext)

    private var callingPlanList=ArrayList<GetCallingPlansDataModel>()
    private var addToCartList=ArrayList<GetCallingPlansDataModel>()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        callingPlansViewModel = ViewModelProvider(this).get(CallingPlansViewModel::class.java)

        _binding = CallingPlansFragmentBinding.inflate(inflater, container, false).apply {
            this.lifecycleOwner = this@CallingPlansFragment
            this.viewModel = callingPlansViewModel
        }

        val root: View = binding.root




        _binding!!.executePendingBindings()
        _binding!!.recyclerView.apply {
            layoutManager = LinearLayoutManager(activity?.applicationContext)
        }


        var dialog= DialogUtils.setProgressDialog(container!!.context,"Please Wait....")


        //calling API
        dialog!!.show()
        callingPlansViewModel.makeCallingPlansApiCall()



        /*..................On Set Click Listener..........*/
        _binding!!.checkOutButton.setOnClickListener(View.OnClickListener {


            if (Constant.checkoutBundleListStatic.size == 0) {
                Constant.checkoutBundleListStatic = addToCartList
            } else {
                Constant.checkoutBundleListStatic.addAll(addToCartList)
            }



            var intent = Intent(context, CheckOutCartActivity::class.java)
            val bundle = Bundle()

            intent.putExtra("Bundle", bundle)
            bundle.putParcelableArrayList("CheckOutBundleList", Constant.checkoutBundleListStatic)

            startActivity(intent)
        })




        /*...........Data Observer..........*/
        callingPlansViewModel.getRecyclerListDataObserver().observe(viewLifecycleOwner, Observer {
            dialog!!.dismiss()

            if (it!=null){
                callingPlansViewModel.setAdapterData(it)
                //saving for use in add to cart list
                callingPlanList=it
            }

        })



        /*............................Data Observer...................*/
        callingPlansViewModel.getBuyPlanItemPositionObserver().observe(viewLifecycleOwner, Observer {
            var itemData=callingPlanList[it]

            Log.d("CallingPlansIssue","Observer")
            if (itemData !=null){

                var isAlreadyInList=false
                //check in both lists
                for (item in addToCartList){
                    if (item.BundleId==itemData.BundleId){
                        Log.d("CallingPlansIssue","item Bundle Id :${item.BundleId}")
                        isAlreadyInList=true
                    }
                }

                for (item in Constant.checkoutBundleListStatic){
                    if (item.BundleId==itemData.BundleId){
                        Log.d("CallingPlansIssue","item Bundle Id :${item.BundleId}")
                        isAlreadyInList=true
                    }
                }


                if (!isAlreadyInList){
                    Log.d("CallingPlansIssue","finally added to cart")
                    addToCartList.add(itemData);

//                    Constant.checkoutBundleListStatic=addToCartList

                    Log.d("CallingPlansIssue","Size :${addToCartList.size}")
                    Toast.makeText(context, "Item added to the cart", Toast.LENGTH_SHORT).show();

                }

            }

        })




        return root;
    }


    override fun onResume() {
        super.onResume()
        addToCartList.clear()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        callingPlansViewModel = ViewModelProvider(this).get(CallingPlansViewModel::class.java)
        // TODO: Use the ViewModel
    }

}