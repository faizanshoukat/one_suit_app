package com.example.one_suite.ui.buyNumber.buyNumberList

import android.util.Log
import android.view.View
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.one_suite.ui.buyNumber.GetLocalRatesDataModel
import com.example.one_suite.ui.buyNumber.SearchTelephoneNumberDataModel
import com.example.one_suite.utils.ClickListener

class BuyNumberListViewModel : ViewModel(),ClickListener{
    // TODO: Implement the ViewModel

    lateinit var recyclerListData: MutableLiveData<ArrayList<SearchTelephoneNumberDataModel>>
    lateinit var recyclerViewAdapter: BuyNumberListAdapter

    val numberClick = MutableLiveData<SearchTelephoneNumberDataModel>()
    val numberPosition=MutableLiveData<Int>()

    init {
        Log.d("SearchTelephoneApiCall", "init view Model")
        recyclerListData= MutableLiveData()
        recyclerViewAdapter = BuyNumberListAdapter(this);

    }

    fun getAdapter(): BuyNumberListAdapter {
        Log.d("SearchTelephoneApiCall", "Buy Number List Adapter")
        return recyclerViewAdapter
    }


    fun setAdapterData(data: ArrayList<SearchTelephoneNumberDataModel>) {
        Log.d("SearchTelephoneApiCall", "Setting DataList :${data.size}")
        recyclerViewAdapter.setDataList(data)
        //recyclerViewAdapter.notifyDataSetChanged()
    }

    fun setLocalRates(localRates: ArrayList<GetLocalRatesDataModel>){
        Log.d("SearchTelephoneApiCall", "Setting LocalRate :${localRates.size}")
        recyclerViewAdapter.setLocalRateList(localRates)
        recyclerViewAdapter.notifyDataSetChanged()
    }


//    fun getNumberClickDataObserver() : MutableLiveData<SearchTelephoneNumberDataModel>{
//        return  numberClick;
//    }

    fun getNumberPositionObserver():MutableLiveData<Int>{
        return numberPosition
    }

    override fun onViewClick(view: View, position: Int) {

        Log.d("AddToCartIssue","Position :$position")
        Log.d("AddToCartIssue","Phone # :${recyclerListData.value?.get(position)?.telephoneNumber}")




        numberPosition.postValue(position)
        //numberClick.postValue(recyclerListData.value?.get(position))
    }

    override fun onRowClick(position: Int) {
    }


}