package com.example.one_suite.ui.callingPlan.callingPlans

import android.os.Parcel
import android.os.Parcelable


data class GetCallingPlansModel(val PROMOTIONS: ArrayList<GetCallingPlansDataModel> )

data class GetCallingPlansDataModel (
    val PROMOTION: String,
    val DESCRIPTION: String,
    val DESTINATION: String,
    val VALIDITY: String,
    val PRICE: String,
    val MINUTES:String,
    val CURRENCY: String,
    val Country: String,
    val UNITS: String,
    val BundleId: String,
):Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString() ?:"",
        parcel.readString()?:"",
        parcel.readString()?:"",
        parcel.readString()?:"",
        parcel.readString()?:"",
        parcel.readString()?:"",
        parcel.readString()?:"",
        parcel.readString()?:"",
        parcel.readString()?:"",
        parcel.readString()?:""
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(PROMOTION)
        parcel.writeString(DESCRIPTION)
        parcel.writeString(DESTINATION)
        parcel.writeString(VALIDITY)
        parcel.writeString(PRICE)
        parcel.writeString(MINUTES)
        parcel.writeString(CURRENCY)
        parcel.writeString(Country)
        parcel.writeString(UNITS)
        parcel.writeString(BundleId)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<GetCallingPlansDataModel> {
        override fun createFromParcel(parcel: Parcel): GetCallingPlansDataModel {
            return GetCallingPlansDataModel(parcel)
        }

        override fun newArray(size: Int): Array<GetCallingPlansDataModel?> {
            return arrayOfNulls(size)
        }
    }
}
