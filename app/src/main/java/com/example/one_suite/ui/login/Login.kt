package com.example.one_suite.ui.login

import android.app.ProgressDialog
import android.content.*
import android.net.ConnectivityManager
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.view.WindowManager
import android.widget.CheckBox
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.example.one_suite.Activity_Select
import com.example.one_suite.MainActivity
import com.example.one_suite.R
import com.example.one_suite.databinding.ActivityLoginBinding
import com.example.one_suite.ui.OfflineActivity
import com.example.one_suite.utils.Constant
import com.example.one_suite.utils.DateConversion
import com.example.one_suite.utils.PreferenceUtils
import kotlinx.coroutines.Dispatchers

class Login : AppCompatActivity() {

    lateinit var activityBinding: ActivityLoginBinding



    //for for internet conectivity
    var broadcastReceiver: BroadcastReceiver? = null
    val intentFilter = IntentFilter()


    var userText=""
    var passwordText=""


    override fun onStart() {
        super.onStart()

        var name1  = PreferenceUtils.getEmail(applicationContext)
        var password1  = PreferenceUtils.getPassword(applicationContext)




        if (name1 != null && password1 != null) {

            Log.d("CredentialsSave", "User :$name1")
            Log.d("CredentialsSave", "Password :$password1")

        }


    }
    lateinit var progressDialog:ProgressDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //setContentView(R.layout.activity_login)

        val viewModel = ViewModelProviders.of(this)
            .get(LoginViewModel::class.java)


        var activityBinding = DataBindingUtil.setContentView<ActivityLoginBinding>(
            this, R.layout.activity_login
        ).apply {
            this.setLifecycleOwner(this@Login)
            this.viewmodel = viewModel
        }




        Constant.isOnline = true
        internetConnectivity()


        progressDialog = ProgressDialog(this@Login)
//        progressDialog.setTitle("Progress Bar")
        progressDialog.setMessage("Please wait...")
        //progressDialog.show()


        //back button
        activityBinding!!.btnBack.setOnClickListener {
            val intent = Intent(this, Activity_Select::class.java)
            startActivity(intent)
            finish()

        }

        activityBinding!!.loginButton.setOnClickListener(View.OnClickListener {

            progressDialog.show()
            var name: String = activityBinding.userName.text.toString()
            var password: String = activityBinding.password.text.toString()

            userText=name
            passwordText=password

//        SharedPreferences perf = getSharedPreferences(PreferenceUtils.PERFS_NAME, MODE_PRIVATE);
//        String p_email = perf.getString(PreferenceUtils.PERFS_EMAIL, null);
//        String p_password = perf.getString(PreferenceUtils.PERFS_PASSWORD, null);

            Log.d("LoginApiCall", "User :$name")
            Log.d("LoginApiCall", "Password :$password")



            activityBinding.userName.addTextChangedListener(object : TextWatcher {
                override fun beforeTextChanged(
                    s: CharSequence?,
                    start: Int,
                    count: Int,
                    after: Int
                ) {
                }

                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                    Log.d("LoginApiCall", "on text changed:")
                    if (start >= 4) {
                        Log.d("LoginApiCall", "count 5: $s")
                    } else {
                        Log.d("LoginApiCall", "count :$s $start $before $count")
                    }
                }

                override fun afterTextChanged(s: Editable?) {
                }

            })

            if (name?.trim().length <= 0) {
                progressDialog.dismiss()
                activityBinding.llUserName.setBackgroundResource(R.drawable.edittext_red_box)
                activityBinding.userName.setError("Please enter your username.")
                activityBinding.txtErrorMessage.setText("")

            } else if (password?.trim().length <= 0) {
                progressDialog.dismiss()
                activityBinding.llpassword.setBackgroundResource(R.drawable.edittext_red_box)
                activityBinding.password.setError("Please enter your password.")
                activityBinding.txtErrorMessage.setText("")
            } else {
                activityBinding.txtErrorMessage.setText("")
                progressDialog.show()
                activityBinding.llpassword.setBackgroundResource(R.drawable.edit_text_box)



                var requestModel = LoginRequestModel(name, password)
                viewModel.makeLoginApiCall(requestModel)
            }
        })




        viewModel.getLoginDataResult().observe(this, androidx.lifecycle.Observer {
            progressDialog.dismiss()

            if (it!=null) {

                if (it.ACCOUNT != null) {
                    Constant.name = it.Name!! + "!"
                    Constant.account = it.ACCOUNT!!

                    if (it.UrlImage==null || it.UrlImage.isNullOrEmpty()){
                        Log.d("ImageIssue", "img null ${it.UrlImage}")

                        // add the default string
                        Constant.imageUrl="http://osportal2.inovedia.com/Content/assests/profile.png";

                    }else{
                        Log.d("ImageIssue", "img not null ${it.UrlImage}")

                        Constant.imageUrl =
                            "http://osportal2.inovedia.com/UploadedStuff/ProfilePictures/" + it.UrlImage

                    }

//                    if (it.UrlImage != null || it.UrlImage != "") {
//                        Constant.imageUrl =
//                            "http://osportal2.inovedia.com/UploadedStuff/ProfilePictures/" + it.UrlImage
//                    }else{
//                        Constant.imageUrl = "http://osportal2.inovedia.com/Content/assests/profile.png"
//                    }

                    Log.d("ImageUrl", "Img :${it.UrlImage}")


                    if (it.UrlImage != null) {
                        PreferenceUtils.setIs_Profile_Link(true, applicationContext)
                        PreferenceUtils.setProfile_Link(
                            "http://osportal2.inovedia.com/UploadedStuff/ProfilePictures/" + it.UrlImage,
                            applicationContext
                        )

                    } else {
                        PreferenceUtils.setIs_Profile_Link(false, applicationContext)
                            PreferenceUtils.setProfile_Link(
                            "",
                            applicationContext
                        )

                    }


//                PreferenceUtils.setFull_Name(it.Name, applicationContext)
//                PreferenceUtils.setPassword(password,applicationContext)

//                PreferenceUtils.setDesignation(it., applicationContext)

//                    if (activityBinding.rememberMe.isChecked) {

                    if (userText.isNullOrBlank()||passwordText.isNullOrBlank()){


                        RememberFunction("", "")
                        Toast.makeText(this@Login, "Data not save", Toast.LENGTH_SHORT).show();


                    } else {

                        Log.d("CredentialsSave", "Saving User :${userText}")
                        Log.d("CredentialsSave", "Saving Password :$passwordText")

                        RememberFunction(userText, passwordText)

                        Toast.makeText(this@Login, "Data save", Toast.LENGTH_SHORT).show();

                    }

                    val intent = Intent(this, MainActivity::class.java)
                    intent.putExtra("Fragment","Dashboard")
                    startActivity(intent)

                } else {
                    activityBinding.txtErrorMessage.text =
                        "The username or password you have entered is invalid."
                }

            }else{
                activityBinding.txtErrorMessage.text =
                    "The username or password you have entered is invalid."
            }
        })


    }





    //check the connectivity status

    private fun internetConnectivity() {
        if (broadcastReceiver == null) {
            broadcastReceiver = object : BroadcastReceiver() {
                override fun onReceive(context: Context, intent: Intent) {
                    val isVisible: Boolean = App.isActivityVisible

                    if (isVisible) {
                        val conn =
                            context.getSystemService(CONNECTIVITY_SERVICE) as ConnectivityManager
                        val networkInfo = conn.activeNetworkInfo
                        if (networkInfo != null && networkInfo.isConnectedOrConnecting) {
                            val connected = Intent("connected")
                            context.sendBroadcast(connected)


//                            if (CommonUtils.is_Online == false) {
                            if (Constant.isOnline === false) {

                                var i = Intent(this@Login, Login::class.java)
                                startActivity(i)

//                                if (p_email != null || p_password != null) {
//                                    Log.wtf("FaizanTesting", "User: $p_email")
//                                    Log.wtf("FaizanTesting", "pwd: $p_password")
//                                    startActivity(Intent(this@SignIn, Dashboard::class.java))
//                                } else {
//                                    startActivity(
//                                        Intent(
//                                            this@SignIn,
//                                            com.inovedia.hrmsInovedia.Activity.SignIn::class.java
//                                        )
//                                    )
//                                }
                            }


                            Log.d("InternetIssue", "Connected to the internet")
                        } else {
                            val connected = Intent("Connection Lost")
                            context.sendBroadcast(connected)

                            if (Constant.isOnline == true) {
                                var i = Intent(this@Login, OfflineActivity::class.java)
                                startActivity(i)
                            }
                            Log.d("InternetIssue", "Please connect to the internet")
                        }
                    }
                }
            }
            intentFilter.addAction(ConnectivityManager.CONNECTIVITY_ACTION)
            registerReceiver(broadcastReceiver, intentFilter)
        }

        changeStatusBarColor()
    }



    // change the status bar color
    fun changeStatusBarColor() {


        if (Build.VERSION.SDK_INT >= 21) {
            val window = this.window
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
            window.statusBarColor = this.resources.getColor(R.color.colorPrimaryDark)
        }

    }

    override fun onResume() {
        super.onResume()
        App.activityResumed()
    }

    override fun onPause() {
        super.onPause()
        App.activityPaused()
    }

    //remember me functoin

    private fun RememberFunction(s_mail:String,  s_password:String ) {
//        save email and password

        PreferenceUtils.setEmail(s_mail,getApplicationContext());
        PreferenceUtils.setPassword(s_password,getApplicationContext());

    }



}