package com.example.one_suite.ui.credit.osCard

import android.util.Log
import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.one_suite.model.credits.GetSystemSettingsResponseModel
import com.example.one_suite.model.credits.osCard.RedeemVoucherRequestModel
import com.example.one_suite.model.credits.osCard.RedeemVoucherResponseModel
import com.example.one_suite.model.credits.osCard.isValidVoucherRequestModel
import com.example.one_suite.model.credits.osCard.isValidVoucherResponseModel
import com.example.one_suite.network.APIService
import com.example.one_suite.network.RetroInstance
import com.example.one_suite.utils.Constant
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class OsCardViewModel : ViewModel() {
    // TODO: Implement the ViewModel

    var voucherNumber = ObservableField<String>("")

    var isValidVoucherData:MutableLiveData<isValidVoucherResponseModel> = MutableLiveData()
    var redeemVoucherData:MutableLiveData<RedeemVoucherResponseModel> = MutableLiveData()

    var enterVoucherMandatoryVisibility = MutableLiveData<Boolean>(false)

    fun setVoucherNumber(value:String) {
        voucherNumber.set(value)
    }


    fun setVoucherNumberMandatoryVisibility(value:Boolean) {
        enterVoucherMandatoryVisibility.postValue(value)
    }

    fun getIsValidVoucherDataObserver():MutableLiveData<isValidVoucherResponseModel>{
        return isValidVoucherData
    }

    fun getRedeemVoucherDataObserver():MutableLiveData<RedeemVoucherResponseModel>{
        return redeemVoucherData
    }


    fun makeIsValidVoucherApiCall(requestModel:isValidVoucherRequestModel) {

        val apiService = RetroInstance.getRetrofitInstance().create(APIService::class.java)
        val call = apiService.isValidVoucher(Constant.credentials,requestModel)

        call.enqueue(object : Callback<isValidVoucherResponseModel> {
            override fun onResponse(call: Call<isValidVoucherResponseModel>, response: Response<isValidVoucherResponseModel>) {

                Log.d("SubscriberApiCall", "response")
                if (response.isSuccessful) {

                    if (response.body() != null) {

                        isValidVoucherData.postValue(response.body())
                    }else{
                        isValidVoucherData.postValue(null)
                    }


                } else {
                    isValidVoucherData.postValue(null)
                }
            }

            override fun onFailure(call: Call<isValidVoucherResponseModel>, t: Throwable) {
                Log.d("SubscriberApiCall", "Failure : ${t.message}")
                isValidVoucherData.postValue(null)
            }

        })

    }


    fun makeRedeemVoucherApiCall(requestModel:RedeemVoucherRequestModel) {

        val apiService = RetroInstance.getRetrofitInstance().create(APIService::class.java)
        val call = apiService.redeemVoucher(Constant.credentials,requestModel)

        call.enqueue(object : Callback<RedeemVoucherResponseModel> {
            override fun onResponse(call: Call<RedeemVoucherResponseModel>, response: Response<RedeemVoucherResponseModel>) {

                Log.d("SubscriberApiCall", "response")
                if (response.isSuccessful) {

                    if (response.body() != null) {

                        redeemVoucherData.postValue(response.body())
                    }else{
                        redeemVoucherData.postValue(null)
                    }


                } else {
                    redeemVoucherData.postValue(null)
                }
            }

            override fun onFailure(call: Call<RedeemVoucherResponseModel>, t: Throwable) {
                Log.d("SubscriberApiCall", "Failure : ${t.message}")
                redeemVoucherData.postValue(null)
            }

        })

    }
}