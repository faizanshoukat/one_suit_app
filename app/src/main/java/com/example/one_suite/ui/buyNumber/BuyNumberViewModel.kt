package com.example.one_suite.ui.buyNumber

import android.util.Log
import android.view.View
import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.one_suite.model.*
import com.example.one_suite.network.APIService
import com.example.one_suite.network.RetroInstance
import com.example.one_suite.utils.ClickListener
import com.example.one_suite.utils.Constant
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class BuyNumberViewModel : ViewModel(), ClickListener {

    var country = ObservableField<String>("")
    var type = ObservableField<String>("")
    var state = ObservableField<String>("")
    var city = ObservableField<String>("")
    var areaCode = ObservableField<String>("")
    var quantity = ObservableField<String>("1")

    var statesListData: MutableLiveData<ArrayList<GetStatesDataModel>> = MutableLiveData()
    var citiesListData: MutableLiveData<ArrayList<String>> = MutableLiveData()


    var numberListData: MutableLiveData<ArrayList<SearchTelephoneNumberDataModel>> =
        MutableLiveData()
    var localRatesListData: MutableLiveData<ArrayList<GetLocalRatesDataModel>> = MutableLiveData()

    val numberData = MutableLiveData<SearchTelephoneNumberDataModel>()



    init {
//        numberData.value=MutableLiveData();
    }


    fun getNumberDataObserver() : MutableLiveData<SearchTelephoneNumberDataModel>{
        return  numberData;
    }


    fun getStatesListDataObserver(): MutableLiveData<ArrayList<GetStatesDataModel>> {
        return statesListData
    }

    fun getCitiesListDataObserver(): MutableLiveData<ArrayList<String>> {
        return citiesListData
    }

    fun getNumberListDataObserver(): MutableLiveData<ArrayList<SearchTelephoneNumberDataModel>> {
        return numberListData
    }

    fun getLocalRatesDataObserver() :MutableLiveData<ArrayList<GetLocalRatesDataModel>>{
        return localRatesListData;
    }

    fun getStatesApiCall() {
        Log.d("StatesIssue", "states api call")
        val apiService = RetroInstance.getRetrofitInstance().create(APIService::class.java)
        val call = apiService.getStates(Constant.credentials)

        call.enqueue(object : Callback<GetStatesModel> {
            override fun onResponse(
                call: Call<GetStatesModel>,
                response: Response<GetStatesModel>
            ) {


                Log.d("StatesIssue", "response")
                if (response.isSuccessful) {


                    if (response.body() != null) {
                        statesListData.postValue(response.body()!!.States);
                    } else {
                        statesListData.postValue(null);
                    }


                } else {
                    statesListData.postValue(null);
                }
            }

            override fun onFailure(call: Call<GetStatesModel>, t: Throwable) {
                Log.d("StatesIssue", "Failure : ${t.message}")
                statesListData.postValue(null);

            }

        })

    }

    fun getCitiesApiCall(state:String) {
        Log.d("CitiesIssue", "Cities api call")

        var requestModel=GetCitiesRequestModel(state)

        val apiService = RetroInstance.getRetrofitInstance().create(APIService::class.java)
        val call = apiService.getCities(Constant.credentials,requestModel)

        call.enqueue(object : Callback<GetCitiesResponseModel> {
            override fun onResponse(
                call: Call<GetCitiesResponseModel>,
                response: Response<GetCitiesResponseModel>
            ) {


                Log.d("CitiesIssue", "response")
                if (response.isSuccessful) {


                    if (response.body() != null) {
                        citiesListData.postValue(response.body()!!.Cities);

                        Log.d("CitiesIssue", "cities list :${response.body()!!.Cities.size}")

                    } else {
                        citiesListData.postValue(null);
                    }


                } else {
                    citiesListData.postValue(null);
                }
            }

            override fun onFailure(call: Call<GetCitiesResponseModel>, t: Throwable) {
                Log.d("CitiesIssue", "Failure : ${t.message}")
                citiesListData.postValue(null);

            }

        })

    }

    fun getLocalRatesApiCall(requestModel: GetLocalRatesRequestModel) {
        Log.d("LocalRateApiCall", "states api call")
        val apiService = RetroInstance.getRetrofitInstance().create(APIService::class.java)
        val call = apiService.getLocalRates(Constant.credentials,requestModel)

        call.enqueue(object : Callback<GetLocalRatesResponseModel> {
            override fun onResponse(
                call: Call<GetLocalRatesResponseModel>,
                response: Response<GetLocalRatesResponseModel>
            ) {

                Log.d("LocalRateApiCall", "response")
                if (response.isSuccessful) {


                    if (response.body() != null) {
                        localRatesListData.postValue(response.body()!!.Rates);
                    } else {
                        localRatesListData.postValue(null);
                    }


                } else {
                    localRatesListData.postValue(null);
                }
            }

            override fun onFailure(call: Call<GetLocalRatesResponseModel>, t: Throwable) {
                Log.d("LocalRateApiCall", "Failure : ${t.message}")
                localRatesListData.postValue(null);

            }

        })

    }

    fun searchTelephoneNumberApiCall(requestModel: SearchTelephoneNumberRequestModel) {
        Log.d("SearchTelephoneApiCall", "Search api call")

        val apiService = RetroInstance.getRetrofitInstance().create(APIService::class.java)
        val call = apiService.searchTelephoneNumber(Constant.credentials, requestModel)

        call.enqueue(object : Callback<SearchTelephoneNumberResponseModel> {
            override fun onResponse(
                call: Call<SearchTelephoneNumberResponseModel>,
                response: Response<SearchTelephoneNumberResponseModel>
            ) {

                Log.d("SearchTelephoneApiCall", "response")
                if (response.isSuccessful) {

                    Log.d("SearchTelephoneApiCall", "Response Status : ${response.body()?.status}");
                    if (response.body() != null) {
                        numberListData.postValue(response.body()!!.tnResult);
                    } else {
                        numberListData.postValue(null);
                    }


                } else {
                    Log.d("SearchTelephoneApiCall", "not successful")
                    Log.d("SearchTelephoneApiCall", "Code :${response.code()}")
                    numberListData.postValue(null);
                }
            }

            override fun onFailure(call: Call<SearchTelephoneNumberResponseModel>, t: Throwable) {
                Log.d("SearchTelephoneApiCall", "Failure : ${t.message}")
                numberListData.postValue(null);

            }

        })

    }

    fun searchTelephoneNumberEmptyApiCall(requestModel: SearchTelephoneNumberEmptyRequestModel) {
        Log.d("SearchTelephoneApiCall", "Search api call")

        val apiService = RetroInstance.getRetrofitInstance().create(APIService::class.java)
        val call = apiService.searchTelephoneNumberEmpty(Constant.credentials, requestModel)

        call.enqueue(object : Callback<SearchTelephoneNumberResponseModel> {
            override fun onResponse(
                call: Call<SearchTelephoneNumberResponseModel>,
                response: Response<SearchTelephoneNumberResponseModel>
            ) {

                Log.d("SearchTelephoneApiCall", "response")
                if (response.isSuccessful) {

                    Log.d("SearchTelephoneApiCall", "Response Status : ${response.body()?.status}");
                    if (response.body() != null) {
                        numberListData.postValue(response.body()!!.tnResult);
                    } else {
                        numberListData.postValue(null);
                    }


                } else {
                    Log.d("SearchTelephoneApiCall", "not successful")
                    Log.d("SearchTelephoneApiCall", "Code :${response.code()}")
                    numberListData.postValue(null);
                }
            }

            override fun onFailure(call: Call<SearchTelephoneNumberResponseModel>, t: Throwable) {
                Log.d("SearchTelephoneApiCall", "Failure : ${t.message}")
                numberListData.postValue(null);

            }

        })

    }

    override fun onViewClick(view: View, position: Int) {
//        TODO("Not yet implemented")
//        Log.d("SearchTelephoneApiCall", "Failure : ${numberListData.}")

        Log.d(
            "SearchTelephoneApiCall",
            "On View Click " + (numberListData.value?.get(position)?.telephoneNumber ?: "Null exception")
        )

        numberData.postValue(numberListData.value?.get(position));



    }

    override fun onRowClick(position: Int) {
        TODO("Not yet implemented")
    }

}