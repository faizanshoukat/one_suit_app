package com.example.one_suite.ui.support

import android.app.AlertDialog
import android.app.Dialog
import android.app.ProgressDialog
import android.content.Intent
import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.example.one_suite.MainActivity
import com.example.one_suite.R
import com.example.one_suite.databinding.BuyNumberListFragmentBinding
import com.example.one_suite.databinding.CallForwardingFragmentBinding
import com.example.one_suite.databinding.TicketFragmentBinding
import com.example.one_suite.model.callForwarding.setSubscriberDID.SetSubscriberDIDRequestModel
import com.example.one_suite.ui.buyNumber.checkOut.AutoRenewNumberRequestModel
import com.example.one_suite.ui.callForwarding.setForwarding.SetForwardingDialog
import com.example.one_suite.ui.callForwarding.setForwarding.SetForwardingDialog1
import com.example.one_suite.ui.support.newTicket.NewTicket
import com.example.one_suite.utils.Constant
import com.resocoder.databinding.utils.DialogUtils

class TicketsFragment : Fragment() {


    companion object {
        fun newInstance() = TicketsFragment()
    }

    private lateinit var ticketsViewModel: TicketsViewModel
    private var _binding: TicketFragmentBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!


    //    var dialog = DialogUtils
    //var dialog = AlertDialog.Builder(activity?.applicationContext)
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        ticketsViewModel = ViewModelProvider(this).get(TicketsViewModel::class.java)

        _binding = TicketFragmentBinding.inflate(inflater, container, false).apply {
            this.lifecycleOwner = this@TicketsFragment
            this.viewModel = ticketsViewModel
        }

        val root: View = binding.root


        _binding!!.newTicketBtn.setOnClickListener(View.OnClickListener {
            var intent=Intent(context,NewTicket::class.java)
            startActivity(intent)
        })

        //progress dialog
        var dialog=DialogUtils.setProgressDialog(container!!.context,"Please Wait....")


        _binding!!.executePendingBindings()
        _binding!!.recyclerView.apply {
            Log.d("SearchTelephoneApiCall", "List size in fragment")
            layoutManager = LinearLayoutManager(activity?.applicationContext)
        }


        //calling API
        dialog!!.show()
        ticketsViewModel.makeAllTicketsApiCall()


        //Data Observer
        ticketsViewModel.getRecyclerListDataObserver().observe(viewLifecycleOwner, Observer {
            Log.d("SubscriberApiCall", "get recycler data ")

            dialog!!.dismiss()
            if (it != null) {
                //update the adapter
                ticketsViewModel.setAdapterData(it)

                Log.d("SubscriberApiCall", "List size :${it.size} ")
            }else{
                Log.d("SubscriberApiCall", "data null")
            }
        })



        return root;
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        ticketsViewModel = ViewModelProvider(this).get(TicketsViewModel::class.java)
        // TODO: Use the ViewModel
    }

}