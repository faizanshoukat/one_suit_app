package com.example.one_suite.ui.setting.changePassword

import android.graphics.Color
import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.one_suite.R
import com.example.one_suite.databinding.ChangePasswordFragmentBinding
import com.example.one_suite.databinding.UpdateProfileFragmentBinding
import com.example.one_suite.ui.setting.updateProfile.UpdateProfileViewModel
import com.resocoder.databinding.utils.DialogUtils
import com.resocoder.databinding.utils.Validator

class ChangePasswordFragment : Fragment() {

    companion object {
        fun newInstance() = ChangePasswordFragment()
    }

    private lateinit var changePasswordViewModel: ChangePasswordViewModel
    private var _binding: ChangePasswordFragmentBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        changePasswordViewModel = ViewModelProvider(this).get(ChangePasswordViewModel::class.java)

        _binding = ChangePasswordFragmentBinding.inflate(inflater, container, false).apply {
            this.lifecycleOwner = this@ChangePasswordFragment
            this.viewModel = changePasswordViewModel
        }

        val root: View = binding.root


        //progress dialog
        var progressDialog = DialogUtils.setProgressDialog(container!!.context, "Please Wait....")


        /*.......................Password Validation............*/
        _binding!!.newPassword.addTextChangedListener(object : TextWatcher{
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                Log.d("PasswordValidations","password :"+_binding!!.newPassword.text.toString())
                var passwordtext:String=_binding!!.newPassword.text.toString()

                var isEightChar:Boolean= Validator.isEightCharPasswordFormat(passwordtext)
                var isUpperCase:Boolean= Validator.isUpperCasePasswordFormat(passwordtext)
                var isOneNumber:Boolean= Validator.isOneNumberPasswordFormat(passwordtext)
                var isSpecialChar:Boolean= Validator.isOneSpecialCharPasswordFormat(passwordtext)


                Log.d("PasswordValidations","EightChar : $isEightChar")
                Log.d("PasswordValidations","UpperCase : $isUpperCase")
                Log.d("PasswordValidations","isOneNumber : $isOneNumber")
                Log.d("PasswordValidations","isSpecialChar : $isSpecialChar")


                //eight char icon
                if (isEightChar){
                    _binding!!.char8.setTextColor(Color.parseColor("#3F51B5"));

                }else{
                    _binding!!.char8.setTextColor(Color.parseColor("#FA1908"));
                }

                //upper case char icon
                if (isUpperCase){
                    _binding!!.charUpper.setTextColor(Color.parseColor("#3F51B5"));
                }else{
                    _binding!!.charUpper.setTextColor(Color.parseColor("#FA1908"));
                }


                //number char icon
                if (isOneNumber){
                    _binding!!.charNumber.setTextColor(Color.parseColor("#3F51B5"));
                }else{
                    _binding!!.charNumber.setTextColor(Color.parseColor("#FA1908"));
                }


                //number char icon
                if (isSpecialChar){
                    _binding!!.charSpecial.setTextColor(Color.parseColor("#3F51B5"));
                }else{
                    _binding!!.charSpecial.setTextColor(Color.parseColor("#FA1908"));
                }

            }

            override fun afterTextChanged(s: Editable?) {
            }

        })


        return root;
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        changePasswordViewModel = ViewModelProvider(this).get(ChangePasswordViewModel::class.java)
        // TODO: Use the ViewModel
    }

}