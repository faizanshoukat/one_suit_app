package com.demo.databindingdemo

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.one_suite.databinding.BuyNumberCheckoutRecyclerviewItemsBinding
import com.example.one_suite.ui.buyNumber.checkOut.BuyNumbersCheckoutModel
import com.example.one_suite.utils.ClickListener

class BuyNumberCheckoutAdapter(private val listener: ClickListener) :
    RecyclerView.Adapter<BuyNumberCheckoutAdapter.MyViewHolder>() {

    var checkOutList=ArrayList<BuyNumbersCheckoutModel>()

//    var items = ArrayList<SearchTelephoneNumberDataModel>()
//    var localRatesList = ArrayList<GetLocalRatesDataModel>()
//
    //var localRate=GetLocalRatesDataModel()

//    fun setDataList(data: ArrayList<SearchTelephoneNumberDataModel>) {
//        this.items = data
//    }
//
//    fun setLocalRateList(data: ArrayList<GetLocalRatesDataModel>) {
//        this.localRatesList = data
//    }

    fun setBuyNumberCheckOutList(data :ArrayList<BuyNumbersCheckoutModel>){
        this.checkOutList=data
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): BuyNumberCheckoutAdapter.MyViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = BuyNumberCheckoutRecyclerviewItemsBinding.inflate(layoutInflater)
        return MyViewHolder(binding)
    }

    override fun getItemCount() = checkOutList.size

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
//        holder.bind(checkOutList[position],localRatesList)

        holder.bind(checkOutList[position])



        holder.binding.switchBtn.setOnClickListener(View.OnClickListener {
            listener.onViewClick(holder.binding.switchBtn, position)
        })

        holder.binding.deleteButton.setOnClickListener(View.OnClickListener {
            listener.onViewClick(holder.binding.deleteButton, position)
        })


    }

    class MyViewHolder(val binding: BuyNumberCheckoutRecyclerviewItemsBinding) :
        RecyclerView.ViewHolder(binding.root) {

//        fun bind(data: SearchTelephoneNumberDataModel,localRateData :ArrayList<GetLocalRatesDataModel>) {
//            binding.viewModel = data
//            if (localRateData!=null ){
//                binding.viewModelLocalRates=localRateData[0]
//            }
//            binding.executePendingBindings()
//        }


            fun  bind(data: BuyNumbersCheckoutModel){
                binding.viewModel=data
            }
    }

}