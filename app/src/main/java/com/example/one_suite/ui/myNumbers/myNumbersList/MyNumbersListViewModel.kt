package com.example.one_suite.ui.myNumbers.myNumbersList

import android.content.Context
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import com.example.one_suite.R
import com.example.one_suite.model.callForwarding.setSubscriberDID.SetSubscriberDIDRequestModel
import com.example.one_suite.model.callForwarding.setSubscriberDID.SetSubscriberDIDResponseModel
import com.example.one_suite.model.subscriberCustomNumbers.SubscriberCustomNumberRequestModel
import com.example.one_suite.model.subscriberCustomNumbers.SubscriberCustomNumbersResponseDataModel
import com.example.one_suite.model.subscriberCustomNumbers.SubscriberCustomNumbersResponseModel
import com.example.one_suite.model.subscriberDid.SubscriberDIDResponseDataModel
import com.example.one_suite.network.APIService
import com.example.one_suite.network.RetroInstance
import com.example.one_suite.ui.buyNumber.checkOut.AutoRenewNumberRequestModel
import com.example.one_suite.ui.buyNumber.checkOut.AutoRenewNumberResponseModel
import com.example.one_suite.utils.Constant
import com.example.one_suite.utils.SwitchClickListener
import com.resocoder.databinding.model.subscriberDid.SubscriberDIDRequestModel
import com.resocoder.databinding.model.subscriberDid.SubscriberDIDResponseModel
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MyNumbersListViewModel : ViewModel(), SwitchClickListener {

    var myNumbersListData: MutableLiveData<ArrayList<SubscriberDIDResponseDataModel>>
    var myNumbersAdapter: MyNumbersListAdapter

    var customNumberListData: MutableLiveData<ArrayList<SubscriberCustomNumbersResponseDataModel>>
    var switchCallerIdValue = MutableLiveData<SubscriberDIDResponseDataModel>()
    var switchCallForwardingValue = MutableLiveData<SubscriberDIDResponseDataModel>()
    var switchAutoRenewValue = MutableLiveData<SubscriberDIDResponseDataModel>()

    var setCallerIdResponseData=MutableLiveData<String>()
    var setCallForwardingResponseData=MutableLiveData<String>()
    var setAutoRenewResponseData=MutableLiveData<String>()

    var callForwardingResult = MutableLiveData<Boolean>()

    init {
        myNumbersListData = MutableLiveData()
        myNumbersAdapter = MyNumbersListAdapter(this)
        customNumberListData = MutableLiveData()
    }


    fun getSetCallerIdResponseDataObserver():MutableLiveData<String>{
        return  setCallerIdResponseData
    }

    fun getSetCallForwardingResponseDataObserver():MutableLiveData<String>{
        return  setCallForwardingResponseData
    }

    fun getAutoRenewResponseDataObserver():MutableLiveData<String>{
        return  setAutoRenewResponseData
    }

    fun getSwitchCallerIdDataObserver():MutableLiveData<SubscriberDIDResponseDataModel>{
        return  switchCallerIdValue
    }

    fun getSwitchCallForwardingDataObserver():MutableLiveData<SubscriberDIDResponseDataModel>{
        return  switchCallForwardingValue
    }

    fun getSwitchAutoRenewDataObserver():MutableLiveData<SubscriberDIDResponseDataModel>{
        return  switchAutoRenewValue
    }

    fun getMyNumbersListAdapter(): MyNumbersListAdapter {
        return myNumbersAdapter
    }

    fun getCallForwardingResultDataObserver() : MutableLiveData<Boolean>{
        return  callForwardingResult;
    }



    fun setMyNumbersListAdapterData(data: ArrayList<SubscriberDIDResponseDataModel>) {
        Log.d("MyNumbersAdapterIssue", "set Adapter Data :${data.size}")
        myNumbersAdapter.setDataList(data)
        myNumbersAdapter.notifyDataSetChanged()
    }


    fun getMyNumbersListDataObserver(): MutableLiveData<ArrayList<SubscriberDIDResponseDataModel>> {
        return myNumbersListData
    }


    fun getCustomNumbersListDataObserver(): MutableLiveData<ArrayList<SubscriberCustomNumbersResponseDataModel>> {
        return customNumberListData
    }


    fun makeSubscriberDIDApiCall() {


        val subscriberDIDRequest: SubscriberDIDRequestModel =
            SubscriberDIDRequestModel(Constant.account)

        val apiService = RetroInstance.getRetrofitInstance().create(APIService::class.java)
        val call = apiService.getSubscriberDID(Constant.credentials, subscriberDIDRequest)

        call.enqueue(object : Callback<SubscriberDIDResponseModel> {
            override fun onResponse(
                call: Call<SubscriberDIDResponseModel>,
                DIDResponse: Response<SubscriberDIDResponseModel>
            ) {

                Log.d("NumberApiCall", "Successful")
                if (DIDResponse.isSuccessful) {


                    if (DIDResponse.body() != null) {
                        Log.d("MyNumbersAdapterIssue", "Body Not Null")
                        myNumbersListData.postValue(DIDResponse.body()!!.SubscriberDIDs)
                    } else {
                        myNumbersListData.postValue(null)
                    }


                }

            }

            override fun onFailure(call: Call<SubscriberDIDResponseModel>, t: Throwable) {
                //Toast.makeText(applicationContext,"Error !! while fetching data", Toast.LENGTH_SHORT).show()
                Log.d("NumberApiCall", "Failure : ${t.message}")

                myNumbersListData.postValue(null)
            }

        })


    }

    fun makeSubscriberCustomNumbersApiCall() {


        val subscriberCustomNumberRequestModel: SubscriberCustomNumberRequestModel =
            SubscriberCustomNumberRequestModel(Constant.account)

        val apiService = RetroInstance.getRetrofitInstance().create(APIService::class.java)
        val call = apiService.getSubscriberCustomNumbers(
            Constant.credentials,
            subscriberCustomNumberRequestModel
        )

        call.enqueue(object : Callback<SubscriberCustomNumbersResponseModel> {
            override fun onResponse(
                call: Call<SubscriberCustomNumbersResponseModel>,
                response: Response<SubscriberCustomNumbersResponseModel>
            ) {

                if (response.isSuccessful) {

                    if (response.body() != null) {

                        customNumberListData.postValue(response.body()!!.Numbers)
                    } else {
                        customNumberListData.postValue(null)
                    }


                } else {
                    customNumberListData.postValue(null)
                }
            }

            override fun onFailure(call: Call<SubscriberCustomNumbersResponseModel>, t: Throwable) {
                Log.d("NumberApiCall", "Failure : ${t.message}")
                customNumberListData.postValue(null)

            }

        })


    }

    fun makeSetCallerIdApiCall(requestModel: SetCallerIdRequestModel) {

        val apiService = RetroInstance.getRetrofitInstance().create(APIService::class.java)
        val call = apiService.setCallerId(
            Constant.credentials,
            requestModel
        )

        call.enqueue(object : Callback<SetCallerIdResponseModel> {
            override fun onResponse(
                call: Call<SetCallerIdResponseModel>,
                response: Response<SetCallerIdResponseModel>
            ) {

                if (response.isSuccessful) {

                    if (response.body() != null) {

                        setCallerIdResponseData.postValue(response.body()!!.RESPONSE)
                    } else {
//                        setCallerIdResponseData.postValue(null)
                        setCallerIdResponseData.postValue("null")
                    }


                } else {
                    setCallerIdResponseData.postValue("null")
                }
            }

            override fun onFailure(call: Call<SetCallerIdResponseModel>, t: Throwable) {
                Log.d("NumberApiCall", "Failure : ${t.message}")
                setCallerIdResponseData.postValue("null")

            }

        })


    }

    fun makeAutoRenewNumberApiCall(requestModel: AutoRenewNumberRequestModel) {


        val apiService = RetroInstance.getRetrofitInstance().create(APIService::class.java)
        val call = apiService.autoRenewNumber(Constant.credentials, requestModel)

        call.enqueue(object : Callback<AutoRenewNumberResponseModel> {
            override fun onResponse(call: Call<AutoRenewNumberResponseModel>, response: Response<AutoRenewNumberResponseModel>) {

                Log.d("SubscriberApiCall", "response")
                if (response.isSuccessful) {


                    if (response.body() != null) {
                        setAutoRenewResponseData.postValue(response.body()!!.RESPONSE!!)
                    }else{
                        setAutoRenewResponseData.postValue("null")
                    }


                } else {
                    setAutoRenewResponseData.postValue("null")
                }
            }

            override fun onFailure(call: Call<AutoRenewNumberResponseModel>, t: Throwable) {
                Log.d("SubscriberApiCall", "Failure : ${t.message}")
                setAutoRenewResponseData.postValue("null")
            }

        })

    }

    fun makeSetDIDSettingApiCall(didModel: SetSubscriberDIDRequestModel) {

        val apiService = RetroInstance.getRetrofitInstance().create(APIService::class.java)
        val call = apiService.setSubscriberDID(Constant.credentials, didModel)

        call.enqueue(object : Callback<SetSubscriberDIDResponseModel> {
            override fun onResponse(call: Call<SetSubscriberDIDResponseModel>, response: Response<SetSubscriberDIDResponseModel>) {

                if (response.isSuccessful) {

                    callForwardingResult.postValue(true)

                    if (response.body() != null) {

                        var account: String = ""
                        account = response.body()?.RESPONSE.toString()
                        Log.d("DIDApiCall", "Number : $account")


                    }


                } else {
                    callForwardingResult.postValue(false)
                }
            }

            override fun onFailure(call: Call<SetSubscriberDIDResponseModel>, t: Throwable) {
                Log.d("DIDApiCall", "Failure : ${t.message}")
                callForwardingResult.postValue(false)

            }

        })


    }


    override fun onViewClick(view: View, position: Int) {

    }

    override fun onSwitchClick(view: View, position: Int, isChecked: Boolean) {

        if (view.id == R.id.setCallerIdSwitch) {

            switchCallerIdValue.postValue(
                myNumbersListData.value?.get(position)
            )
        }

        //call forwarding
        if (view.id == R.id.setCallForwardingSwitch) {

            switchCallForwardingValue.postValue(
                myNumbersListData.value?.get(position)
            )

        }

        //Auto Renew
        if (view.id == R.id.autoRenewSwitch) {


            switchAutoRenewValue.postValue(
                myNumbersListData.value?.get(position)
            )

        }






    }


}