package com.example.one_suite.ui.myNumbers.addName

import android.opengl.Visibility
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.one_suite.databinding.AddNameRecyclerItemBinding
import com.example.one_suite.databinding.TestFileDebugingBinding
import com.example.one_suite.model.subscriberDid.SubscriberDIDResponseDataModel
import com.example.one_suite.utils.ClickListener
import com.example.one_suite.utils.EditTextClickListener


class AddNameAdapter(val editTextClickListener: EditTextClickListener) :
        RecyclerView.Adapter<AddNameAdapter.MyViewHolder>() {
    var items = ArrayList<SubscriberDIDResponseDataModel>()

    fun setDataList(data: ArrayList<SubscriberDIDResponseDataModel>) {
        this.items = data
    }

    override fun onCreateViewHolder(
            parent: ViewGroup,
            viewType: Int
    ): AddNameAdapter.MyViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = TestFileDebugingBinding.inflate(layoutInflater)
        return MyViewHolder(binding)
    }

    override fun getItemCount() = items.size

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.bind(items[position])


        var isEdit=true

        holder.binding.AddName.setOnClickListener(View.OnClickListener {
 //           listener.onViewClick(holder.binding.AddName, position,)
//            items[position].Name=""
//            notifyDataSetChanged()
            //notifyDataSetChanged()

            //hide the current button view
            isEdit=false
            holder.binding.AddName.visibility=View.GONE

            holder.binding.enterNameLayout.visibility=View.VISIBLE

        })



        holder.binding.EditName.setOnClickListener(View.OnClickListener {
//            listener.onViewClick(holder.binding.EditName, position,)

            //hide the current button view
            isEdit=true
            holder.binding.EditName.visibility=View.GONE




            editTextClickListener.onViewClick(holder.binding.EditName,position,holder.binding.name.text.toString())
            holder.binding.enterNameLayout.visibility=View.VISIBLE
            //setting data
            holder.binding.name.setText(items.get(position).Name)
        })


        holder.binding.cancelButton.setOnClickListener(View.OnClickListener {
            holder.binding.enterNameLayout.visibility=View.GONE

            if (isEdit){
                holder.binding.EditName.visibility=View.VISIBLE
            }else{
                holder.binding.AddName.visibility=View.VISIBLE
            }

        })



        holder.binding.addButton.setOnClickListener(View.OnClickListener {
            editTextClickListener.onViewClick(holder.binding.addButton,position,holder.binding.name.text.toString())
        })




    }

    class MyViewHolder(val binding: TestFileDebugingBinding) :
            RecyclerView.ViewHolder(binding.root) {

        fun bind(data: SubscriberDIDResponseDataModel) {
            binding.viewModel = data
            binding.executePendingBindings()
        }
    }



}