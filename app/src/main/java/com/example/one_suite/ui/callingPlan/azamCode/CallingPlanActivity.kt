package com.example.one_suite.ui.callingPlan.azamCode

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.one_suite.R
import com.example.one_suite.databinding.ActivityCallingPlanBinding

class CallingPlanActivity : AppCompatActivity() {

    val cpModels = listOf(
        CallingPlanModel("10 Mins", "Germany","When what matters is the content itself, avoid any distraction and be clear with our simple fonts. Ready to personalize and share in Facebook and Twitter."),
        CallingPlanModel("15 Mins", "Germany","When what matters is the content itself, avoid any distraction and be clear with our simple fonts. Ready to personalize and share in Facebook and Twitter."),
        CallingPlanModel("20 Mins", "Germany","When what matters is the content itself, avoid any distraction and be clear with our simple fonts. Ready to personalize and share in Facebook and Twitter."),
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//         setContentView(R.layout.activity_calling_plan)

        var activityBinding = DataBindingUtil.setContentView<ActivityCallingPlanBinding>(
            this, R.layout.activity_calling_plan
        ).apply {
            this.setLifecycleOwner(this@CallingPlanActivity)
//            this.viewModel = viewModel
        }

        activityBinding.executePendingBindings()
        activityBinding.cPRecyclerView.apply {

            layoutManager = LinearLayoutManager(context)
//            val decoration = DividerItemDecoration(context, LinearLayoutManager.VERTICAL)
//            addItemDecoration(decoration)

        }
        //activityBinding.cPRecyclerView.adapter = CallingPlaning_RvAdapter(cpModels)


        /*val cP_recyclerView = findViewById<RecyclerView>(R.id.cP_recyclerView)
        cP_recyclerView.layoutManager = LinearLayoutManager(this@CallingPlanActivity)
        cP_recyclerView.adapter = CallingPlaning_RvAdapter(cpModels)
    */
    }

}