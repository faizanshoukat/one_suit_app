package com.example.one_suite.ui.history.creditNotes

import android.app.AlertDialog
import android.app.Dialog
import android.app.ProgressDialog
import android.content.Intent
import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.example.one_suite.MainActivity
import com.example.one_suite.R
import com.example.one_suite.databinding.*
import com.example.one_suite.model.callForwarding.setSubscriberDID.SetSubscriberDIDRequestModel
import com.example.one_suite.ui.buyNumber.checkOut.AutoRenewNumberRequestModel
import com.example.one_suite.ui.callForwarding.setForwarding.SetForwardingDialog
import com.example.one_suite.ui.callForwarding.setForwarding.SetForwardingDialog1
import com.example.one_suite.utils.Constant
import com.resocoder.databinding.utils.DialogUtils

class CreditNotesFragment : Fragment() {


    companion object {
        fun newInstance() = CreditNotesFragment()
    }

    private lateinit var creditNotesViewModel: CreditNotesViewModel
    private var _binding: CreditNotesFragmentBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!


    //    var dialog = DialogUtils
    //var dialog = AlertDialog.Builder(activity?.applicationContext)
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        creditNotesViewModel = ViewModelProvider(this).get(CreditNotesViewModel::class.java)

        _binding = CreditNotesFragmentBinding.inflate(inflater, container, false).apply {
            this.lifecycleOwner = this@CreditNotesFragment
            this.viewModel = creditNotesViewModel
        }

        val root: View = binding.root


       var dialog=DialogUtils.setProgressDialog(container!!.context,"Please Wait....")



        _binding!!.executePendingBindings()
        _binding!!.recyclerView.apply {
            Log.d("SearchTelephoneApiCall", "List size in fragment")
            layoutManager = LinearLayoutManager(activity?.applicationContext)
        }


        //calling API
        dialog!!.show()
        creditNotesViewModel.makeCreditNotesApiCall()


        //Data Observer
        creditNotesViewModel.getCreditNotesListDataObserver().observe(viewLifecycleOwner, Observer {
            Log.d("SubscriberApiCall", "get recycler data")

            dialog!!.dismiss()
            if (it != null) {
                //update the adapter
                creditNotesViewModel.setAdapterData(it)
            }
        })



        return root;
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        creditNotesViewModel = ViewModelProvider(this).get(CreditNotesViewModel::class.java)
        // TODO: Use the ViewModel
    }

}