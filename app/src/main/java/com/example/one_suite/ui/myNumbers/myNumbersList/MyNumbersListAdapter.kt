package com.example.one_suite.ui.myNumbers.myNumbersList



import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.one_suite.databinding.MyNumbersRecyclerItemsBinding
import com.example.one_suite.model.subscriberDid.SubscriberDIDResponseDataModel
import com.example.one_suite.utils.SwitchClickListener


class MyNumbersListAdapter(private val listener: SwitchClickListener) :
        RecyclerView.Adapter<MyNumbersListAdapter.MyViewHolder>() {
    var items = ArrayList<SubscriberDIDResponseDataModel>()

    fun setDataList(data: ArrayList<SubscriberDIDResponseDataModel>) {
        this.items = data
    }

    override fun onCreateViewHolder(
            parent: ViewGroup,
            viewType: Int
    ): MyNumbersListAdapter.MyViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = MyNumbersRecyclerItemsBinding.inflate(layoutInflater)
        return MyViewHolder(binding)
    }

    override fun getItemCount() = items.size

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.bind(items[position])

        holder.binding.setCallerIdSwitch.setOnClickListener(View.OnClickListener {

            var isChecked:Boolean=false

            isChecked=holder.binding.setCallerIdSwitch.isChecked
            listener.onSwitchClick(holder.binding.setCallerIdSwitch, position,isChecked)

        })

        holder.binding.setCallForwardingSwitch.setOnClickListener(View.OnClickListener {
            var isChecked:Boolean=false

            isChecked=holder.binding.setCallForwardingSwitch.isChecked
            listener.onSwitchClick(holder.binding.setCallForwardingSwitch, position,isChecked)

        })


        holder.binding.autoRenewSwitch.setOnClickListener(View.OnClickListener {
            var isChecked:Boolean=false

            isChecked=holder.binding.autoRenewSwitch.isChecked
            listener.onSwitchClick(holder.binding.autoRenewSwitch, position,isChecked)
        })

    }

    class MyViewHolder(val binding: MyNumbersRecyclerItemsBinding) :
            RecyclerView.ViewHolder(binding.root) {

        fun bind(data: SubscriberDIDResponseDataModel) {
            binding.viewModel = data
            binding.executePendingBindings()
        }
    }



}