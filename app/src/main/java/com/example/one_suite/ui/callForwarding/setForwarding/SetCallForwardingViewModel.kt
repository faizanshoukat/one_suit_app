package com.example.one_suite.ui.callForwarding.setForwarding

import android.content.Context
import android.util.Log
import android.view.View
import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.one_suite.model.*
import com.example.one_suite.model.callForwarding.GetCallForwardingRateListResponseModel
import com.example.one_suite.model.callForwarding.GetCallForwardingRateRequestModel
import com.example.one_suite.model.callForwarding.GetCallForwardingRateResponseModel
import com.example.one_suite.model.callForwarding.setSubscriberDID.SetSubscriberDIDRequestModel
import com.example.one_suite.model.callForwarding.setSubscriberDID.SetSubscriberDIDResponseModel
import com.example.one_suite.model.country.GetCountryDataModel
import com.example.one_suite.model.country.GetCountryModel
import com.example.one_suite.model.subscriberDid.SubscriberDIDResponseDataModel
import com.example.one_suite.network.APIService
import com.example.one_suite.network.RetroInstance
import com.example.one_suite.utils.ClickListener
import com.example.one_suite.utils.Constant
import com.resocoder.databinding.model.subscriberDid.SubscriberDIDRequestModel
import com.resocoder.databinding.model.subscriberDid.SubscriberDIDResponseModel
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SetCallForwardingViewModel : ViewModel(), ClickListener {

    var code = ObservableField<String>("")
    var number = ObservableField<String>("")
    var location = ObservableField<String>("")
    var rate = ObservableField<String>("")

    lateinit var countryListData: MutableLiveData<ArrayList<GetCountryDataModel>>
    lateinit var rateResult: MutableLiveData<GetCallForwardingRateListResponseModel>

    var callForwardingResult = MutableLiveData<Boolean>()

    init {
        countryListData= MutableLiveData()
        rateResult= MutableLiveData()
    }


    fun getCountryListDataObserver() : MutableLiveData<ArrayList<GetCountryDataModel>>{
        return  countryListData;
    }

    fun getRateDataObserver() : MutableLiveData<GetCallForwardingRateListResponseModel>{
        return  rateResult;
    }

    fun getCallForwardingResultDataObserver() : MutableLiveData<Boolean>{
        return  callForwardingResult;
    }


    fun makeCountryApiCall() {

        Log.d("SubscriberApiCall", "country list api call")
        val apiService = RetroInstance.getRetrofitInstance().create(APIService::class.java)
        val call = apiService.getCountryList(Constant.credentials,)

        call.enqueue(object : Callback<GetCountryModel> {
            override fun onResponse(call: Call<GetCountryModel>, response: Response<GetCountryModel>) {

                Log.d("SubscriberApiCall", "response")
                if (response.isSuccessful) {

                    if (response.body() != null) {
                        countryListData.postValue(response.body()!!.ListOfCountries)
                    }else{
                        countryListData.postValue(null)
                    }


                } else {
                    countryListData.postValue(null)
                }
            }

            override fun onFailure(call: Call<GetCountryModel>, t: Throwable) {
                Log.d("SubscriberApiCall", "Failure : ${t.message}")
                countryListData.postValue(null)
            }

        })

    }

    fun makeGetCallForwardingRateApiCall( rateModel: GetCallForwardingRateRequestModel) {

        val apiService = RetroInstance.getRetrofitInstance().create(APIService::class.java)
        val call = apiService.getCallForwardingRate(Constant.credentials, rateModel)

        call.enqueue(object : Callback<GetCallForwardingRateResponseModel> {
            override fun onResponse(call: Call<GetCallForwardingRateResponseModel>, response: Response<GetCallForwardingRateResponseModel>) {

                if (response.isSuccessful) {


                    if (response.body()!=null) {

//                        var account: String = ""
//                        account = response.body()?.RateList?.size.toString()
//                        Log.d("RateApiCall", "Number : $account")

                        var size=response.body()?.RateList?.size
                        Log.d("RateApiCall", "Size : $size")

                        if (size != null) {
                            if (size>0){
                                rateResult.postValue(response.body()?.RateList?.get(0))
                            }else{
                                rateResult.postValue(null)
                            }
                        }else{
                            rateResult.postValue(null)
                        }

                    }

                } else {
                    rateResult.postValue(null)
                }
            }

            override fun onFailure(call: Call<GetCallForwardingRateResponseModel>, t: Throwable) {
                Log.d("RateApiCall", "Failure : ${t.message}")
                rateResult.postValue(null)

            }

        })


    }

    fun makeSetDIDSettingApiCall(didModel: SetSubscriberDIDRequestModel) {

        val apiService = RetroInstance.getRetrofitInstance().create(APIService::class.java)
        val call = apiService.setSubscriberDID(Constant.credentials, didModel)

        call.enqueue(object : Callback<SetSubscriberDIDResponseModel> {
            override fun onResponse(call: Call<SetSubscriberDIDResponseModel>, response: Response<SetSubscriberDIDResponseModel>) {

                if (response.isSuccessful) {

                    callForwardingResult.postValue(true)

                    if (response.body() != null) {

                        var account: String = ""
                        account = response.body()?.RESPONSE.toString()
                        Log.d("DIDApiCall", "Number : $account")


                    }


                } else {
                    callForwardingResult.postValue(false)
                }
            }

            override fun onFailure(call: Call<SetSubscriberDIDResponseModel>, t: Throwable) {
                Log.d("DIDApiCall", "Failure : ${t.message}")
                callForwardingResult.postValue(false)

            }

        })


    }


    override fun onViewClick(view: View, position: Int) {
//        numberData.postValue(numberListData.value?.get(position));
    }

    override fun onRowClick(position: Int) {
        TODO("Not yet implemented")
    }

}