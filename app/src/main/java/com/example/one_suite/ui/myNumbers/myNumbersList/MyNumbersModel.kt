package com.example.one_suite.ui.myNumbers.myNumbersList

data class SetCallerIdRequestModel(
    var ACCOUNT: String,
    var CLI_NUMBER:String,
    var DEFAULT_CLI:Boolean,
)

data class SetCallerIdResponseModel(
    var RESPONSE: String,
)


