package com.example.one_suite.ui.callBack

import android.util.Log
import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.one_suite.model.callBack.SetCallBackRequestModel
import com.example.one_suite.model.callBack.SetCallBackResponseModel
import com.example.one_suite.model.subscriberCustomNumbers.SubscriberCustomNumberRequestModel
import com.example.one_suite.model.subscriberCustomNumbers.SubscriberCustomNumbersResponseDataModel
import com.example.one_suite.model.subscriberCustomNumbers.SubscriberCustomNumbersResponseModel
import com.example.one_suite.model.subscriberDid.SubscriberDIDResponseDataModel
import com.example.one_suite.network.APIService
import com.example.one_suite.network.RetroInstance
import com.example.one_suite.utils.Constant
import com.resocoder.databinding.model.subscriberDid.SubscriberDIDRequestModel
import com.resocoder.databinding.model.subscriberDid.SubscriberDIDResponseModel
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class CallBackViewModel : ViewModel() {

    var callBack = ObservableField<String>("")
    var hideCli = ObservableField<String>("")
    var wantCall = ObservableField<String>("")

    var _txt_toolbarTitle = MutableLiveData("CallBack")
    var txt_toolbarTitle: LiveData<String> = _txt_toolbarTitle


    var recyclerListData: MutableLiveData<ArrayList<SubscriberDIDResponseDataModel>> =
        MutableLiveData()
    var customNumberListData: MutableLiveData<ArrayList<SubscriberCustomNumbersResponseDataModel>> =
        MutableLiveData()

    var setCallBackresult = MutableLiveData<Boolean>()


    fun getRecyclerListDataObserver(): MutableLiveData<ArrayList<SubscriberDIDResponseDataModel>> {
        return recyclerListData
    }


    fun getCallBackResult(): MutableLiveData<Boolean> {
        return setCallBackresult
    }


    fun getCustomNumbersListDataObserver(): MutableLiveData<ArrayList<SubscriberCustomNumbersResponseDataModel>> {
        return customNumberListData
    }


    fun makeSubscriberDIDApiCall() {

        val subscriberDIDRequest: SubscriberDIDRequestModel =
            SubscriberDIDRequestModel(Constant.account)

        Log.d("SubscriberApiCall", "country list api call")
        val apiService = RetroInstance.getRetrofitInstance().create(APIService::class.java)
        val call = apiService.getSubscriberDID(Constant.credentials, subscriberDIDRequest)

        call.enqueue(object : Callback<SubscriberDIDResponseModel> {
            override fun onResponse(
                call: Call<SubscriberDIDResponseModel>,
                response: Response<SubscriberDIDResponseModel>
            ) {

                Log.d("SubscriberApiCall", "response")
                if (response.isSuccessful) {

                    if (response.body() != null) {

                        recyclerListData.postValue(response.body()!!.SubscriberDIDs)
//                        var responseModel:SubscriberProfileResponseModel= response.body()!!
                    } else {
                        recyclerListData.postValue(null)
                    }


                } else {
                    recyclerListData.postValue(null)
                }
            }

            override fun onFailure(call: Call<SubscriberDIDResponseModel>, t: Throwable) {
                Log.d("SubscriberApiCall", "Failure : ${t.message}")
                recyclerListData.postValue(null)
            }

        })

    }

    fun makeSetCallBackApiCall(requestModel: SetCallBackRequestModel) {
        Log.d("CallbackApiCall", "country list api call")
        val apiService = RetroInstance.getRetrofitInstance().create(APIService::class.java)
        val call = apiService.setCallBack(Constant.credentials, requestModel)

        call.enqueue(object : Callback<SetCallBackResponseModel> {
            override fun onResponse(
                call: Call<SetCallBackResponseModel>,
                response: Response<SetCallBackResponseModel>
            ) {

//                Log.d("CallbackApiCall", "response : ${response.body()?.RESPONSE}")
//                Log.d("CallbackApiCall", "response Code : ${response.body()?.ResponseCode}")

                if (response.isSuccessful) {


                    if (response.body() != null) {
                        setCallBackresult.postValue(true)
                    } else {
                        setCallBackresult.postValue(false)
                    }


                } else {
                    setCallBackresult.postValue(false)
                }
            }

            override fun onFailure(call: Call<SetCallBackResponseModel>, t: Throwable) {
                Log.d("CallbackApiCall", "Failure : ${t.message}")
                setCallBackresult.postValue(false)

            }

        })

    }

    fun makeSubscriberCustomNumbersApiCall() {


        val subscriberCustomNumberRequestModel: SubscriberCustomNumberRequestModel =
            SubscriberCustomNumberRequestModel(Constant.account)

        val apiService = RetroInstance.getRetrofitInstance().create(APIService::class.java)
        val call = apiService.getSubscriberCustomNumbers(
            Constant.credentials,
            subscriberCustomNumberRequestModel
        )

        call.enqueue(object : Callback<SubscriberCustomNumbersResponseModel> {
            override fun onResponse(
                call: Call<SubscriberCustomNumbersResponseModel>,
                response: Response<SubscriberCustomNumbersResponseModel>
            ) {

                if (response.isSuccessful) {

                    if (response.body() != null) {

                        customNumberListData.postValue(response.body()!!.Numbers)
                    } else {
                        customNumberListData.postValue(null)
                    }


                } else {
                    customNumberListData.postValue(null)
                }
            }

            override fun onFailure(call: Call<SubscriberCustomNumbersResponseModel>, t: Throwable) {
                Log.d("NumberApiCall", "Failure : ${t.message}")
                customNumberListData.postValue(null)

            }

        })


    }

}