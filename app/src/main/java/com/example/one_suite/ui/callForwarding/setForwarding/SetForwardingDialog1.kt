package com.example.one_suite.ui.callForwarding.setForwarding

import android.app.ProgressDialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.AdapterView
import android.widget.ImageView
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.one_suite.MainActivity
import com.example.one_suite.R
import com.example.one_suite.databinding.BuyNumberFragmentBinding
import com.example.one_suite.databinding.SetForwardingDialogBinding
import com.example.one_suite.model.callForwarding.GetCallForwardingRateRequestModel
import com.example.one_suite.model.callForwarding.setSubscriberDID.SetSubscriberDIDRequestModel
import com.example.one_suite.ui.buyNumber.BuyNumberViewModel
import com.example.one_suite.ui.buyNumber.SearchTelephoneNumberDataModel
import com.example.one_suite.utils.Constant
import com.example.one_suite.utils.customSpiner.CustomSpinerAdapter
import com.example.one_suite.utils.customSpiner.Data
import com.resocoder.databinding.utils.DialogUtils

//import kotlinx.android.synthetic.main.dialog_setforwording.view.*

class SetForwardingDialog1 : DialogFragment(), AdapterView.OnItemSelectedListener {

    var countydataModel: MutableList<Data> = mutableListOf<Data>()


    //    private lateinit var galleryViewModel: GalleryViewModel
    private lateinit var setForwardingViewModel: SetCallForwardingViewModel
    private var _binding: SetForwardingDialogBinding? = null
    private val binding get() = _binding!!

    lateinit var progressDialog: ProgressDialog

    var didNumber=""
    var fragmentName=""

    var rowPosition=-1

    companion object {

        const val TAG = "CustomDialogFragment"

        private const val KEY_NUMBER = "Number"
        private const val KEY_NAME = "Name"


        //take the title and subtitle form the Activity
        fun newInstance(number: String,fragmentName:String): SetForwardingDialog1 {
            val args = Bundle()
            args.putString(KEY_NUMBER, number)
            args.putString(KEY_NAME,fragmentName)

            val fragment = SetForwardingDialog1()
            fragment.arguments = args

            return fragment
        }
    }



    //creating the Dialog Fragment.
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        setForwardingViewModel = ViewModelProvider(this).get(SetCallForwardingViewModel::class.java)

        _binding = SetForwardingDialogBinding.inflate(inflater, container, false)
        val root: View = binding.root


        //getting data
        didNumber = arguments?.getString(KEY_NUMBER)!!
        fragmentName=arguments?.getString(KEY_NAME)!!


        //for click listener
        progressDialog = ProgressDialog(getContext())
        progressDialog.setMessage("Please wait...")



        val countrySpinner = _binding!!.countrySpinner
        val customAdapter = CustomSpinerAdapter()

        _binding!!.closeDialog.setOnClickListener(View.OnClickListener {
            dialog?.dismiss()
        })


        //API Call
        progressDialog.show()
        setForwardingViewModel.makeCountryApiCall()


        //Country API Implementation
        setForwardingViewModel.getCountryListDataObserver().observe(viewLifecycleOwner, Observer {
            progressDialog.dismiss()

            if (it != null) {

                for (item in it) {
                    countydataModel.add(Data(item.Code.toInt(), item.Name))
                }

                customAdapter.customeSpinnerAdapter(requireContext(), countydataModel)
                countrySpinner.adapter = customAdapter
            }
        })


        countrySpinner!!.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, p3: Long) {
                if (parent!=null){
                    if (countydataModel.size>0) {
                        Log.d("SpinnerValue","value :${countydataModel[position].country}")
                        //set value in country code
                        _binding!!.code.text="+"+countydataModel[position].id.toString()
                    }
                }else{
                    Log.d("SpinnerValue","parent null")
                }

            }

            override fun onNothingSelected(p0: AdapterView<*>?) {
                TODO("Not yet implemented")
            }

        }

        _binding!!.number.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                Log.d("OnTextChangeIssue", "on text changed start: $start  before:$before  after:$")


                if (start == 2){
                    Log.d("OnTextChangeIssue", "count 5: $s")

                    var number=_binding!!.code.text.toString()+s.toString()

                    progressDialog.show()
                    var requestModel= GetCallForwardingRateRequestModel(number,"","","")
                    setForwardingViewModel.makeGetCallForwardingRateApiCall(requestModel)

                }else{
                    Log.d("OnTextChangeIssue", "count :$s $start $before $count")
                }
            }

            override fun afterTextChanged(s: Editable?) {
            }

        })

        _binding!!.saveButton.setOnClickListener(View.OnClickListener {

            var accountNumber= Constant.account
//            var didNumber=it
            var IS_AUTO_RENEW=false
            var IS_FORWARDING_ENABLE=true
            var forwardPhoneNumber:String=""+_binding!!.code.text.toString()+_binding!!.number.text.toString()
            var NumberType=""
            var Location=_binding!!.location.text.toString()


            progressDialog!!.show()
            var requestModel= SetSubscriberDIDRequestModel(accountNumber,didNumber,IS_AUTO_RENEW,IS_FORWARDING_ENABLE,forwardPhoneNumber,NumberType,Location)
            setForwardingViewModel.makeSetDIDSettingApiCall(requestModel)
        })

        setForwardingViewModel.getRateDataObserver().observe(viewLifecycleOwner, Observer {
            progressDialog.dismiss()

            if (it!=null){
                _binding!!.location.text=it.DESCRIPTION
                _binding!!.rate.text="\$"+it.RATE+"/min"
            }
        })

        setForwardingViewModel.getCallForwardingResultDataObserver().observe(viewLifecycleOwner,
            Observer {
                progressDialog.dismiss()
//                val intent = Intent (getActivity(), MainActivity::class.java)
//                getActivity()?.startActivity(intent)

                val intent = Intent(getActivity(), MainActivity::class.java)
                intent.putExtra("Fragment",fragmentName)
                getActivity()?.startActivity(intent)
            })

        return root
    }

    //tasks that need to be done after the creation of Dialog
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
//        setupView(view)
//        setupClickListeners(view)
    }

    override fun onStart() {
        super.onStart()
        dialog?.window?.setLayout(
            WindowManager.LayoutParams.MATCH_PARENT,
            WindowManager.LayoutParams.MATCH_PARENT
        )
        dialog?.getWindow()?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT));

    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {
    }

//    //setting the text in CustomDialog
//    private fun setupView(view: View) {
//
//        var closeButton=view.findViewById<ImageView>(R.id.closeDialog)
//        closeButton.setOnClickListener(View.OnClickListener {
//            dialog?.dismiss()
//        })
//
////        view.img_settingicon.setOnClickListener {
////            dismiss()
////        }
//
////        view.tvTitle.text = arguments?.getString(KEY_TITLE)
////        view.tvSubTitle.text = arguments?.getString(KEY_SUBTITLE)
//    }
//
//    //setting all the click listeners of the CustomDialog
//    private fun setupClickListeners(view: View) {
//
//
//
//        // On clicking the positive/negative button,
//        // the dialog will be closed with the help of dismiss()
///*
//        view.btnPositive.setOnClickListener {
//            dismiss()
//        }
//        view.btnNegative.setOnClickListener {
//            dismiss()
//        }
//*/
//    }

}