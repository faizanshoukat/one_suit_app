package com.example.one_suite.ui.credit.autoRechargeFragment

import android.util.Log
import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.one_suite.model.credits.GetSystemSettingsResponseModel
import com.example.one_suite.model.credits.autoRecharge.AutoRechargeRequestModel
import com.example.one_suite.model.credits.autoRecharge.AutoRechargeResponseModel
import com.example.one_suite.model.credits.autoRecharge.deleteCard.DeleteCardRequestModel
import com.example.one_suite.model.credits.autoRecharge.deleteCard.DeleteCardResponseModel
import com.example.one_suite.model.credits.autoRecharge.paymentInformation.PaymentInformationRequestModel
import com.example.one_suite.model.credits.autoRecharge.paymentInformation.PaymentInformationResponseModel
import com.example.one_suite.network.APIService
import com.example.one_suite.network.RetroInstance
import com.example.one_suite.utils.Constant
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class AutoRechargeViewModel : ViewModel() {
    //var amount = ObservableField<String>("")

    var isAutoRecharge = MutableLiveData<Boolean>(false)
    var isAmountFive = MutableLiveData<Boolean>(false)
    var isAmountTen = MutableLiveData<Boolean>(false)
    var isAmountTwenty = MutableLiveData<Boolean>(false)
    var isAmountThirty = MutableLiveData<Boolean>(false)
    var isAmountOther = MutableLiveData<Boolean>(false)


    var autoRechargeData: MutableLiveData<AutoRechargeResponseModel> = MutableLiveData()

    var addAutoRechargeData: MutableLiveData<AddAutoRechargeResponseModel> = MutableLiveData()
    var updateThresholdResponseData: MutableLiveData<UpdateThresholdResponseModel> = MutableLiveData()

    var systemSettingData: MutableLiveData<GetSystemSettingsResponseModel> = MutableLiveData()
    var deleteCardData: MutableLiveData<DeleteCardResponseModel> = MutableLiveData()


    //Payment Information
    var name = ObservableField<String>("")
    var cardNumber = ObservableField<String>("")
    var expiryDate = ObservableField<String>("")
    var securityCode = ObservableField<String>("")


    var paymentInfoData: MutableLiveData<PaymentInformationResponseModel> = MutableLiveData()


    var enterAmount = ObservableField<String>("")
    var thresholdAmount = ObservableField<String>("")
    var enterAmountVisibility = MutableLiveData<Boolean>(false)
    var thresholdVisibility = MutableLiveData<Boolean>(false)
    var enterAmountMandatoryVisibility = MutableLiveData<Boolean>(false)




    fun setEnterAmountVisibility(value:Boolean) {
        enterAmountVisibility.postValue(value)
    }


    fun setEnterAmountMandatoryVisibility(value:Boolean) {
        enterAmountMandatoryVisibility.postValue(value)
    }

    fun setThresholdVisibility(value:Boolean) {
        thresholdVisibility.postValue(value)
    }


    fun setAmount(value:String) {
        enterAmount.set(value)
    }

    fun setThreshold(value:String) {
        thresholdAmount.set(value)
    }


    fun setIsAutoRecharge(value:Boolean) {
        isAutoRecharge.postValue(value)
    }

    fun setIsAmountFive(value:Boolean) {
        isAmountFive.postValue(value)
    }
    fun setIsAmountTen(value:Boolean) {
        isAmountTen.postValue(value)
    }
    fun setIsAmountTwenty(value:Boolean) {
        isAmountTwenty.postValue(value)
    }
    fun setIsAmountThirty(value:Boolean) {
        isAmountThirty.postValue(value)
    }
    fun setIsAmountOther(value:Boolean) {
        isAmountOther.postValue(value)
    }



    fun getAutoRechargeDataObserver(): MutableLiveData<AutoRechargeResponseModel> {
        return autoRechargeData
    }

    fun getAddAutoRechargeDataObserver(): MutableLiveData<AddAutoRechargeResponseModel> {
        return addAutoRechargeData
    }




    fun getPaymentInfoDataObserver(): MutableLiveData<PaymentInformationResponseModel> {
        return paymentInfoData
    }

    fun getThresholdDataObserver(): MutableLiveData<UpdateThresholdResponseModel> {
        return updateThresholdResponseData
    }

    fun getSystemSettingDataObserver():MutableLiveData<GetSystemSettingsResponseModel>{
        return systemSettingData
    }

    fun getDeleteCardDataObserver():MutableLiveData<DeleteCardResponseModel>{
        return deleteCardData
    }



    fun makeAutoRechargeApiCall() {

        val requestModel= AutoRechargeRequestModel(Constant.account)

        val apiService = RetroInstance.getRetrofitInstance().create(APIService::class.java)
        val call = apiService.getAutoRechargeAmount(Constant.credentials,requestModel)

        call.enqueue(object : Callback<AutoRechargeResponseModel> {
            override fun onResponse(call: Call<AutoRechargeResponseModel>, response: Response<AutoRechargeResponseModel>) {

                Log.d("SubscriberApiCall", "response")
                if (response.isSuccessful) {


                    if (response.body() != null) {
                        autoRechargeData.postValue(response.body())
                    }else{
                        autoRechargeData.postValue(null)
                    }


                } else {
                    autoRechargeData.postValue(null)
                }
            }

            override fun onFailure(call: Call<AutoRechargeResponseModel>, t: Throwable) {
                Log.d("SubscriberApiCall", "Failure : ${t.message}")
                autoRechargeData.postValue(null)

            }

        })

    }

    fun makePaymentInfoApiCall() {

        val requestModel= PaymentInformationRequestModel(Constant.account)

        val apiService = RetroInstance.getRetrofitInstance().create(APIService::class.java)
        val call = apiService.getPaymentInformation(Constant.credentials,requestModel)

        call.enqueue(object : Callback<PaymentInformationResponseModel> {
            override fun onResponse(call: Call<PaymentInformationResponseModel>, response: Response<PaymentInformationResponseModel>) {

                Log.d("SubscriberApiCall", "response")
                if (response.isSuccessful) {


                    if (response.body() != null) {
                        paymentInfoData.postValue(response.body())
                    }else{
                        paymentInfoData.postValue(null)
                    }

                } else {
                    paymentInfoData.postValue(null)
                }
            }

            override fun onFailure(call: Call<PaymentInformationResponseModel>, t: Throwable) {
                Log.d("SubscriberApiCall", "Failure : ${t.message}")
                paymentInfoData.postValue(null)
            }

        })

    }

    fun makeAddAutoRechargeApiCall(requestModel: AddAutoRechargeRequestModel) {


        val apiService = RetroInstance.getRetrofitInstance().create(APIService::class.java)
        val call = apiService.addAutoRecharge(Constant.credentials,requestModel)

        call.enqueue(object : Callback<AddAutoRechargeResponseModel> {
            override fun onResponse(call: Call<AddAutoRechargeResponseModel>, response: Response<AddAutoRechargeResponseModel>) {

                Log.d("SubscriberApiCall", "response")
                if (response.isSuccessful) {


                    if (response.body() != null) {
                        addAutoRechargeData.postValue(response.body())
                    }else{
                        addAutoRechargeData.postValue(null)
                    }


                } else {
                    addAutoRechargeData.postValue(null)
                }
            }

            override fun onFailure(call: Call<AddAutoRechargeResponseModel>, t: Throwable) {
                Log.d("SubscriberApiCall", "Failure : ${t.message}")
                addAutoRechargeData.postValue(null)

            }

        })

    }

    fun makeUpdateThresholdApiCall(requestModel: UpdateThresholdRequestModel) {


        val apiService = RetroInstance.getRetrofitInstance().create(APIService::class.java)
        val call = apiService.updateThreshold(Constant.credentials,requestModel)

        call.enqueue(object : Callback<UpdateThresholdResponseModel> {
            override fun onResponse(call: Call<UpdateThresholdResponseModel>, response: Response<UpdateThresholdResponseModel>) {

                Log.d("SubscriberApiCall", "response")
                if (response.isSuccessful) {


                    if (response.body() != null) {
                        updateThresholdResponseData.postValue(response.body())
                    }else{
                        updateThresholdResponseData.postValue(null)
                    }


                } else {
                    updateThresholdResponseData.postValue(null)
                }
            }

            override fun onFailure(call: Call<UpdateThresholdResponseModel>, t: Throwable) {
                Log.d("SubscriberApiCall", "Failure : ${t.message}")
                updateThresholdResponseData.postValue(null)

            }

        })

    }

    fun makeSystemSettingApiCall() {

        val apiService = RetroInstance.getRetrofitInstance().create(APIService::class.java)
        val call = apiService.getSystemSettings(Constant.credentials)

        call.enqueue(object : Callback<GetSystemSettingsResponseModel> {
            override fun onResponse(call: Call<GetSystemSettingsResponseModel>, response: Response<GetSystemSettingsResponseModel>) {

                Log.d("SubscriberApiCall", "response")
                if (response.isSuccessful) {

                    if (response.body() != null) {

                        systemSettingData.postValue(response.body())
                    }else{
                        systemSettingData.postValue(null)
                    }


                } else {
                    systemSettingData.postValue(null)
                }
            }

            override fun onFailure(call: Call<GetSystemSettingsResponseModel>, t: Throwable) {
                Log.d("SubscriberApiCall", "Failure : ${t.message}")
                systemSettingData.postValue(null)
            }

        })

    }

    fun makeDeleteCardApiCall() {

        var requestModel=DeleteCardRequestModel(Constant.account)

        val apiService = RetroInstance.getRetrofitInstance().create(APIService::class.java)
        val call = apiService.deleteCard(Constant.credentials,requestModel)

        call.enqueue(object : Callback<DeleteCardResponseModel> {
            override fun onResponse(call: Call<DeleteCardResponseModel>, response: Response<DeleteCardResponseModel>) {

                Log.d("SubscriberApiCall", "response")
                if (response.isSuccessful) {

                    if (response.body() != null) {

                        deleteCardData.postValue(response.body())
                    }else{
                        deleteCardData.postValue(null)
                    }


                } else {
                    deleteCardData.postValue(null)
                }
            }

            override fun onFailure(call: Call<DeleteCardResponseModel>, t: Throwable) {
                Log.d("SubscriberApiCall", "Failure : ${t.message}")
                deleteCardData.postValue(null)
            }

        })

    }
}