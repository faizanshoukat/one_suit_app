package com.example.one_suite.ui.myNumbers.addName

import android.app.AlertDialog
import android.app.Dialog
import android.app.ProgressDialog
import android.content.Intent
import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.example.one_suite.MainActivity
import com.example.one_suite.R
import com.example.one_suite.databinding.AddNameFragmentBinding
import com.example.one_suite.databinding.BuyNumberListFragmentBinding
import com.example.one_suite.databinding.MyNumbersListFragmentBinding
import com.example.one_suite.ui.buyNumber.checkOut.AutoRenewNumberRequestModel
import com.example.one_suite.ui.myNumbers.myNumbersList.MyNumbersListViewModel
import com.example.one_suite.utils.Constant
import com.resocoder.databinding.utils.DialogUtils

class AddNameFragment : Fragment() {

    companion object {
        fun newInstance() = AddNameFragment()
    }

    private lateinit var addNameViewModel: AddNameViewModel
    private var _binding: AddNameFragmentBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!


    //    var dialog = DialogUtils
    //var dialog = AlertDialog.Builder(activity?.applicationContext)
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        addNameViewModel = ViewModelProvider(this).get(AddNameViewModel::class.java)

        _binding = AddNameFragmentBinding.inflate(inflater, container, false).apply {
            this.lifecycleOwner = this@AddNameFragment
            this.viewModel = addNameViewModel
        }

        val root: View = binding.root


        //progress dialog
        //var dialog = activity?.let { DialogUtils.setProgressDialog(it.applicationContext, "Please wait...") }
//        var dialog = DialogUtils.setProgressDialog(container!!.context, "Please Wait....Please Wait....")

        val dialog = ProgressDialog(getContext())
        dialog.setMessage("Please wait...")

        dialog.show()
        dialog.dismiss()




        _binding!!.executePendingBindings()
        _binding!!.recyclerView.apply {
            Log.d("SearchTelephoneApiCall", "List size in fragment")
            layoutManager = LinearLayoutManager(activity?.applicationContext)
            val decoration = DividerItemDecoration(
                activity?.applicationContext,
                StaggeredGridLayoutManager.VERTICAL
            )
            //addItemDecoration(decoration)
        }


        //calling API
        dialog.show()
        addNameViewModel.makeSubscriberDIDApiCall()


        addNameViewModel.getMyNumbersListDataObserver()
            .observe(viewLifecycleOwner, androidx.lifecycle.Observer {
                addNameViewModel.setMyNumbersListAdapterData(it)

                dialog.dismiss()

            })


        //Observe name text field
        addNameViewModel.getNameTextObserver().observe(viewLifecycleOwner, Observer {
            Log.d("HideIssue", "Name Text Observer")
            dialog.show()
            addNameViewModel.makeAddNameApiCall(it)
        })

        addNameViewModel.getAddNameResponseDataObserver().observe(
            viewLifecycleOwner, Observer {
                dialog.dismiss()

//                val intent = Intent (getActivity(), MainActivity::class.java)
//                getActivity()?.startActivity(intent)


                val intent = Intent(getActivity(), MainActivity::class.java)
                intent.putExtra("Fragment","My Numbers")
                getActivity()?.startActivity(intent)
            }
        )


        return root;
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        addNameViewModel = ViewModelProvider(this).get(AddNameViewModel::class.java)
        // TODO: Use the ViewModel
    }

}