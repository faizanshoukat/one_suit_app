package com.example.one_suite.ui.callingPlan.azamCode

import android.os.Parcel
import android.os.Parcelable

data class CallingPlanModel(
    val mints: String?,
    val country: String?,
    val des: String?,
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readString(),
        parcel.readString()
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(mints)
        parcel.writeString(country)
        parcel.writeString(des)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<CallingPlanModel> {
        override fun createFromParcel(parcel: Parcel): CallingPlanModel {
            return CallingPlanModel(parcel)
        }

        override fun newArray(size: Int): Array<CallingPlanModel?> {
            return arrayOfNulls(size)
        }
    }
}