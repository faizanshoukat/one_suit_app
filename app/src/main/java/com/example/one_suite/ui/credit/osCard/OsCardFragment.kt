package com.example.one_suite.ui.credit.osCard

import android.app.Dialog
import android.app.ProgressDialog
import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import com.example.one_suite.MainActivity
import com.example.one_suite.R
import com.example.one_suite.databinding.AutoRechargeFragmentBinding
import com.example.one_suite.databinding.OsCardFragmentBinding
import com.example.one_suite.model.credits.osCard.RedeemVoucherRequestModel
import com.example.one_suite.model.credits.osCard.isValidVoucherRequestModel
import com.example.one_suite.ui.credit.autoRechargeFragment.AutoRechargeViewModel
import com.example.one_suite.utils.ClickListener
import com.example.one_suite.utils.Constant
import com.resocoder.databinding.utils.DialogUtils

class OsCardFragment : Fragment(), ClickListener {

    companion object {
        fun newInstance() = OsCardFragment()
    }


    private lateinit var osCardViewModel: OsCardViewModel
    private var _binding: OsCardFragmentBinding? = null
    private val binding get() = _binding!!


    lateinit var confirmationDialog: Dialog;
    lateinit var validDialog: Dialog;
    lateinit var progressDialog: ProgressDialog;

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        osCardViewModel = ViewModelProvider(this).get(OsCardViewModel::class.java)

        _binding = OsCardFragmentBinding.inflate(inflater, container, false).apply {
            this.lifecycleOwner = this@OsCardFragment
            this.viewModel = osCardViewModel
        }
        val root: View = binding.root


        if (activity!=null) {
            progressDialog = DialogUtils.progressDialog(requireActivity())
        }

        _binding!!.doneButton.setOnClickListener(View.OnClickListener {
            Log.d("OsCardIssue", "Done Button")
            var voucherNumber = _binding!!.voucherNumber.text.toString()

            if (voucherNumber.trim().isEmpty()){
                osCardViewModel.setVoucherNumberMandatoryVisibility(true)
            }else{
                osCardViewModel.setVoucherNumberMandatoryVisibility(false)

                progressDialog.show()
                var requestModel = isValidVoucherRequestModel(voucherNumber)
                osCardViewModel.makeIsValidVoucherApiCall(requestModel)

            }




        })


        /*.............Data Observer ..........................*/
        osCardViewModel.getIsValidVoucherDataObserver().observe(viewLifecycleOwner, Observer {
            progressDialog.dismiss()
            if (activity != null) {

                if (it.Status==-1) {
                    confirmationDialog =
                        DialogUtils.getVoucherConfirmationDialog(requireActivity(), "Invalid!!!","Invalid os voucher number",this)
                    confirmationDialog.show()
                }else if (it.Status==1){
                    var message="Your account will be recharged with \$${it.VoucherAmount}."
                    validDialog =
                        DialogUtils.getValidVoucherDialog(requireActivity(), this,message)
                    validDialog.show()
                }

            }
        })

        osCardViewModel.getRedeemVoucherDataObserver().observe(viewLifecycleOwner, Observer {
            progressDialog.dismiss()

            (activity as MainActivity?)?.displaySelectedScreen("Dashboard")
        })

        return root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        osCardViewModel = ViewModelProvider(this).get(OsCardViewModel::class.java)
        // TODO: Use the ViewModel
    }

    override fun onViewClick(view: View, position: Int) {

        if (view.id == R.id.okBtn) {
            confirmationDialog.dismiss()

        }


        if (view.id == R.id.validCancelBtn) {
            validDialog.dismiss()
        }

        if (view.id == R.id.validOkBtn) {
            validDialog.dismiss()

            /*..........Redeem Voucher API..............*/
            progressDialog.show()
            var voucherNumber=_binding!!.voucherNumber.text.toString()
            var requestModel=RedeemVoucherRequestModel(Constant.account,voucherNumber)
            osCardViewModel.makeRedeemVoucherApiCall(requestModel)

        }


    }

    override fun onRowClick(position: Int) {
    }

}