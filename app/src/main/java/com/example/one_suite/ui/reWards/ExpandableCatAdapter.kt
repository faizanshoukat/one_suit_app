package com.example.one_suite.ui.reWards

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import com.example.one_suite.R
import com.example.one_suite.databinding.CategoryListParantItemBinding
import com.example.one_suite.model.subscriberDid.SubscriberDIDResponseDataModel


//class ExpandableCatAdapter  (var context: Context, private var categories: List<Category> ):
class ExpandableCatAdapter  ():
RecyclerView.Adapter<ExpandableCatAdapter.ViewHolder>() {
    var items = ArrayList<Category>()
    lateinit var setonClickListeners: SetOnClickListner

    fun setClickListener(setonClickListener: SetOnClickListner) {
        setonClickListeners = setonClickListener
    }

    fun setDataList(data: ArrayList<Category>) {
        this.items = data
    }


    //parent view
    inner class ViewHolder( val binding: CategoryListParantItemBinding) : RecyclerView.ViewHolder(binding.root),
        SubCatagoryGridAdapter.SetOnClickListner {
        @SuppressLint("ResourceAsColor")
        fun bind(item: Category){
            binding.category = item
            if(item.isExpanded){
                binding.viewLine.isVisible=true
                binding.layoutParent.setBackgroundResource(R.color.blue)


//                var categoryList:ArrayList<Category> = ArrayList()
////                for ( i in 0..4){ categoryList.add(Category(i,"Category Name","image url")) }
//                categoryList.add(Category(1,"Total Available Points","5"))
//                categoryList.add(Category(2,"Added In the last Three Days","2"))
//                categoryList.add(Category(2,"Category sss","2"))


                var catAdapter = SubCatagoryGridAdapter(item.suCatagories!!)
                binding.recyclerViewSubCatagory.adapter = catAdapter
                catAdapter.setClickListener(this)
                binding.layoutParentBack.setBackgroundResource(R.drawable.blue_round_corner)
                binding.imgExpand.setImageResource(R.drawable.ic_negative)
                binding.textViewCatName.setTextColor(Color.WHITE)

            }
            else {
                binding.textViewCatName.setTextColor(R.color.blue)
                binding.imgExpand.setImageResource(R.drawable.ic_baseline_add_24)

                binding.layoutParentBack.setBackgroundResource(R.color.white)
                binding.viewLine.isVisible=false
                binding.layoutParent.setBackgroundResource(R.color.white)
            }
        }


        override fun onClick(testType: SubCatagory, pos: Int) {
            //  TODO("Not yet implemented")
        }
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = CategoryListParantItemBinding.inflate(LayoutInflater.from(parent.context),parent,false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(items[position])
        holder.itemView.setOnClickListener {
            setonClickListeners.onClick(items[position],position)
        }
    }

    fun updateData(categories: ArrayList<Category>) {
        this.items = categories
        notifyDataSetChanged()
    }

    override fun getItemCount() = items.size
    interface SetOnClickListner {
        fun onClick(testType: Category, pos:Int)
    }

}