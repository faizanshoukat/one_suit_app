package com.example.one_suite.ui.setting.updateProfile

import android.util.Log
import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.one_suite.model.country.GetCountryDataModel
import com.example.one_suite.model.country.GetCountryModel
import com.example.one_suite.model.credits.autoRecharge.paymentInformation.PaymentInformationRequestModel
import com.example.one_suite.model.credits.autoRecharge.paymentInformation.PaymentInformationResponseModel
import com.example.one_suite.model.credits.autoRecharge.paymentInformation.setPaymentInfo.SetPaymentInfoRequestModel
import com.example.one_suite.model.credits.autoRecharge.paymentInformation.setPaymentInfo.SetPaymentInfoResponseModel
import com.example.one_suite.model.setting.profile.SubscriberProfileRequestModel
import com.example.one_suite.model.setting.profile.SubscriberProfileResponseModel
import com.example.one_suite.network.APIService
import com.example.one_suite.network.RetroInstance
import com.example.one_suite.utils.Constant
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class UpdateProfileViewModel : ViewModel() {
    var accountNumber = ObservableField<String>("")

    var firstName = ObservableField<String>("")
    var lastName = ObservableField<String>("")
    var email = ObservableField<String>("")
    var contactNumber = ObservableField<String>("")

    var address1 = ObservableField<String>("")
    var address2 = ObservableField<String>("")
    var city = ObservableField<String>("")
    var state = ObservableField<String>("")
    var country = ObservableField<String>("")
    var zipCode = ObservableField<String>("")


    var profileData:MutableLiveData<SubscriberProfileResponseModel> = MutableLiveData()
    var countryList:MutableLiveData<ArrayList<GetCountryDataModel>> = MutableLiveData()
    var result= MutableLiveData<Boolean>()


    var paymentInfoData: MutableLiveData<PaymentInformationResponseModel> = MutableLiveData()
    var setPaymentInfoResult:MutableLiveData<String> =MutableLiveData()


    fun getProfileDataObserver():MutableLiveData<SubscriberProfileResponseModel>{
        return profileData
    }

    fun getCountryListDataObserver():MutableLiveData<ArrayList<GetCountryDataModel>>{
        return countryList
    }

    fun getProfileResult():MutableLiveData<Boolean>{
        return result
    }

    fun getPaymentInfoDataObserver(): MutableLiveData<PaymentInformationResponseModel> {
        return paymentInfoData
    }

    fun getSetPaymentInfoResultObserver():MutableLiveData<String>{
        return setPaymentInfoResult
    }


    fun makeCountryListApiCall() {
        Log.d("CountryApiCall", "country list api call")
        val apiService = RetroInstance.getRetrofitInstance().create(APIService::class.java)
        val call = apiService.getCountryList(Constant.credentials)

        call.enqueue(object : Callback<GetCountryModel> {
            override fun onResponse(call: Call<GetCountryModel>, response: Response<GetCountryModel>) {

                Log.d("CountryApiCall", "response")
                if (response.isSuccessful) {

                    if (response.body()!=null) {
                        countryList.postValue(response.body()!!.ListOfCountries)
                    }else{
                        countryList.postValue(null)
                    }


                } else {
                    countryList.postValue(null)
                }
            }

            override fun onFailure(call: Call<GetCountryModel>, t: Throwable) {
                Log.d("CountryApiCall", "Failure : ${t.message}")
                countryList.postValue(null)
            }

        })

    }

    fun makeProfileApiCall( profileModel: SubscriberProfileRequestModel) {
        Log.d("ProfileApiCall", "profile Model : "+profileModel.ACCOUNT)

        val apiService = RetroInstance.getRetrofitInstance().create(APIService::class.java)
        val call = apiService.getSubscriberProfile(Constant.credentials, profileModel)

        call.enqueue(object : Callback<SubscriberProfileResponseModel> {
            override fun onResponse(call: Call<SubscriberProfileResponseModel>, response: Response<SubscriberProfileResponseModel>) {

                if (response.isSuccessful) {

//                    result.postValue(true)

//                    makeCountryListApiCall()

                    if (response.body()!=null) {


                        profileData.postValue(response.body())


//                        var responseModel:SubscriberProfileResponseModel= response.body()!!
//                        Log.d("ProfileApiCall", "name : ${responseModel.FIRST_NAME}")
//
//                        firstName.set(responseModel.FIRST_NAME)
//                        lastName.set(responseModel.LAST_NAME)
//                        email.set(responseModel.E_MAIL)
//                        contactNumber.set(responseModel.LOCAL_PHONE)
//
//                        address1.set(responseModel.ADDRESS1)
//                        address2.set(responseModel.ADDRESS2)
//                        city.set(responseModel.CITY)
//                        state.set(responseModel.STATE_REGION)
//
//                        country.set(responseModel.COUNTRY)
//                        zipCode.set(responseModel.POSTAL_CODE)

                    }else{
                        profileData.postValue(null)
                    }


                } else {
                    // result.postValue(false)
//                    makeCountryListApiCall()
                    profileData.postValue(null)
                }
            }

            override fun onFailure(call: Call<SubscriberProfileResponseModel>, t: Throwable) {
                Log.d("ProfileApiCall", "Failure : ${t.message}")
                profileData.postValue(null)
                //result.postValue(false)

//                makeCountryListApiCall()
            }

        })


    }

    fun makePaymentInfoApiCall() {

        val requestModel= PaymentInformationRequestModel(Constant.account)

        val apiService = RetroInstance.getRetrofitInstance().create(APIService::class.java)
        val call = apiService.getPaymentInformation(Constant.credentials,requestModel)

        call.enqueue(object : Callback<PaymentInformationResponseModel> {
            override fun onResponse(call: Call<PaymentInformationResponseModel>, response: Response<PaymentInformationResponseModel>) {

                Log.d("SubscriberApiCall", "response")
                if (response.isSuccessful) {


                    if (response.body() != null) {
                        paymentInfoData.postValue(response.body())
                    }else{
                        paymentInfoData.postValue(null)
                    }

                } else {
                    paymentInfoData.postValue(null)
                }
            }

            override fun onFailure(call: Call<PaymentInformationResponseModel>, t: Throwable) {
                Log.d("SubscriberApiCall", "Failure : ${t.message}")
                paymentInfoData.postValue(null)
            }

        })

    }


    fun makeSetPaymentInfoApiCall( requestModel: SetPaymentInfoRequestModel) {

        val apiService = RetroInstance.getRetrofitInstance().create(APIService::class.java)
        val call = apiService.setPaymentInformation(Constant.credentials,requestModel)

        call.enqueue(object : Callback<SetPaymentInfoResponseModel> {
            override fun onResponse(call: Call<SetPaymentInfoResponseModel>, response: Response<SetPaymentInfoResponseModel>) {

                if (response.isSuccessful) {

                    //result.postValue(true)

                    if (response.body()!=null) {

                        var responseModel:SetPaymentInfoResponseModel= response.body()!!
                        Log.d("ProfileApiCall", "name : ${responseModel.RESPONSE}")

                        makePaymentInfoApiCall()
                    }


                } else {
                    result.postValue(false)
                }
            }

            override fun onFailure(call: Call<SetPaymentInfoResponseModel>, t: Throwable) {
                Log.d("ProfileApiCall", "Failure : ${t.message}")
                result.postValue(false)

            }

        })


    }


//    fun makeSetProfileApiCall( setProfileModel: SetProfileRequestModel, getProfileModel: SubscriberProfileRequestModel) {
//        Log.d("ProfileApiCall", "Set Profile Model : "+setProfileModel.ACCOUNT)
//
//        Log.d("ProfileApiCall", "name set : ${setProfileModel.FIRST_NAME}")
//
//        val apiService = RetroInstance.getRetrofitInstance().create(APIService::class.java)
//        val call = apiService.setSubscriberProfile(Constant.credentials, setProfileModel)
//
//        call.enqueue(object : Callback<SetProfileResponseModel> {
//            override fun onResponse(call: Call<SetProfileResponseModel>, response: Response<SetProfileResponseModel>) {
//
//                if (response.isSuccessful) {
//
//                    //result.postValue(true)
//
//                    if (response.body()!=null) {
//
//                        var responseModel:SetProfileResponseModel= response.body()!!
//                        Log.d("ProfileApiCall", "name : ${responseModel.RESPONSE}")
//
//                        makePaymentInfoApiCall()
//                    }
//
//
//                } else {
//                    result.postValue(false)
//                }
//            }
//
//            override fun onFailure(call: Call<SetProfileResponseModel>, t: Throwable) {
//                Log.d("ProfileApiCall", "Failure : ${t.message}")
//                result.postValue(false)
//
//            }
//
//        })
//
//
//    }

}