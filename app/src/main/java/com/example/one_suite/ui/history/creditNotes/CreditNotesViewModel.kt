package com.example.one_suite.ui.history.creditNotes

import android.content.Context
import android.util.Log
import android.view.View
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.one_suite.R
import com.example.one_suite.model.callForwarding.setSubscriberDID.SetSubscriberDIDRequestModel
import com.example.one_suite.model.callForwarding.setSubscriberDID.SetSubscriberDIDResponseModel
import com.example.one_suite.model.subscriberDid.SubscriberDIDResponseDataModel
import com.example.one_suite.network.APIService
import com.example.one_suite.network.RetroInstance
import com.example.one_suite.ui.callForwarding.CallForwardingAdapter
import com.example.one_suite.ui.home.GetSubscriberPromotionRequestModel
import com.example.one_suite.ui.home.GetSubscriberPromotionResponseDataModel
import com.example.one_suite.ui.home.GetSubscriberPromotionResponseModel
import com.example.one_suite.utils.Constant
import com.example.one_suite.utils.SwitchClickListener
import com.resocoder.databinding.model.subscriberDid.SubscriberDIDRequestModel

import com.resocoder.databinding.model.subscriberDid.SubscriberDIDResponseModel
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class CreditNotesViewModel() : ViewModel() {

    var creditNotesListdata: MutableLiveData<ArrayList<CreditNotesResponseDataModel>> = MutableLiveData()
    var recyclerViewAdapter= CreditNotesAdapter()


    fun setAdapterData(data: ArrayList<CreditNotesResponseDataModel>) {
        recyclerViewAdapter.setDataList(data)
        recyclerViewAdapter.notifyDataSetChanged()
    }


    fun getAdapter(): CreditNotesAdapter {
        return recyclerViewAdapter
    }

    fun getCreditNotesListDataObserver():MutableLiveData<ArrayList<CreditNotesResponseDataModel>>{
        return creditNotesListdata
    }


    fun makeCreditNotesApiCall() {


        val requestModel: CreditNotesRequestModel = CreditNotesRequestModel(Constant.account)

        Log.d("SubscriberApiCall", "country list api call")
        val apiService = RetroInstance.getRetrofitInstance().create(APIService::class.java)
        val call = apiService.getSubscriberCreditNotesApiCall(Constant.credentials, requestModel)

        call.enqueue(object : Callback<CreditNotesResponseModel> {
            override fun onResponse(call: Call<CreditNotesResponseModel>, response: Response<CreditNotesResponseModel>) {

                Log.d("SubscriberApiCall", "response")
                if (response.isSuccessful) {



                    if (response.body() != null) {
                        creditNotesListdata.postValue(response.body()!!.List)
                    }else{
                        creditNotesListdata.postValue(null)
                    }


                } else {
                    creditNotesListdata.postValue(null)
                }
            }

            override fun onFailure(call: Call<CreditNotesResponseModel>, t: Throwable) {
                Log.d("SubscriberApiCall", "Failure : ${t.message}")
                creditNotesListdata.postValue(null)

            }

        })

    }

}
