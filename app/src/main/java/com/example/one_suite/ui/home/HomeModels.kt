package com.example.one_suite.ui.home


//get account balance
class GetBalanceRequestModel {
    var ACCOUNT:String

    constructor(ACCOUNT: String) {
        this.ACCOUNT = ACCOUNT
    }
}


class GetBalanceResponseModel {
    var BALANCE:String
    var CURRENCY:String
    var THRESHOLD_AMOUNT:Double

    constructor(BALANCE: String, CURRENCY: String, THRESHOLD_AMOUNT: Double) {
        this.BALANCE = BALANCE
        this.CURRENCY = CURRENCY
        this.THRESHOLD_AMOUNT = THRESHOLD_AMOUNT
    }
}



//call history models
class GetSubscriberCallHistoryRequestModel {
    var ACCOUNT:String
    var FROM_DATE:String
    var TO_DATE:String
    var PAGE_SIZE:Int
    var PAGE_NUMBER:Int

    constructor(ACCOUNT: String, FROM_DATE: String, TO_DATE: String, PAGE_SIZE: Int, PAGE_NUMBER: Int) {
        this.ACCOUNT = ACCOUNT
        this.FROM_DATE = FROM_DATE
        this.TO_DATE = TO_DATE
        this.PAGE_SIZE = PAGE_SIZE
        this.PAGE_NUMBER = PAGE_NUMBER
    }
}

class GetSubscriberCallHistoryResponseModel {
    var TOTAL_COUNT:Int
    var HISTORY:ArrayList<GetSubscriberCallHistoryResponseDataModel>
    var PAGE_NUMBER:Int
    var PAGE_SIZE:Int

    constructor(TOTAL_COUNT: Int, HISTORY: ArrayList<GetSubscriberCallHistoryResponseDataModel>, PAGE_NUMBER: Int, PAGE_SIZE: Int) {
        this.TOTAL_COUNT = TOTAL_COUNT
        this.HISTORY = HISTORY
        this.PAGE_NUMBER = PAGE_NUMBER
        this.PAGE_SIZE = PAGE_SIZE
    }
}




class GetSubscriberCallHistoryResponseDataModel {
    var StartTime:String
    var EndTime:String
    var DESTINATION:String
    var DURATION:String
    var DATE:String
    var CHARGE:Double
    var DESCRIPTION:String
    var RatePerMinute:Double

    constructor(StartTime: String, EndTime: String, DESTINATION: String, DURATION: String, DATE: String, CHARGE: Double, DESCRIPTION: String, RatePerMinute: Double) {
        this.StartTime = StartTime
        this.EndTime = EndTime
        this.DESTINATION = DESTINATION
        this.DURATION = DURATION
        this.DATE = DATE
        this.CHARGE = CHARGE
        this.DESCRIPTION = DESCRIPTION
        this.RatePerMinute = RatePerMinute
    }
}


//custom model for My History
class CustomMyHistoryHomeModel{
    var number:String
    var destination:String
    var lastValue:String
    var status:Boolean
    var switchVisibility:Boolean


    constructor(
        number: String,
        destination: String,
        lastValue: String,
        status: Boolean,
        switchVisibility: Boolean
    ) {
        this.number = number
        this.destination = destination
        this.lastValue = lastValue
        this.status = status
        this.switchVisibility = switchVisibility
    }

}


//Calling Plan History
data class GetSubscriberPromotionRequestModel(var ACCOUNT:String)

data class GetSubscriberPromotionResponseModel(var Response:String,var Promotions:ArrayList<GetSubscriberPromotionResponseDataModel>)

data class GetSubscriberPromotionResponseDataModel(var Price:Double, var BundleName:String,var ActivationDate:String,var ExpirationDate:String,var TotalMinutes:Double,var RemainingMinutes:String)