package com.example.one_suite.ui.callingPlan.callingPlans

import android.content.Context
import android.util.Log
import android.view.View
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.one_suite.R
import com.example.one_suite.model.callForwarding.setSubscriberDID.SetSubscriberDIDRequestModel
import com.example.one_suite.model.callForwarding.setSubscriberDID.SetSubscriberDIDResponseModel
import com.example.one_suite.model.subscriberDid.SubscriberDIDResponseDataModel
import com.example.one_suite.network.APIService
import com.example.one_suite.network.RetroInstance
import com.example.one_suite.utils.ClickListener
import com.example.one_suite.utils.Constant
import com.example.one_suite.utils.SwitchClickListener
import com.resocoder.databinding.model.subscriberDid.SubscriberDIDRequestModel

import com.resocoder.databinding.model.subscriberDid.SubscriberDIDResponseModel
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class CallingPlansViewModel() : ViewModel(), ClickListener {

    var recyclerListData: MutableLiveData<ArrayList<GetCallingPlansDataModel>> = MutableLiveData()
    var recyclerViewAdapter: CallingPlansAdapter = CallingPlansAdapter(this)

    var buyPlanItemPosition = MutableLiveData<Int>()



    fun getBuyPlanItemPositionObserver(): MutableLiveData<Int> {
        return buyPlanItemPosition;
    }

    fun getAdapter(): CallingPlansAdapter {
        return recyclerViewAdapter
    }

    fun setAdapterData(data: ArrayList<GetCallingPlansDataModel>) {
        recyclerViewAdapter.setDataList(data)
        recyclerViewAdapter.notifyDataSetChanged()
    }


    fun getRecyclerListDataObserver(): MutableLiveData<ArrayList<GetCallingPlansDataModel>> {
        return recyclerListData
    }


    fun makeCallingPlansApiCall() {

        val apiService = RetroInstance.getRetrofitInstance().create(APIService::class.java)
        val call = apiService.getCallingPlans(Constant.credentials)

        call.enqueue(object : Callback<GetCallingPlansModel> {
            override fun onResponse(call: Call<GetCallingPlansModel>, response: Response<GetCallingPlansModel>) {

                Log.d("SubscriberApiCall", "response")
                if (response.isSuccessful) {

                    if (response.body() != null) {
                        recyclerListData.postValue(response.body()!!.PROMOTIONS)
                    }else{
                        recyclerListData.postValue(null)
                    }


                } else {
                    recyclerListData.postValue(null)
                }
            }

            override fun onFailure(call: Call<GetCallingPlansModel>, t: Throwable) {
                Log.d("SubscriberApiCall", "Failure : ${t.message}")
                recyclerListData.postValue(null)
            }

        })

    }


    override fun onViewClick(view: View, position: Int) {
        if (view.id==R.id.buyCallingPlan){
            buyPlanItemPosition.postValue(position)
        }
    }

    override fun onRowClick(position: Int) {
        TODO("Not yet implemented")
    }

}
