package com.example.one_suite.ui.setting.changePassword

import androidx.databinding.ObservableField
import androidx.lifecycle.ViewModel

class ChangePasswordViewModel : ViewModel() {

    var accountNumber = ObservableField<String>("")
    var oldPassword=ObservableField<String>("")
    var newPassword=ObservableField<String>("")
    var confirmPassword=ObservableField<String>("")
}