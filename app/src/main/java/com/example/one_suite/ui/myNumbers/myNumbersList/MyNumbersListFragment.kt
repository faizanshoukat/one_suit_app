package com.example.one_suite.ui.myNumbers.myNumbersList

import android.app.AlertDialog
import android.app.Dialog
import android.app.ProgressDialog
import android.content.Intent
import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.example.one_suite.MainActivity
import com.example.one_suite.R
import com.example.one_suite.databinding.BuyNumberListFragmentBinding
import com.example.one_suite.databinding.MyNumbersListFragmentBinding
import com.example.one_suite.model.callForwarding.setSubscriberDID.SetSubscriberDIDRequestModel
import com.example.one_suite.ui.buyNumber.checkOut.AutoRenewNumberRequestModel
import com.example.one_suite.ui.callForwarding.setForwarding.SetForwardingDialog
import com.example.one_suite.ui.callForwarding.setForwarding.SetForwardingDialog1
import com.example.one_suite.utils.Constant
import com.resocoder.databinding.utils.DialogUtils

class MyNumbersListFragment : Fragment() {

    companion object {
        fun newInstance() = MyNumbersListFragment()
    }

    private lateinit var myNumberListViewModel: MyNumbersListViewModel
    private var _binding: MyNumbersListFragmentBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!


    //    var dialog = DialogUtils
    //var dialog = AlertDialog.Builder(activity?.applicationContext)
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        myNumberListViewModel = ViewModelProvider(this).get(MyNumbersListViewModel::class.java)

        _binding = MyNumbersListFragmentBinding.inflate(inflater, container, false).apply {
            this.lifecycleOwner = this@MyNumbersListFragment
            this.viewModel = myNumberListViewModel
        }

        val root: View = binding.root


        //progress dialog
        //var dialog = activity?.let { DialogUtils.setProgressDialog(it.applicationContext, "Please wait...") }
//        var dialog=DialogUtils.setProgressDialog(container!!.context,"Please Wait....Please Wait....")


        val dialog = ProgressDialog(getContext())
        dialog.setMessage("Please wait...")





        _binding!!.executePendingBindings()
        _binding!!.recyclerView.apply {
            Log.d("SearchTelephoneApiCall", "List size in fragment")
            layoutManager = LinearLayoutManager(activity?.applicationContext)
//            val decoration = DividerItemDecoration(
//                activity?.applicationContext,
//                StaggeredGridLayoutManager.VERTICAL
//            )
//            addItemDecoration(decoration)
        }


        //calling API
        dialog.show()
        myNumberListViewModel.makeSubscriberDIDApiCall()


        /* getting data from DID Number API */
        myNumberListViewModel.getMyNumbersListDataObserver().observe(viewLifecycleOwner, Observer {
            dialog.dismiss()
            if (it != null) {
                myNumberListViewModel.setMyNumbersListAdapterData(it)

                var number: String = ""
                //var isVisible=false


//check for value of Caller Id
                for (item in it) {
                    if (item.CallerId == true) {

                        //isVisible=true

                        if (item.Name == null) {
                            number = "+" + item.Number
                        } else {
                            number = "+" + item.Number + " - " + item.Name
                        }

                    } else {
                        //isVisible=false
                    }
                }

                //now set value in view
                if (number.trim().isNotEmpty()) {
                    _binding!!.callerId.visibility = View.VISIBLE
                    _binding!!.callerIdText.visibility = View.VISIBLE

                    _binding!!.callerId.text = number


                } else {
                    _binding!!.callerId.visibility = View.GONE
                    _binding!!.callerIdText.visibility = View.GONE
                }


            }

        })


        /* calling Switch API's */
        myNumberListViewModel.getSwitchCallerIdDataObserver()
            .observe(viewLifecycleOwner, androidx.lifecycle.Observer {
                Log.d("SwitchValue", "Number :$it")
                dialog.show()

                var number = ""
                var callerId = false
                if (it != null) {
                    number = it.Number.toString()
                    callerId = !it.CallerId!!
                }
                var requestModel = SetCallerIdRequestModel(Constant.account, number, callerId);
                myNumberListViewModel.makeSetCallerIdApiCall(requestModel)
            })

        myNumberListViewModel.getSwitchCallForwardingDataObserver()
            .observe(viewLifecycleOwner, androidx.lifecycle.Observer {
                Log.d("SwitchValue", "Number :$it")

                var number = ""
                var callForwarding = false
                if (it != null) {
                    number = it.Number.toString()
                    callForwarding = it.IsForwardingEnable!!

                    if (callForwarding){

                        var accountNumber= Constant.account
                        var didNumber=it.Number
                        var IS_AUTO_RENEW=false
                        var IS_FORWARDING_ENABLE=false
                        var forwardPhoneNumber:String=""
                        var NumberType=""
                        var Location=""

                        dialog!!.show()
                        var requestModel= SetSubscriberDIDRequestModel(accountNumber,didNumber!!,IS_AUTO_RENEW,IS_FORWARDING_ENABLE,forwardPhoneNumber,NumberType,Location)
                        myNumberListViewModel.makeSetDIDSettingApiCall(requestModel)


                    }else{
                        if (activity != null) {
                            SetForwardingDialog1.newInstance(
                                number,
                                "My Numbers"
                            ).show(
                                requireActivity().supportFragmentManager,
                                SetForwardingDialog1.TAG
                            )
                        }
                    }

                }


            })




        myNumberListViewModel.getSwitchAutoRenewDataObserver()
            .observe(viewLifecycleOwner, androidx.lifecycle.Observer {
                Log.d("SwitchValue", "Number :$it")

                dialog.show()

                var number = ""
                var autoRenewNumber = false
                if (it != null) {
                    number = it.Number.toString()
                    autoRenewNumber = !it.AutoRenewNumber!!
                }

                var requestModel =
                    AutoRenewNumberRequestModel(Constant.account, autoRenewNumber, number)

                myNumberListViewModel.makeAutoRenewNumberApiCall(requestModel)
            })



        myNumberListViewModel.getCallForwardingResultDataObserver().observe(viewLifecycleOwner,
            Observer {
                dialog.dismiss()

                Log.d("BundleIssue", "Fragment : My Numbers passing")

                val intent = Intent(getActivity(), MainActivity::class.java)
                intent.putExtra("Fragment","My Numbers")
                getActivity()?.startActivity(intent)


            })

        //getting Switch API's response
        myNumberListViewModel.getSetCallerIdResponseDataObserver()
            .observe(viewLifecycleOwner, androidx.lifecycle.Observer {
                dialog!!.dismiss()

                Log.d("BundleIssue", "Fragment : My Numbers passing")

                val intent = Intent(getActivity(), MainActivity::class.java)
                intent.putExtra("Fragment","My Numbers")
                getActivity()?.startActivity(intent)
            })


        myNumberListViewModel.getAutoRenewResponseDataObserver()
            .observe(viewLifecycleOwner, androidx.lifecycle.Observer {
                dialog!!.dismiss()

                Log.d("BundleIssue", "Fragment : My Numbers passing")

                val intent = Intent(getActivity(), MainActivity::class.java)
                intent.putExtra("Fragment","My Numbers")
                getActivity()?.startActivity(intent)
            })



        return root;
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        myNumberListViewModel = ViewModelProvider(this).get(MyNumbersListViewModel::class.java)
        // TODO: Use the ViewModel
    }

}