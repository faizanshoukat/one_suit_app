package com.example.one_suite.ui.reWards




import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.one_suite.databinding.ActivityRewardChildBinding

class RewardChildActivity : Fragment(), ExpandableCatAdapter.SetOnClickListner {

    private var _binding: ActivityRewardChildBinding? = null
    var categoryList:ArrayList<Category> = ArrayList()
    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!
    private lateinit var catAdapter: ExpandableCatAdapter


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        //setContentView(R.layout.activity_main)


        _binding = ActivityRewardChildBinding.inflate(inflater, container, false).apply {
            this.lifecycleOwner = this@RewardChildActivity
        }

        var childArrayList=ArrayList<SubCatagory>()
        childArrayList.add(SubCatagory(1,"child 1","img 1"))



        categoryList.add(Category(1,"Parent 1 ","image url",childArrayList))
        categoryList.add(Category(2,"Parent  2 ","image url"))

        catAdapter.setDataList(categoryList)

        catAdapter = ExpandableCatAdapter()
        binding.recyclerViewExpCategory.adapter = catAdapter
        catAdapter.setClickListener(this)


        val root: View = _binding!!.root
        return root
    }

    override fun onClick(testType: Category, pos: Int) {
        //TODO("Not yet implemented")
        if(categoryList[pos].isExpanded) {
            categoryList[pos].isExpanded = false

        }else {
            categoryList[pos].isExpanded = true
        }
        catAdapter.updateData(categoryList)
    }
}