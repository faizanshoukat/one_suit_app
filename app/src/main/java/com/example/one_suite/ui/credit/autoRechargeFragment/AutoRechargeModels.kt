package com.example.one_suite.ui.credit.autoRechargeFragment

data class AddAutoRechargeRequestModel(
    var ACCOUNT:String,
    var AMOUNT:Double,
    var IS_AUTO_RENEW:Boolean
)

data class AddAutoRechargeResponseModel(
    var RESPONSE:String,
)


//threshold model
data class UpdateThresholdRequestModel(
    var Account:String,
    var Value:Double
)

data class UpdateThresholdResponseModel(
    var ResponseCode:String,
    var PersonalThresholdUpdated:Double
)
