package com.example.one_suite.ui.credit

import android.graphics.Color
import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import com.android.navigationtesting.Category1Fragment
import com.example.one_suite.R
import com.example.one_suite.ui.buyNumber.buyNumberList.BuyNumberListFragment
import com.example.one_suite.ui.credit.addBalance.AddBalanceFragment
import com.example.one_suite.ui.credit.autoRechargeFragment.AutoRechargeFragment
import com.example.one_suite.ui.credit.osCard.OsCardFragment
import com.example.one_suite.ui.myNumbers.addName.AddNameFragment
 import com.skyhope.showmoretextview.ShowMoreTextView

class CreditFragment : Fragment() {

    companion object {
        fun newInstance() = CreditFragment()
    }

    //private lateinit var viewModel: MyNumbersViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {


        val view = inflater.inflate(R.layout.credit_fragment, container, false)


        /*..............For Show More And Less Text..........*/
        val textView = view.findViewById<ShowMoreTextView>(R.id.text_view_show_more)
        textView.setShowingChar(30)
        textView.setShowingLine(2)
        textView.addShowMoreText("see more");
        textView.addShowLessText("Less");

        textView.setShowMoreColor(Color.RED); // or other color
        textView.setShowLessTextColor(Color.RED); // or other color

        /*..............For Show More And Less Text..........*/




        /* added fragment My Numbers by default  */
        attachAddBalanceFragment()

        val addBalance = view.findViewById<TextView>(R.id.add_balance)
        val autoRechargeButton = view.findViewById<TextView>(R.id.auto_recharge)
        val osCardButton = view.findViewById<TextView>(R.id.os_card)


        addBalance.setOnClickListener(View.OnClickListener {
            addBalance.setTextColor(Color.parseColor("#7ec243"))
//            addBalance.textSize = 16F

            autoRechargeButton.setTextColor(Color.parseColor("#959898"))
            osCardButton.setTextColor(Color.parseColor("#959898"))
            attachAddBalanceFragment()
        })


        autoRechargeButton.setOnClickListener(View.OnClickListener {
            autoRechargeButton.setTextColor(Color.parseColor("#7ec243"))
//            autoRechargeButton.textSize = 16F


            addBalance.setTextColor(Color.parseColor("#959898"))
            osCardButton.setTextColor(Color.parseColor("#959898"))
            attachAutoRechargeFragment()
        })

        osCardButton.setOnClickListener(View.OnClickListener {
            osCardButton.setTextColor(Color.parseColor("#7ec243"))
//            osCardButton.textSize = 16F


            addBalance.setTextColor(Color.parseColor("#959898"))
            autoRechargeButton.setTextColor(Color.parseColor("#959898"))
            attachOsCardFragment()
        })

        return view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        //viewModel = ViewModelProvider(this).get(MyNumbersViewModel::class.java)
        // TODO: Use the ViewModel
    }

    private fun attachAddBalanceFragment() {
        //creating fragment object
        var fragment: Fragment? = null


        fragment = AddBalanceFragment()

        //replacing the fragment
        if (fragment != null) {
            val ft = activity?.supportFragmentManager?.beginTransaction()
            ft?.replace(R.id.creditContainer, fragment)
            ft?.commit()
        }


    }


    private fun attachAutoRechargeFragment() {
        //creating fragment object
        var fragment: Fragment? = null


        fragment = AutoRechargeFragment()

        //replacing the fragment
        if (fragment != null) {
            val ft = activity?.supportFragmentManager?.beginTransaction()
            ft?.replace(R.id.creditContainer, fragment)
            ft?.commit()
        }


    }


    private fun attachOsCardFragment() {
        //creating fragment object
        var fragment: Fragment? = null


        fragment = OsCardFragment()

        //replacing the fragment
        if (fragment != null) {
            val ft = activity?.supportFragmentManager?.beginTransaction()
            ft?.replace(R.id.creditContainer, fragment)
            ft?.commit()
        }


    }

}