//package com.example.one_suite.ui.buyNumber.checkOutNew
//
//import android.os.Parcel
//import android.os.Parcelable
//0
//data class BuyNumbersCheckoutModel(
//    var country: String?,
//    var state: String?,
//    var number: String?,
//    var price: String?,
//    var type: String?,
//    var isAutoRenew: Boolean
//) : Parcelable {
//    constructor(parcel: Parcel) : this(
//        parcel.readString()?:"",
//        parcel.readString()?:"",
//        parcel.readString()?:"",
//        parcel.readString()?:"",
//        parcel.readString()?:"",
//        parcel.readByte() != 0.toByte()
//    ) {
//    }
//
//    override fun writeToParcel(parcel: Parcel, flags: Int) {
//        parcel.writeString(country)
//        parcel.writeString(state)
//        parcel.writeString(number)
//        parcel.writeString(price)
//        parcel.writeString(type)
//        parcel.writeByte(if (isAutoRenew) 1 else 0)
//    }
//
//    override fun describeContents(): Int {
//        return 0
//    }
//
//    companion object CREATOR : Parcelable.Creator<BuyNumbersCheckoutModel> {
//        override fun createFromParcel(parcel: Parcel): BuyNumbersCheckoutModel {
//            return BuyNumbersCheckoutModel(parcel)
//        }
//
//        override fun newArray(size: Int): Array<BuyNumbersCheckoutModel?> {
//            return arrayOfNulls(size)
//        }
//    }
//}
//
//
////Pay with OS Balance
//data class PayWithOSBalanceRequestModel(
//    var ACCOUNT: String,
//    var DEBIT_AMOUNT: Double
//)
//
//
//data class PayWithOSBalanceResponseModel(
//    var NEW_BALANCE: String,
//    var OLD_BALANCE: String,
//    var PaymentId: Int
//)
//
//
////Pay with Reward Points
//data class PayWithRewardPointsRequestModel(
//    var ACCOUNT: String,
//    var DEBIT_AMOUNT: Double,
//    var PaymentId: Int
//)
//
//data class PayWithRewardPointsResponseModel(
//    var ResponseCode: String?,
//    var PayId: Int
//)
//
//
////Pay with Reward Points
//data class AutoRenewNumberRequestModel(
//    var Account: String,
//    var AutoRenew: Boolean,
//    var Number: String
//)
//
//data class AutoRenewNumberResponseModel(
//    var RESPONSE: String?,
//)
//
//
////Order Telephone Number..........................................................
//data class OrderTelephoneNumberTNItemMessagingModel(
////    var messageClass: Nothing? =null,
////    var messageType :Nothing?=null
//
//    var messageClass: String,
//    var messageType: String
//)
//
//data class OrderTelephoneNumberTNFeatureModel(
//    var callerId: String?,
//    var messaging: OrderTelephoneNumberTNItemMessagingModel
//)
//
//data class OrderTelephoneNumberTnModel(
//    var tn: String,
//    var vendor: String?,
//    var e911: String?,
//    var tnFeature: OrderTelephoneNumberTNFeatureModel,
//    var AutoRenew: Boolean
//)
//
//data class OrderTelephoneNumberTNListModel(
//    var tnItem: ArrayList<OrderTelephoneNumberTnModel>
//)
//
////    var tnOrder:OrderTelephoneNumberTNListModel
//data class OrderTelephoneNumberTNOrderModel(
//    var tnList: OrderTelephoneNumberTNListModel
//)
//
//data class OrderTelephoneNumberRequestModel(
//    var PaymentHistoryId: Int,
//    var Account: String,
//    var tnOrder: OrderTelephoneNumberTNOrderModel
//)
//
//data class OrderTelephoneNumberResponseModel(
//    var status: String,
//    var statusCode: String,
//    var orderId: String
//)
////"status": "TN Excluded",
////"statusCode": "432",
////"orderId": null
//
////{
////    "PaymentHistoryId": 667,
////    "Account": "52426418377",
////    "tnOrder": {
////    "tnList": {
////        "tnItem": [
////        {
////            "tn": "2063503832",
////            "vendor": "",
////            "province": null,
////            "trunkGroup": null,
////            "e911": null,
////            "endUser": null,
////            "tnFeature": {
////            "callerId": null,
////            "messaging": {
////            "messageClass": "A2PLC",
////            "messageType": "SMS",
////            "netNumberId": null
////        },
////            "directoryListing": null
////        },
////            "portOutPin": null,
////            "tnNote": null,
////            "AutoRenew": true
////        }
////        ]
////    }
////}
////}
//
//
////Order Telephone Number..........................................................
//class OrderTelephoneNumberTNItemMessagingModel1 {
//    var messageClass: String
//    var messageType: String
//
//    constructor(messageClass: String, messageType: String) {
//        this.messageClass = messageClass
//        this.messageType = messageType
//    }
//}
//
//
//class OrderTelephoneNumberTNFeatureModel1 {
//    var callerId: String?
//    var messaging: OrderTelephoneNumberTNItemMessagingModel1
//
//
//    constructor(callerId: String?, messaging: OrderTelephoneNumberTNItemMessagingModel1) {
//        this.callerId = callerId
//        this.messaging = messaging
//    }
//}
//
//class OrderTelephoneNumberTnModel1 {
//    var tn: String
//    var vendor: String?
//    var e911: String?
//    var tnFeature: OrderTelephoneNumberTNFeatureModel1
//    var AutoRenew: Boolean
//
//    constructor(
//        tn: String,
//        vendor: String?,
//        e911: String?,
//        tnFeature: OrderTelephoneNumberTNFeatureModel1,
//        AutoRenew: Boolean
//    ) {
//        this.tn = tn
//        this.vendor = vendor
//        this.e911 = e911
//        this.tnFeature = tnFeature
//        this.AutoRenew = AutoRenew
//    }
//}
//
//
//class OrderTelephoneNumberTNListModel1 {
//    var tnItem: ArrayList<OrderTelephoneNumberTnModel1>
//
//    constructor(tnItem: ArrayList<OrderTelephoneNumberTnModel1>) {
//        this.tnItem = tnItem
//    }
//}
//
////    var tnOrder:OrderTelephoneNumberTNListModel
//class OrderTelephoneNumberTNOrderModel1 {
//    var tnList: OrderTelephoneNumberTNListModel1
//
//    constructor(tnList: OrderTelephoneNumberTNListModel1) {
//        this.tnList = tnList
//    }
//}
//
//class OrderTelephoneNumberRequestModel1{
//    var PaymentHistoryId: Int
//    var Account: String
//    var tnOrder: OrderTelephoneNumberTNOrderModel1
//
//    constructor(PaymentHistoryId: Int, Account: String, tnOrder: OrderTelephoneNumberTNOrderModel1) {
//        this.PaymentHistoryId = PaymentHistoryId
//        this.Account = Account
//        this.tnOrder = tnOrder
//    }
//}
//
//
//
//data class GetCallingPlansModel(val PROMOTIONS: ArrayList<GetCallingPlansDataModel> )
//
//data class GetCallingPlansDataModel (
//    val PROMOTION: String,
//    val DESCRIPTION: String,
//    val DESTINATION: String,
//    val VALIDITY: String,
//    val PRICE: String,
//    val MINUTES:String,
//    val CURRENCY: String,
//    val Country: String,
//    val UNITS: String,
//    val BundleId: String,
//)