package com.example.one_suite.ui.history.callHistory

import android.content.Context
import android.util.Log
import android.view.View
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.one_suite.R
import com.example.one_suite.model.callForwarding.setSubscriberDID.SetSubscriberDIDRequestModel
import com.example.one_suite.model.callForwarding.setSubscriberDID.SetSubscriberDIDResponseModel
import com.example.one_suite.model.subscriberDid.SubscriberDIDResponseDataModel
import com.example.one_suite.network.APIService
import com.example.one_suite.network.RetroInstance
import com.example.one_suite.ui.callForwarding.CallForwardingAdapter
import com.example.one_suite.ui.home.GetSubscriberPromotionRequestModel
import com.example.one_suite.ui.home.GetSubscriberPromotionResponseDataModel
import com.example.one_suite.ui.home.GetSubscriberPromotionResponseModel
import com.example.one_suite.utils.Constant
import com.example.one_suite.utils.SwitchClickListener
import com.resocoder.databinding.model.subscriberDid.SubscriberDIDRequestModel

import com.resocoder.databinding.model.subscriberDid.SubscriberDIDResponseModel
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class CallHistoryViewModel() : ViewModel() {

    var callingPlanHistoryListdata: MutableLiveData<ArrayList<GetSubscriberPromotionResponseDataModel>> = MutableLiveData()
    var recyclerViewAdapter= CallHistoryAdapter()


    fun setAdapterData(data: ArrayList<GetSubscriberPromotionResponseDataModel>) {
        recyclerViewAdapter.setDataList(data)
        recyclerViewAdapter.notifyDataSetChanged()
    }


    fun getAdapter(): CallHistoryAdapter {
        return recyclerViewAdapter
    }

    fun getCallingPlanHistoryListDataObserver():MutableLiveData<ArrayList<GetSubscriberPromotionResponseDataModel>>{
        return callingPlanHistoryListdata
    }


    fun makeCallingPlanHistoryApiCall() {


        val requestModel: GetSubscriberPromotionRequestModel = GetSubscriberPromotionRequestModel(Constant.account)

        Log.d("SubscriberApiCall", "country list api call")
        val apiService = RetroInstance.getRetrofitInstance().create(APIService::class.java)
        val call = apiService.getCallingPlanHistory(Constant.credentials, requestModel)

        call.enqueue(object : Callback<GetSubscriberPromotionResponseModel> {
            override fun onResponse(call: Call<GetSubscriberPromotionResponseModel>, response: Response<GetSubscriberPromotionResponseModel>) {

                Log.d("SubscriberApiCall", "response")
                if (response.isSuccessful) {



                    if (response.body() != null) {
                        callingPlanHistoryListdata.postValue(response.body()!!.Promotions)
                    }else{
                        callingPlanHistoryListdata.postValue(null)
                    }


                } else {
                    callingPlanHistoryListdata.postValue(null)
                }
            }

            override fun onFailure(call: Call<GetSubscriberPromotionResponseModel>, t: Throwable) {
                Log.d("SubscriberApiCall", "Failure : ${t.message}")
                callingPlanHistoryListdata.postValue(null)

            }

        })

    }

}
