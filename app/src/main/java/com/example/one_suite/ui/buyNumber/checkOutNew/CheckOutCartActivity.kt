package com.example.one_suite.ui.buyNumber.checkOutNew

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.one_suite.MainActivity
import com.example.one_suite.R
import com.example.one_suite.databinding.ActivityCheckOutCartBinding
import com.example.one_suite.databinding.ActivityCheckoutBinding
import com.example.one_suite.ui.buyNumber.checkOut.BuyNumberCheckoutViewModel
import com.example.one_suite.ui.buyNumber.checkOut.BuyNumbersCheckoutModel
import com.example.one_suite.ui.callingPlan.callingPlans.GetCallingPlansDataModel
import com.example.one_suite.utils.Constant

class CheckOutCartActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //setContentView(R.layout.activity_check_out_cart)


        //getting data using Parcelable
        val bundle = intent.getBundleExtra("Bundle");
        var checkOutBundleList = ArrayList<GetCallingPlansDataModel>();
        if (bundle != null) {
            checkOutBundleList =
                bundle.getParcelableArrayList<GetCallingPlansDataModel>("CheckOutBundleList")!!
        }


        var viewModel = ViewModelProviders.of(this)
            .get(CheckoutCartViewModel::class.java)


        var activityBinding = DataBindingUtil.setContentView<ActivityCheckOutCartBinding>(
            this, R.layout.activity_check_out_cart
        ).apply {
            this.setLifecycleOwner(this@CheckOutCartActivity)
            this.viewModel = viewModel
        }

        activityBinding.executePendingBindings()
        activityBinding.recyclerView.apply {
            layoutManager = LinearLayoutManager(context)
        }


//        var bundleList=ArrayList<GetCallingPlansDataModel>();
//        bundleList.add(GetCallingPlansDataModel("Germany Bundle","description text","Germany","12/12/2030","13","30","$","USA","30","1"))
//        bundleList.add(GetCallingPlansDataModel("Germany Bundle","description text","Germany","12/12/2030","13","30","$","USA","30","1"))
//        bundleList.add(GetCallingPlansDataModel("Germany Bundle","description text","Germany","12/12/2030","13","30","$","USA","30","1"))
//        bundleList.add(GetCallingPlansDataModel("Germany Bundle","description text","Germany","12/12/2030","11","30","$","USA","30","1"))
//        viewModel.setBundleAdapterData(bundleList)
//


        viewModel.setBundleAdapterData(checkOutBundleList)


        /*.....................On Click Listener...................*/
        activityBinding.payWithOSButton.setOnClickListener(View.OnClickListener {

        })
        activityBinding.payWithRewardButton.setOnClickListener(View.OnClickListener {

        })
        activityBinding.continueShoppingButton.setOnClickListener(View.OnClickListener {
            Log.d("ClickIssue", "On Continue button click")
            val intent = Intent(this, MainActivity::class.java)
            val bundle = Bundle()
            intent.putExtra("Bundle", bundle)
            intent.putExtra("Fragment", "Calling Plans")
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            startActivity(intent)
        })


        /*......................................Data Observer................*/
        viewModel.getDeleteBundleItemPositionObserver().observe(this, Observer {
            if (it != null) {
                Toast.makeText(this, "Delete at Position :${it}", Toast.LENGTH_SHORT).show()
                checkOutBundleList.removeAt(it)

                Constant.checkoutBundleListStatic = checkOutBundleList

                viewModel.setBundleAdapterData(checkOutBundleList)
            }
        })
    }
}