package com.example.one_suite.ui.callBack

import android.app.Dialog
import android.app.ProgressDialog
import android.content.Intent
import android.graphics.Color
import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.one_suite.MainActivity
import com.example.one_suite.R
import com.example.one_suite.databinding.CallBackFragmentBinding
import com.example.one_suite.databinding.CallForwardingFragmentBinding
import com.example.one_suite.model.callBack.SetCallBackRequestModel
import com.example.one_suite.model.callForwarding.setSubscriberDID.SetSubscriberDIDRequestModel
import com.example.one_suite.ui.callForwarding.CallForwardingViewModel
import com.example.one_suite.ui.callForwarding.setForwarding.SetForwardingDialog1
import com.example.one_suite.ui.login.LoginRequestModel
import com.example.one_suite.utils.ClickListener
import com.example.one_suite.utils.Constant
import com.example.one_suite.utils.customSpiner.CustomSpinerAdapter
import com.example.one_suite.utils.customSpiner.Data
import com.resocoder.databinding.utils.DialogUtils

class CallBackFragment : Fragment(), ClickListener {


    companion object {
        fun newInstance() = CallBackFragment()
    }

    private lateinit var callBackViewModel: CallBackViewModel
    private var _binding: CallBackFragmentBinding? = null
    private val binding get() = _binding!!


    var callerIdDataModel: MutableList<Data> = mutableListOf<Data>()
    var callerId = ""

    lateinit var confirmationDialog: Dialog;
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        callBackViewModel = ViewModelProvider(this).get(CallBackViewModel::class.java)

        _binding = CallBackFragmentBinding.inflate(inflater, container, false).apply {
            this.lifecycleOwner = this@CallBackFragment
            this.viewModel = callBackViewModel
        }

        val root: View = binding.root


        /*..............For Show More And Less Text..........*/
        _binding!!.textViewShowMore.setShowingChar(30)
        _binding!!.textViewShowMore.setShowingLine(2)
        _binding!!.textViewShowMore.addShowMoreText("see more");
        _binding!!.textViewShowMore.addShowLessText("Less");

        _binding!!.textViewShowMore.setShowMoreColor(Color.RED); // or other color
        _binding!!.textViewShowMore.setShowLessTextColor(Color.RED); // or other color
        /*..............For Show More And Less Text..........*/


        //progress dialog
        var dialog = DialogUtils.setProgressDialog(container!!.context, "Please Wait....")


        /* .............State Spinner..............*/
        val stateAdapter = CustomSpinerAdapter()
        val stateSpinner = binding.statesSpinner

        stateSpinner!!.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                adapter: AdapterView<*>?,
                view: View?,
                position: Int,
                p3: Long
            ) {


                if (callerIdDataModel.size > 0) {

                    if (position == 0) {
                        callerId = ""
                    } else {
                        callerId = callerIdDataModel[position].country.toString()
                    }
                    Log.d("SpinnerText", "City :$callerId")
                }

            }

            override fun onNothingSelected(p0: AdapterView<*>?) {
            }

        }


        /* .............State Spinner..............*/


        //calling API
        dialog!!.show()
        callBackViewModel.makeSubscriberDIDApiCall()


        //Receiving Data
        callBackViewModel.getRecyclerListDataObserver().observe(viewLifecycleOwner, Observer {
            //dialog.dismiss()

            if (it != null) {
                callerIdDataModel.add(Data(1, "Hide Caller Id"))
                for (item in it) {
                    if (item.Name == null) {
                        callerIdDataModel.add(Data(1, "+" + item.Number))
                    } else {
                        callerIdDataModel.add(Data(1, "+" + item.Number + "-" + item.Name))
                    }
                }


                /*...............Calling custom subscriber number API Call..........*/
                callBackViewModel.makeSubscriberCustomNumbersApiCall()


            }
        })



        callBackViewModel.getCustomNumbersListDataObserver().observe(viewLifecycleOwner, Observer {
            dialog.dismiss()


            if (it != null) {
                for (item in it) {
                    callerIdDataModel.add(Data(1, "+" + item.Number))
                }

            }


            stateAdapter.customeSpinnerAdapter(requireContext(), callerIdDataModel)
            stateSpinner.adapter = stateAdapter

        })


        callBackViewModel.getCallBackResult().observe(viewLifecycleOwner, Observer {
            dialog.dismiss()


            var description =
                "Your call back request is received, and we will call you back in minutes. Your approximated talk time is 2683 minute(s)"

            confirmationDialog =
                DialogUtils.getVoucherConfirmationDialog(
                    requireActivity(),
                    "Success",
                    description,
                    this
                )
            confirmationDialog.show()


            //(activity as MainActivity?)?.displaySelectedScreen("Call Back")
        })

        /*.............OnButtonClick.......*/
        _binding!!.callNow.setOnClickListener(View.OnClickListener {
//            var callBack: String = _binding!!.callBack.text.toString()
//            var wantCall: String = _binding!!.wantCall.text.toString()


            var callBack: String = _binding!!.callBack.text.toString()
            var wantCall: String = _binding!!.wantCall.text.toString()


            Log.d("ValidationIssue","Call back :$callBack")
            Log.d("ValidationIssue","Want Call :$wantCall")


            if (callBack.trim().length <= 0) {
//                _binding!!.callBack.error = "This field is required."
                _binding!!.callBack.setError("This field is required.")
                Log.d("ValidationIssue","call back empty")
            } else if (callBack.trim().length < 10) {
                _binding!!.callBack.error = "Please enter at least 10 characters."
            }else if (callBack.trim().length > 15) {
                _binding!!.callBack.error = "Please enter no more than 15 characters."
            }else  if (wantCall.trim().isEmpty()) {
                _binding!!.wantCall.error = "This field is required."

            } else if (wantCall.trim().length < 10) {
                _binding!!.wantCall.error = "Please enter at least 10 characters."
            }else if (wantCall.trim().length > 15) {
                _binding!!.wantCall.error = "Please enter no more than 15 characters."
            } else {

                dialog.show()
                var requestModel =
                    SetCallBackRequestModel(Constant.account, callBack, wantCall, callerId)
                callBackViewModel.makeSetCallBackApiCall(requestModel); }


        })

        _binding!!.reset.setOnClickListener(View.OnClickListener {
            (activity as MainActivity?)?.displaySelectedScreen("Call Back")
        })







        return root;
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        // callForwardingViewModel = ViewModelProvider(this).get(CallForwardingViewModel::class.java)
        // TODO: Use the ViewModel
    }

    override fun onViewClick(view: View, position: Int) {
        if (view.id == R.id.okBtn) {
            confirmationDialog.dismiss()

            (activity as MainActivity?)?.displaySelectedScreen("Call Back")

        }
    }

    override fun onRowClick(position: Int) {
    }

}