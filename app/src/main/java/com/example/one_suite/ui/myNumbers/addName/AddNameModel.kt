package com.example.one_suite.ui.myNumbers.addName

data class AddNameRequestModel(
    var Account: String,
    var Name: String,
    var Number: String
)

data class AddNameResponseModel(
    var RESPONSE
    : String,
)