package com.example.one_suite.ui.support

import android.content.Context
import android.util.Log
import android.view.View
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.one_suite.R
import com.example.one_suite.model.callForwarding.setSubscriberDID.SetSubscriberDIDRequestModel
import com.example.one_suite.model.callForwarding.setSubscriberDID.SetSubscriberDIDResponseModel
import com.example.one_suite.model.subscriberDid.SubscriberDIDResponseDataModel
import com.example.one_suite.model.support.GetAllTicketsRequestModel
import com.example.one_suite.model.support.GetAllTicketsResponseDataModel
import com.example.one_suite.model.support.GetAllTicketsResponseModel
import com.example.one_suite.network.APIService
import com.example.one_suite.network.RetroInstance
import com.example.one_suite.utils.ClickListener
import com.example.one_suite.utils.Constant
import com.example.one_suite.utils.SwitchClickListener
import com.resocoder.databinding.model.subscriberDid.SubscriberDIDRequestModel

import com.resocoder.databinding.model.subscriberDid.SubscriberDIDResponseModel
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class TicketsViewModel() : ViewModel(), ClickListener {

    lateinit var recyclerListData: MutableLiveData<ArrayList<GetAllTicketsResponseDataModel>>
    lateinit var recyclerViewAdapter: TicketsAdapter


    init {
        recyclerListData = MutableLiveData()
        recyclerViewAdapter = TicketsAdapter(this)
    }

    fun getAdapter(): TicketsAdapter {
        return recyclerViewAdapter
    }

    fun setAdapterData(data: ArrayList<GetAllTicketsResponseDataModel>) {
        recyclerViewAdapter.setDataList(data)
        recyclerViewAdapter.notifyDataSetChanged()
    }


    fun getRecyclerListDataObserver(): MutableLiveData<ArrayList<GetAllTicketsResponseDataModel>> {
        return recyclerListData
    }



    fun makeAllTicketsApiCall() {

        var statusId: Int = 1
        var isAdmin: Boolean = false


        val requestModel: GetAllTicketsRequestModel =
            GetAllTicketsRequestModel(Constant.account, statusId, isAdmin)

        Log.d("SubscriberApiCall", "country list api call")
        val apiService = RetroInstance.getRetrofitInstance().create(APIService::class.java)
        val call = apiService.getAllTickets(Constant.credentials, requestModel)

        call.enqueue(object : Callback<GetAllTicketsResponseModel> {
            override fun onResponse(
                call: Call<GetAllTicketsResponseModel>,
                response: Response<GetAllTicketsResponseModel>
            ) {

                Log.d("SubscriberApiCall", "response")
                if (response.isSuccessful) {


                    if (response.body() != null) {

                        recyclerListData.postValue(response.body()!!.Tickets)

                    } else {
                        recyclerListData.postValue(null)
                    }


                } else {
                    recyclerListData.postValue(null)
                }
            }

            override fun onFailure(call: Call<GetAllTicketsResponseModel>, t: Throwable) {
                Log.d("SubscriberApiCall", "Failure : ${t.message}")
                recyclerListData.postValue(null)

            }

        })

    }

    override fun onViewClick(view: View, position: Int) {
    }

    override fun onRowClick(position: Int) {
        TODO("Not yet implemented")
    }


}
