package com.example.one_suite.ui.support.newTicket

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.AdapterView
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.one_suite.MainActivity
import com.example.one_suite.R
import com.example.one_suite.databinding.ActivityNewTicketBinding
import com.example.one_suite.model.support.newTicket.generateTicket.GenerateTicketRequestModel
import com.example.one_suite.model.support.newTicket.ticketPriorities.GetTicketPrioritiesRequestModel
import com.example.one_suite.utils.Constant
import com.example.one_suite.utils.customSpiner.CustomSpinerAdapter
import com.example.one_suite.utils.customSpiner.Data
import com.resocoder.databinding.utils.DialogUtils

class NewTicket : AppCompatActivity() {

    var ticketsTypesDataModel: MutableList<Data> = mutableListOf<Data>()
    var ticketsPrioritiesDataModel: MutableList<Data> = mutableListOf<Data>()


    var typeId = ""
    var priorityId = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //setContentView(R.layout.activity_new_ticket)

        val viewModel = ViewModelProvider(this).get(NewTicketViewModel::class.java)


        var activityBinding= DataBindingUtil.setContentView<ActivityNewTicketBinding>(
            this, R.layout.activity_new_ticket
        ).apply {
            this.setLifecycleOwner(this@NewTicket)
            this.viewModel = viewModel

        }


        //progress dialog
        var progressDialog = DialogUtils.setProgressDialog(this, "Please Wait....")



        //Ticket Types custom spinner
        ticketsTypesDataModel.add(Data(-1, "Select Ticket"))


        val ticketSpinner = activityBinding.ticketSpinner
        val ticketAdapter = CustomSpinerAdapter()
        ticketAdapter.customeSpinnerAdapter(this, ticketsTypesDataModel)
        ticketSpinner.adapter = ticketAdapter

        ticketSpinner!!.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                adapter: AdapterView<*>?,
                view: View?,
                position: Int,
                p3: Long
            ) {


                if (ticketsTypesDataModel.size > 0) {
                    //type = ticketsTypesDataModel[position].country.toString()
                    typeId= ticketsTypesDataModel[position].id.toString()!!
                    Log.d("SpinnerText","type :$typeId")
                }


                if (typeId!="-1"){
                    progressDialog!!.show()
                    var request= GetTicketPrioritiesRequestModel(typeId)
                    viewModel.makeTicketPriorityApiCall(request)
                }




            }

            override fun onNothingSelected(p0: AdapterView<*>?) {
            }

        }



        //Ticket Priorities custom spinner
        ticketsPrioritiesDataModel.add(Data(-1, "Select Priority"))


        val prioritySpinner = activityBinding.prioritySpinner
        val priorityAdapter = CustomSpinerAdapter()
        priorityAdapter.customeSpinnerAdapter(this, ticketsPrioritiesDataModel)
        prioritySpinner.adapter = priorityAdapter

        prioritySpinner!!.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                adapter: AdapterView<*>?,
                view: View?,
                position: Int,
                p3: Long
            ) {


                if (ticketsPrioritiesDataModel.size > 0) {
                    priorityId = ticketsPrioritiesDataModel[position].id.toString()
                    Log.d("SpinnerText","type :$priorityId")
                }

            }

            override fun onNothingSelected(p0: AdapterView<*>?) {
            }

        }





        /*..................API Calls.............*/

        progressDialog!!.show()
        viewModel.makeTicketTypeApiCall()

        //Generate New Ticket
        activityBinding!!.generateTicketBtn.setOnClickListener(View.OnClickListener {
            Log.d("TAG", "generateTicketButton: ")
            var createdBy:Int=2
            var title:String=activityBinding.title.text.toString()
            var description:String=activityBinding.description.text.toString()

            var requestModel= GenerateTicketRequestModel(Constant.account,createdBy,typeId.toInt(),priorityId.toInt(),title,description)
            progressDialog.show()
            viewModel.makeGenerateTicketApiCall(requestModel)

        })


        //Cancel
        activityBinding!!.cancelButton.setOnClickListener(View.OnClickListener {
            finish()
        })

        /*...................................Data Observers..................*/
        viewModel.getTicketTypesDataObserver().observe(this, Observer {
            progressDialog.dismiss()

            if (it!=null){
                ticketsTypesDataModel.clear()
                ticketsTypesDataModel.add(Data(-1, "Select Ticket"))
                for (item in it) {
                    ticketsTypesDataModel.add(Data(item.TypeId, item.Title))

                }

//                ticketsTypesDataModel.sortBy { Data -> Data.country }

                ticketAdapter.customeSpinnerAdapter(this, ticketsTypesDataModel)
                ticketSpinner.adapter = ticketAdapter

            }
        })


        viewModel.getTicketPriorityDataObserver().observe(this, Observer {
            progressDialog.dismiss()

            if (it!=null){
                ticketsPrioritiesDataModel.clear()
//                ticketsPrioritiesDataModel.add(Data(1, "--ALL--"))
                for (item in it) {
                    ticketsPrioritiesDataModel.add(Data(item.PriorityId, item.PriorityTitle))

                }

//                ticketsPrioritiesDataModel.sortBy { Data -> Data.country }

                priorityAdapter.customeSpinnerAdapter(this, ticketsPrioritiesDataModel)
                prioritySpinner.adapter = priorityAdapter

            }
        })

        viewModel.getGenerateTicketResultObserver().observe(this, Observer {
            progressDialog.dismiss()
            if (it!=null){
                Log.d("TicketResult","Ticket ID :${it.TicketId}")

                val intent = Intent(this, MainActivity::class.java)
                intent.putExtra("Fragment","Support")
                startActivity(intent)
                finish()
            }
        })


    }
}