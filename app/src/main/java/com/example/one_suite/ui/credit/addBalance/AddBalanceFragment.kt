package com.example.one_suite.ui.credit.addBalance

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Observer
import com.example.one_suite.databinding.AddBalanceFragmentBinding
import com.example.one_suite.model.credits.GetSystemSettingsResponseModel
import com.example.one_suite.ui.callForwarding.CallForwardingViewModel
import com.resocoder.databinding.utils.DialogUtils

class AddBalanceFragment : Fragment() {

    companion object {
        fun newInstance() = AddBalanceFragment()
    }

    private lateinit var addBalanceViewModel: AddBalanceViewModel
    private var _binding: AddBalanceFragmentBinding? = null
    private val binding get() = _binding!!


    var amountDouble: Double? = 0.0
    var isAmountLabelVisible: Boolean = false

    lateinit var systemSettingsData: GetSystemSettingsResponseModel
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        addBalanceViewModel = ViewModelProvider(this).get(AddBalanceViewModel::class.java)

        _binding = AddBalanceFragmentBinding.inflate(inflater, container, false).apply {
            this.lifecycleOwner=this@AddBalanceFragment
            this.viewModel=addBalanceViewModel
        }
        val root: View = binding.root


        //to show and hide the enter amount edit text
        _binding!!.fiveDollar.setOnClickListener(View.OnClickListener {
            addBalanceViewModel.setEnterAmountVisibility(false)
            addBalanceViewModel.setEnterAmountMandatoryVisibility(false)
            isAmountLabelVisible = false
            amountDouble = 5.0
        })

        _binding!!.tenDollar.setOnClickListener(View.OnClickListener {
            addBalanceViewModel.setEnterAmountVisibility(false)
            addBalanceViewModel.setEnterAmountMandatoryVisibility(false)
            isAmountLabelVisible = false
            amountDouble = 10.0
        })
        _binding!!.twentyDollar.setOnClickListener(View.OnClickListener {
            addBalanceViewModel.setEnterAmountVisibility(false)
            addBalanceViewModel.setEnterAmountMandatoryVisibility(false)
            isAmountLabelVisible = false
            amountDouble = 20.0
        })
        _binding!!.thirtyDollar.setOnClickListener(View.OnClickListener {
            addBalanceViewModel.setEnterAmountVisibility(false)
            addBalanceViewModel.setEnterAmountMandatoryVisibility(false)
            isAmountLabelVisible = false
            amountDouble = 30.0
        })


        _binding!!.otherAmount.setOnClickListener(View.OnClickListener {
            Log.d("ViewVisibility","other btn click")
            addBalanceViewModel.setEnterAmountVisibility(true)
            addBalanceViewModel.setEnterAmountMandatoryVisibility(false)
            isAmountLabelVisible = true
        })



        _binding!!.continueBtn.setOnClickListener(View.OnClickListener {
            if (isAmountLabelVisible) {

                if (systemSettingsData != null) {

                    val amount = _binding!!.enterAmount.text.toString()

                    if (amount.isEmpty()) {
                        addBalanceViewModel.setEnterAmountMandatoryVisibility(true)
                    } else {

//                        val amountDouble: Double? = amount.toDouble()
                        amountDouble = amount.toDouble()
                        if (amountDouble != null) {
                            if (amountDouble!! >= systemSettingsData.MinimumTopup && amountDouble!! <= systemSettingsData.MaximumTopup) {
                                addBalanceViewModel.setEnterAmountMandatoryVisibility(false)

//                                var intent = Intent(this, CreditPayInvoice::class.java)
//                                intent.putExtra("Amount", amountDouble!!)
//                                startActivity(intent)


                                Toast.makeText(context,"This section is under development",Toast.LENGTH_SHORT).show()

                            } else {
                                Log.d("FaizanTesting", "empty")
                                addBalanceViewModel.setEnterAmountMandatoryVisibility(true)
                            }


                        }

                    }

                }

            }
        }
        )




        //progress dialog
        var dialog = DialogUtils.setProgressDialog(container!!.context, "Please Wait....")
        dialog!!.show()
        addBalanceViewModel.makeSystemSettingApiCall()

        addBalanceViewModel.getSystemSettingsResult().observe(viewLifecycleOwner, Observer {
            if (it == true) {
                dialog!!.dismiss()
            }
        })

        addBalanceViewModel.getSystemSettingsData().observe(viewLifecycleOwner, Observer {
            systemSettingsData = it
        })


        return root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        addBalanceViewModel = ViewModelProvider(this).get(AddBalanceViewModel::class.java)
        // TODO: Use the ViewModel
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }


}