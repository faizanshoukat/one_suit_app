package com.example.one_suite.ui.history.callHistory

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.one_suite.databinding.CallForwardingRecyclerItemBinding
import com.example.one_suite.databinding.CallHistoryRecyclerItemBinding
import com.example.one_suite.databinding.CallHistoryRecyclerviewItemsBinding
import com.example.one_suite.databinding.MyPlansRecyclerviewItemsBinding
import com.example.one_suite.model.subscriberDid.SubscriberDIDResponseDataModel
import com.example.one_suite.ui.home.GetSubscriberPromotionResponseDataModel
import com.example.one_suite.ui.home.GetSubscriberPromotionResponseModel
import com.example.one_suite.utils.SwitchClickListener

class CallHistoryAdapter() :
    RecyclerView.Adapter<CallHistoryAdapter.MyViewHolder>() {
    var items = ArrayList<GetSubscriberPromotionResponseDataModel>()

    fun setDataList(data: ArrayList<GetSubscriberPromotionResponseDataModel>) {
        this.items = data
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): CallHistoryAdapter.MyViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = CallHistoryRecyclerviewItemsBinding.inflate(layoutInflater)
        return MyViewHolder(binding)
    }

    override fun getItemCount() = items.size

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.bind(items[position])

    }

    class MyViewHolder(val binding: CallHistoryRecyclerviewItemsBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(data: GetSubscriberPromotionResponseDataModel) {
            binding.viewModel = data
            binding.executePendingBindings()
        }
    }


}