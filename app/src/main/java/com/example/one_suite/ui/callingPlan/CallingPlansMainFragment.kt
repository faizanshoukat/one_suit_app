package com.example.one_suite.ui.callingPlan

import android.graphics.Color
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.example.one_suite.R
import com.example.one_suite.ui.callingPlan.callingPlans.CallingPlansFragment
import com.example.one_suite.ui.callingPlan.myPlans.MyPlansFragment
import com.example.one_suite.ui.myNumbers.addName.AddNameFragment
 
class CallingPlansMainFragment : Fragment() {

    companion object {
        fun newInstance() = CallingPlansMainFragment()
    }

    //private lateinit var viewModel: MyNumbersViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {


        val view =inflater.inflate(R.layout.calling_plans_main_fragment, container, false)

        /* added fragment My Numbers by default  */
        attachCallingPlansFragment()
        val button1=view.findViewById<TextView>(R.id.callingPlans)
        val button2=view.findViewById<TextView>(R.id.myPlans)


        button1.setOnClickListener(View.OnClickListener {
            button1.setTextColor(Color.parseColor("#7ec243"))
            button2.setTextColor(Color.parseColor("#959898"))
            attachCallingPlansFragment()
        })


        button2.setOnClickListener(View.OnClickListener {
            button2.setTextColor(Color.parseColor("#7ec243"))
            button1.setTextColor(Color.parseColor("#959898"))
            attachMyPlansFragment()
        })

        return view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        //viewModel = ViewModelProvider(this).get(MyNumbersViewModel::class.java)
        // TODO: Use the ViewModel
    }

   private fun attachCallingPlansFragment(){
       //creating fragment object
       var fragment: Fragment? = null


       fragment = CallingPlansFragment()

       //replacing the fragment
       if (fragment != null) {
           val ft = activity?.supportFragmentManager?.beginTransaction()
           ft?.replace(R.id.callingPlansContainer, fragment)
           ft?.commit()
       }


   }


    fun attachMyPlansFragment(){
        //creating fragment object
        var fragment: Fragment? = null


        fragment = MyPlansFragment()

        //replacing the fragment
        if (fragment != null) {
            val ft = activity?.supportFragmentManager?.beginTransaction()
            ft?.replace(R.id.callingPlansContainer, fragment)
            ft?.commit()
        }


    }

}