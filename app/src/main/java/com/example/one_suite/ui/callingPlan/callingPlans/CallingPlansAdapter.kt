package com.example.one_suite.ui.callingPlan.callingPlans

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.one_suite.R
import com.example.one_suite.databinding.CallForwardingRecyclerItemBinding
import com.example.one_suite.databinding.CallingPlanRecyclerviewItemsBinding
import com.example.one_suite.model.subscriberDid.SubscriberDIDResponseDataModel
import com.example.one_suite.utils.ClickListener
import com.example.one_suite.utils.SwitchClickListener

class CallingPlansAdapter(private val listener: ClickListener) :
    RecyclerView.Adapter<CallingPlansAdapter.MyViewHolder>() {
    var items = ArrayList<GetCallingPlansDataModel>()

    fun setDataList(data: ArrayList<GetCallingPlansDataModel>) {
        this.items = data
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): CallingPlansAdapter.MyViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = CallingPlanRecyclerviewItemsBinding.inflate(layoutInflater)
        return MyViewHolder(binding)
    }

    override fun getItemCount() = items.size

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.bind(items[position])

//        holder.binding.switchBtn.setOnClickListener(View.OnClickListener {
//            var isChecked:Boolean
//
//            isChecked=holder.binding.switchBtn.isChecked
//            listener.onSwitchClick(holder.binding.switchBtn, position,isChecked)
//        })


        holder.binding.buyCallingPlan.setOnClickListener(View.OnClickListener {
            listener.onViewClick(holder.binding.buyCallingPlan,position)
        })

        if (position % 2 == 0) {
            println("$position is even")
            holder.binding.llCpBg.setBackgroundResource(R.drawable.ic_blue_bg)
        } else {
            holder.binding.llCpBg.setBackgroundResource(R.drawable.ic_greenbg)
            println("$position is odd")
        }

    }

    class MyViewHolder(val binding: CallingPlanRecyclerviewItemsBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(data: GetCallingPlansDataModel) {
            binding.viewModel = data
            binding.executePendingBindings()
        }
    }


}