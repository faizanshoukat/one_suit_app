package com.example.one_suite.ui.reWards

import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.one_suite.R
import com.example.one_suite.databinding.ActivityRewardBinding
import com.example.one_suite.ui.history.transactionHistory.TransactionHistoryViewModel
import com.example.one_suite.ui.home.HomeViewModel
import com.example.one_suite.ui.myNumbers.myNumbersList.MyNumbersListFragment
import com.example.one_suite.ui.support.TicketsFragment
import com.resocoder.databinding.utils.DialogUtils
import com.skyhope.showmoretextview.ShowMoreTextView

class RewardsMainFragments : Fragment(), ExpandableCatAdapter.SetOnClickListner {


    lateinit var binding: ActivityRewardBinding

    lateinit var rewardViewModel: RewardViewModel

    var parentList:ArrayList<Category> = ArrayList()
    var rewardPointsChildList=ArrayList<SubCatagory>()
    var referralChildList=ArrayList<SubCatagory>()
    var earnedPointsChildList=ArrayList<SubCatagory>()
    var redeemPointsChildList=ArrayList<SubCatagory>()

    companion object {
        fun newInstance() = RewardsMainFragments()
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        //setContentView(R.layout.activity_main)

        rewardViewModel = ViewModelProvider(this).get(RewardViewModel::class.java)

        binding = ActivityRewardBinding.inflate(inflater, container, false).apply {
            this.lifecycleOwner = this@RewardsMainFragments
            this.viewModel = rewardViewModel

        }

        val root: View = binding!!.root


        /*..............For Show More And Less Text..........*/
        binding.textViewShowMore.setShowingChar(30)
        binding.textViewShowMore.setShowingLine(2)
        binding.textViewShowMore.addShowMoreText("see more");
        binding.textViewShowMore.addShowLessText("Less");

        binding.textViewShowMore.setShowMoreColor(Color.RED); // or other color
        binding.textViewShowMore.setShowLessTextColor(Color.RED); // or other color

        /*..............For Show More And Less Text..........*/


        //default attach list Fragment
        //attachRewardListFragment()


        //tab's click listeners
        binding.referralCode.setOnClickListener(View.OnClickListener {

        })


        var dialog= DialogUtils.setProgressDialog(container!!.context,"Please Wait....")


        binding!!.executePendingBindings()
        binding!!.recyclerView.apply {
            Log.d("SearchTelephoneApiCall", "List size in fragment")
            layoutManager = LinearLayoutManager(activity?.applicationContext)
        }

        rewardViewModel.rewardAdapter.setClickListener(this)


        //calling API
        dialog!!.show()
//        rewardViewModel.makeReferralHistoryApiCall("54")
        rewardViewModel.makeRewardPointsApiCall("54")


        /*...........Data Observer..............*/
        rewardViewModel.getRewardPointsObserver().observe(viewLifecycleOwner, Observer {

            if (it!=null){

                redeemPointsChildList.add(SubCatagory(1,"Total Available Points",it.SumOfRewards.toString()))
                redeemPointsChildList.add(SubCatagory(1,"Added in last 30 Days",it.AddThirthyDays.toString()))

                redeemPointsChildList.add(SubCatagory(1,"Redeemed in last 30 Days",it.RedeemedThirthyDays.toString()))
                redeemPointsChildList.add(SubCatagory(1,"Expired in last 30 Days",it.ExpiredThirthyDays.toString()))

                redeemPointsChildList.add(SubCatagory(1,"Expiring in last 30 Days",it.ExpiringThirthyDays.toString()))

                parentList.add(Category(1,"Your Reward Points","",redeemPointsChildList))

            }

            rewardViewModel.makeReferralHistoryApiCall("54")
        })

        rewardViewModel.getReferralHistoryListObserver().observe(viewLifecycleOwner, Observer {
           // dialog.dismiss()
            if (it!=null){


                for (item in it){
                    referralChildList.add(SubCatagory(1,item.ReferredEmail,item.Status))
                }

                parentList.add(Category(1,"Referral History","",referralChildList))

//                // now set data
//                rewardViewModel.setAdapterData(parentList)

            }

            rewardViewModel.makeRewardHistoryApiCall("54")
        })


        rewardViewModel.getRewardHistoryListObserver().observe(viewLifecycleOwner, Observer {
            //dialog.dismiss()
            if (it!=null){


                for (item in it){
                    earnedPointsChildList.add(SubCatagory(1,item.ReferredUsername,item.Status))
                }

                parentList.add(Category(1,"Earned Points","",earnedPointsChildList))

                // now set data
                rewardViewModel.setAdapterData(parentList)

            }

            rewardViewModel.makeRedeemApiCall()
        })


        rewardViewModel.getRedeemHistoryListObserver().observe(viewLifecycleOwner, Observer {
            dialog.dismiss()
            if (it!=null){


                for (item in it){
                    redeemPointsChildList.add(SubCatagory(1,item.Description,item.Points))
                }

                parentList.add(Category(1,"Redeem Points","",redeemPointsChildList))

                // now set data
                rewardViewModel.setAdapterData(parentList)

            }


        })






//        rewardEditText

//        binding.txtSave.setOnClickListener {
//            Log.d("TAG", "onCreateView: " + binding.txtSave)
//            binding.txtSave.setTextColor(Color.RED)
//
//            attachMyNumberFragment()
//            //replaceFragment(RewardChildActivity())
//        }


//
//        binding.referal.setOnClickListener {
////            var fm = supportFragmentManager
//       /*     var tc = fm.beginTransaction()
//            tc.add(R.id.my_frame, RewardChildActivity())
//            tc.commit()*/
//
//            Toast.makeText(context, "Referal", Toast.LENGTH_SHORT).show()
//
//            Log.d("TAG", "onCreateView: binding.referal"+binding.referal)
//            replaceFragment(RewardChildActivity())
//
//        }
//
//        binding.ReferalLink.setOnClickListener {
//
//            Log.d("TAG", "onCreateView: binding.ReferalLink"+binding.ReferalLink)
//            Toast.makeText(context, "ReferalLink", Toast.LENGTH_SHORT).show()
//            replaceFragment(RewardChildActivity())
//        }
//
//        binding.inviteAFriend.setOnClickListener {
//            Log.d("TAG", "onCreateView: inviteAFriend"+binding.inviteAFriend)
//            Toast.makeText(context, "Invite A Friend", Toast.LENGTH_SHORT).show()
//            replaceFragment(RewardChildActivity())
//        }


        return root
    }

    override fun onClick(testType: Category, pos: Int) {

        if(parentList[pos].isExpanded) {
            parentList[pos].isExpanded = false

        }else {
            parentList[pos].isExpanded = true
        }
        rewardViewModel.rewardAdapter.updateData(parentList)
    }

//    private fun attachRewardListFragment() {
//        //creating fragment object
//        var fragment: Fragment? = null
//
//        fragment = RewardChildActivity()
//
//        //replacing the fragment
//        if (fragment != null) {
//            val ft = activity?.supportFragmentManager?.beginTransaction()
//            ft?.replace(R.id.rewardContainer, fragment)
//            ft?.commit()
//        }
//    }

//    private fun replaceFragment(fragment: Fragment) {
//        val fragmentManager = getFragmentManager()
//        val fragmentTransaction = fragmentManager?.beginTransaction()
//        fragmentTransaction?.replace(R.id.my_frame, fragment)
//        fragmentTransaction?.commit()
//    }
}