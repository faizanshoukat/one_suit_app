package com.example.one_suite.ui.buyNumber.checkOutNew

import android.util.Log
import android.view.View
import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

import com.demo.databindingdemo.BuyNumberCheckoutAdapter
import com.example.one_suite.R
import com.example.one_suite.network.APIService
import com.example.one_suite.network.RetroInstance
import com.example.one_suite.ui.buyNumber.SearchTelephoneNumberDataModel
import com.example.one_suite.ui.buyNumber.checkOut.*
import com.example.one_suite.ui.callingPlan.callingPlans.GetCallingPlansDataModel
import com.example.one_suite.utils.ClickListener
import com.example.one_suite.utils.Constant
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class CheckoutCartViewModel : ViewModel(), ClickListener {
    var numberData = MutableLiveData<SearchTelephoneNumberDataModel?>()
    var recyclerViewAdapter: BuyNumberCheckoutAdapter

    var deleteItemPosition = MutableLiveData<Int>()
    var switchItemPosition = MutableLiveData<Int>()
    var isEmpty = MutableLiveData<Boolean>()
    var isEmptyBundleList = MutableLiveData<Boolean>()


    var osCost = ObservableField<String>()
    var costData = MutableLiveData<Double>()


    var payWithOSBalanceData = MutableLiveData<PayWithOSBalanceResponseModel?>()
    var payWithRewardPointsData = MutableLiveData<PayWithRewardPointsResponseModel?>()
    var autoRenewNumberData = MutableLiveData<AutoRenewNumberResponseModel?>()
    var orderTelephoneNumberResponseData = MutableLiveData<OrderTelephoneNumberResponseModel?>()

    var switchItemData=MutableLiveData<BuyNumbersCheckoutModel>()
    //var buyNumberCheckoutList=MutableLiveData<ArrayList<BuyNumbersCheckoutModel>>()

    /*.......................Buy Calling Plans................*/
    var buyBundleAdapter: CheckOutCallingPlansAdapter
    var deleteBundleItemPosition = MutableLiveData<Int>()

    init {
        Log.d("ViewModelIssue","init view model")
        numberData.value=null;
        isEmpty.value = true;
        recyclerViewAdapter = BuyNumberCheckoutAdapter(this);
        buyBundleAdapter= CheckOutCallingPlansAdapter(this);
    }

    fun getAdapter(): BuyNumberCheckoutAdapter {
        Log.d("ViewModelIssue","get Adapter")
        return recyclerViewAdapter
    }




    fun getBundleAdapter(): CheckOutCallingPlansAdapter {
        Log.d("ViewModelIssue","get Adapter")
        return buyBundleAdapter
    }


    fun setBundleAdapterData(data : ArrayList<GetCallingPlansDataModel>){
        buyBundleAdapter.setDataList(data)
        buyBundleAdapter.notifyDataSetChanged()



        if (data.isEmpty() || data == null) {
            isEmpty.postValue(true)
        } else {
            isEmpty.postValue(false)
        }

        var totalCost: Double = 0.0
        //getting total OS Cost
        for (item in data) {
            var itemPrice: Double = item.PRICE?.toDouble() ?: 0.0
            totalCost += itemPrice
        }

        Log.d("ViewModelIssue","$totalCost")
        osCost.set("\$ $totalCost")

        costData.postValue(totalCost)
    }

    fun setAdapterData(data: ArrayList<BuyNumbersCheckoutModel>) {
        Log.d("ViewModelIssue","set Adapter Data")
        Log.d("ViewModelIssue","List size :${data.size}")

        //buyNumberCheckoutList.postValue(data)

        recyclerViewAdapter.setBuyNumberCheckOutList(data)
        recyclerViewAdapter.notifyDataSetChanged()

        if (data.isEmpty() || data == null) {
            isEmpty.postValue(true)
        } else {
            isEmpty.postValue(false)
        }

        var totalCost: Double = 0.0
        //getting total OS Cost
        for (item in data) {
            var itemPrice: Double = item.price?.toDouble() ?: 0.0
            totalCost += itemPrice
        }

        Log.d("ViewModelIssue","total cost :$totalCost")
        osCost.set("Total OS Cost :$totalCost")

        costData.postValue(totalCost)
    }

    fun getDeleteBundleItemPositionObserver(): MutableLiveData<Int> {
        return deleteBundleItemPosition;
    }

    fun getDeleteItemPositionObserver(): MutableLiveData<Int> {
        return deleteItemPosition;
    }

    fun getSwitchItemPositionObserver(): MutableLiveData<Int> {
        return switchItemPosition;
    }

    fun  getPayWithOSBalanceDataObserver() : MutableLiveData<PayWithOSBalanceResponseModel?> {
        return payWithOSBalanceData;
    }


    fun  getPayWithRewardPointsDataObserver() : MutableLiveData<PayWithRewardPointsResponseModel?> {
        return payWithRewardPointsData;
    }

    fun  getAutoRenewNumberDataObserver() : MutableLiveData<AutoRenewNumberResponseModel?> {
        return autoRenewNumberData;
    }


    fun  getOrderTelephoneNumberResponseDataObserver() : MutableLiveData<OrderTelephoneNumberResponseModel?> {
        return orderTelephoneNumberResponseData;
    }

    fun getCostDataObserver():MutableLiveData<Double>{
        return costData;
    }

    fun getSwitchItemDataObserver():MutableLiveData<BuyNumbersCheckoutModel>{
        return switchItemData
    }



    fun makePayWithOSBalanceApiCall(requestModel: PayWithOSBalanceRequestModel) {


        val apiService = RetroInstance.getRetrofitInstance().create(APIService::class.java)
        val call = apiService.payWithOsBalance(Constant.credentials, requestModel)

        call.enqueue(object : Callback<PayWithOSBalanceResponseModel> {
            override fun onResponse(call: Call<PayWithOSBalanceResponseModel>, response: Response<PayWithOSBalanceResponseModel>) {

                Log.d("SubscriberApiCall", "response")
                if (response.isSuccessful) {


                    if (response.body() != null) {

                        payWithOSBalanceData.postValue(response.body())
                    }else{
                        payWithOSBalanceData.postValue(null)
                    }


                } else {
                    payWithOSBalanceData.postValue(null)
                }
            }

            override fun onFailure(call: Call<PayWithOSBalanceResponseModel>, t: Throwable) {
                Log.d("SubscriberApiCall", "Failure : ${t.message}")
                payWithOSBalanceData.postValue(null)
            }

        })

    }

    fun makePayWithRewardPointsApiCall(requestModel: PayWithRewardPointsRequestModel) {


        val apiService = RetroInstance.getRetrofitInstance().create(APIService::class.java)
        val call = apiService.payWithRewardPoints(Constant.credentials, requestModel)

        call.enqueue(object : Callback<PayWithRewardPointsResponseModel> {
            override fun onResponse(call: Call<PayWithRewardPointsResponseModel>, response: Response<PayWithRewardPointsResponseModel>) {

                Log.d("SubscriberApiCall", "response")
                if (response.isSuccessful) {


                    if (response.body() != null) {

                        payWithRewardPointsData.postValue(response.body())
                    }else{
                        payWithRewardPointsData.postValue(null)
                    }


                } else {
                    payWithRewardPointsData.postValue(null)
                }
            }

            override fun onFailure(call: Call<PayWithRewardPointsResponseModel>, t: Throwable) {
                Log.d("SubscriberApiCall", "Failure : ${t.message}")
                payWithRewardPointsData.postValue(null)
            }

        })

    }

    fun makeAutoRenewNumberApiCall(requestModel: AutoRenewNumberRequestModel) {


        val apiService = RetroInstance.getRetrofitInstance().create(APIService::class.java)
        val call = apiService.autoRenewNumber(Constant.credentials, requestModel)

        call.enqueue(object : Callback<AutoRenewNumberResponseModel> {
            override fun onResponse(call: Call<AutoRenewNumberResponseModel>, response: Response<AutoRenewNumberResponseModel>) {

                Log.d("SubscriberApiCall", "response")
                if (response.isSuccessful) {


                    if (response.body() != null) {

                        autoRenewNumberData.postValue(response.body())
                    }else{
                        autoRenewNumberData.postValue(null)
                    }


                } else {
                    autoRenewNumberData.postValue(null)
                }
            }

            override fun onFailure(call: Call<AutoRenewNumberResponseModel>, t: Throwable) {
                Log.d("SubscriberApiCall", "Failure : ${t.message}")
                autoRenewNumberData.postValue(null)
            }

        })

    }


    fun makeOrderTelephoneNumberApiCall(requestModel: OrderTelephoneNumberRequestModel) {


        val apiService = RetroInstance.getRetrofitInstance().create(APIService::class.java)
        val call = apiService.orderTelephoneNumber(Constant.credentials, requestModel)

        call.enqueue(object : Callback<OrderTelephoneNumberResponseModel> {
            override fun onResponse(call: Call<OrderTelephoneNumberResponseModel>, response: Response<OrderTelephoneNumberResponseModel>) {

                Log.d("OrderNumberIssue", "on response")
                if (response.isSuccessful) {


                    if (response.body() != null) {
                        Log.d("OrderNumberIssue", "body not null")
                        Log.d("OrderNumberIssue", "response body  :${response.body().toString()}")
                        orderTelephoneNumberResponseData.postValue(response.body())
                    }else{
                        Log.d("OrderNumberIssue", "response body null :${response.body()}")

                        orderTelephoneNumberResponseData.postValue(null)
                    }


                } else {
                    Log.d("OrderNumberIssue", "response not successful :${response.isSuccessful}")
                    orderTelephoneNumberResponseData.postValue(null)
                }
            }

            override fun onFailure(call: Call<OrderTelephoneNumberResponseModel>, t: Throwable) {
                Log.d("OrderNumberIssue", "Failure : ${t.message}")
                Log.d("OrderNumberIssue", "Failure :${t.message}")
                orderTelephoneNumberResponseData.postValue(null)
            }

        })

    }





    override fun onViewClick(view: View, position: Int) {
        /*............Delete button in bundle...................*/
        if (view.id==R.id.deleteBundleBtn){
            Log.d("DeleteBundle","Delete ")
            deleteBundleItemPosition.postValue(position);
        }

        if (view.id == R.id.deleteButton) {
            deleteItemPosition.postValue(position);
        } else if (view.id == R.id.switchBtn) {
            switchItemPosition.postValue(position)
            //           switchItemData.postValue(buyNumberCheckoutList.value?.get(position))
        }
    }

    override fun onRowClick(position: Int) {
        TODO("Not yet implemented")
    }
}