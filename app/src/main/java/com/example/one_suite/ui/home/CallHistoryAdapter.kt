package com.example.one_suite.ui.home

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.one_suite.databinding.CallForwardingRecyclerItemBinding
import com.example.one_suite.databinding.CallHistoryRecyclerItemBinding
import com.example.one_suite.model.subscriberDid.SubscriberDIDResponseDataModel
import com.example.one_suite.utils.SwitchClickListener

class CallHistoryAdapter(private val listener: SwitchClickListener) :
    RecyclerView.Adapter<CallHistoryAdapter.MyViewHolder>() {
    var items = ArrayList<CustomMyHistoryHomeModel>()

    fun setDataList(data: ArrayList<CustomMyHistoryHomeModel>) {
        this.items = data
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): CallHistoryAdapter.MyViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = CallHistoryRecyclerItemBinding.inflate(layoutInflater)
        return MyViewHolder(binding)
    }

    override fun getItemCount() = items.size

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.bind(items[position])

        holder.binding.switchBtn.setOnClickListener(View.OnClickListener {
            var isChecked:Boolean

            isChecked=holder.binding.switchBtn.isChecked
            listener.onSwitchClick(holder.binding.switchBtn, position,isChecked)
        })

    }

    class MyViewHolder(val binding: CallHistoryRecyclerItemBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(data: CustomMyHistoryHomeModel) {
            binding.viewModel = data
            binding.executePendingBindings()
        }
    }


}