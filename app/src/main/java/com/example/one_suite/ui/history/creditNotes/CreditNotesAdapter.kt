package com.example.one_suite.ui.history.creditNotes

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.one_suite.databinding.*
import com.example.one_suite.model.subscriberDid.SubscriberDIDResponseDataModel
import com.example.one_suite.ui.home.GetSubscriberPromotionResponseDataModel
import com.example.one_suite.ui.home.GetSubscriberPromotionResponseModel
import com.example.one_suite.utils.SwitchClickListener

class CreditNotesAdapter() :
    RecyclerView.Adapter<CreditNotesAdapter.MyViewHolder>() {
    var items = ArrayList<CreditNotesResponseDataModel>()

    fun setDataList(data: ArrayList<CreditNotesResponseDataModel>) {
        this.items = data
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): CreditNotesAdapter.MyViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = CreditNotesRecyclerviewItemsBinding.inflate(layoutInflater)
        return MyViewHolder(binding)
    }

    override fun getItemCount() = items.size

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.bind(items[position])

    }

    class MyViewHolder(val binding: CreditNotesRecyclerviewItemsBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(data: CreditNotesResponseDataModel) {
            binding.viewModel = data
            binding.executePendingBindings()
        }
    }


}