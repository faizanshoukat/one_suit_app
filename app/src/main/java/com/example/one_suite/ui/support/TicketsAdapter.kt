package com.example.one_suite.ui.support

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.one_suite.databinding.CallForwardingRecyclerItemBinding
import com.example.one_suite.databinding.TicketsRecyclerViewItemsBinding
import com.example.one_suite.model.subscriberDid.SubscriberDIDResponseDataModel
import com.example.one_suite.model.support.GetAllTicketsResponseDataModel
import com.example.one_suite.utils.ClickListener
import com.example.one_suite.utils.SwitchClickListener

class TicketsAdapter(private val listener: ClickListener) :
    RecyclerView.Adapter<TicketsAdapter.MyViewHolder>() {
    var items = ArrayList<GetAllTicketsResponseDataModel>()

    fun setDataList(data: ArrayList<GetAllTicketsResponseDataModel>) {
        this.items = data
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): TicketsAdapter.MyViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = TicketsRecyclerViewItemsBinding.inflate(layoutInflater)
        return MyViewHolder(binding)
    }

    override fun getItemCount() = items.size

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.bind(items[position])

        holder.binding.ticketDetailBtn.setOnClickListener(View.OnClickListener {
            listener.onViewClick(holder.binding.ticketDetailBtn,position)
        })

    }

    class MyViewHolder(val binding: TicketsRecyclerViewItemsBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(data: GetAllTicketsResponseDataModel) {
            binding.viewModel = data
            binding.executePendingBindings()
        }
    }


}