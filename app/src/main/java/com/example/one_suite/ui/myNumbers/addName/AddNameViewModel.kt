package com.example.one_suite.ui.myNumbers.addName

import android.app.PendingIntent.getActivity
import android.content.Context
import android.content.Intent
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import com.example.one_suite.MainActivity
import com.example.one_suite.R
import com.example.one_suite.model.subscriberDid.SubscriberDIDResponseDataModel
import com.example.one_suite.network.APIService
import com.example.one_suite.network.RetroInstance
import com.example.one_suite.utils.ClickListener
import com.example.one_suite.utils.Constant
import com.example.one_suite.utils.EditTextClickListener
import com.resocoder.databinding.model.subscriberDid.SubscriberDIDRequestModel
import com.resocoder.databinding.model.subscriberDid.SubscriberDIDResponseModel
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class AddNameViewModel : ViewModel(), EditTextClickListener {

    var myNumbersListData: MutableLiveData<ArrayList<SubscriberDIDResponseDataModel>>
    var myNumbersAdapter: AddNameAdapter

    var numberData: MutableLiveData<SubscriberDIDResponseDataModel>
    var addNameResponseData = MutableLiveData<String?>()

    var nameTextData = MutableLiveData<AddNameRequestModel>()

    init {
        myNumbersListData = MutableLiveData()
        myNumbersAdapter = AddNameAdapter(this)
        numberData = MutableLiveData()
    }


    fun getMyNumbersListAdapter(): AddNameAdapter {
        return myNumbersAdapter
    }

    fun getAddNameResponseDataObserver(): MutableLiveData<String?> {
        return addNameResponseData
    }

    fun setMyNumbersListAdapterData(data: ArrayList<SubscriberDIDResponseDataModel>) {
        Log.d("MyNumbersAdapterIssue", "set Adapter Data :${data.size}")
        myNumbersAdapter.setDataList(data)
        myNumbersAdapter.notifyDataSetChanged()
    }

    private fun setNumberData(data: SubscriberDIDResponseDataModel) {
        numberData.postValue(data)
    }


    fun getMyNumbersListDataObserver(): MutableLiveData<ArrayList<SubscriberDIDResponseDataModel>> {
        return myNumbersListData
    }


    fun getNameTextObserver(): MutableLiveData<AddNameRequestModel> {
        return nameTextData
    }


    fun getNumberDataObserver(): MutableLiveData<SubscriberDIDResponseDataModel> {
        return numberData
    }


    fun makeSubscriberDIDApiCall() {


        val subscriberDIDRequest: SubscriberDIDRequestModel =
            SubscriberDIDRequestModel(Constant.account)

        val apiService = RetroInstance.getRetrofitInstance().create(APIService::class.java)
        val call = apiService.getSubscriberDID(Constant.credentials, subscriberDIDRequest)

        call.enqueue(object : Callback<SubscriberDIDResponseModel> {
            override fun onResponse(
                call: Call<SubscriberDIDResponseModel>,
                DIDResponse: Response<SubscriberDIDResponseModel>
            ) {

                Log.d("NumberApiCall", "Successful")
                if (DIDResponse.isSuccessful) {


                    if (DIDResponse.body() != null) {
                        Log.d("NumberApiCall", "Body Not Null")
                        myNumbersListData.postValue(DIDResponse.body()!!.SubscriberDIDs)
                    } else {
                        myNumbersListData.postValue(null)
                    }


                }

            }

            override fun onFailure(call: Call<SubscriberDIDResponseModel>, t: Throwable) {
                //Toast.makeText(applicationContext,"Error !! while fetching data", Toast.LENGTH_SHORT).show()
                Log.d("NumberApiCall", "Failure : ${t.message}")

                myNumbersListData.postValue(null)
            }

        })


    }

    fun makeAddNameApiCall(requestModel: AddNameRequestModel) {


        val apiService = RetroInstance.getRetrofitInstance().create(APIService::class.java)
        val call = apiService.addName(Constant.credentials, requestModel)

        call.enqueue(object : Callback<AddNameResponseModel> {
            override fun onResponse(
                call: Call<AddNameResponseModel>,
                DIDResponse: Response<AddNameResponseModel>
            ) {

                Log.d("NumberApiCall", "Successful")
                if (DIDResponse.isSuccessful) {


                    if (DIDResponse.body() != null) {
                        Log.d("MyNumbersAdapterIssue", "Body Not Null")
                        addNameResponseData.postValue(DIDResponse.body()!!.RESPONSE)
                    } else {
                        addNameResponseData.postValue(null)
                    }


                } else {
                    addNameResponseData.postValue(null)
                }

            }

            override fun onFailure(call: Call<AddNameResponseModel>, t: Throwable) {
                //Toast.makeText(applicationContext,"Error !! while fetching data", Toast.LENGTH_SHORT).show()
                Log.d("NumberApiCall", "Failure : ${t.message}")

                addNameResponseData.postValue(null)
            }

        })


    }


    override fun onViewClick(view: View, position: Int, text: String) {
        Log.d("HideIssue", "on View Click Text :${myNumbersListData.value?.get(position)?.Number}")

        if (view.id == R.id.addButton) {
            if (text.isNotEmpty()){
                var number=myNumbersListData.value?.get(position)?.Number
                if (number!=null){
                    Log.d("HideIssue", "Name Text")
                    var requestModel=AddNameRequestModel(Constant.account,text,number)
                    nameTextData.postValue(requestModel)
//



                }
            }

        }else if (view.id == R.id.EditName) {
            if (text.isNotEmpty()){
                var number=myNumbersListData.value?.get(position)?.Number
                if (number!=null){
                    Log.d("HideIssue", "Name Text")
                    var requestModel=AddNameRequestModel(Constant.account,text,number)
                    nameTextData.postValue(requestModel)
                    myNumbersAdapter.notifyDataSetChanged()
                }
            }

        }
    }


}