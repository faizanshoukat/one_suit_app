package com.example.one_suite.ui.buyNumber

import android.app.AlertDialog
import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.Spinner
import android.widget.Toast
import androidx.core.text.set
import androidx.core.view.get
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.one_suite.MainActivity
import com.example.one_suite.databinding.BuyNumberFragmentBinding
import com.example.one_suite.model.*
import com.example.one_suite.ui.buyNumber.buyNumberList.BuyNumberListActivity
import com.example.one_suite.utils.customSpiner.CustomSpinerAdapter
import com.example.one_suite.utils.customSpiner.Data
import com.resocoder.databinding.utils.DialogUtils
import com.resocoder.databinding.utils.DialogUtils.Companion.setProgressDialog


class BuyNumberFragment : Fragment(){
//    , AdapterView.OnItemSelectedListener

    //spiner
    var countydataModel: MutableList<Data> = mutableListOf<Data>()
    var typedataModel: MutableList<Data> = mutableListOf<Data>()
    var statedataModel: MutableList<Data> = mutableListOf<Data>()
    var citiesdataModel: MutableList<Data> = mutableListOf<Data>()


    //    private lateinit var galleryViewModel: GalleryViewModel
    private lateinit var buyNumberViewModel: BuyNumberViewModel
    private var _binding: BuyNumberFragmentBinding? = null


    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    var searchList = ArrayList<SearchTelephoneNumberDataModel>();
    var priceList = ArrayList<GetLocalRatesDataModel>();


    lateinit var progressDialog: ProgressDialog

    var country = ""
    var type = ""
    var state = "WA"
    var city = ""

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        buyNumberViewModel = ViewModelProvider(this).get(BuyNumberViewModel::class.java)

        _binding = BuyNumberFragmentBinding.inflate(inflater, container, false).apply {
            this.lifecycleOwner = this@BuyNumberFragment
            this.viewmodel = buyNumberViewModel
        }
        val root: View = binding.root


        //progress dialog
        var dialog = DialogUtils.setProgressDialog(container!!.context, "Please Wait....")



        Log.d("FaizanTesting1","buy Number Fragment")
        //for click listener
        progressDialog = ProgressDialog(getContext())
        progressDialog.setMessage("Please wait...")

        //default value for quantity
        //_binding!!.quantity.setText("1")
        buyNumberViewModel.quantity.set("1")


        //Country custom spinner
        countydataModel.add(Data(1, "USA"))


        val countrySpinner = binding.coutrySpinner
        val customAdapter = CustomSpinerAdapter()
        customAdapter.customeSpinnerAdapter(requireContext(), countydataModel)
        countrySpinner.adapter = customAdapter

        countrySpinner!!.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                adapter: AdapterView<*>?,
                view: View?,
                position: Int,
                p3: Long
            ) {
                if (countydataModel.size > 0) {
                    country = countydataModel[position].country.toString()
                    Log.d("SpinnerText","Country :$country")
                }

            }

            override fun onNothingSelected(p0: AdapterView<*>?) {
            }

        }


        /* .............Type Spinner..............*/
        ///Type Custom
        typedataModel.add(Data(1, "Local"))
        typedataModel.add(Data(1, "Toll-Free"))


        val typeSpiner = binding.typeSpinner
        val typeAdapter = CustomSpinerAdapter()
        typeAdapter.customeSpinnerAdapter(requireContext(), typedataModel)
        typeSpiner.adapter = typeAdapter
        typeSpiner!!.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                adapter: AdapterView<*>?,
                view: View?,
                position: Int,
                p3: Long
            ) {
                if (typedataModel.size > 0) {
                    type = typedataModel[position].country.toString()
                    Log.d("SpinnerText","Type :$type")
                }

            }

            override fun onNothingSelected(p0: AdapterView<*>?) {
            }

        }
        /* .............Type Spinner..............*/


/* .............State Spinner..............*/
        val stateAdapter = CustomSpinerAdapter()
        val stateSpinner = binding.statesSpinner

        stateSpinner!!.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                adapter: AdapterView<*>?,
                view: View?,
                position: Int,
                p3: Long
            ) {


                if (statedataModel.size > 0) {

                    if (position==0){
                        state="WA"


                        progressDialog.show()
                        buyNumberViewModel.getCitiesApiCall("")

//                        progressDialog.show()
//                        buyNumberViewModel.getCitiesApiCall(statedataModel[position]!!.country!!)
                    }else{
                        state = statedataModel[position].country.toString()
                        Log.d("SpinnerText","State :$state")

                        progressDialog.show()
                        buyNumberViewModel.getCitiesApiCall(statedataModel[position]!!.country!!)
                    }
                }

            }

            override fun onNothingSelected(p0: AdapterView<*>?) {
            }

        }


/* .............State Spinner..............*/


        /* .............City Spinner..............*/
        val cityAdapter = CustomSpinerAdapter()
        val citySpinner = binding.citiesSpinner

        citiesdataModel.add(Data(1,"--ALL--"))
        //set Data
        cityAdapter.customeSpinnerAdapter(requireContext(), citiesdataModel)
        citySpinner.adapter = cityAdapter



        citySpinner!!.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                adapter: AdapterView<*>?,
                view: View?,
                position: Int,
                p3: Long
            ) {
                if (citiesdataModel.size > 0) {

                    if (position==0){
                        city=""
                    }else{
                        city = citiesdataModel[position].country.toString()
                    }
                    Log.d("SpinnerText","City :$city")
                }

            }

            override fun onNothingSelected(p0: AdapterView<*>?) {
            }

        }
/* .............City Spinner..............*/


        //calling API
        dialog!!.show()
        buyNumberViewModel.getStatesApiCall()


        //var statesList = ArrayList<CustomDialogModel>()
        //Receiving States Data
        buyNumberViewModel.getStatesListDataObserver().observe(viewLifecycleOwner, Observer {
            //progressDialog.dismiss()

            dialog.dismiss()
            if (it == null) {
                Log.d("StatesIssue", "There is an issue in fetching data")
            } else {
                Log.d("StatesIssue", "Sates Size: ${it.size}")
                statedataModel.clear()
                statedataModel.add(Data(1, "--ALL--"))
                for (item in it) {
                    statedataModel.add(Data(1, item.State))

                }

//                statedataModel.sortBy { data: Data -> item.State }
                statedataModel.sortBy { Data -> Data.country }

                stateAdapter.customeSpinnerAdapter(requireContext(), statedataModel)
                stateSpinner.adapter = stateAdapter

            }
        })


        //Receiving Cities Data
        buyNumberViewModel.getCitiesListDataObserver().observe(viewLifecycleOwner, Observer {
            //progressDialog.dismiss()

            progressDialog.dismiss()
            if (it == null) {
                Log.d("StatesIssue", "ThePROre is an issue in fetching data")
                citiesdataModel.clear()
                citiesdataModel.add(Data(1, "--ALL--"))
            } else {
                Log.d("StatesIssue", "Sates Size: ${it.size}")
                citiesdataModel.clear()
                citiesdataModel.add(Data(1, "--ALL--"))
                for (item in it) {
                    citiesdataModel.add(Data(1, item))

                }

                citiesdataModel.sortBy { Data -> Data.country }

                //set Data
                cityAdapter.customeSpinnerAdapter(requireContext(), citiesdataModel)
                citySpinner.adapter = cityAdapter


            }
        })


        /* search button functionality */
        _binding!!.searchButton.setOnClickListener(View.OnClickListener {

//            var cityText: String = _binding!!.citiesSpinner
//            Log.d("SpinnerIssue", "City :$cityText")

            var qty: String = _binding!!.quantity.text.toString()
            var code: String = _binding!!.areaCode.text.toString()
            //var state: String = _binding!!.s.text.toString()
//            var state: String = _binding!!.s.text.toString()

//            var state: String = ""


            Log.d("SearchTelephoneApiCall", "qty :$qty")

            try {
                var qtyInteger: Int = qty.toInt();
                if (qtyInteger < 5) {
                    qty = "5"
                }
                println("The value is $qtyInteger")
            } catch (ex: NumberFormatException) {
                println("The given string is non-numeric")
                qty = "5"
            }


            var Account: String = "52426418377"
            var tnMask: String = "xxxxxxxxxx"
            var province = state
            var city = city
            var quantity = qty
            var pageSort = ""

            Log.d(
                "SearchTelephoneApiCall",
                "Account :$Account , quantity :$quantity ,  Province :$province"
            )

            if (province == "" || province == null) {

                Log.d("SearchTelephoneApiCall", "Province null")

                var request = SearchTelephoneNumberEmptyRequestModel(
                    Account,
                    tnMask,
                    city,
                    quantity,
                    pageSort
                );

                //progressDialog.show()
                dialog.show()
                buyNumberViewModel.searchTelephoneNumberEmptyApiCall(request);
            } else {


                Log.d("SearchTelephoneApiCall", "Province not null")

                var request = SearchTelephoneNumberRequestModel(
                    Account,
                    tnMask,
                    province,
                    city,
                    quantity,
                    pageSort
                );

                //progressDialog.show()
                dialog.show()
                buyNumberViewModel.searchTelephoneNumberApiCall(request);
            }

        })


        /* getting search list API result */
        buyNumberViewModel.getNumberListDataObserver().observe(viewLifecycleOwner, Observer {
            //progressDialog.dismiss()
            //Toast.makeText(this,"error in fecting data",Toast.LENGTH_SHORT).show();
            if (it == null) {
                Log.d("SearchTelephoneApiCall", "Data received is null")
            } else {
                //viewModel.setAdapterData(it)
                searchList = it
                Log.d("SearchTelephoneApiCall", "Data is not null :${it.size}")
            }


            var requestModel = GetLocalRatesRequestModel("", "Local")
            buyNumberViewModel.getLocalRatesApiCall(requestModel)


//            _binding!!.searchButton.findNavController().navigate(R.id.action_buyNumberFragment_to_category1Fragment)
//           findNavController().navigate(R.id.action_buyNumberFragment_to_category1Fragment)


        })


        /* getting Local Rates  API result */
        buyNumberViewModel.getLocalRatesDataObserver().observe(viewLifecycleOwner, Observer {
            dialog.dismiss()
            if (it == null) {

            } else {
                priceList = it
            }

            var qty: String = _binding!!.quantity.text.toString()
            if (qty==""){
                qty="1"
            }
//            (activity as MainActivity?)?.displayBuyNumberListFragment(searchList, priceList)

            var intent=Intent(context,BuyNumberListActivity::class.java)
//            val bundle = Bundle()
//            intent.putExtra("Bundle", bundle)


            intent.putParcelableArrayListExtra("DataList",searchList)
            intent.putParcelableArrayListExtra("RateList",priceList)
            intent.putExtra("Qty",qty)
            intent.putExtra("State",state)
            startActivity(intent)


        })

        return root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

//    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
//
//        Log.d("SpinnerIssue", "Position :$position")
//        if (statedataModel != null) {
//            progressDialog.show()
//            buyNumberViewModel.getCitiesApiCall(statedataModel[position]!!.country!!)
//        }
//
////
////        Toast.makeText(
////            getContext(),
////            countydataModel[position].id.toString() + " " + countydataModel[position].country,
////            Toast.LENGTH_LONG
////        ).show();
//    }
//
//    override fun onNothingSelected(parent: AdapterView<*>?) {
//        TODO("Not yet implemented")
//    }
}