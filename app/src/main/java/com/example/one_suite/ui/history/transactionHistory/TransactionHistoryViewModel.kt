package com.example.one_suite.ui.history.transactionHistory

import android.content.Context
import android.util.Log
import android.view.View
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.one_suite.R
import com.example.one_suite.model.callForwarding.setSubscriberDID.SetSubscriberDIDRequestModel
import com.example.one_suite.model.callForwarding.setSubscriberDID.SetSubscriberDIDResponseModel
import com.example.one_suite.model.subscriberDid.SubscriberDIDResponseDataModel
import com.example.one_suite.model.transactionHistory.GetTransactionHistoryRequestModel
import com.example.one_suite.model.transactionHistory.GetTransactionHistoryResponseDataModel
import com.example.one_suite.model.transactionHistory.GetTransactionHistoryResponseModel
import com.example.one_suite.network.APIService
import com.example.one_suite.network.RetroInstance
import com.example.one_suite.ui.callForwarding.CallForwardingAdapter
import com.example.one_suite.ui.home.GetSubscriberPromotionRequestModel
import com.example.one_suite.ui.home.GetSubscriberPromotionResponseDataModel
import com.example.one_suite.ui.home.GetSubscriberPromotionResponseModel
import com.example.one_suite.utils.Constant
import com.example.one_suite.utils.SwitchClickListener
import com.resocoder.databinding.model.subscriberDid.SubscriberDIDRequestModel

import com.resocoder.databinding.model.subscriberDid.SubscriberDIDResponseModel
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class TransactionHistoryViewModel() : ViewModel() {

    var transactionHistoryListdata: MutableLiveData<ArrayList<GetTransactionHistoryResponseDataModel>> = MutableLiveData()
    var recyclerViewAdapter= TransactionHistoryAdapter()


    fun setAdapterData(data: ArrayList<GetTransactionHistoryResponseDataModel>) {
        recyclerViewAdapter.setDataList(data)
        recyclerViewAdapter.notifyDataSetChanged()
    }


    fun getAdapter(): TransactionHistoryAdapter {
        return recyclerViewAdapter
    }

    fun getTransactionHistoryListDataObserver():MutableLiveData<ArrayList<GetTransactionHistoryResponseDataModel>>{
        return transactionHistoryListdata
    }



    fun makeTransactionHistoryApiCall() {


        val sdf = SimpleDateFormat("yyyy-MM-dd")
        val currentDate = sdf.format(Date())

        var FROM_DATE: String = "2015-10-31"
        var TO_DATE: String = currentDate


        val requestModel: GetTransactionHistoryRequestModel =
            GetTransactionHistoryRequestModel(Constant.account, FROM_DATE, TO_DATE)

        Log.d("SubscriberApiCall", "country list api call")
        val apiService = RetroInstance.getRetrofitInstance().create(APIService::class.java)
        val call = apiService.getTransactionHistory(Constant.credentials, requestModel)

        call.enqueue(object : Callback<GetTransactionHistoryResponseModel> {
            override fun onResponse(call: Call<GetTransactionHistoryResponseModel>, response: Response<GetTransactionHistoryResponseModel>) {

                Log.d("SubscriberApiCall", "response")
                if (response.isSuccessful) {



                    if (response.body() != null) {
                        transactionHistoryListdata.postValue(response.body()!!.PaymentHistories)
                    }else{
                        transactionHistoryListdata.postValue(null)
                    }


                } else {
                    transactionHistoryListdata.postValue(null)
                }
            }

            override fun onFailure(call: Call<GetTransactionHistoryResponseModel>, t: Throwable) {
                Log.d("SubscriberApiCall", "Failure : ${t.message}")
                transactionHistoryListdata.postValue(null)

            }

        })

    }

}
