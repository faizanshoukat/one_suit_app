package com.example.one_suite.ui.support.newTicket

import android.util.Log
import android.view.View
import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.one_suite.model.*
import com.example.one_suite.model.support.newTicket.generateTicket.GenerateTicketRequestModel
import com.example.one_suite.model.support.newTicket.generateTicket.GenerateTicketResponseModel
import com.example.one_suite.model.support.newTicket.ticketPriorities.GetTicketPrioritiesRequestModel
import com.example.one_suite.model.support.newTicket.ticketPriorities.GetTicketPrioritiesResponseDataModel
import com.example.one_suite.model.support.newTicket.ticketPriorities.GetTicketPrioritiesResponseModel
import com.example.one_suite.model.support.newTicket.ticketTypes.GetTicketTypesResponseDataModel
import com.example.one_suite.model.support.newTicket.ticketTypes.GetTicketTypesResponseModel
import com.example.one_suite.network.APIService
import com.example.one_suite.network.RetroInstance
import com.example.one_suite.ui.buyNumber.*
import com.example.one_suite.utils.ClickListener
import com.example.one_suite.utils.Constant
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class NewTicketViewModel : ViewModel(), ClickListener {

    //    var ticket = ObservableField<String>("")
//    var priority = ObservableField<String>("")
    var title = ObservableField<String>("")
    var description = ObservableField<String>("")


    var ticketTypesListData: MutableLiveData<ArrayList<GetTicketTypesResponseDataModel>> =
        MutableLiveData()
    var ticketPriorityListData: MutableLiveData<ArrayList<GetTicketPrioritiesResponseDataModel>> =
        MutableLiveData()
    var generateTicketResult: MutableLiveData<GenerateTicketResponseModel> = MutableLiveData()


    fun getTicketTypesDataObserver(): MutableLiveData<ArrayList<GetTicketTypesResponseDataModel>> {
        return ticketTypesListData
    }

    fun getTicketPriorityDataObserver(): MutableLiveData<ArrayList<GetTicketPrioritiesResponseDataModel>> {
        return ticketPriorityListData
    }

    fun getGenerateTicketResultObserver(): MutableLiveData<GenerateTicketResponseModel> {
        return generateTicketResult
    }

    fun makeTicketTypeApiCall() {

        Log.d("SubscriberApiCall", "country list api call")
        val apiService = RetroInstance.getRetrofitInstance().create(APIService::class.java)
        val call = apiService.getTicketTypes(Constant.credentials)

        call.enqueue(object : Callback<GetTicketTypesResponseModel> {
            override fun onResponse(
                call: Call<GetTicketTypesResponseModel>,
                response: Response<GetTicketTypesResponseModel>
            ) {

                Log.d("SubscriberApiCall", "response")
                if (response.isSuccessful) {


                    if (response.body() != null) {

                        ticketTypesListData.postValue(response.body()!!.TicketTypes)
                    } else {
                        ticketTypesListData.postValue(null)
                    }


                } else {
                    ticketTypesListData.postValue(null)
                }
            }

            override fun onFailure(call: Call<GetTicketTypesResponseModel>, t: Throwable) {
                Log.d("SubscriberApiCall", "Failure : ${t.message}")
                ticketTypesListData.postValue(null)

            }

        })

    }

    fun makeTicketPriorityApiCall(requestModel: GetTicketPrioritiesRequestModel) {
        Log.d("SubscriberApiCall", "Ticket Priority API Call")
        val apiService = RetroInstance.getRetrofitInstance().create(APIService::class.java)
        val call = apiService.getTicketPriorities(Constant.credentials, requestModel)

        call.enqueue(object : Callback<GetTicketPrioritiesResponseModel> {
            override fun onResponse(
                call: Call<GetTicketPrioritiesResponseModel>,
                response: Response<GetTicketPrioritiesResponseModel>
            ) {

                Log.d("SubscriberApiCall", "On response")
                if (response.isSuccessful) {


                    if (response.body() != null) {

                        ticketPriorityListData.postValue(response.body()!!.TicketPriorities)
                    } else {
                        ticketPriorityListData.postValue(null)
                    }


                } else {
                    ticketPriorityListData.postValue(null)
                }
            }

            override fun onFailure(call: Call<GetTicketPrioritiesResponseModel>, t: Throwable) {
                Log.d("SubscriberApiCall", "Failure : ${t.message}")
                ticketPriorityListData.postValue(null)

            }

        })

    }


    fun makeGenerateTicketApiCall(requestModel: GenerateTicketRequestModel) {
        Log.d("SubscriberApiCall", "Ticket Priority API Call")
        val apiService = RetroInstance.getRetrofitInstance().create(APIService::class.java)
        val call = apiService.generateTicket(Constant.credentials, requestModel)

        call.enqueue(object : Callback<GenerateTicketResponseModel> {
            override fun onResponse(
                call: Call<GenerateTicketResponseModel>,
                response: Response<GenerateTicketResponseModel>
            ) {

                Log.d("SubscriberApiCall", "On response")
                if (response.isSuccessful) {


                    if (response.body() != null) {
                        generateTicketResult.postValue(response.body())
                    } else {
                        generateTicketResult.postValue(null)
                    }


                } else {
                    generateTicketResult.postValue(null)
                }
            }

            override fun onFailure(call: Call<GenerateTicketResponseModel>, t: Throwable) {
                Log.d("SubscriberApiCall", "Failure : ${t.message}")
                generateTicketResult.postValue(null)

            }

        })

    }


    override fun onViewClick(view: View, position: Int) {
//        TODO("Not yet implemented")
//        Log.d("SearchTelephoneApiCall", "Failure : ${numberListData.}")

//        Log.d(
//            "SearchTelephoneApiCall",
//            "On View Click " + (numberListData.value?.get(position)?.telephoneNumber ?: "Null exception")
//        )
//
//        numberData.postValue(numberListData.value?.get(position));
//
//

    }

    override fun onRowClick(position: Int) {
        TODO("Not yet implemented")
    }

}