import android.app.Application

object App : Application() {
    // return true or false
    // Gloabl declaration of variable to use in whole app
    var isActivityVisible // Variable that will check the
            = false

    // current activity state
    var isFirstTime = false

    fun activityResumed() {
        isActivityVisible = true // this will set true when activity resumed
    }

    fun activityPaused() {
        isActivityVisible = false // this will set false when activity paused
    }

    fun isFirstTime(isFirstTime: Boolean?) {
        var isFirstTime = isFirstTime
        isFirstTime = isFirstTime
    }
}