package com.example.one_suite.utils

import android.view.View

interface EditTextClickListener {
    fun onViewClick(view:View,position:Int,text:String)

}