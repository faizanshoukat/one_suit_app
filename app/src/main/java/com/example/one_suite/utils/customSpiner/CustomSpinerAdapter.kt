package com.example.one_suite.utils.customSpiner

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import androidx.lifecycle.MutableLiveData
import com.example.one_suite.R
import com.example.one_suite.utils.ClickListener

class CustomSpinerAdapter : BaseAdapter() {

    lateinit var layoutInflater: LayoutInflater
    lateinit var country: MutableList<Data>
    lateinit var context: Context
//    lateinit var clickListener: ClickListener


    override fun getCount(): Int {
        return country.size
    }

    override fun getItem(p0: Int): Any? {
        return null
    }

    override fun getItemId(p0: Int): Long {
        return 0
    }

    override fun getView(p0: Int, p1: View?, p2: ViewGroup?): View {
        var view = layoutInflater.inflate(R.layout.custom_spinner_items, null)
        view.findViewById<TextView>(R.id.textView).setText(country.get(p0).country)

//        var country = view.findViewById<TextView>(R.id.textView)
//        country.setOnClickListener {
//            clickListener.onRowClick(p0)
//        }



        return view
    }

    fun customeSpinnerAdapter(context: Context, name: MutableList<Data>) {
        this.country = name
        this.context = context
        layoutInflater = LayoutInflater.from(context)
    }


}