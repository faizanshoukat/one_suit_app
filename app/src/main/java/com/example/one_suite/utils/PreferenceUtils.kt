package com.example.one_suite.utils

import android.content.Context
import android.content.SharedPreferences
import com.example.one_suite.utils.PreferenceUtils

object PreferenceUtils {

    var DB_NAME = "LOGIN_DB"

    var NAME = "login"
    var EMAIL = "email"
    var PASSWORD = "password"
    var API_KEY = "APIKey"
    var CONTENT_TYPE = "ContentType"
    var Is_Profile_Link = "IsProfileLink"
    var Profile_Link = "ProfileLink"
    var Full_Name = "FullName"
    var Designation = "Designation"
    var Is_Online = "IsOnline"


    fun getEmail(context: Context): String? {
        val prefs = context.getSharedPreferences(DB_NAME, Context.MODE_PRIVATE)
        return prefs.getString(EMAIL, null)
    }

    fun setEmail(Email: String?, context: Context) {
        val prefs = context.getSharedPreferences(DB_NAME, Context.MODE_PRIVATE)
        val prefsEditor = prefs.edit()
        prefsEditor.putString(EMAIL, Email)
        prefsEditor.apply()
    }

    fun getPassword(context: Context): String? {
        val prefs = context.getSharedPreferences(DB_NAME, Context.MODE_PRIVATE)
        return prefs.getString(PASSWORD, null)
    }

    fun setPassword(Password: String?, context: Context) {
        val prefs = context.getSharedPreferences(DB_NAME, Context.MODE_PRIVATE)
        val prefsEditor = prefs.edit()
        prefsEditor.putString(PASSWORD, Password)
        prefsEditor.apply()
    }

    fun getApiKey(context: Context): String? {
        val prefs = context.getSharedPreferences(DB_NAME, Context.MODE_PRIVATE)
        return prefs.getString(API_KEY, null)
    }

    fun setApiKey(API_KEY: String?, context: Context) {
        val prefs = context.getSharedPreferences(DB_NAME, Context.MODE_PRIVATE)
        val prefsEditor = prefs.edit()
        prefsEditor.putString(PreferenceUtils.API_KEY, API_KEY)
        prefsEditor.apply()
    }

    fun getContentType(context: Context): String? {
        val prefs = context.getSharedPreferences(DB_NAME, Context.MODE_PRIVATE)
        return prefs.getString(CONTENT_TYPE, null)
    }

    fun setContentType(CONTENT_TYPE: String?, context: Context) {
        val prefs = context.getSharedPreferences(DB_NAME, Context.MODE_PRIVATE)
        val prefsEditor = prefs.edit()
        prefsEditor.putString(PreferenceUtils.CONTENT_TYPE, CONTENT_TYPE)
        prefsEditor.apply()
    }

    fun getIs_Profile_Link(context: Context): Boolean {
        val prefs = context.getSharedPreferences(DB_NAME, Context.MODE_PRIVATE)
        return prefs.getBoolean(Is_Profile_Link, false)
    }

    fun setIs_Profile_Link(Is_Profile_Link: Boolean?, context: Context) {
        val prefs = context.getSharedPreferences(DB_NAME, Context.MODE_PRIVATE)
        val prefsEditor = prefs.edit()
        prefsEditor.putBoolean(PreferenceUtils.Is_Profile_Link, Is_Profile_Link!!)
        prefsEditor.apply()
    }

    fun getProfile_Link(context: Context): String? {
        val prefs = context.getSharedPreferences(DB_NAME, Context.MODE_PRIVATE)
        return prefs.getString(Profile_Link, null)
    }

    fun setProfile_Link(Profile_Link: String?, context: Context) {
        val prefs = context.getSharedPreferences(DB_NAME, Context.MODE_PRIVATE)
        val prefsEditor = prefs.edit()
        prefsEditor.putString(PreferenceUtils.Profile_Link, Profile_Link)
        prefsEditor.apply()
    }

    fun getFull_Name(context: Context): String? {
        val prefs = context.getSharedPreferences(DB_NAME, Context.MODE_PRIVATE)
        return prefs.getString(Full_Name, null)
    }

    fun setFull_Name(Full_Name: String?, context: Context) {
        val prefs = context.getSharedPreferences(DB_NAME, Context.MODE_PRIVATE)
        val prefsEditor = prefs.edit()
        prefsEditor.putString(PreferenceUtils.Full_Name, Full_Name)
        prefsEditor.apply()
    }

    fun getDesignation(context: Context): String? {
        val prefs = context.getSharedPreferences(DB_NAME, Context.MODE_PRIVATE)
        return prefs.getString(Designation, null)
    }

    fun setDesignation(Designation: String?, context: Context) {
        val prefs = context.getSharedPreferences(DB_NAME, Context.MODE_PRIVATE)
        val prefsEditor = prefs.edit()
        prefsEditor.putString(PreferenceUtils.Designation, Designation)
        prefsEditor.apply()
    }

    fun getIsOnline(context: Context): Boolean {
        val prefs = context.getSharedPreferences(DB_NAME, Context.MODE_PRIVATE)
        return prefs.getBoolean(Is_Online, false)
    }

    fun setIs_Online(Is_Online: Boolean?, context: Context) {
        val prefs = context.getSharedPreferences(DB_NAME, Context.MODE_PRIVATE)
        val prefsEditor = prefs.edit()
        prefsEditor.putBoolean(PreferenceUtils.Is_Online, Is_Online!!)
        prefsEditor.apply()
    }

    fun DeleteDBData(context: Context) {
        val sharedPreferences = context.getSharedPreferences(DB_NAME, Context.MODE_PRIVATE)
        val editor = sharedPreferences.edit()
        editor.clear()
        editor.commit()
    }
}