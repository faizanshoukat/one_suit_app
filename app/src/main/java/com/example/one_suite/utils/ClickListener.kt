package com.example.one_suite.utils

import android.view.View

interface ClickListener {
    fun onViewClick(view:View,position:Int)
    fun onRowClick(position:Int)
}