package com.example.one_suite.utils

import android.util.Log
import java.text.ParseException
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.time.format.DateTimeFormatter

class DateConversion {
    companion object {
        fun getAbbreviatedFromDateTime(
            dateTime: String,
            dateFormat: String,
            field: String
        ): String? {
            val input = SimpleDateFormat(dateFormat)
            val output = SimpleDateFormat(field)
            try {
                val getAbbreviate = input.parse(dateTime)    // parse input
                return output.format(getAbbreviate)    // format output
            } catch (e: ParseException) {
                e.printStackTrace()
            }

            return null
        }
    }
}