package com.example.one_suite.utils

import android.view.View

interface SwitchClickListener {
    fun onViewClick(view: View, position:Int)
    fun onSwitchClick(view: View, position:Int,isChecked:Boolean)
}