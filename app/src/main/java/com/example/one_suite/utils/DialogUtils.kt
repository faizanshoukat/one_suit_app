package com.resocoder.databinding.utils

import android.app.AlertDialog
import android.app.Dialog
import android.app.ProgressDialog
import android.content.Context
import android.graphics.Color
import android.util.Log
import android.view.*
import android.widget.*
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.example.one_suite.R
import com.example.one_suite.utils.ClickListener

class DialogUtils {
    companion object {


        fun getValidVoucherDialog(
            context: Context,
            listner: ClickListener,
            message: String
        ): Dialog {
            val dialog = Dialog(context)
            dialog.setContentView(R.layout.valid_voucher_dialog)

            val description = dialog.findViewById<TextView>(R.id.description)
            description.text=message

            val okButton = dialog.findViewById<Button>(R.id.validOkBtn)
            okButton.setOnClickListener {
                listner.onViewClick(okButton,0)
            }


            val cancelButton = dialog.findViewById<Button>(R.id.validCancelBtn)
            cancelButton.setOnClickListener {
                listner.onViewClick(cancelButton,0)
            }


            return dialog
        }


        fun getVoucherConfirmationDialog(
            context: Context,
            titleText:String,
            descriptionText:String,
            listener: ClickListener
        ): Dialog {
            val dialog = Dialog(context)
            dialog.setContentView(R.layout.voucher_confirmation_dialog)

            val title=dialog.findViewById<TextView>(R.id.title)
            val description=dialog.findViewById<TextView>(R.id.description)

            title.text=titleText
            description.text=descriptionText


            val okButton = dialog.findViewById<Button>(R.id.okBtn)
            okButton.setOnClickListener {
                listener.onViewClick(okButton,0)
            }


            return dialog
        }



        fun getCustomConfirmationDialog(
            context: Context,
            listner: ClickListener
        ): Dialog {
            val dialog = Dialog(context)
            dialog.setContentView(R.layout.confirmation_dialog)
            val okButton = dialog.findViewById<Button>(R.id.okBtn)
            okButton.setOnClickListener {
                listner.onViewClick(okButton,0)
            }


            val cancelButton = dialog.findViewById<Button>(R.id.cancelBtn)
            cancelButton.setOnClickListener {
                listner.onViewClick(cancelButton,0)
            }



            //dialog.show()
            return dialog
        }


        //
//        fun setProgressDialog(context: Context, message: String): AlertDialog {
//
//
//            Log.d("ProgressDialogIssue", "Progress title : $message")
//
//            val progressDialog = ProgressDialog(context)
//            progressDialog.setTitle("")
//            progressDialog.setMessage(" Please wait....")
//
//
//            return progressDialog
//        }
        fun setProgressDialog(context: Context, message: String): AlertDialog? {
            val padding = 50
            val linearLayout = LinearLayout(context)
            linearLayout.orientation = LinearLayout.HORIZONTAL
            linearLayout.setPadding(padding, padding, padding, padding)
            linearLayout.gravity = Gravity.START
            var params = LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
            )
            params.gravity = Gravity.CENTER
            linearLayout.layoutParams = params

            val progressBar = ProgressBar(context)
            progressBar.isIndeterminate = true
            progressBar.setPadding(0, 0, padding, 0)
            progressBar.layoutParams = params

            params = LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
            )
            params.gravity = Gravity.CENTER
            val tvText = TextView(context)
            tvText.text = message
            tvText.setTextColor(Color.parseColor("#000000"))
            tvText.textSize = 20.toFloat()
            tvText.layoutParams = params

            linearLayout.addView(progressBar)
            linearLayout.addView(tvText)

            val builder = AlertDialog.Builder(context)
            builder.setCancelable(true)
            builder.setView(linearLayout)

            val dialog = builder.create()
            val window = dialog.window
            if (window != null) {
                val layoutParams = WindowManager.LayoutParams()
                layoutParams.copyFrom(dialog.window?.attributes)
                layoutParams.width = LinearLayout.LayoutParams.WRAP_CONTENT
                layoutParams.height = LinearLayout.LayoutParams.WRAP_CONTENT
                dialog.window?.attributes = layoutParams
            }
            return dialog
        }

        fun progressDialog(context: Context) : ProgressDialog{
            //init progress dialog
            var progressDialog = ProgressDialog(context)
            progressDialog.setTitle("")
            progressDialog.setMessage("Please Wait")

            return progressDialog
        }

//        fun getCustomConfirmationDialog(
//            context: Context,
//            listner: ClickListener
//        ): Dialog {
//            val dialog = Dialog(context)
//            dialog.setContentView(R.layout.confirmation_dialog)
//            val okButton = dialog.findViewById<Button>(R.id.okBtn)
//            okButton.setOnClickListener {
////                dialog.dismiss()
//                Toast.makeText(context, "ok button..!!", Toast.LENGTH_SHORT).show()
//                listner.onViewClick(okButton,0)
//            }
//
//
//            val cancelButton = dialog.findViewById<Button>(R.id.cancelBtn)
//            cancelButton.setOnClickListener {
////                dialog.dismiss()
//                Toast.makeText(context, "Dismissed..!!", Toast.LENGTH_SHORT).show()
//                listner.onViewClick(cancelButton,0)
//            }
//
//
//
//            //dialog.show()
//            return dialog
//        }
//
//
//
//
//
//        fun getDialog(
//            activity: Profile,
//            context: Context,
//            viewModel: DialogViewModel
//        ): Dialog {
//            val dialog = Dialog(context)
//            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
//
//            val binding: DialogBinding =
//                DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.dialog, null, false)
//            binding.viewModel = viewModel
//            dialog.setContentView(binding.root)
//
//            binding.executePendingBindings()
//
//            binding.recyclerView.apply {
//
//                layoutManager = LinearLayoutManager(context)
//                val decoration = DividerItemDecoration(context, StaggeredGridLayoutManager.VERTICAL)
//                addItemDecoration(decoration)
//            }
//
//            viewModel.getRecyclerListDataObserver().observe(activity, Observer {
//                Log.d("CountryApiCall", "get recycler data")
//                if (it != null) {
//                    //update the adapter
//                    viewModel.setAdapterData(it)
//                    dialog.show()
//                } else {
//                    Toast.makeText(context, "Error in fetching data", Toast.LENGTH_LONG).show()
//                }
//            })
//
////            viewModel.getRecyclerListDataObserver().observe(activity, Observer<List<GetCountryDataModel>> {
////                if (it != null) {
////                    //update the adapter
////                    viewModel.setAdapterData(it)
////                    dialog.show()
////                } else {
////                    Toast.makeText(context, "Error in fetching data", Toast.LENGTH_LONG).show()
////                }
////            })
//            // viewModel.makeAPICall("newyork")
//
//
////            val d: Drawable = ColorDrawable(ContextCompat.getColor(dialog.context, R.color.dark_grey_color))
////            d.alpha = 200
////            dialog.window
////                ?.setLayout(
////                    WindowManager.LayoutParams.MATCH_PARENT,
////                    WindowManager.LayoutParams.MATCH_PARENT
////                )
////            dialog.window?.setBackgroundDrawable(d)
//            return dialog
//        }
//
//
//        fun getCallBackDialog(
//            activity: CallBack,
//            life: LifecycleOwner,
//            context: Context,
//            viewModel: CallBackDialogViewModel
//        ): Dialog {
//            val dialog = Dialog(context)
//            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
//
//            val binding: CallbackDialogBinding =
//                DataBindingUtil.inflate(
//                    LayoutInflater.from(context),
//                    R.layout.callback_dialog,
//                    null,
//                    false
//                )
//            binding.viewModel = viewModel
//            dialog.setContentView(binding.root)
//
//            binding.executePendingBindings()
//
//            binding.recyclerView.apply {
//
//                layoutManager = LinearLayoutManager(context)
//                val decoration = DividerItemDecoration(context, StaggeredGridLayoutManager.VERTICAL)
//                addItemDecoration(decoration)
//            }
//
//
//            viewModel.getRecyclerListDataObserver().observe(life, Observer {
//                Log.d("CountryApiCall", "get recycler data")
//                if (it != null) {
//                    //update the adapter
//                    viewModel.setAdapterData(it)
////                    dialog.show()
//                } else {
//                    Toast.makeText(context, "Error in fetching data", Toast.LENGTH_LONG).show()
//                }
//            })
//
//            viewModel.getIsShowDialogObserver().observe(life, Observer {
//                Log.d("CountryApiCall", "isShow= $it")
//                if (it!=null){
//                    Log.d("CountryApiCall", "not null")
//                    if (it==false){
//                        Log.d("CountryApiCall", "false")
//                        dialog.dismiss()
//                    }else if (it==true){
//                        dialog.show()
//                    }
//                }
//
//            })
//
//
//
//
//
//            return dialog
//        }
//
//
//        fun getTicketTypeDialog(
//            activity: NewTicket,
//            context: Context,
//            viewModel: TicketTypeDialogViewModel
//        ): Dialog {
//            val dialog = Dialog(context)
//            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
//
//            val binding: TicketTypeDialogBinding =
//                DataBindingUtil.inflate(
//                    LayoutInflater.from(context),
//                    R.layout.ticket_type_dialog,
//                    null,
//                    false
//                )
//            binding.viewModel = viewModel
//            dialog.setContentView(binding.root)
//
//            binding.executePendingBindings()
//
//            binding.recyclerView.apply {
//
//                layoutManager = LinearLayoutManager(context)
//                val decoration = DividerItemDecoration(context, StaggeredGridLayoutManager.VERTICAL)
//                addItemDecoration(decoration)
//            }
//
//
//            viewModel.getRecyclerListDataObserver().observe(activity, Observer {
//                Log.d("CountryApiCall", "get recycler data")
//                if (it != null) {
//                    //update the adapter
//                    viewModel.setAdapterData(it)
//                    dialog.show()
//                } else {
//                    Toast.makeText(context, "Error in fetching data", Toast.LENGTH_LONG).show()
//                }
//            })
//
////            viewModel.getRecyclerListDataObserver().observe(activity, Observer<List<GetCountryDataModel>> {
////                if (it != null) {
////                    //update the adapter
////                    viewModel.setAdapterData(it)
////                    dialog.show()
////                } else {
////                    Toast.makeText(context, "Error in fetching data", Toast.LENGTH_LONG).show()
////                }
////            })
//            // viewModel.makeAPICall("newyork")
//
//
////            val d: Drawable = ColorDrawable(ContextCompat.getColor(dialog.context, R.color.colorAccent))
////            d.alpha = 200
////            dialog.window
////                ?.setLayout(
////                    WindowManager.LayoutParams.WRAP_CONTENT,
////                    WindowManager.LayoutParams.WRAP_CONTENT
////                )
////            dialog.window?.setBackgroundDrawable(d)
//            return dialog
//        }
//
//
//        fun getTicketPrioritiesDialog(
//            activity: NewTicket,
//            context: Context,
//            viewModel: TicketPrioritiesDialogViewModel
//        ): Dialog {
//            val dialog = Dialog(context)
//            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
//
//            val binding: TicketPrioritiesDialogBinding =
//                DataBindingUtil.inflate(
//                    LayoutInflater.from(context),
//                    R.layout.ticket_priorities_dialog,
//                    null,
//                    false
//                )
//            binding.viewModel = viewModel
//            dialog.setContentView(binding.root)
//
//            binding.executePendingBindings()
//
//            binding.recyclerView.apply {
//
//                layoutManager = LinearLayoutManager(context)
//                val decoration = DividerItemDecoration(context, StaggeredGridLayoutManager.VERTICAL)
//                addItemDecoration(decoration)
//            }
//
//
//            viewModel.getTicketPrioritiesRecyclerListDataObserver().observe(activity, Observer {
//                Log.d("CountryApiCall", "get recycler data")
//                if (it != null) {
//                    //update the adapter
//                    viewModel.setTicketPrioritiesAdapterData(it)
//                    dialog.show()
//                } else {
//                    Toast.makeText(context, "Error in fetching data", Toast.LENGTH_LONG).show()
//                }
//            })
//
//
////            val d: Drawable = ColorDrawable(ContextCompat.getColor(dialog.context, R.color.colorAccent))
////            d.alpha = 200
////            dialog.window
////                ?.setLayout(
////                    WindowManager.LayoutParams.WRAP_CONTENT,
////                    WindowManager.LayoutParams.WRAP_CONTENT
////                )
////            dialog.window?.setBackgroundDrawable(d)
//            return dialog
//        }
//
//
//        fun getSignUpDialog(
//            activity: SignUp,
//            context: Context,
//            viewModel: SignUpDialogViewModel
//        ): Dialog {
//            val dialog = Dialog(context)
//            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
//
//            val binding: SignUpDialogBinding =
//                DataBindingUtil.inflate(
//                    LayoutInflater.from(context),
//                    R.layout.sign_up_dialog,
//                    null,
//                    false
//                )
//            binding.viewModel = viewModel
//            dialog.setContentView(binding.root)
//
//            binding.executePendingBindings()
//
//            binding.recyclerView.apply {
//
//                layoutManager = LinearLayoutManager(context)
//                val decoration = DividerItemDecoration(context, StaggeredGridLayoutManager.VERTICAL)
//                addItemDecoration(decoration)
//            }
//
//
//            viewModel.getRecyclerListDataObserver().observe(activity, Observer {
//                Log.d("CountryApiCall", "get recycler data")
//                if (it != null) {
//                    //update the adapter
//                    viewModel.setAdapterData(it)
//                    dialog.show()
//                } else {
//                    Toast.makeText(context, "Error in fetching data", Toast.LENGTH_LONG).show()
//                }
//            })
//
//
////            val d: Drawable = ColorDrawable(ContextCompat.getColor(dialog.context, R.color.colorAccent))
////            d.alpha = 200
////            dialog.window
////                ?.setLayout(
////                    WindowManager.LayoutParams.WRAP_CONTENT,
////                    WindowManager.LayoutParams.WRAP_CONTENT
////                )
////            dialog.window?.setBackgroundDrawable(d)
//            return dialog
//        }
//
//
//        fun getNumberTypeDialog(
//            activity: SignupSendCode,
//            context: Context,
//            viewModel: NumberTypeDialogViewModel
//        ): Dialog {
//            val dialog = Dialog(context)
//            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
//
//            val binding: NumberTypeDialogBinding =
//                DataBindingUtil.inflate(
//                    LayoutInflater.from(context),
//                    R.layout.number_type_dialog,
//                    null,
//                    false
//                )
//            binding.viewModel = viewModel
//            dialog.setContentView(binding.root)
//
//            binding.executePendingBindings()
//
//            binding.recyclerView.apply {
//
//                layoutManager = LinearLayoutManager(context)
//                val decoration = DividerItemDecoration(context, StaggeredGridLayoutManager.VERTICAL)
//                addItemDecoration(decoration)
//            }
//
//
//            viewModel.getRecyclerListDataObserver().observe(activity, Observer {
//                Log.d("CountryApiCall", "get recycler data")
//                if (it != null) {
//                    //update the adapter
//                    viewModel.setAdapterData(it)
//                    dialog.show()
//                } else {
//                    Toast.makeText(context, "Error in fetching data", Toast.LENGTH_LONG).show()
//                }
//            })
//
//
////            val d: Drawable = ColorDrawable(ContextCompat.getColor(dialog.context, R.color.colorAccent))
////            d.alpha = 200
////            dialog.window
////                ?.setLayout(
////                    WindowManager.LayoutParams.WRAP_CONTENT,
////                    WindowManager.LayoutParams.WRAP_CONTENT
////                )
////            dialog.window?.setBackgroundDrawable(d)
//            return dialog
//        }
//
//
//
//
//
//        fun getCustomDialog(
//            lifeCycleOwner:LifecycleOwner,
//            context: Context,
//            viewModel: CustomDialogViewModel
//        ): Dialog {
//            val dialog = Dialog(context)
//            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
//
//            val binding: CustomDialogBinding =
//                DataBindingUtil.inflate(
//                    LayoutInflater.from(context),
//                    R.layout.custom_dialog,
//                    null,
//                    false
//                )
//            binding.viewModel = viewModel
//            dialog.setContentView(binding.root)
//
//            binding.executePendingBindings()
//
//            binding.recyclerView.apply {
//
//                layoutManager = LinearLayoutManager(context)
//                val decoration = DividerItemDecoration(context, StaggeredGridLayoutManager.VERTICAL)
//                addItemDecoration(decoration)
//            }
//
//
//            viewModel.getRecyclerListDataObserver().observe(lifeCycleOwner, Observer {
//                Log.d("CountryApiCall", "get recycler data")
//                if (it != null) {
//                    //update the adapter
//                    viewModel.setAdapterData(it)
////                    dialog.show()
//                } else {
//                    Toast.makeText(context, "Error in fetching data", Toast.LENGTH_LONG).show()
//                }
//            })
//
//            viewModel.getIsShowDialogObserver().observe(lifeCycleOwner, Observer {
//                Log.d("CountryApiCall", "isShow= $it")
//                if (it!=null){
//                    Log.d("CountryApiCall", "not null")
//                    if (it==false){
//                        Log.d("CountryApiCall", "false")
//                        dialog.dismiss()
//                    }else if (it==true){
//                        dialog.show()
//                    }
//                }
//
//            })
//
//
//            return dialog
//        }















    }
}


