package com.example.one_suite.navigation;

import android.content.Context;
import android.util.Log;

import com.example.one_suite.R;

import java.util.ArrayList;
import java.util.List;

public class CustomDataProvider {

    private static final int MAX_LEVELS = 3;

    private static final int LEVEL_1 = 1;
    private static final int LEVEL_2 = 2;
    private static final int LEVEL_3 = 3;

    private static List<BaseItem> mMenu = new ArrayList<>();
    Context context;

    public  static List<BaseItem> getInitialItems(){

        List<BaseItem> rootMenu = new ArrayList<>();

        //for  the Group menu
        rootMenu.add(new Item("Dashboard", R.drawable.menu_home));
        rootMenu.add(new GroupItem("My OS Numbers", R.drawable.menu_globe));
        rootMenu.add(new Item("Call Forwarding", R.drawable.menu_call_forwarding));
        rootMenu.add(new Item("Call Back", R.drawable.menu_call_back));
        rootMenu.add(new Item("Calling Plans", R.drawable.menu_globe));
        rootMenu.add(new Item("Credit", R.drawable.menu_call_back));
        rootMenu.add(new Item("History", R.drawable.menu_call_back));
        rootMenu.add(new GroupItem("Settings", R.drawable.menu_setting));
        rootMenu.add(new Item("Support", R.drawable.menu_call_back));
        rootMenu.add(new Item("Rewards", R.drawable.menu_call_back));


//        rootMenu.add(new GroupItem("Call", R.drawable.ic_widgets_black_24dp));
//
//        rootMenu.add(new GroupItem("Assignments", R.drawable.ic_assignment_black_24dp));
//        rootMenu.add(new Item("Help", R.drawable.ic_help_black_24dp));
//        rootMenu.add(new Item("AboutUs", R.drawable.ic_error_black_24dp));
        return rootMenu;

    }

    //get sub items
    public static List<BaseItem> getSubItems(BaseItem baseItem){
        List<BaseItem> result = new ArrayList<>();
        int level = ((GroupItem) baseItem).getLevel() + 1;
        String menuItem = baseItem.getName();
        if (!(baseItem instanceof GroupItem)) {
            throw new IllegalArgumentException("GroupItem required");
        }

        GroupItem groupItem = (GroupItem) baseItem;
        if (groupItem.getLevel() >= MAX_LEVELS) {
            return null;
        }

        //set expandable listview
        switch (level) {
            case LEVEL_1:
                switch (menuItem.toUpperCase()) {

                    case "MY OS NUMBERS":
                        Log.d("TAG", "getSubItems: "+menuItem);
                        result = getMyOsListCategory();
                        break;

                    case "SETTINGS":
                        result = getSettingList();
                        break;

//                    case "ASSIGNMENTS":
//                        result = getListAssignments();
//                        break;

                }
                break;
        }
        return result;

    }

    public static boolean isExpandable(BaseItem baseItem) {
        return baseItem instanceof GroupItem;
    }



    private static List<BaseItem> getMyOsListCategory() {
        List<BaseItem> list = new ArrayList<>();

        list.add(new Item("      Buy Numbers"));
        list.add(new Item("      My Numbers"));
        Log.d("TAG", "getListCategory: create the sub items "+list);


        return list;
    }


    private static List<BaseItem> getSettingList() {
        List<BaseItem> list = new ArrayList<>();
        list.add(new Item("      Update Profile"));
        list.add(new Item("      Change Password"));


        return list;
    }

}
