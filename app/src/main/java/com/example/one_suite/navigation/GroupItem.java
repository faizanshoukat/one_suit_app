package com.example.one_suite.navigation;

public class GroupItem extends BaseItem {

    private int mLevel;

    public GroupItem(String mName, int icon) {
        super(mName, icon);
        mLevel = 0;
    }

    public GroupItem(String mName) {
        super(mName);
        mLevel = 0;
    }

    public int getLevel() {
        return mLevel;
    }

    public void setLevel(int mLevel) {
        this.mLevel = mLevel;
    }
}
