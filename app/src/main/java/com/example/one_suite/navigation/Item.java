package com.example.one_suite.navigation;

public class Item extends BaseItem {

    public Item(String mName, int icon) {
        super(mName, icon);
    }

    public Item(String mName) {
        super(mName);
    }
}
