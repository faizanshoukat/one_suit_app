package com.example.one_suite.navigation;

public class BaseItem {
    private String mName;
    private int  icon;

    public BaseItem(String mName, int icon) {
        this.mName = mName;
        this.icon = icon;
    }

    public BaseItem(String mName) {
        this.mName = mName;
    }

    public String getName() {
        return mName;
    }

    public int getIcon() {
        return icon;
    }
}
