package com.example.one_suite.network


import com.example.one_suite.model.*
import com.example.one_suite.model.callBack.SetCallBackRequestModel
import com.example.one_suite.model.callBack.SetCallBackResponseModel
import com.example.one_suite.model.callForwarding.GetCallForwardingRateRequestModel
import com.example.one_suite.model.callForwarding.GetCallForwardingRateResponseModel
import com.example.one_suite.model.callForwarding.setSubscriberDID.SetSubscriberDIDRequestModel
import com.example.one_suite.model.callForwarding.setSubscriberDID.SetSubscriberDIDResponseModel
import com.example.one_suite.model.country.GetCountryModel
import com.example.one_suite.model.credits.GetSystemSettingsResponseModel
import com.example.one_suite.model.credits.autoRecharge.AutoRechargeRequestModel
import com.example.one_suite.model.credits.autoRecharge.AutoRechargeResponseModel
import com.example.one_suite.model.credits.autoRecharge.deleteCard.DeleteCardRequestModel
import com.example.one_suite.model.credits.autoRecharge.deleteCard.DeleteCardResponseModel
import com.example.one_suite.model.credits.autoRecharge.paymentInformation.PaymentInformationRequestModel
import com.example.one_suite.model.credits.autoRecharge.paymentInformation.PaymentInformationResponseModel
import com.example.one_suite.model.credits.autoRecharge.paymentInformation.setPaymentInfo.SetPaymentInfoRequestModel
import com.example.one_suite.model.credits.autoRecharge.paymentInformation.setPaymentInfo.SetPaymentInfoResponseModel
import com.example.one_suite.model.credits.osCard.RedeemVoucherRequestModel
import com.example.one_suite.model.credits.osCard.RedeemVoucherResponseModel
import com.example.one_suite.model.credits.osCard.isValidVoucherRequestModel
import com.example.one_suite.model.credits.osCard.isValidVoucherResponseModel
import com.example.one_suite.model.credits.payInvoice.CreditPayInvoiceRequestModel
import com.example.one_suite.model.credits.payInvoice.CreditPayInvoiceResponseModel
import com.example.one_suite.model.setting.profile.SubscriberProfileRequestModel
import com.example.one_suite.model.setting.profile.SubscriberProfileResponseModel
import com.example.one_suite.model.setting.setProfile.SetProfileRequestModel
import com.example.one_suite.model.setting.setProfile.SetProfileResponseModel
import com.example.one_suite.model.subscriberCustomNumbers.SubscriberCustomNumberRequestModel
import com.example.one_suite.model.subscriberCustomNumbers.SubscriberCustomNumbersResponseModel
import com.example.one_suite.model.support.GetAllTicketsRequestModel
import com.example.one_suite.model.support.GetAllTicketsResponseModel
import com.example.one_suite.model.support.newTicket.generateTicket.GenerateTicketRequestModel
import com.example.one_suite.model.support.newTicket.generateTicket.GenerateTicketResponseModel
import com.example.one_suite.model.support.newTicket.ticketPriorities.GetTicketPrioritiesRequestModel
import com.example.one_suite.model.support.newTicket.ticketPriorities.GetTicketPrioritiesResponseModel
import com.example.one_suite.model.support.newTicket.ticketTypes.GetTicketTypesResponseModel
import com.example.one_suite.model.transactionHistory.GetTransactionHistoryRequestModel
import com.example.one_suite.model.transactionHistory.GetTransactionHistoryResponseModel
import com.example.one_suite.ui.buyNumber.*
import com.example.one_suite.ui.buyNumber.checkOut.*
import com.example.one_suite.ui.callingPlan.callingPlans.GetCallingPlansModel
import com.example.one_suite.ui.credit.autoRechargeFragment.AddAutoRechargeRequestModel
import com.example.one_suite.ui.credit.autoRechargeFragment.AddAutoRechargeResponseModel
import com.example.one_suite.ui.credit.autoRechargeFragment.UpdateThresholdRequestModel
import com.example.one_suite.ui.credit.autoRechargeFragment.UpdateThresholdResponseModel
import com.example.one_suite.ui.history.creditNotes.CreditNotesRequestModel
import com.example.one_suite.ui.history.creditNotes.CreditNotesResponseDataModel
import com.example.one_suite.ui.history.creditNotes.CreditNotesResponseModel
import com.example.one_suite.ui.home.*
import com.example.one_suite.ui.login.LoginRequestModel
import com.example.one_suite.ui.login.LoginResponseModel
import com.example.one_suite.ui.myNumbers.addName.AddNameRequestModel
import com.example.one_suite.ui.myNumbers.addName.AddNameResponseModel
import com.example.one_suite.ui.myNumbers.myNumbersList.SetCallerIdRequestModel
import com.example.one_suite.ui.myNumbers.myNumbersList.SetCallerIdResponseModel
import com.example.one_suite.ui.reWards.models.*
import com.resocoder.databinding.model.subscriberDid.SubscriberDIDRequestModel
import com.resocoder.databinding.model.subscriberDid.SubscriberDIDResponseModel
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.Header
import retrofit2.http.POST


interface APIService {

    @POST("SubscriberLogin")
    fun getLogin(
        @Header("credentials") APIKey: String,
        @Body loginRequest: LoginRequestModel
    ): Call<LoginResponseModel>


    //My Number Section
    @POST("GetSubscriberDID")
    fun getSubscriberDID(
        @Header("credentials") APIKey: String,
        @Body subscriberDIDRequest: SubscriberDIDRequestModel
    ): Call<SubscriberDIDResponseModel>


    @POST("GetSubscriberCustomNumbers")
    fun getSubscriberCustomNumbers(
        @Header("credentials") APIKey: String,
        @Body customNumbersRequest: SubscriberCustomNumberRequestModel
    ): Call<SubscriberCustomNumbersResponseModel>

    @POST("SetCLI")
    fun setCallerId(
        @Header("credentials") APIKey: String,
        @Body requestModel: SetCallerIdRequestModel
    ): Call<SetCallerIdResponseModel>


    @POST("AddNumberName")
    fun addName(
        @Header("credentials") APIKey: String,
        @Body requestModel: AddNameRequestModel
    ): Call<AddNameResponseModel>


    //Call Forwarding
    @POST("SetDIDSettings")
    fun setSubscriberDID(
        @Header("credentials") APIKey: String,
        @Body subscriberRequest: SetSubscriberDIDRequestModel
    ): Call<SetSubscriberDIDResponseModel>

    @POST("GetCountriesData")
    fun getCountryList(
        @Header("credentials") APIKey: String,
    ): Call<GetCountryModel>


    @POST("GetCallForwardingRate")
    fun getCallForwardingRate(
        @Header("credentials") APIKey: String,
        @Body callRequest: GetCallForwardingRateRequestModel
    ): Call<GetCallForwardingRateResponseModel>


    /*.......Call Back Section..............*/
    //Call Back
    @POST("SetCallback")
    fun setCallBack(
        @Header("credentials") APIKey: String,
        @Body callRequest: SetCallBackRequestModel
    ): Call<SetCallBackResponseModel>


    //History
    @POST("GetSubscriberCallHistory")
    fun getCallHistory(
        @Header("credentials") APIKey: String,
        @Body callRequest: GetSubscriberCallHistoryRequestModel
    ): Call<GetSubscriberCallHistoryResponseModel>


    @POST("GetSubscriberPaymentHistory")
    fun getTransactionHistory(
        @Header("credentials") APIKey: String,
        @Body callRequest: GetTransactionHistoryRequestModel
    ): Call<GetTransactionHistoryResponseModel>


    /*....................Support Section..............*/
    @POST("GetAllTickets")
    fun getAllTickets(
        @Header("credentials") APIKey: String,
        @Body request: GetAllTicketsRequestModel
    ): Call<GetAllTicketsResponseModel>

    @POST("GetTicketTypes")
    fun getTicketTypes(
        @Header("credentials") APIKey: String
    ): Call<GetTicketTypesResponseModel>

    @POST("GetTicketPriorities")
    fun getTicketPriorities(
        @Header("credentials") APIKey: String,
        @Body request: GetTicketPrioritiesRequestModel
    ): Call<GetTicketPrioritiesResponseModel>


    @POST("GenerateTicket")
    fun generateTicket(
        @Header("credentials") APIKey: String,
        @Body request: GenerateTicketRequestModel
    ): Call<GenerateTicketResponseModel>


    /*....................Credit Section...........*/
    @POST("GetSystemSettings")
    fun getSystemSettings(
        @Header("credentials") APIKey: String
    ): Call<GetSystemSettingsResponseModel>


    @POST("ApplySubscriberCredit")
    fun setCreditPayInvoice(
        @Header("credentials") APIKey: String,
        @Body request: CreditPayInvoiceRequestModel
    ): Call<CreditPayInvoiceResponseModel>

    @POST("GetAutoRechargeAmount")
    fun getAutoRechargeAmount(
        @Header("credentials") APIKey: String,
        @Body request: AutoRechargeRequestModel
    ): Call<AutoRechargeResponseModel>

    @POST("GetPaymentInfo")
    fun getPaymentInformation(
        @Header("credentials") APIKey: String,
        @Body request: PaymentInformationRequestModel
    ): Call<PaymentInformationResponseModel>


    @POST("SetPaymentInfo")
    fun setPaymentInformation(
        @Header("credentials") APIKey: String,
        @Body request: SetPaymentInfoRequestModel
    ): Call<SetPaymentInfoResponseModel>

    @POST("AutoRecharge")
    fun addAutoRecharge(
        @Header("credentials") APIKey: String,
        @Body request: AddAutoRechargeRequestModel
    ): Call<AddAutoRechargeResponseModel>

    @POST("UpdateSubscriberFundThreshold")
    fun updateThreshold(
        @Header("credentials") APIKey: String,
        @Body request: UpdateThresholdRequestModel
    ): Call<UpdateThresholdResponseModel>


    @POST("IsValidVoucher")
    fun isValidVoucher(
        @Header("credentials") APIKey: String,
        @Body request: isValidVoucherRequestModel
    ): Call<isValidVoucherResponseModel>

    @POST("RedeemVoucher")
    fun redeemVoucher(
        @Header("credentials") APIKey: String,
        @Body request: RedeemVoucherRequestModel
    ): Call<RedeemVoucherResponseModel>


    @POST("DeleteSubscriberCreditCard")
    fun deleteCard(
        @Header("credentials") APIKey: String,
        @Body request: DeleteCardRequestModel
    ): Call<DeleteCardResponseModel>
/*....................Credit Section...........*/


    /*....................Dashboard.......................*/
    @POST("GetBalance")
    fun getBalance(
        @Header("credentials") APIKey: String,
        @Body request: GetBalanceRequestModel
    ): Call<GetBalanceResponseModel>

    @POST("GetSubscriberPromotions")
    fun getCallingPlanHistory(
        @Header("credentials") APIKey: String,
        @Body request: GetSubscriberPromotionRequestModel
    ): Call<GetSubscriberPromotionResponseModel>


    /*.......................Profile Section......................*/
    @POST("GetSubscriberProfile")
    fun getSubscriberProfile(
        @Header("credentials") APIKey: String,
        @Body subscriberRequest: SubscriberProfileRequestModel
    ): Call<SubscriberProfileResponseModel>

    @POST("SetSubscriberProfile")
    fun setSubscriberProfile(
        @Header("credentials") APIKey: String,
        @Body profileRequest: SetProfileRequestModel
    ): Call<SetProfileResponseModel>


    //    @POST("UpdatePassword")
//    fun updatePassword(
//        @Header("credentials") APIKey: String,
//        @Body request: UpdatePasswordRequestModel
//    ): Call<UpdatePasswordResponseModel>
//


    /*...............Calling Plans.........*/
    @POST("GetAllPromotions")
    fun getCallingPlans(
        @Header("credentials")
        APIKey: String
    ): Call<GetCallingPlansModel>


    /*.....................History Section................*/
    @POST("SubscriberCreditNotes")
    fun getSubscriberCreditNotesApiCall(
        @Header("credentials") APIKey: String,
        @Body request: CreditNotesRequestModel
    ): Call<CreditNotesResponseModel>


    /*..................Reward Section.....................*/
    @POST("GetAccountReward")
    fun getRewardPointsApiCall(
        @Header("credentials") APIKey: String,
        @Body Account: GetRewardPointRequestModel
    ): Call<GetRewardPointResponseModel>


    @POST("GetRewardHistory")
    fun getRewardHistoryApiCall(
        @Header("credentials") APIKey: String,
        @Body userId: GetRewardHistoryRequestModel
    ): Call<GetRewardHistoryResponseModel>


    @POST("GetReferralHistory")
    fun getReferralHistoryApiInstance(
        @Header("credentials") credentials: String,
        @Body userId: GetReferralHistoryRequestModel
    ): Call<GetReferralHistoryResponseModel>


    @POST("GetRedeemRewardHistory")
    fun getRedeemHistoryApiCall(
        @Header("credentials") APIKey: String,
        @Body Account: GetRedeemRewardRequestModel
    ): Call<GetRedeemRewardResponseModel>


    //    @POST("ForgotPassword")
//    fun getLogin(
//        @Header("credentials") APIKey: String,
//        @Body forgotRequest: RequestForgotPassword
//    ): Call<ResponseForgotPassword>
//
//
//    //Sign Up
//    @POST("UniqueSubscriberUsername")
//    fun uniqueUsernameApiCall(
//        @Header("credentials") APIKey: String,
//        @Body request: UniqueUsernameRequestModel
//    ): Call<UniqueUsernameResponseModel>
//
//    @POST("UniqueSubscriberPHoneNumber")
//    fun uniquePhoneNumberApiCall(
//        @Header("credentials") APIKey: String,
//        @Body request: UniquePhoneRequestModel
//    ): Call<UniquePhoneResponseModel>
//
//    @POST("UniqueSubscriberEmail")
//    fun uniqueEmailApiCall(
//        @Header("credentials") APIKey: String,
//        @Body request: UniqueEmailRequestModel
//    ): Call<UniqueEmailResponseModel>
//
//
//    @POST("Register")
//    fun registerApiCall(
//        @Header("credentials") APIKey: String,
//        @Body request: RegisterRequestModel
//    ): Call<RegisterResponseModel>
//
//
//    @POST("IncompleteRegister")
//    fun inCompleteRegisterApiCall(
//        @Header("credentials") APIKey: String,
//        @Body request: InCompleteRegisterRequestModel
//    ): Call<InCompleteRegisterResponseModel>
//
//    @POST("SendVerificationCodeViaSMS")
//    fun sendVerificationCodeViaSMSApiCall(
//        @Header("credentials") APIKey: String,
//        @Body request: SendVerificationCodeViaSMSRequestModel
//    ): Call<SendVerificationCodeViaSMSResponseModel>
//
//    @POST("SendVerificationCodeViaCall")
//    fun sendVerificationCodeViaCallApiCall(
//        @Header("credentials") APIKey: String,
//        @Body request: SendVerificationCodeViaCallRequestModel
//    ): Call<SendVerificationCodeViaCallResponseModel>
//
//
    //Buy Numbers
    @POST("GetStates")
    fun getStates(
        @Header("credentials") APIKey: String,
    ): Call<GetStatesModel>

    @POST("GetCities")
    fun getCities(
        @Header("credentials") APIKey: String,
        @Body request: GetCitiesRequestModel
    ): Call<GetCitiesResponseModel>


    @POST("SearchTelephoneNumber")
    fun searchTelephoneNumber(
        @Header("credentials") APIKey: String,
        @Body request: SearchTelephoneNumberRequestModel
    ): Call<SearchTelephoneNumberResponseModel>

    //in case of empty parameters
    @POST("SearchTelephoneNumber")
    fun searchTelephoneNumberEmpty(
        @Header("credentials") APIKey: String,
        @Body request: SearchTelephoneNumberEmptyRequestModel
    ): Call<SearchTelephoneNumberResponseModel>


    @POST("GetLocalRates")
    fun getLocalRates(
        @Header("credentials") APIKey: String,
        @Body request: GetLocalRatesRequestModel
    ): Call<GetLocalRatesResponseModel>


    @POST("ApplySubscriberDebit")
    fun payWithOsBalance(
        @Header("credentials") APIKey: String,
        @Body request: PayWithOSBalanceRequestModel
    ): Call<PayWithOSBalanceResponseModel>

    @POST("ClaimRewardPointsPayment")
    fun payWithRewardPoints(
        @Header("credentials") APIKey: String,
        @Body request: PayWithRewardPointsRequestModel
    ): Call<PayWithRewardPointsResponseModel>

    @POST("AutoRenewNumber")
    fun autoRenewNumber(
        @Header("credentials") APIKey: String,
        @Body request: AutoRenewNumberRequestModel
    ): Call<AutoRenewNumberResponseModel>

//    @POST("OrderTelephoneNumber")
//    fun orderTelephoneNumber(
//        @Header("credentials") APIKey: String,
//        @Body request: OrderTelephoneNumberRequestModel1
//    ): Call<OrderTelephoneNumberResponseModel>


    @POST("OrderTelephoneNumber")
    fun orderTelephoneNumber(
        @Header("credentials") APIKey: String,
        @Body request: OrderTelephoneNumberRequestModel
    ): Call<OrderTelephoneNumberResponseModel>


//    @POST("GetSubscriberPaymentHistory")
//    fun getSubscriberPaymentHistoryApiCall(@Header("credentials") APIKey: String,
//                                           @Body request: Request_GetSubscriberPaymentHistory
//    ): Call<Response_GetSubscriberPaymentHistory>
//


//    @POST("GetBalance")
//    fun showBalance(
//        @Header("credentials")  APIKey: String,
//        @Body RequestgetBalance: Request_GetBalance
//    ): Call<Responce_GetBalance>
}