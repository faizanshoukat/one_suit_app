package com.example.one_suite

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.widget.*
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import com.example.one_suite.navigation.BaseItem
import com.example.one_suite.navigation.CustomDataProvider
import com.example.one_suite.ui.buyNumber.BuyNumberFragment
import com.example.one_suite.ui.buyNumber.GetLocalRatesDataModel
import com.example.one_suite.ui.buyNumber.SearchTelephoneNumberDataModel
import com.example.one_suite.ui.buyNumber.buyNumberList.BuyNumberListFragment
import com.example.one_suite.ui.callForwarding.CallForwardingFragment
import com.example.one_suite.ui.home.HomeFragment
import com.example.one_suite.ui.login.Login
import com.example.one_suite.ui.myNumbers.MyNumbersFragment
import com.example.one_suite.utils.Constant
import com.google.android.material.navigation.NavigationView
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso
import pl.openrnd.multilevellistview.ItemInfo
import pl.openrnd.multilevellistview.MultiLevelListAdapter
import pl.openrnd.multilevellistview.MultiLevelListView
import pl.openrnd.multilevellistview.OnItemClickListener
import java.lang.Exception
import android.os.Build

import android.content.*
import android.view.*
import com.example.one_suite.ui.buyNumber.checkOut.BuyNumberCheckoutActivity
import com.example.one_suite.ui.callBack.CallBackFragment
import com.example.one_suite.ui.callingPlan.CallingPlansMainFragment
import com.example.one_suite.ui.credit.CreditFragment
import com.example.one_suite.ui.history.HistoryFragment
import com.example.one_suite.ui.reWards.RewardsMainFragments
import com.example.one_suite.ui.setting.changePassword.ChangePasswordFragment
import com.example.one_suite.ui.setting.updateProfile.UpdateProfile
import com.example.one_suite.ui.support.TicketsFragment
import com.example.one_suite.utils.PreferenceUtils
import java.io.FileNotFoundException


class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

    lateinit var sharedPreference: SharedPreferences



    lateinit var password: String

    private var multiLevelListView: MultiLevelListView? = null
    private val mOnItemClickListener = object : OnItemClickListener {

        private fun showItemDescription(`object`: Any, itemInfo: ItemInfo) {

            if ((`object` as BaseItem).name.contains("Dashboard")) {
                displaySelectedScreen("Dashboard")
            }
            if (`object`.name.contains("Buy Numbers")) {
                displaySelectedScreen("Buy Numbers")
            }

            if (`object`.name.contains("My Numbers")) {
                displaySelectedScreen("My Numbers")
            }

            if (`object`.name.contains("Call Forwarding")) {
                displaySelectedScreen("Call Forwarding")
            }


            /*........................Calling Plan...........*/
            if (`object`.name.contains("Calling Plans")) {
                displaySelectedScreen("Calling Plans")
            }

            if (`object`.name.contains("Call Back")) {
                displaySelectedScreen("Call Back")
            }


            if (`object`.name.contains("Credit")) {
                displaySelectedScreen("Credit")
            }

/*......................History Section.................*/
            if (`object`.name.contains("History")) {
                displaySelectedScreen("History")
            }


/*......................Setting Section.................*/
            if (`object`.name.contains("Update Profile")) {
                displaySelectedScreen("Update Profile")
            }

            if (`object`.name.contains("Change Password")) {
                displaySelectedScreen("Change Password")
            }

            /*.........................Support Section.............*/
            if (`object`.name.contains("Support")) {
                displaySelectedScreen("Support")
            }

            /*.........................Rewards Section.............*/
            if (`object`.name.contains("Rewards")) {
                displaySelectedScreen("Rewards")
            }

//            if (`object`.name.contains("Category3")) {
//                displaySelectedScreen("CATEGORY3")
//            }
//
//
//            if (`object`.name.contains("Call1")) {
//                displaySelectedScreen("Call1")
//            }
//            if (`object`.name.contains("CALL2")) {
//                displaySelectedScreen("CALL2")
//            }
//            if (`object`.name.contains("CALL3")) {
//                displaySelectedScreen("CALL3")
//            }
//
//
//
//
//            if (`object`.name.contains("Assignment1")) {
//                displaySelectedScreen("ASSIGNMENT1")
//            }
//            if (`object`.name.contains("Assignment2")) {
//                displaySelectedScreen("ASSIGNMENT2")
//            }
//            if (`object`.name.contains("Assignment3")) {
//                displaySelectedScreen("ASSIGNMENT3")
//            }
//            if (`object`.name.contains("Assignment4")) {
//                displaySelectedScreen("ASSIGNMENT4")
//            }
//            if (`object`.name.contains("Help")) {
//                displaySelectedScreen("HELP")
//            }
//            if (`object`.name.contains("AboutUs")) {
//                displaySelectedScreen("ABOUTUS")
//            }


        }

        override fun onItemClicked(
            parent: MultiLevelListView,
            view: View,
            item: Any,
            itemInfo: ItemInfo
        ) {
            showItemDescription(item, itemInfo)
        }

        override fun onGroupItemClicked(
            parent: MultiLevelListView,
            view: View,
            item: Any,
            itemInfo: ItemInfo
        ) {
            showItemDescription(item, itemInfo)
        }
    }

    lateinit var toolbarTitle: TextView


    var fragmentName = "Dashboard"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        val intent = intent
//        fragmentName = intent.getStringExtra("Fragment").toString()
//        Log.d("BundleIssue","Fragment :$fragmentName")

//        val bundle = intent.getBundleExtra("Bundle");
        if (intent != null) {
            Log.d("BundleIssue", "Bundle not null")
            fragmentName = intent.getStringExtra("Fragment").toString()
            Log.d("BundleIssue", "before Fragment :$fragmentName")
            if (fragmentName == null || fragmentName == "") {
                Log.d("BundleIssue", "Fragment Name Null")
                fragmentName = "Dashboard"
            }

        }

        Log.d("BundleIssue", "Fragment :$fragmentName")

        displayData()


        changeStatusBarColor()

        val txtheader = findViewById<TextView>(R.id.txtHeader)
        txtheader.text = Constant.name
        //load imagex
        val imageView = findViewById<ImageView>(R.id.imageView)
        val progressBar = findViewById<ProgressBar>(R.id.progress)


        //Do some Network Request

        try {

//                val url = URL(Constant.imageUrl)
//                val bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream())


            Log.d("ImageIssue", Constant.imageUrl)

            Thread {

                runOnUiThread {
                    Picasso.get()
                        .load(Constant.imageUrl)
//                        .noPlaceholder(R.drawable.ic_profile)
                        .into(imageView, object : Callback {
                            override fun onSuccess() {
                                progressBar.visibility = View.GONE
                                Log.d("TAG", "success")

                            }

                            override fun onError(e: Exception?) {
                                Log.d("TAG", "error")
                                progressBar.visibility = View.GONE
                            }
                        })
                    //Update UI
                }
            }.start()

        } catch (e: FileNotFoundException) {

        } catch (e: Exception) {

        }




        toolbarTitle = findViewById(R.id.toolbarTitle)


/////////////////////////////////////////////
        sharedPreference = getSharedPreferences("login_pref", Context.MODE_PRIVATE)
        val name = sharedPreference.getString("USER_NAME", "")
        Log.d("TAG", "UserNmae: " + name)
        password = sharedPreference.getString("USER_PASSWORD", "")!!
        Log.d("TAG", "password: " + password)
        ////////////////////////


        val btn_logout = findViewById<TextView>(R.id.btn_logout).setOnClickListener {
            val editor = sharedPreference.edit()
            PreferenceUtils.DeleteDBData(applicationContext)
            editor.clear()
            editor.apply()
            val logout = Intent(this@MainActivity, Login::class.java)
            logout.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            logout.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            logout.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET)
            startActivity(logout)
            this.finish()
        }


//        internetConnectivity()


        confMenu()
        val drawer = findViewById<DrawerLayout>(R.id.drawer_layout)
//        val toolbar = findViewById(R.id.toolbar) as Toolbar
//        val toggle = ActionBarDrawerToggle(
//            this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close
//        )
//        //drawer.setDrawerListener(toggle)
//        toggle.syncState()


        val cartIcon = findViewById<LinearLayout>(R.id.cartIcon)
        cartIcon.setOnClickListener(View.OnClickListener {
            var cartIntent = Intent(this, BuyNumberCheckoutActivity::class.java)
            val bundle = Bundle()

            cartIntent.putExtra("Bundle", bundle)
            bundle.putParcelableArrayList("CheckOutList", Constant.checkoutListStatic)
            startActivity(cartIntent)


//            startActivity(Intent(this,CallingPlanActivity::class.java))
        })

        val menuIcon = findViewById<LinearLayout>(R.id.menuIcon)
        menuIcon.setOnClickListener(View.OnClickListener {
            if (drawer.isDrawerOpen(GravityCompat.START)) {
                drawer.closeDrawer(GravityCompat.START)
            } else {
                drawer.openDrawer(GravityCompat.START)
            }
        })

        val navigationView = findViewById<NavigationView>(R.id.nav_view)
        navigationView.setNavigationItemSelectedListener(this)
        //displaySelectedScreen("Dashboard")
        displaySelectedScreen(fragmentName)

    }

    fun displayData() {
        val showPref: SharedPreferences =
            getSharedPreferences("userinformation", Context.MODE_PRIVATE)
        val userName: String? = showPref.getString("username", "")
        val userPassword: String? = showPref.getString("password", "")
        Log.d("TAG", "displayData: " + "username=" + userName + " password=" + userPassword)
//        msgTv.setText("username="+userName+" password="+userPassword)
    }

    private fun confMenu() {
        multiLevelListView = findViewById<MultiLevelListView>(R.id.multi_nav)
        // custom ListAdapter
        val listAdapter = ListAdapter()
        multiLevelListView!!.setAdapter(listAdapter)
        multiLevelListView!!.setOnItemClickListener(mOnItemClickListener)

        listAdapter.setDataItems(CustomDataProvider.getInitialItems())
    }


    private var doubleBackToExitPressedOnce: Boolean = false
    override fun onBackPressed() {

        //close side menu
        val drawer = findViewById<DrawerLayout>(R.id.drawer_layout)
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }

        if (doubleBackToExitPressedOnce) {
            super.onBackPressed()
            return
        }
        this.doubleBackToExitPressedOnce = true
        Toast.makeText(this, "Press back again to exit", Toast.LENGTH_SHORT).show()
        Handler().postDelayed({ doubleBackToExitPressedOnce = false }, 2000)
    }

//    private var backPressedTime:Long = 0
//    lateinit var backToast:Toast
//    override fun onBackPressed() {
//        //close side menu
//        val drawer = findViewById(R.id.drawer_layout) as DrawerLayout
//        if (drawer.isDrawerOpen(GravityCompat.START)) {
//            drawer.closeDrawer(GravityCompat.START)
//        } else {
//            super.onBackPressed()
//        }
//
//        //close athe app
//        backToast = Toast.makeText(this, "Press back again to leave the app.", Toast.LENGTH_LONG)
//        if (backPressedTime + 2000 > System.currentTimeMillis()) {
//            backToast.cancel()
//            super.onBackPressed()
//            return
//        } else {
//            backToast.show()
//        }
//        backPressedTime = System.currentTimeMillis()
//    }


    // display activity or fragments
    fun displaySelectedScreen(itemName: String) {
        Log.d("ProgressDialogIssue", "Fragment Name : $itemName")
        //creating fragment object
        var fragment: Fragment? = null

        //initializing the fragment object which is selected
        when (itemName) {


            "Dashboard" -> {
                Log.d("ProgressDialogIssue", "Home Fragment")
                toolbarTitle.text = "Dashboard"
                fragment = HomeFragment()
            }

            "Buy Numbers" -> {
                Log.d("ProgressDialogIssue", "buy number fragment")
                toolbarTitle.text = "Buy Number(s)"
                fragment = BuyNumberFragment()
            }


            "My Numbers" -> {
                toolbarTitle.text = "My Number(s)"
                fragment = MyNumbersFragment()
            }


            "Call Forwarding" -> {
                toolbarTitle.text = "Call Forwarding"
                fragment = CallForwardingFragment()
            }



            "Calling Plans" -> {
                toolbarTitle.text = "Calling Plans"
                fragment = CallingPlansMainFragment()
            }


            "Call Back" -> {
                toolbarTitle.text = "Call Back"
                fragment = CallBackFragment()
            }


            "Credit" -> {
                toolbarTitle.text = "Credit"
                fragment = CreditFragment()
            }

            "History" -> {
                toolbarTitle.text = "History"
                fragment = HistoryFragment()
            }

            /*.........................Setting Section............*/
            "Update Profile" -> {
                toolbarTitle.text = "Profile Update"
                fragment = UpdateProfile()
            }

            "Change Password" -> {
                toolbarTitle.text = "Change Password"
                fragment = ChangePasswordFragment ()
            }

            /*.........................Setting Section............*/
            "Support" -> {
                toolbarTitle.text = "Support"
                fragment = TicketsFragment()
            }

   /*.........................Rewards Section............*/
            "Rewards" -> {
                toolbarTitle.text = "Rewards"
                fragment = RewardsMainFragments()
            }


//
//            "CATEGORY2" -> fragment = Category2Fragment()
//            "CATEGORY3" -> Toast.makeText(
//                applicationContext,
//                "Category3 Clicked",
//                Toast.LENGTH_LONG
//            ).show()


//            "Call1" -> Toast.makeText(
//                applicationContext,
//                "Call1  Clicked",
//                Toast.LENGTH_LONG
//            ).show()
//
//            "CALL2" -> Toast.makeText(
//                applicationContext,
//                "Call2 Clicked",
//                Toast.LENGTH_LONG
//            ).show()
//            "CALL3" -> Toast.makeText(
//                applicationContext,
//                "Call3 Clicked",
//                Toast.LENGTH_LONG
//            ).show()
//
//
//            "ASSIGNMENT1" -> fragment = Assignment1Fragment()
//            "ASSIGNMENT2" -> fragment = Assignment2Fragment()
//            "ASSIGNMENT3" -> fragment = Assignment1Fragment()
//            "ASSIGNMENT4" -> fragment = Assignment2Fragment()
//            "HELP" -> startActivity(Intent(applicationContext, HelpActivity::class.java))
//            "ABOUTUS" -> startActivity(Intent(applicationContext, AboutUsActivity::class.java))
        }

        //replacing the fragment
        if (fragment != null) {
            val ft = supportFragmentManager.beginTransaction()
            ft.replace(R.id.content_frame, fragment)
            ft.commit()
            val drawer = findViewById<DrawerLayout>(R.id.drawer_layout)
            drawer.closeDrawer(GravityCompat.START)
        }


    }

    fun displayBuyNumberListFragment(
        dataList: ArrayList<SearchTelephoneNumberDataModel>,
        rateList: ArrayList<GetLocalRatesDataModel>
    ) {

        //creating fragment object
        var fragment: Fragment? = null


        fragment = BuyNumberListFragment()

        //replacing the fragment
        if (fragment != null) {
            val ft = supportFragmentManager.beginTransaction()
            ft.replace(R.id.content_frame, fragment)

            val bundle = Bundle()
//            bundle.putString("inputText", editTextInput)
            bundle.putParcelableArrayList("DataList", dataList)
            bundle.putParcelableArrayList("RateList", rateList)

            fragment.arguments = bundle

            ft.commit()
            val drawer = findViewById<DrawerLayout>(R.id.drawer_layout)
            drawer.closeDrawer(GravityCompat.START)
        }


    }


    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        //CALLing the method displayselectedscreen and passing the id of selected menu

        Log.d("ProgressDialogIssue", "on navigation item selected")
        displaySelectedScreen(item.itemId.toString())

        //make this method blank
        return true
    }


    // adapter of listAdapter

    private inner class ListAdapter : MultiLevelListAdapter() {

        public override fun getSubObjects(`object`: Any): List<*> {
            // EXECUTED WHEN CLICKING ON GROUP-ITEM
            return CustomDataProvider.getSubItems(`object` as BaseItem)
        }

        public override fun isExpandable(`object`: Any): Boolean {
            return CustomDataProvider.isExpandable(`object` as BaseItem)
        }

        public override fun getViewForObject(
            `object`: Any,
            convertView: View?,
            itemInfo: ItemInfo
        ): View {
            var convertView = convertView
            val viewHolder: ViewHolder
            if (convertView == null) {
                viewHolder = ViewHolder()
                convertView =
                    LayoutInflater.from(this@MainActivity).inflate(R.layout.data_item, null)
                //viewHolder.infoView = (TextView) convertView.findViewById(R.id.dataItemInfo);
                viewHolder.nameView = convertView!!.findViewById(R.id.dataItemName)
                viewHolder.arrowView = convertView.findViewById(R.id.dataItemArrow)
                viewHolder.icon = convertView.findViewById(R.id.di_image)

                convertView.tag = viewHolder
            } else {
                viewHolder = convertView.tag as ViewHolder
            }

            viewHolder.nameView!!.text = (`object` as BaseItem).name
            //viewHolder.infoView.setText(getItemInfoDsc(itemInfo));

            if (itemInfo.isExpandable) {
                viewHolder.arrowView!!.visibility = View.VISIBLE
                viewHolder.arrowView!!.setImageResource(
                    if (itemInfo.isExpanded) {
//                        R.drawable.bottomarrow
                        R.drawable.ic_baseline_remove_24
                    } else {
                        R.drawable.ic_baseline_add_24
                    })


             /*   if (itemInfo.isExpanded){
                    Log.d("ExpandingIssue","Expanded")
                    val param = viewHolder.nameView!!.layoutParams as ViewGroup.MarginLayoutParams
                    param.setMargins(50,10,10,10)
                    viewHolder.nameView!!.layoutParams = param
                }else{
                    Log.d("ExpandingIssue","not Expanded")

                    val param = viewHolder.nameView!!.layoutParams as ViewGroup.MarginLayoutParams
                    param.setMargins(150,10,10,10)
                    viewHolder.nameView!!.layoutParams = param
                }*/

            } else {
                viewHolder.arrowView!!.visibility = View.GONE



            }
            viewHolder.icon!!.setImageResource(`object`.icon)
            return convertView
        }

        private inner class ViewHolder {
            var nameView: TextView? = null
            var arrowView: ImageView? = null
            var icon: ImageView? = null

        }
    }

    //add change the status bar color
    fun changeStatusBarColor() {


        if (Build.VERSION.SDK_INT >= 21) {
            val window = this.window
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
            window.statusBarColor = this.resources.getColor(R.color.colorPrimaryDark)
        }

    }


    fun initWindow() {
        this.window.setSoftInputMode(
            WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
        )
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
        //  supportActionBar!!.hide()
    }

    override fun onResume() {
        super.onResume()
        App.activityResumed()
    }

    override fun onPause() {
        super.onPause()
        App.activityPaused()
    }


}


