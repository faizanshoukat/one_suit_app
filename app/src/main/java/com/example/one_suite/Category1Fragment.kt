package com.android.navigationtesting


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.example.one_suite.R


/**
 * A simple [Fragment] subclass.
 */
class Category1Fragment : Fragment() {

    //internal var title = "Call1"


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.activity_about_us, container, false)

//        val textView = view.findViewById<TextView>(R.id.text_view)
//        textView.text = "Category1 Fragment"

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        //you can set the title for your toolbar here for different fragments different titles
        //requireActivity().title = title
    }
}// Required empty public constructor
